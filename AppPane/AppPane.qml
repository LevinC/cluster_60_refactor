import QtQuick 2.3
import "../Common/"

Item{
    id:root

    Column{

        SpaText {
            id: applicationPaneTitle
            anchors.left: parent.left
            anchors.leftMargin: 35
            height: 50
            text: qsTr("Applications")
        }

        GridView{
            id:grid
            width: root.width -x*2
            x:24
            height:root.height

            model: objectModel
            interactive: false

            cellWidth: (grid.width)/6
            cellHeight: cellWidth

            VisualItemModel{
                id:objectModel

                AppPaneIcon{
                    image: "/Icons/img_hmc_bt"
                    title: "BlueTooth"
                    widgetID:1
                    onClicked: {
                        coreScreen.widgetPane.mediaWidget.changeFromAppPane("bt",true)
                    }
                }
                AppPaneIcon{
                    image: "/Icons/img_hmc_ipod"
                    title: "Ipod"
                    widgetID:1
                    onClicked: {
                        coreScreen.widgetPane.mediaWidget.changeFromAppPane("ipod",true)
                    }
                }
                AppPaneIcon{
                    image: "/Icons/img_hmc_fm"
                    title: "FM Radio"
                    widgetID:1
                    onClicked: {
                        coreScreen.widgetPane.mediaWidget.changeFromAppPane("fm",false)

                    }
                }
                AppPaneIcon{
                    image: "/Icons/img_hmc_am"
                    title: "AM Radio"
                    widgetID:1
                    onClicked:{
                        coreScreen.widgetPane.mediaWidget.changeFromAppPane("am",false)

                    }
                }
                AppPaneIcon{
                    image: "/Icons/img_hmc_dab"
                    title: "DAB Radio"
                    widgetID:1
                    onClicked:{
                        coreScreen.widgetPane.mediaWidget.changeFromAppPane("dab",false)
                    }
                }
                AppPaneIcon{
                    image: "/Icons/img_hmc_sirius"
                    title: "Sirius"
                    widgetID:1
                    onClicked:{
                        coreScreen.widgetPane.mediaWidget.changeFromAppPane("sirius",false)
                    }
                }
                AppPaneIcon{
                    image: "/Icons/img_hmc_usb"
                    title: "USB"
                    widgetID:1
                    onClicked: {
                        coreScreen.widgetPane.mediaWidget.changeFromAppPane("usb",true)
                    }
                }
                AppPaneIcon{
                    image: "/Icons/img_hmc_aux"
                    title: "AUX in"
                    widgetID:1
                    onClicked: {
                        coreScreen.widgetPane.mediaWidget.changeFromAppPane("aux",true)
                    }
                }
                AppPaneIcon{
                    image: "/Icons/img_hmc_mrf"
                    title: "Radio Favorites"
                    widgetID:1
                    onClicked: {
                        coreScreen.widgetPane.mediaWidget.changeFromAppPane("fav",false)
                    }
                }
                AppPaneIcon{
                    widgetID: 0
                    opacity:0
                }
                AppPaneIcon{
                    widgetID: 0
                    opacity:0
                }
                AppPaneIcon{
                    widgetID: 0
                    opacity:0
                }
                AppPaneIcon{
                    widgetID: 0
                    opacity:0
                }
                AppPaneIcon{
                    widgetID: 0
                    opacity:0
                }
                AppPaneIcon{
                    widgetID: 0
                    opacity:0
                }
                AppPaneIcon{
                    widgetID: 0
                    opacity:0
                }
                AppPaneIcon{
                    widgetID: 0
                    opacity:0
                }
                AppPaneIcon{
                    widgetID: 0
                    opacity:0
                }

                AppPaneIcon{
                    image: "/Icons/img_hmc_pre_sound"
                    title: "Sound Experience"
                    widgetID:3
                    onClicked: coreScreen.notAvailableNotify()
                }
                AppPaneIcon{
                    image: "/Icons/img_hmc_msg"
                    title: "Messages"
                    widgetID:3
                    onClicked: coreScreen.notAvailableNotify()
                }
                AppPaneIcon{
                    image: "/Icons/img_hmc_carinfo"
                    title: "Service"
                    widgetID:3
                    onClicked: coreScreen.notAvailableNotify()
                }
                AppPaneIcon{
                    image: "/Icons/img_hmc_dvr_perf"
                    title: "Driver performances"
                    widgetID:3
                    onClicked: {
                        coreScreen.widgetPane.extraWidget.changeFromAppPane("performance",true)

                    }
                }
                AppPaneIcon{
                    image: "/Icons/img_hmc_ota"
                    title: "Download Manager"
                    widgetID:3
                    onClicked: {
                        coreScreen.widgetPane.extraWidget.changeFromAppPane("weather",true)
                    }
                }
                AppPaneIcon{
                    image: "/ICON_APP_SCREEN_IOS_IN_CAR_no_text_0"
                    title: "Apple Carplay"
                    widgetID:3
                    onClicked: coreScreen.notAvailableNotify()
                }
                AppPaneIcon{
                    image: "/android-auto-android"
                    title: "Android Auto"
                    widgetID:3
                    onClicked: coreScreen.notAvailableNotify()
                }
//                AppPaneIcon{
//                    image: "/Icons/img_hmc_browser"
//                    title: "Browser"
//                    widgetID:3
//                    onClicked: coreScreen.notAvailableNotify()
//                }
                AppPaneIcon{
                    image: "/Icons/img_hmc_cd"
                    title: "Sirius Travellink with extra long text"
                    widgetID:3
                    onClicked:coreScreen.widgetPane.extraWidget.changeFromAppPane("travellink",true)

                }
                AppPaneIcon{
                    image: "/Glympse_logo"
                    title: "Glympse"
                    widgetID:3
                    onClicked: coreScreen.notAvailableNotify()
                }
            }
        }
    }
}

