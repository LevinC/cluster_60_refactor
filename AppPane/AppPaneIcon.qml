import QtQuick 2.0

import "../Common/"
import QtGraphicalEffects 1.0


Spa9PatchButton{
    id:root
    width: 118
    height: 114
    bg.visible:false


    property int widgetID
    property string title
    property string image

    Rectangle {
        id: gradientOne
        width: parent.height-2
        height: parent.width-4
        rotation: -90
        anchors.centerIn: parent
        anchors.verticalCenterOffset: -3

        opacity: 0.8//globalStyle.componentSkinUrl === "_S4" ? 0.8 : globalStyle.coloredBgOpacityOne
        gradient:Gradient{
            GradientStop { position: 0.0; color:globalStyle.appPaneColor[widgetID][0]
            }
            GradientStop { position: 2.0; color:globalStyle.appPaneColor[widgetID][1]
            }
        }

        Rectangle{
            id:pressedRect
            opacity:0
            anchors.fill: parent
        }
    }

    onPressedChanged: {
        if(pressed){
            pressedRect.opacity=.1
        }else{
            pressedRect.opacity=0
        }
    }

    SpaText {
        id: buttonText
        text: title
        opacity: 1
        width: parent.height-2
        height: parent.width-4
        anchors.centerIn: parent
        lineHeight: 0.8
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        textStyle: helpText
        font.pixelSize: 16
        anchors.verticalCenterOffset: 26
        color:"white"
        wrapMode: Text.WordWrap
    }
    Image {
        id: icon
        source: image.length > 0 ? globalStyle.imageUrl + image + globalStyle.skinUrl + ".png" : ""
        anchors.centerIn: parent
        anchors.verticalCenterOffset: -18
        scale: 1
    }
}
