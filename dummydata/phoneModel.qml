/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import QtMultimedia 5.0

ListModel {
    id: phoneModelRoot

    property string name: "Work Phone" //"My Phone"
    property string network: "AT&T" //"swisscom"
    property bool musicPlayerWasPlaying: false
    // [iOSInCar]
    property bool iOSInCarActive: false
    //property string noPhone: "No Phone Connected"
    property string noPhone: "Work Phone"
    property string noNetwork: "No Network"
    // [iOSInCar]
    property string activeCallImageSource: ""

    function dial( number ) {
        // Pause Music
        pauseMusic();
        if (typeof number === "undefined") {
            console.log("Dialing undefined number")
            return;
        }
        // Simulate incomming call by placing a call with ** prefix
        if (number.indexOf("**") === 0) {
            _callIn = 10;
            _callInNbr = number.slice(2);
            console.log("Simulating incomming call from " + _callInNbr + " in " + (_callIn / 2) + " seconds.");
            return;
        }
        var i;
        for (i = 0; i < count; i++) {
            var call = get(i);
            if (call.callState === "active")
                call.callState = "held";
        }
        var now = new Date();
        append( { "lineIdentification": number,
                  "startTime": now,
                   "duration": 0,
                  "callState": "dialing",
                  "uri": "/modem0/voicecall" + Math.floor(Math.random() * 10000),
                  "__cnt": 0 } );
        _activeIndex = count - 1;
    }

    function swapCalls() {
        var i;
        for (i = 0; i < count; i++) {
            var call = get(i);
            if (call.callState === "active")
                call.callState = "held";
            else if (call.callState === "held") {
                call.callState = "active";
                _activeIndex = i;
            }
        }
    }

    function hangupAll() {
        // Resume Music
        resumeMusic();
        var i;
        for (i = 0; i < count; i++) {
            get(i).callState = "disconnected";
        }
    }

    function hangup( uri ) {
        // Resume Music
        resumeMusic();
        var i;
        for (i = 0; i < count; i++) {
            if (get(i).uri === uri)
                get(i).callState = "disconnected";
        }
    }

    function answer( uri ) {
        var i;
        for (i = 0; i < count; i++) {
            var call = get(i);
            if (call.uri === uri && call.callState === "incoming") {
                call.callState = "active";
                call.__cnt = 0;
                call.startTime = new Date();
                _activeIndex = i;
            }
        }
    }

    function releaseAndAnswer() {
        var i;
        for (i = 0; i < count; i++) {
            var call = get(i);
            if (call.callState === "waiting") {
                call.callState = "active";
                call.__cnt = 0;
                call.startTime = new Date();
                _activeIndex = i;
            }
            else if (call.callState === "active") {
                call.callState = "disconnected";
                call.__cnt = 0;
            }
        }
    }

    function createMultiparty() {
        var i;
        for (i = 0; i < count; i++) {
            var call = get(i);
            if (call.callState === "held")
                call.callState = "active";
        }
    }

    function hangupMultiparty() {
        // Resume Music
        resumeMusic();
        var i;
        for (i = 0; i < count; i++) {
            get(i).callState = "disconnected";
        }
    }

    function holdAndAnswer() {
        var i;
        for (i = 0; i < count; i++) {
            var call = get(i);
            if (call.callState === "waiting") {
                call.callState = "active";
                call.__cnt = 0;
                call.startTime = new Date();
                _activeIndex = i;
            }
            else if (call.callState === "active") {
                call.callState = "held";
                call.__cnt = 0;
            }
        }
    }

    // Pause Music
    function pauseMusic() {
        // Get Music Player Playback State
        phoneModelRoot.musicPlayerWasPlaying = (musicPlayer.playbackState === MediaPlayer.PlayingState);
        // Check If It Was Playing
        if (phoneModelRoot.musicPlayerWasPlaying) {
            // Pause Music Player
            musicPlayer.pause();
        }
    }

    // Resume Music
    function resumeMusic() {
        // Check If It Was Playing
        if (phoneModelRoot.musicPlayerWasPlaying) {
            // Resume Music Player
            musicPlayer.play();
            // Reset Music Player Was Playing
            phoneModelRoot.musicPlayerWasPlaying = false;
        }
    }

    property int _activeIndex: -1

    property int _callIn: -1
    property string _callInNbr: ""

    property string activeLineIdentification: _activeIndex >= 0 ? get(_activeIndex).lineIdentification : ""

    property int activeDuration: _activeIndex >= 0 ? get(_activeIndex).duration : 0

    property string activeCallState: _activeIndex >= 0 ? get(_activeIndex).callState : "active"

    property Timer timer: Timer {
        interval: 500
        running: true
        repeat: true
        onTriggered: {
            var i;
            var dcon = -1;
            for (i = 0; i < phoneModelRoot.count; i++) {
                var call = phoneModelRoot.get(i);
                if (call.callState === "dialing") {
                    if ( ++ call.__cnt > 1) {
                        call.callState = "alerting";
                        call.__cnt = 0;
                    }
                }
                else if (call.callState === "alerting") {
                    if ( ++ call.__cnt > 4) {
                        call.callState = "active";
                        call.__cnt = 0;
                        var now = new Date();
                        call.startTime = now;
                        // Resume Music
                        //resumeMusic();
                    }
                }
                else if (call.callState === "incoming") {
                    if ( ++ call.__cnt > 20) {
                        call.callState = "disconnected";
                        call.__cnt = 0;
                        // Resume Music
                        //resumeMusic();
                    }
                }
                else if (call.callState === "disconnected") {
                    if ( ++ call.__cnt > 2) {
                        dcon = i;
                        // Resume Music
                        resumeMusic();
                    }
                }
                else {
                    call.duration += interval;
                }
            }
            if (dcon >= 0) {
                phoneModelRoot.remove(dcon);
                if (_activeIndex >= dcon)
                    _activeIndex--;
            }
            if (_callIn >= 0) {
                if (_callIn-- === 0) {
                    var nowIn = new Date();
                    append( { "lineIdentification": _callInNbr,
                              "startTime": nowIn,
                               "duration": 0,
                              "callState": "incoming",
                              "uri": "/modem0/voicecall" + Math.floor(Math.random() * 10000),
                              "__cnt": 0 } );
                    if (_activeIndex < 0)
                        _activeIndex = count - 1;
                }
            }
        }
    }
}
