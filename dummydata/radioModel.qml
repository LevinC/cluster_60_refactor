import QtQuick 2.0

QtObject {
    property string stationNow: "SR P1"
    property string progNow: "Filosofiska Rummet"
    property var freqNow: (freqNowreal).toFixed(1);
    property real freqNowreal: 89.3
    property string hdNow: "2"
    property string logoNow: ""

    onFreqNowChanged: {
        if (freqNow === "89.3") {
            stationNow = "SR P1"
            progNow = "Filosofiska Rummet"
            hdNow = "2"
            logoNow = "/SRP1_logo.png"
        }
        else if (freqNow === "94.9") {
            stationNow = "GNF 94,9"
            progNow = "Nyheter"
            hdNow = "2"
            logoNow = ""
        }
        else if (freqNow === "96.3") {
            stationNow = "SR P2"
            progNow = "CD-revyn i P2"
            hdNow = "3"
            logoNow = "/SRP2_logo.png"
        }
        else if (freqNow === "99.4") {
            stationNow = "SR P3"
            progNow = "DigiListan"
            hdNow = "2"
            logoNow = "/SRP3_logo.png"
        }
        else if (freqNow === "101.9") {
            stationNow = "SR P4"
            progNow = "Bildoktorn"
            hdNow = "1"
            logoNow = "/SRP4_logo.png"
        }
        else if (freqNow === "89.3") {
            stationNow = "SR P1"
            progNow = "Filosofiska Rummet"
            hdNow = "2"
            logoNow = ""
        }
        else if (freqNow === "104.8") {
            stationNow = "Bandit Rock Göteborg"
            progNow = "Bandit Rock Party"
            hdNow = "3"
            logoNow = "/bandit_rock.png"
        }
        else if (freqNow === "105.3") {
            stationNow = "NRJ Göteborg"
            progNow = "Jazon"
            hdNow = "1"
            logoNow = "/nrj_logo.png"
        }
        else if (freqNow === "105.9") {
            stationNow = "RIX FM Göteborg"
            progNow = "RIX MorronZoo"
            hdNow = "1"
            logoNow = "/RIX-FM_logo.png"
        }
        else if (freqNow === "107.3") {
            stationNow = "Mix Megapol Göteborg"
            progNow = "Jesse & Peter"
            hdNow = "1"
            logoNow = "/mix_logo.png"
        }
        else {
            stationNow = ""
            progNow = ""
            hdNow = ""
            logoNow = ""
        }
    }

    property ListModel genres: ListModel {
        ListElement {
            genre: "News"
        }
        ListElement {
            genre: "Information"
        }
        ListElement {
            genre: "Sport"
        }
        ListElement {
            genre: "Popular Music (Pop)"
        }
        ListElement {
            genre: "Rock Music"
        }
        ListElement {
            genre: "Easy Listening"
        }
        ListElement {
            genre: "Jazz"
        }
        ListElement {
            genre: "Country Music"
        }
        // Add More
    }

    property ListModel stations: ListModel {
        ListElement {
            station: "SR P1"
            freq: "89.3"
            prog: "Filosofiska Rummet"
            hd: "2"
            logo: "/SRP1_logo.png"
        }
        ListElement {
            station: "GNF 94,9"
            freq: "94.9"
            prog: "Nyheter"
            hd: ""
            logo: ""
        }
        ListElement {
            station: "SR P2"
            freq: "96.3"
            prog: "CD-revyn i P2"
            hd: "3"
            logo: "/SRP2_logo.png"
        }
        ListElement {
            station: "SR P3"
            freq: "99.4"
            prog: "DigiListan"
            hd: "2"
            logo: "/SRP3_logo.png"
        }
        ListElement {
            station: "SR P4"
            freq: "101.9"
            prog: "Bildoktorn"
            hd: "1"
            logo: "/SRP4_logo.png"
        }
        ListElement {
            station: "Bandit Rock Göteborg"
            freq: "104.8"
            prog: "Bandit Rock Party"
            hd: "3"
            logo: "/bandit_rock.png"
        }
        ListElement {
            station: "NRJ Göteborg"
            freq: "105.3"
            prog: "Jazon"
            hd: "1"
            logo: "/nrj_logo.png"
        }
        ListElement {
            station: "RIX FM Göteborg"
            freq: "105.9"
            prog: "RIX MorronZoo"
            hd: "1"
            logo: "/RIX-FM_logo.png"
        }
        ListElement {
            station: "Mix Megapol Göteborg"
            freq: "107.3"
            prog: "Jesse & Peter"
            hd: "1"
            logo: "/mix_logo.png"
        }

        // Add More
    }
}
