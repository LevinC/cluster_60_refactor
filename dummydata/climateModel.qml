/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0

QtObject {
    property int leftSeatHeat : 0
    property int rightSeatHeat : 0
    property int leftSeatFan : 0
    property int rightSeatFan : 0
    property int steeringwhealHeat: 0

    property int leftTemp: 12
    property int rightTemp: 12

    property int fanSpeed: 3

    property int fanMode: 1

    property bool fanAuto: true
    property bool fanTop: false
    property bool fanMiddle: false
    property bool fanBottom: false
    property bool fanOff: false
    property bool fanMax: false

    property bool recirc: false
    property bool recircenabled: true
    property bool ac: true
    property bool acenabled: true
    property bool front: false
    property bool rear: false
    property bool defrost: false

    // [iOSInCar]
    property bool leftTempShown: false
    property bool rightTempShown: false
    property bool climatePageShown: false
    // [iOSInCar]

    property var celsiusValues :    ["LO", 16.5, 17.0, 17.5, 18.0, 18.5, 19.0, 19.5, 20.0, 20.5, 21.0, 21.5, 22.0, 22.5, 23.0, 23.5, 24.0, 24.5, 25.0, 25.5, 26.0, 26.5, 27.0, 27.5, "HI"]
    property var fahrenheitValues : ["LO", 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, "HI"]

    function stepLeftSeatHeat() {
        if (leftSeatHeat < 3)
            leftSeatHeat++
        else
            leftSeatHeat = 0
    }

    function stepRightSeatHeat() {
        if (rightSeatHeat < 3)
            rightSeatHeat++
        else
            rightSeatHeat = 0
    }

    function stepLeftSeatFan() {
        if (leftSeatFan < 3)
            leftSeatFan++
        else
            leftSeatFan = 0
    }

    function stepRightSeatFan() {
        if (rightSeatFan < 3)
            rightSeatFan++
        else
            rightSeatFan = 0
    }

    function stepSteeringwhealHeat() {
        steeringwhealHeat = (steeringwhealHeat + 1) % 4;
    }

    function clickFanTop() {
        if (fanAuto) {
            fanAuto = false;
            fanTop = true;
            fanMiddle = false;
            fanBottom = false;
        } else {
            fanTop = !fanTop;
        }
    }

    function clickFanMiddle() {
        if (fanAuto) {
            fanAuto = false;
            fanTop = false;
            fanMiddle = true;
            fanBottom = false;
        } else {
            fanMiddle = !fanMiddle;
        }
    }

    function clickFanBottom() {
        if (fanAuto) {
            fanAuto = false;
            fanTop = false;
            fanMiddle = false;
            fanBottom = true;
        } else {
            fanBottom = !fanBottom;
        }
    }

    function clickAC() {
        ac = !ac;
    }

    function clickRecirc() {
        recirc = !recirc;
    }

    function clickFront() {
        front = !front;
    }

    function clickRear() {
        rear = !rear;
    }

    function clickDefrost() {
        if (fanAuto || fanOff || fanMax) {
            defrost = !defrost;
            //fanMode = 2;
            fanAuto = false;
            fanOff = false;
            fanMax = false;
            fanSpeed = 6
            fanTop = true;
            recircenabled = false;
            acenabled = false;
        }
//        else if (fanOff) {
//            defrost = !defrost;
//            fanMode = 2;
//            fanAuto = false;
//            fanOff = false;
//            fanSpeed = 6
//            fanTop = true;
//            recircenabled = false;
//            acenabled = false;
//        }
//        else if (fanMax) {
//            defrost = !defrost;
//            fanMode = 2;
//            fanAuto = false;
//            fanOff = false;
//            fanSpeed = 6
//            fanTop = true;
//            recircenabled = false;
//            acenabled = false;
//        }
        else {
            defrost = !defrost;
            //fanMode = 1;
            fanAuto = true;
            fanOff= false;
            fanMax = false;
            fanSpeed = 3;
            recircenabled = true;
            acenabled = true;
        }
    }

    function clickFanOff() {
        //fanMode = 0;
        fanAuto = false;
        fanOff = true;
        fanMax = false;
        fanSpeed = 0;
        defrost = false;
        recircenabled = false;
        acenabled = false;
    }

    function clickFanAuto() {
        defrost = false;
        //fanMode = 1;
        fanAuto = true;
        fanOff= false
        fanMax = false
        fanTop = false;
        fanMiddle = false;
        fanBottom = false;
        fanSpeed = 3;
        recircenabled = true;
        acenabled = true;
    }

    function clickFanMax() {
        //fanMode = 2;
        fanAuto = false;
        fanOff = false;
        fanSpeed = 6;
        fanTop = false;
        fanMax=true;
        defrost = false;
        recircenabled = true;
        acenabled = true;
    }

    function setLeftTemp(temp) {
        leftTemp = temp;
    }

    function setRightTemp(temp) {
        rightTemp = temp;
    }
}
