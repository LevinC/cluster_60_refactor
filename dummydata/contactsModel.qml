/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0

QtObject {

    property string searchString: ""
    onSearchStringChanged: console.log("searchstring is " + searchString)

    function _numcmp(num1, num2) {
        return num1 === num2;
    }

    function setFavorite(index, favorite) {
        _favs.append({"contact": index, "number":"mobile"})
    }

    function getContactFromNumber(num) {
        var i;
        for (i = 0; i < _contacts.count; i++) {
            var item = _contacts.get(i);
            if (item.mobile === num) return { "name": item.name }
            if (item.work === num) return { "name": item.name }
            if (item.home === num) return { "name": item.name }
        }
        return { "name": "undefined" }
    }

    property ListModel contactList: ListModel {
        Component.onCompleted: {
            var i;
            for (i = 0; i < _contacts.count; i++) {
                var item = _contacts.get(i);
                item.photo = Qt.resolvedUrl(item.photo);
                item.defaultnumber = item.mobile ? item.mobile : item.work ? item.work : item.home ? item.home : null;
                //item.favorite = false
                append( item );
            }
        }
    }

    property ListModel searchList: ListModel {
        property string query: searchString
        onQueryChanged: {
            console.log("searching....?")
            clear();
            var i;
            for (i = 0; i < _contacts.count; i++) {
                var item = _contacts.get(i);
                if (item.name && (item.name.length > 0) && (item.name.toLocaleLowerCase().indexOf(query.toLocaleLowerCase()) !== -1)) {
                    if (item.photo) item.photo = Qt.resolvedUrl(item.photo);
                    item.defaultnumber = item.mobile ? item.mobile : item.work ? item.work : item.home ? item.home : null;
                    append( item );
                }
            }
        }
    }

    property ListModel callList: ListModel {
        Component.onCompleted: {
            var i;
            for (i = 0; i < _calls.count; i++) {
                var item = _calls.get(i);
                if (item.contact >= 0) {
                    var contact = _contacts.get(item.contact);
                    var number = _contacts.get(item.number);
                    var date = _contacts.get(item.date);
                    var time = _contacts.get(item.time)
                    var type = _contacts.get(item.type);
                    if (contact.photo) item.photo = Qt.resolvedUrl(contact.photo);
                    if (contact.name) item.name = contact.name;
                    if (contact.type) item.type = contact.number;
                    if (contact.date) item.date = contact.date;
                    if (contact.time) item.time = contact.time;
                    contact.date = item.date
                    contact.time = item.time
                    contact.type = item.type
                    append(contact)
                }
            }
        }
    }

    property ListModel favList: ListModel {
        Component.onCompleted: {
            var i;
            for (i = 0; i < _favs.count; i++) {
                var item = _favs.get(i);
                var contact = _contacts.get(item.contact);
                var number = _contacts.get(item.number);
                if (contact.photo) {
                    contact.photo = Qt.resolvedUrl(contact.photo);
                }
                else {
                    contact.photo = Qt.resolvedUrl("../resources/images/contact_photos/f1.png")
                }
                //contact.defaultnumber = contact.mobile ? contact.mobile : contact.work ? contact.work : contact.home ? contact.home : null;
                if (item.number === "work") contact.number = ("Work: %1").arg(contact.work)
                if (item.number === "mobile") contact.number = ("Mobile: %1").arg(contact.mobile)
                if (item.number === "home") contact.number = ("Home: %1").arg(contact.home)
                append( contact );
            }
        }
    }

    property ListModel _calls: ListModel {
        ListElement {
            contact: 8
            type: "in"
            number: "work"
            time: "13:20"
            date: "2015-05-21"
        }
        ListElement {
            contact: 0
            type: "missed"
            number: "mobile"
            time: "09:40"
            date: "2015-05-20"
        }
        ListElement {
            contact: 4
            type: "out"
            number: "mobile"
            time: "10:15"
            date: "2015-05-19"
        }
        ListElement {
            contact: 2
            type: "in"
            number: "mobile"
            time: "11:05"
            date: "2015-05-18"
        }
        ListElement {
            contact: 3
            type: "missed"
            number: "mobile"
            time: "01:05"
            date: "2015-05-18"
        }
        ListElement {
            contact: 15
            type: "out"
            number: "mobile"
            time: "09:05"
            date: "2015-05-17"
        }
        ListElement {
            contact: 10
            type: "in"
            number: "mobile"
            time: "11:05"
            date: "2015-05-17"
        }
        ListElement {
            contact: 9
            type: "out"
            number: "mobile"
            time: "11:05"
            date: "2015-05-17"
        }

        ListElement {
            contact: 20
            type: "in"
            number: "mobile"
            time: "11:05"
            date: "2015-05-17"
        }
    }

    property ListModel _favs: ListModel {
        ListElement {
            contact: 3
            number: "work"
            favorite: "true"
        }
        ListElement {
            contact: 7
            number: "mobile"
            favorite: "true"
        }
        ListElement {
            contact: 6
            number: "work"
            favorite: "true"
        }
        ListElement {
            contact: 0
            number: "home"
        }
        ListElement {
            contact: 1
            number: "work"
        }
        ListElement {
            contact: 11
            number: "work"
        }
        ListElement {
            contact: 10
            number: "work"
        }
        ListElement {
            contact: 8
            number: "work"
        }
    }

    property ListModel _contacts: ListModel {
        ListElement {
            name: "Aida Lodin"
            mobile: "07537167239"
            home: "0317845687"
            work: "07219673519"
            homeadress: "Storgatan 6 GÖTEBORG 120 53 Sweden"
            workadress: "Gunnar Engellaus väg 10 TORSLANDA 321 00 Sweden"
            url: "gooddesign.com"
            photo: "../resources/images/contact_photos/f1.png"
        }
        ListElement {
            name: "Alfhild Aasgaard"
            mobile: "07577815323"
            home: "05206557215"
            work: "07016120220"
            homeadress: "Fornäng 10 NYGÅRD 460 11 Sweden"
            workadress: "Hagagatan 16 JÄRPÅS 531 67 Sweden"
            url: "TornadoCameras.com"
            photo: "../resources/images/contact_photos/f2.png"
        }
        ListElement {
            name: "Amelie Bergqvist"
            mobile: "07244681367"
            home: "0352365219"
            work: "07405583028"
            homeadress: "Björkvägen 3 GULLBRANDSTORP 310 41 Sweden"
            workadress: "Karlsängen 45 ÖSMO 148 00 Sweden"
            url: "TopTweeter.com"
            photo: "../resources/images/contact_photos/f3.png"
        }
        ListElement {
            name: "Anthony Bergqvist"
            mobile: "07783785111"
            home: "03016496224"
            work: "07391941085"
            homeadress: "Överhogdal 94 LANDVETTER 438 00 Sweden"
            workadress: "Främsteby Vena 96 TRENSUM 374 24 Sweden"
            url: "JollyForum.com"
            photo: "../resources/images/contact_photos/m1.png"
        }
        ListElement {
            name: "Brad Ankney"
            mobile: "07068327538"
            home: "05716038643"
            work: "07874570708"
            homeadress: "Verdandi Gränd 36 ÅMOTFORS 670 40 Sweden"
            workadress: "Nils Grises Sträte 17 BANDHAGEN 124 99 Sweden"
            url: "TripsReview.com"
            photo: "../resources/images/contact_photos/m2.png"
        }
        ListElement {
            name: "Bruce Blakeman"
            mobile: "07340326388"
            home: "02942951246"
            work: "07477775725"
            homeadress: "Vikarna 26 KARLHOMSBRUK 810 64 Sweden"
            workadress: "Skolspåret 88 DALHEM 620 24 Sweden"
            url: "NightlifeSpecials.com"
            photo: "../resources/images/contact_photos/m3.png"
        }
        ListElement {
            name: "Christopher Rhem"
            mobile: "07414038469"
            home: "04513268482"
            work: "07013988874"
            homeadress: "Esplanadgatan 60 SÖSDALA 280 10 Sweden"
            workadress: "Långlöt 9 KÖPINGSVIK 387 33 Sweden"
            url: "FutureRequest.com"
            photo: "../resources/images/contact_photos/m4.png"
        }
        ListElement {
            name: "Dani Ekström"
            mobile: "07114162669"
            home: "02945286242"
            work: "07210533199"
            homeadress: "Vikarna 4 KARLHOMSBRUK 810 64 Sweden"
            workadress: "Östanlid 7 KUNGSÄTER 510 11 Sweden"
            url: "InteriorSale.com"
            photo: "../resources/images/contact_photos/f4.png"
        }
        ListElement {
            name: "Dino Henriksson"
            mobile: "07728338833"
            home: "03408624921"
            work: "07721182522"
            homeadress: "Östanlid 6 VÄRÖBACKA 430 22 Sweden"
            workadress: "Eggelstad 79 BLENTARP 270 35 Sweden"
            url: "MensFederation.com"
            photo: "../resources/images/contact_photos/m5.png"
        }
        ListElement {
            name: "Edward Hines"
            mobile: "07159814186"
            home: "05708029074"
            work: "07500735548"
            homeadress: "Gökst 39 GUNNARSKOG 670 35 Sweden"
            workadress: "Granträsk 87 HJÄRNARP 262 63 Sweden"
            url: "AccountingBuzz.com"
            photo: "../resources/images/contact_photos/m6.png"
        }
        ListElement {
            name: "Edwin Contreras"
            mobile: "07587275455"
            home: "01236981036"
            work: "07060002740"
            homeadress: "Valldammsgatan 3 VALDEMARSVIK 615 00 Sweden"
            workadress: "Knektv 13 GRANGÄRDE 770 13 Sweden"
            url: "PopupLeads.com"
            photo: "../resources/images/contact_photos/m7.png"
        }
        ListElement {
            name: "Elliott Nordin"
            mobile: "07433517372"
            home: "02464859219"
            work: "07154035866"
            homeadress: "Hålebäcksvägen 89 BJURSÅS 790 21 Sweden"
            workadress: "Långlöt 71 BYXELKROK 380 75 Sweden"
            url: "ParishJobs.com"
            photo: "../resources/images/contact_photos/m8.png"
        }
        ListElement {
            name: "Engla Dahlberg"
            mobile: "07880272100"
            home: "06118658273"
            work: "07070349563"
            homeadress: "Buanvägen 82 HÄRNÖSAND 871 48 Sweden"
            workadress: "Vipgränden 78 MARSFJÄLL 910 88 Sweden"
            url: "StLouisCarWash.com"
            photo: "../resources/images/contact_photos/f6.png"
        }
        ListElement {
            name: "Felix Wolf"
            mobile: "07003108084"
            home: "0332942999"
            work: "07347184977"
            homeadress: "Edeforsvägen 93 BRÄMHULT 510 54 Sweden"
            workadress: "Hundbergsvägen 16 MATTMAR 830 02 Sweden"
            url: "VerifiedOwners.com"
            photo: "../resources/images/contact_photos/m9.png"
        }
        ListElement {
            name: "Flora Solvoll"
            mobile: "07562934422"
            home: "0423535349"
            work: "07044095837"
            homeadress: "Läktargatan 51 HASSLARP 260 39 Sweden"
            workadress: "Långgatan 16 HABO 566 00 Sweden"
            url: "CheapMessenger.com"
            photo: "../resources/images/contact_photos/f7.png"
        }
        ListElement {
            name: "Gladys Beauford"
            mobile: "07505647810"
            home: "02518042962"
            work: "07550909739"
            homeadress: "Ängsv 66 ÄLVDALEN 796 00 Sweden"
            workadress: "Granträsk 26 ARBOGA 732 22 Sweden"
            url: "BingoAdventure.com"
            photo: "../resources/images/contact_photos/f8.png"
        }
        ListElement {
            name: "Gulluv Liverød"
            mobile: "07747518080"
            home: "0402883053"
            work: "07978750981"
            homeadress: "Handslagarevägen 75 MALMÖ 206 73 Sweden"
            workadress: "Liljerum Grenadjärtorpet 71 SOLNA 171 48 Sweden"
            url: "EthanolGel.com"
            photo: "../resources/images/contact_photos/f9.png"
        }
        ListElement {
            name: "Hoda Svensson"
            mobile: "07095494133"
            home: "0364648577"
            work: "07782887716"
            homeadress: "Västerviksgatan 56 VISINGSÖ 560 34 Sweden"
            workadress: "Hagag 37 TÄLLBERG 793 63 Sweden"
            url: "EgoNews.com"
        }
        ListElement {
            name: "Isis Peace"
            mobile: "07581463765"
            home: "09113646164"
            work: "07225140293"
            homeadress: "Lidbovägen 15 JÄVREBYN 940 13 Sweden"
            workadress: "Skolspåret 49 LÄRBRO 620 34 Sweden"
            url: "ConnectShops.com"
        }
        ListElement {
            name: "Izak Andersson"
            mobile: "07638538201"
            home: "01564307063"
            work: "07838261235"
            homeadress: "Edeby 99 TYSTBERGA 610 60 Sweden"
            workadress: "Alsteråvägen 73 VENJAN 790 40 Sweden"
            url: "PokerWatcher.com"
        }
        ListElement {
            name: "Jean Mæle"
            mobile: "07350022373"
            home: "05333538515"
            work: "07757137458"
            homeadress: "Gulleråsen Västabäcksgatu 52 VÄRMLANDS NYSÄTER 660 30 Sweden"
            workadress: "Utveda 10 SKÅNES FAGERHULT 280 40 Sweden"
            url: "JuiceSearch.com"
        }
        ListElement {
            name: "Kendall Clevenger"
            mobile: "07853327537"
            home: "04703030465"
            work: "07626035926"
            homeadress: "Östantorp Vinö 89 LAMMHULT 360 30 Sweden"
            workadress: "Alingsåsvägen 77 DANNIKE 516 02 Sweden"
            url: "PreviewJet.com"
        }
        ListElement {
            name: "Kenneth Samuelsson"
            mobile: "07667431966"
            home: "07342949780"
            work: "07293814493"
            homeadress: "Tuvvägen 66 FRILLESÅS 430 30 Sweden"
            workadress: "Gökst 23 KLÄSSBOL 671 96 Sweden"
            url: "BloggingIndex.com"
        }
        ListElement {
            name: "Kian Ek"
            mobile: "07953569519"
            home: "05216332192"
            work: "07058003317"
            homeadress: "Valldammsgatan 25 VÄNERSBORG 462 69 Sweden"
            workadress: "Idvägen 42 VADSTENA 592 00 Sweden"
            url: "LifeIdentity.com"
        }
        ListElement {
            name: "Larry Roberts"
            mobile: "07256764324"
            home: "03035335099"
            work: "07796882541"
            homeadress: "Eriksbo Västergärde 92 HISINGS BACKA 422 10 Sweden"
            workadress: "Eggelstad 72 BOLIDEN 936 00 Sweden"
            url: "RoboNetwork.com"
        }
        ListElement {
            name: "Laura Powers"
            mobile: "07457268149"
            home: "0409397661"
            work: "07229282372"
            homeadress: "Knektv 88 BJÄRRED 237 00 Sweden"
            workadress: "Luddingsbo Mekanikusv 45 SKÄRHOLMEN 127 21 Sweden"
            url: "WeekendPromotions.com"
        }
        ListElement {
            name: "Line Åkesson"
            mobile: "07085528090"
            home: "02936476990"
            work: "07850670676"
            homeadress: "Föreningsgatan 87 SÖDERFORS 810 60 Sweden"
            workadress: "Törneby 86 STOCKSUND 182 63 Sweden"
            url: "MilwaukeePage.com"
        }
        ListElement {
            name: "Matthew Palacios"
            mobile: "07703586097"
            home: "04819152361"
            work: "07346260000"
            homeadress: "Norrfjäll 49 ALSTERBRO 380 44 Sweden"
            workadress: "Överhogdal 40 RÄVLANDA 430 65 Sweden"
            url: "ConspiracyInsider.com"
        }
        ListElement {
            name: "Michael Boehmer"
            mobile: "07483647897"
            home: "04856321463"
            work: "07413590682"
            homeadress: "Alsteråvägen 41 DEGERHAMN 380 65 Sweden"
            workadress: "Magasinsgatan 39 STORA BLÅSJÖN 830 93 Sweden"
            url: "PostTags.com"
        }
        ListElement {
            name: "Mischa Forsberg"
            mobile: "07873965420"
            home: "0167100365"
            work: "07187269188"
            homeadress: "Kläppinge 72 KVICKSUND 640 45 Sweden"
            workadress: "Främsteby Karlsborg 53 VALLA 640 23 Sweden"
            url: "BrideHunter.com"
        }
        ListElement {
            name: "Niklas Bergström"
            mobile: "07494461993"
            home: "04548808588"
            work: "07935537450"
            homeadress: "Främsteby Vena 83 ASARUM 374 51 Sweden"
            workadress: "Lillesäter 34 FORSHAGA 667 00 Sweden"
            url: "Dumbor.com"
        }
        ListElement {
            name: "Nisse Anda"
            mobile: "07356744083"
            home: "0407134802"
            work: "07444026816"
            homeadress: "Handslagarevägen 17 MALMÖ 202 67 Sweden"
            workadress: "Södra Kroksdal 72 BERGKVARA 385 42 Sweden"
            url: "BlunderHead.com"
        }
        ListElement {
            name: "Olof Hellström"
            mobile: "07587590151"
            home: "0462932431"
            work: "07679357057"
            homeadress: "Knektvägen 40 DALBY 240 10 Sweden"
            workadress: "Knektvägen 16 SÖDRA SUNDERBYN 954 40 Sweden"
            url: "CourseOrganizer.com"
        }
        ListElement {
            name: "Orgna Jonsson"
            mobile: "07686158378"
            home: "02472683791"
            work: "07106852403"
            homeadress: "Hagag 2 INSJÖN 790 30 Sweden"
            workadress: "Mogata Sjöhagen 43 TECKOMATORP 260 20 Sweden"
            url: "MajorFlirt.com"
        }
        ListElement {
            name: "Oxo Dray"
            mobile: "07232220225"
            home: "06809728861"
            work: "07441134775"
            homeadress: "Buanvägen 1 YTTERHOGDAL 840 90 Sweden"
            workadress: "Skärpinge 70 KVISSLEBY 862 00 Sweden"
            url: "NewsScholar.com"
        }
        ListElement {
            name: "Patrik Evenstad"
            mobile: "07602280925"
            home: "0216777824"
            work: "07233474144"
            homeadress: "Nybyvägen 32 VÄSTERÅS 729 34 Sweden"
            workadress: "Skällebo 89 BURSERYD 330 26 Sweden"
            url: "DNSSpecialist.com"
        }
        ListElement {
            name: "Paula Martinsson"
            mobile: "07812872896"
            home: "01759383490"
            work: "07491389669"
            homeadress: "Simpnäs 22 BJÖRKÖ 760 42 Sweden"
            workadress: "Törneby 47 STOCKSUND 182 67 Sweden"
            url: "FunnyVisions.com"
        }
        ListElement {
            name: "Peter Löfgren"
            mobile: "07159410528"
            home: "0467858637"
            work: "07696781806"
            homeadress: "Främsteby Karlsborg 65 KÄVLINGE 244 37 Sweden"
            workadress: "Ovansjövägen 2 STORA SKEDVI 783 89 Sweden"
            url: "SanDiegoRugs.com"
        }
        ListElement {
            name: "Pergunn Bjønness"
            mobile: "07215281197"
            home: "06638354774"
            work: "07517778145"
            homeadress: "Dyvik 70 LÅNGVIKSMON 890 51 Sweden"
            workadress: "Tjörneröd 3 BOGRANGEN 680 61 Sweden"
            url: "InvestmentInstructor.com"
        }
        ListElement {
            name: "Pär Ivarsson"
            mobile: "07417054487"
            home: "06119472290"
            work: "07104249088"
            homeadress: "Buanvägen 27 ÄLANDSBRO 870 10 Sweden"
            workadress: "Nästvattnet 17 KRISTIANSTAD 291 18 Sweden"
            url: "CableDudes.com"
        }
        ListElement {
            name: "Rebecka Lundqvist"
            mobile: "07653821502"
            home: "04355198843"
            work: "07060110715"
            homeadress: "Dadelvägen 13 HANASKOG 280 62 Sweden"
            workadress: "Koppramåla 77 VÄSE 660 57 Sweden"
            url: "TeeItOff.com"
        }
        ListElement {
            name: "Rignor Enge"
            mobile: "07314109165"
            home: "05908344252"
            work: "07028341975"
            homeadress: "Ellenö 12 LESJÖFORS 680 96 Sweden"
            workadress: "Norra Bäckebo 3 BLOMSTERMÅLA 384 87 Sweden"
            url: "SalonJournal.com"
        }
        ListElement {
            name: "Steffan Skrede"
            mobile: "07100276516"
            home: "0703711510"
            work: "07030669982"
            homeadress: "Långlöt 65 TULLINGE 146 00 Sweden"
            workadress: "Skolspåret 94 KLINTEHAMN 620 20 Sweden"
            url: "MarketCities.com"
        }
        ListElement {
            name: "Stephan Fidje"
            mobile: "07236961823"
            home: "09205987629"
            work: "07259861715"
            homeadress: "Knektv 65 GAMMELSTAD 954 30 Sweden"
            workadress: "Hagagatan 53 VINNINGA 531 57 Sweden"
            url: "NaturePreview.com"
        }
        ListElement {
            name: "Theodore Larsson"
            mobile: "07411500093"
            home: "0737385748"
            work: "07511573639"
            homeadress: "Violvägen 28 BILLESHOLM 260 50 Sweden"
            workadress: "Handslagarevägen 38 BUNKEFLOSTRAND 230 44 Sweden"
            url: "TwilightCruises.com"
        }
        ListElement {
            name: "Torkil Furuheim"
            mobile: "07520910156"
            home: "02515610113"
            work: "07212020022"
            homeadress: "Ängsv 16 ÄLVDALEN 796 00 Sweden"
            workadress: "Håbyn 69 HÄLLEFORSNÅS 640 30 Sweden"
            url: "TrafficLogos.com"
        }
        ListElement {
            name: "Tychonius Haarberg"
            mobile: "07424003452"
            home: "04857048174"
            work: "07153343087"
            homeadress: "Långlöt 96 LÖTTORP 380 74 Sweden"
            workadress: "Vansövägen 64 SÄTILA 510 21 Sweden"
            url: "ChildrensReviews.com"
        }
        ListElement {
            name: "Ulf Jonasson"
            mobile: "07703315967"
            home: "09116137285"
            work: "07875589941"
            homeadress: "Pite Långvik 39 SVENSBYN 940 16 Sweden"
            workadress: "Storgatan 39 DALARÖ 130 54 Sweden"
            url: "GemRewards.com"
        }
        //        ListElement {
        //            name: "Mauritz Fredriksson"
        //            mobile: "07862421062"
        //            home: "03208466104"
        //            work: "07991995280"
        //            homeadress: "Vansövägen 8 ÖXABÄCK 511 99 Sweden"
        //            workadress: "Fornäng 23 FRÖVI 710 40 Sweden"
        //            url: "MomentumMarket.com"
        //        }
        //        ListElement {
        //            name: "Lois Poole"
        //            mobile: "07248539259"
        //            home: "06429145770"
        //            work: "07711010791"
        //            homeadress: "Stallstigen 51 LIT 830 30 Sweden"
        //            workadress: "Skärpinge 46 GRÄSMARK 686 06 Sweden"
        //            url: "CeramicsBlog.com"
        //        }
        //        ListElement {
        //            name: "Christoffer Lyse"
        //            mobile: "07277919905"
        //            home: "06924845252"
        //            work: "07785674002"
        //            homeadress: "Magasinsgatan 96 KOVLAND 860 25 Sweden"
        //            workadress: "Tulpanv 2 FAGERSTA 737 88 Sweden"
        //            url: "RebateSecrets.com"
        //        }
        //        ListElement {
        //            name: "Liliana Marshall"
        //            mobile: "07349916191"
        //            home: "04188492280"
        //            work: "07296607271"
        //            homeadress: "Ängsgatan 88 KÅGERÖD 260 23 Sweden"
        //            workadress: "Nils Grises Sträte 50 STENUNGSUND 444 79 Sweden"
        //            url: "ThisRestaurant.com"
        //        }
        //        ListElement {
        //            name: "Walid Johansson"
        //            mobile: "07865880088"
        //            home: "03712535211"
        //            work: "07869558984"
        //            homeadress: "Skällebo 67 GISLAVED 332 00 Sweden"
        //            workadress: "Skolspåret 94 LÄRBRO 620 34 Sweden"
        //            url: "CurrentComics.com"
        //        }
        //        ListElement {
        //            name: "Hervor Norderhaug"
        //            mobile: "07321810208"
        //            home: "05255230708"
        //            work: "07194242923"
        //            homeadress: "Vikarna 8 RABBALSHEDE 450 73 Sweden"
        //            workadress: "Trädgårdsgatan 71 HUDIKSVALL 824 00 Sweden"
        //            url: "AttractionPlanet.com"
        //        }
        //        ListElement {
        //            name: "Judy Evans"
        //            mobile: "07762546596"
        //            home: "04796244137"
        //            work: "07809201120"
        //            homeadress: "Lemesjö 47 KILLEBERG 280 72 Sweden"
        //            workadress: "Främsteby Vena 96 TRENSUM 374 77 Sweden"
        //            url: "ScottsdaleSource.com"
        //        }
        //        ListElement {
        //            name: "Bobby Ellison"
        //            mobile: "07015493435"
        //            home: "03212266396"
        //            work: "07240081407"
        //            homeadress: "Sörbylund 64 BLIDSBERG 520 75 Sweden"
        //            workadress: "Änggårda Anga 44 GRUMS 664 00 Sweden"
        //            url: "LawnSuppliers.com"
        //        }
        //        ListElement {
        //            name: "Lowa Axelsson"
        //            mobile: "07769323212"
        //            home: "03816696363"
        //            work: "07208727356"
        //            homeadress: "Klinta 95 HJÄLTEVAD 570 32 Sweden"
        //            workadress: "Violvägen 42 SVENSTAVIK 840 40 Sweden"
        //            url: "HormoneTracker.com"
        //        }
        //        ListElement {
        //            name: "Luna Nyman"
        //            mobile: "07992770324"
        //            home: "0708483302"
        //            work: "07770385640"
        //            homeadress: "Hantverkarg 37 GÄLLNÖBY 130 33 Sweden"
        //            workadress: "Ovansjövägen 47 RÖRVIK 570 01 Sweden"
        //            url: "RewardPets.com"
        //        }
        //        ListElement {
        //            name: "Unnveig Bjørkli"
        //            mobile: "07311291404"
        //            home: "03462471415"
        //            work: "07371015950"
        //            homeadress: "Tulpanv 46 ÄTRAN 310 61 Sweden"
        //            workadress: "Bodbysund 88 SKELLEFTEÅ 931 72 Sweden"
        //            url: "MegaRush.com"
        //        }
        //        ListElement {
        //            name: "Ladonna Taylor"
        //            mobile: "07230768973"
        //            home: "03817960847"
        //            work: "07036581222"
        //            homeadress: "Loftaheden 95 DROTTNINGHOLM 170 11 Sweden"
        //            workadress: "Mellangården 67 HOLMSVEDEN 823 42 Sweden"
        //            url: "PartyJudge.com"
        //        }
        //        ListElement {
        //            name: "Danielle Sylvia"
        //            mobile: "07650642078"
        //            home: "05848863935"
        //            work: "07345457571"
        //            homeadress: "Kantorsvägen 38 HASSELFORS 690 33 Sweden"
        //            workadress: "Läktargatan 39 GARPENBERG 770 73 Sweden"
        //            url: "AffinityClicks.com"
        //        }
        //        ListElement {
        //            name: "Johar Svela"
        //            mobile: "07671505316"
        //            home: "09297539641"
        //            work: "07345413021"
        //            homeadress: "Barkargatan 6 VIDSEL 940 45 Sweden"
        //            workadress: "Åbyhöjden 51 ÅRYD 360 43 Sweden"
        //            url: "GamingWild.com"
        //        }
        //        ListElement {
        //            name: "Lina Axelsson"
        //            mobile: "07314984338"
        //            home: "03204839325"
        //            work: "07051953466"
        //            homeadress: "Vansövägen 90 ÖXABÄCK 511 58 Sweden"
        //            workadress: "Gökst 10 KLÄSSBOL 671 72 Sweden"
        //            url: "SmokeReport.com"
        //        }
        //        ListElement {
        //            name: "Virginia Matthews"
        //            mobile: "07596511624"
        //            home: "02265409222"
        //            work: "07461747703"
        //            homeadress: "Klubbvägen 82 BY KYRKBY 770 65 Sweden"
        //            workadress: "Håbyn 25 HÄLLEFORSNÅS 640 30 Sweden"
        //            url: "AbacusMath.com"
        //        }
        //        ListElement {
        //            name: "Karenanna Hovdal"
        //            mobile: "07475135549"
        //            home: "05913638446"
        //            work: "07120897831"
        //            homeadress: "Libecksvägen 94 HÄLLEFORS 712 23 Sweden"
        //            workadress: "Knektv 44 LOMMA 234 41 Sweden"
        //            url: "TucsonEntrepreneur.com"
        //        }
        //        ListElement {
        //            name: "Berfin Dahl"
        //            mobile: "07043491143"
        //            home: "05265674326"
        //            work: "07733963984"
        //            homeadress: "Korsgatan 38 SYDKOSTER 452 05 Sweden"
        //            workadress: "Libecksvägen 63 KOLBÄCK 730 40 Sweden"
        //            url: "SyntheticNames.com"
        //        }
        //        ListElement {
        //            name: "Kylee Baker"
        //            mobile: "07020087631"
        //            home: "0547785985"
        //            work: "07176678737"
        //            homeadress: "Koppramåla 88 EDSVALLA 660 52 Sweden"
        //            workadress: "Djursbo 51 HYLTEBRUK 314 00 Sweden"
        //            url: "StandardProgramming.com"
        //        }
        //        ListElement {
        //            name: "Julia Stott"
        //            mobile: "07587062145"
        //            home: "05237090064"
        //            work: "07497323894"
        //            homeadress: "Hagaskog 17 KIMSTAD 610 20 Sweden"
        //            workadress: "Torggatan 66 ÅRSUNDA 810 22 Sweden"
        //            url: "mMinneapolis.com"
        //        }
        //        ListElement {
        //            name: "Elia Samuelsson"
        //            mobile: "07240358655"
        //            home: "0704244658"
        //            work: "07500388609"
        //            homeadress: "Gärdslösa 31 TUMBA 147 27 Sweden"
        //            workadress: "Vansövägen 43 SÄTILA 510 21 Sweden"
        //            url: "CoedClassifieds.com"
        //        }
        //        ListElement {
        //            name: "Charin Løvseth"
        //            mobile: "07978020597"
        //            home: "04575915163"
        //            work: "07574193881"
        //            homeadress: "Gulleråsen Västabäcksgatu 57 KALLINGE 372 10 Sweden"
        //            workadress: "Gulleråsen Västabäcksgatu 99 SALA 733 22 Sweden"
        //            url: "TripFlick.com"
        //        }
        //        ListElement {
        //            name: "Hektor Johnsson"
        //            mobile: "07465033145"
        //            home: "05859606719"
        //            work: "07323345653"
        //            homeadress: "Sandviken 54 GARPHYTTAN 710 16 Sweden"
        //            workadress: "Främsteby Vena 5 GRANBERGSDAL 691 40 Sweden"
        //            url: "RingSupply.com"
        //        }
        //        ListElement {
        //            name: "Edin Lundin"
        //            mobile: "07219789019"
        //            home: "05318384269"
        //            work: "07507155954"
        //            homeadress: "Violvägen 31 GUSTAVSFORS 666 03 Sweden"
        //            workadress: "Karlsängen 66 NYNÄSHAMN 149 74 Sweden"
        //            url: "DeathFilm.com"
        //        }
        //        ListElement {
        //            name: "Kelly Eklund"
        //            mobile: "07997780765"
        //            home: "01552348773"
        //            work: "07193206175"
        //            homeadress: "Edeby 61 NYKÖPING 611 78 Sweden"
        //            workadress: "Mållångsbo 78 ALSTERSBRUK 940 26 Sweden"
        //            url: "SpeedMessaging.com"
        //        }
        //        ListElement {
        //            name: "Robert Smith"
        //            mobile: "07153734685"
        //            home: "04576526509"
        //            work: "07921329354"
        //            homeadress: "Gulleråsen Västabäcksgatu 99 RONNEBY 372 92 Sweden"
        //            workadress: "Östra Förstadsgatan 64 JÄRLÅSA 740 21 Sweden"
        //            url: "VetAwards.com"
        //        }
        //        ListElement {
        //            name: "Zacharias Lid"
        //            mobile: "07803058672"
        //            home: "01239390726"
        //            work: "07966156550"
        //            homeadress: "Valldammsgatan 79 GRYT 610 42 Sweden"
        //            workadress: "Ängsv 62 ÄLVDALEN 796 00 Sweden"
        //            url: "StrictlyCoupons.com"
        //        }
        //        ListElement {
        //            name: "Frid Kolstø"
        //            mobile: "07577050020"
        //            home: "05657348462"
        //            work: "07835522668"
        //            homeadress: "Ängsgatan 16 VÄSTRA ÄMTERVIK 686 03 Sweden"
        //            workadress: "Syrengården 99 HOVÅS 436 18 Sweden"
        //            url: "MileAwards.com"
        //        }
        //        ListElement {
        //            name: "Hadi Lindqvist"
        //            mobile: "07566935380"
        //            home: "02818240859"
        //            work: "07602442156"
        //            homeadress: "Nöjesgatan 22 VANSBRO 780 50 Sweden"
        //            workadress: "Hagagatan 95 LILLA EDET 463 15 Sweden"
        //            url: "TightMarket.com"
        //        }
        //        ListElement {
        //            name: "Ville Dahl"
        //            mobile: "07247587781"
        //            home: "04167735842"
        //            work: "07645500464"
        //            homeadress: "Eggelstad 39 LÖVESTAD 270 30 Sweden"
        //            workadress: "Bottna Berghem 78 BERGSJÖ 820 70 Sweden"
        //            url: "DropChaser.com"
        //        }
        //        ListElement {
        //            name: "Marcus Fairchild"
        //            mobile: "07245312157"
        //            home: "02233903425"
        //            work: "07547141811"
        //            homeadress: "Tulpanv 25 ÄNGELSBERG 737 55 Sweden"
        //            workadress: "Gamla Svedalavägen 96 HYSSNA 510 22 Sweden"
        //            url: "WesternTunes.com"
        //        }
        //        ListElement {
        //            name: "Tørris Mehus"
        //            mobile: "07317873346"
        //            home: "01756515573"
        //            work: "07313630511"
        //            homeadress: "Simpnäs 8 RIMBO 762 00 Sweden"
        //            workadress: "Luddingsbo Mekanikusv 76 STOCKHOLM 123 92 Sweden"
        //            url: "JobBonds.com"
        //        }
        //        ListElement {
        //            name: "Tuva Öberg"
        //            mobile: "07508039719"
        //            home: "06132949021"
        //            work: "07147191719"
        //            homeadress: "Mjövattnet 90 NORDINGRÅ 870 30 Sweden"
        //            workadress: "Främsteby Karlsborg 34 KATRINEHOLM 641 82 Sweden"
        //            url: "Massdamage.com"
        //        }
        //        ListElement {
        //            name: "Kimberly Foster"
        //            mobile: "07391675912"
        //            home: "01717192158"
        //            work: "07303667571"
        //            homeadress: "Kläppinge 99 FJÄRDHUNDRA 190 70 Sweden"
        //            workadress: "Koppramåla 63 RÖDEBY 370 30 Sweden"
        //            url: "VeteranBikers.com"
        //        }
        //        ListElement {
        //            name: "Karl Dahlberg"
        //            mobile: "07885314239"
        //            home: "09224542830"
        //            work: "07377475779"
        //            homeadress: "Storgatan 36 HAPARANDA 953 32 Sweden"
        //            workadress: "Skällebo 21 SKEPPSHULT 333 03 Sweden"
        //            url: "TonerSquad.com"
        //        }
        //        ListElement {
        //            name: "Olena Alstad"
        //            mobile: "07045367830"
        //            home: "09134292570"
        //            work: "07857683737"
        //            homeadress: "Bursiljum 28 KÅGE 934 38 Sweden"
        //            workadress: "Lemesjö 80 OSBY 283 00 Sweden"
        //            url: "RetirementTrips.com"
        //        }
        //        ListElement {
        //            name: "Joseph Brazil"
        //            mobile: "07532588115"
        //            home: "04552422221"
        //            work: "07316956129"
        //            homeadress: "Koppramåla 8 TVING 370 33 Sweden"
        //            workadress: "Simpnäs 68 GRISSLEHAMN 760 45 Sweden"
        //            url: "FinancialLimit.com"
        //        }
        //        ListElement {
        //            name: "Malva Söderberg"
        //            mobile: "07629315323"
        //            home: "04985882380"
        //            work: "07611422718"
        //            homeadress: "Skolspåret 62 DALHEM 620 24 Sweden"
        //            workadress: "Barrgatan 99 ABBORRTRÄSK 930 82 Sweden"
        //            url: "NeverFamous.com"
        //        }
        //        ListElement {
        //            name: "Mollie Danielsson"
        //            mobile: "07646495939"
        //            home: "03207654937"
        //            work: "07698164171"
        //            homeadress: "Gamla Svedalavägen 28 HORRED 510 10 Sweden"
        //            workadress: "Simpnäs 71 GRÄDDÖ 760 15 Sweden"
        //            url: "PicVisions.com"
        //        }
        //        ListElement {
        //            name: "Engelhart Lønning"
        //            mobile: "07125961942"
        //            home: "0707811256"
        //            work: "07573276832"
        //            homeadress: "Hantverkarg 48 GÄLLNÖBY 130 33 Sweden"
        //            workadress: "Anders Sadelmakares Gränd 63 HÖGSJÖ 640 10 Sweden"
        //            url: "NukeSpy.com"
        //        }
        //        ListElement {
        //            name: "Elliott Henriksson"
        //            mobile: "07838753475"
        //            home: "0639171905"
        //            work: "07775155159"
        //            homeadress: "Stallstigen 44 ÖSTERSUND 831 23 Sweden"
        //            workadress: "Storgatan 50 GRIMSLÖV 340 32 Sweden"
        //            url: "RoyalLives.com"
        //        }
        //        ListElement {
        //            name: "Filip Johnsson"
        //            mobile: "07472074230"
        //            home: "04808982377"
        //            work: "07198193388"
        //            homeadress: "Ånhult 79 TREKANTEN 388 41 Sweden"
        //            workadress: "Ellenö 71 FÄRGELANDA 458 81 Sweden"
        //            url: "SwapRentals.com"
        //        }
        //        ListElement {
        //            name: "Drifa Hylland"
        //            mobile: "07803469014"
        //            home: "06807231587"
        //            work: "07680238001"
        //            homeadress: "Buanvägen 11 YTTERHOGDAL 840 90 Sweden"
        //            workadress: "Idvägen 11 ÅSEDA 360 70 Sweden"
        //            url: "ParkingConversion.com"
        //        }
        //        ListElement {
        //            name: "Ivar Hansson"
        //            mobile: "07914086265"
        //            home: "0708299890"
        //            work: "07032970319"
        //            homeadress: "Hantverkarg 57 GÄLLNÖBY 130 33 Sweden"
        //            workadress: "Korsgatan 51 STRÄNGNÄS 645 23 Sweden"
        //            url: "WirelessDialog.com"
        //        }
        //        ListElement {
        //            name: "Vilda Holmgren"
        //            mobile: "07509667408"
        //            home: "05202728014"
        //            work: "07527583972"
        //            homeadress: "Hagagatan 9 LILLA EDET 463 39 Sweden"
        //            workadress: "Loftaheden 22 BRUZAHOLM 570 34 Sweden"
        //            url: "IronJet.com"
        //        }
        //        ListElement {
        //            name: "Runi Rykkje"
        //            mobile: "07287246734"
        //            home: "05736644126"
        //            work: "07884777906"
        //            homeadress: "Fälloheden 77 TÖCKSFORS 670 10 Sweden"
        //            workadress: "Dyvik 97 SJÄLEVAD 890 23 Sweden"
        //            url: "TodaysExcuse.com"
        //        }
        //        ListElement {
        //            name: "Hyberte Mørch"
        //            mobile: "07510475455"
        //            home: "03013626004"
        //            work: "07280351738"
        //            homeadress: "Överhogdal 17 MÖLNLYCKE 435 36 Sweden"
        //            workadress: "Loftaheden 58 CHARLOTTENBERG 670 50 Sweden"
        //            url: "GuamCondo.com"
        //        }
        ListElement {
            name: "Yngve Thigpen"
            mobile: "07580873724"
            home: "09166632823"
            work: "07612253293"
            homeadress: "Bursiljum 15 FÄLLFORS 930 52 Sweden"
            workadress: "Östanlid 68 VEDDIGE 430 20 Sweden"
            url: "IndependentBookshop.com"
        }
        ListElement {
            name: "Ysta Reilly"
            mobile: "07709393460"
            home: "07045300263"
            work: "07605857434"
            homeadress: "Bottna Knutsgård 87 SALTSJÖBADEN 133 39 Sweden"
            workadress: "Karlsängen 63 NYNÄSHAMN 149 14 Sweden"
            url: "PicVisions.com"
        }
        ListElement {
            name: "Åsa McBroom"
            mobile: "07783681109"
            home: "03465214937"
            work: "07562744554"
            homeadress: "Löberöd 92 LÅNGÅS 311 38 Sweden"
            workadress: "Gärdslösa 49 GÄLLÖ 840 50 Sweden"
            url: "AntiHot.com"
        }
        ListElement {
            name: "Åke Wilder"
            mobile: "07886982081"
            home: "09343234730"
            work: "07021764722"
            homeadress: "Nittsjö Kvarngatu 42 ÅNÄSET 910 44 Sweden"
            workadress: "Orrspelsv 88 ÖRTRÄSK 920 20 Sweden"
            url: "LaunchPlans.com"
        }
        ListElement {
            name: "Örjan Grout"
            mobile: "07604738141"
            home: "04546607864"
            work: "07595414776"
            homeadress: "Främsteby Vena 78 KARLSHAMN 374 94 Sweden"
            workadress: "Vipgränden 31 VIMMERBY 598 00 Sweden"
            url: "BargainMaintenance.com"
        }
    }
}
