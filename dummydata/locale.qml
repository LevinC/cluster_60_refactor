/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0

QtObject {
    property var meassureSystem:   'metric'    // imperial or metric
    property var temperatureScale: 'celsius'   // fahrenheit or celsius
    property var timeConvention:   '24hour'    // 12hour or 24hour
}
