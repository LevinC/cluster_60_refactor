/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import QtMultimedia 5.0

Audio {

    property real mediaVolume
    property bool playingState

    function saveVol() {
            mediaVolume = volume
            console.log("media volume is... " + mediaVolume + "volume is = " + volume)
    }

    function saveState() {
            if (playbackState === Audio.PlayingState) { playingState = true }
            else {
                playingState = false
            }
    console.log("playingstate ? " + playingState)
    }

    function setTrack(index) {
        mediaModel.index = index;
        play();
    }

    function next() {
        playlistModel.moved = false;
        if (mediaModel.index<100){
            mediaModel.index++
        }
        play();
    }

    function prev() {
        if (position <= 3000) {
            playlistModel.moved = false;
            if (mediaModel.index>0){
                mediaModel.index--
            }
        }
        else {
            musicPlayer.seek(0);
        }
        play();
    }

    function increaseVol() {
        volume += 0.01;
    }

    function decreaseVol() {
        volume -= 0.01;
    }

    autoLoad: true
    autoPlay: false

    source: mediaModel.path

    onPlaybackStateChanged: {
        switch (playbackState) {
            case Audio.StoppedState:
                if (duration != -1 && position >= duration-50) {
                    next()
                }
            break
        }
    }

    //volume: volumeModel.mediaPlayerVolume - Make Sure It's Updated
    volume: 0.8

    // [iOSInCar]
    Behavior on volume {
        NumberAnimation {
            duration: 500
        }
    }
    // [iOSInCar]
}

