/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0

Item {
    id: volumeControllerRoot

    property alias volume: systemVolumeControl.systemVolume
    property alias minVolume: systemVolumeControl.systemMinVolume
    property alias maxVolume: systemVolumeControl.systemMaxVolume
    property alias percentage: systemVolumeControl.volume
    property int percentageStep: 5

    // Internal Media Player Volume
    property real mediaPlayerVolume: 0.8
    // Internal Media Player Duck Volume
    property real mediaPlayerDuckVolume: 0.1

    function increaseVol() {
        if (percentage < 100) {
            percentage = Math.min(percentage + percentageStep, 100);
        }
    }

    function decreaseVol() {
        if (percentage > 0) {
            percentage = Math.max(percentage - percentageStep, 0);
        }
    }

    Item {
        id: systemVolumeControl

        property int volume: 0
        property int systemVolume: 0 // unused
        property int systemMinVolume: 0
        property int systemMaxVolume: 100
    }
}
