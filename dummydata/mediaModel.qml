/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0

QtObject {
    property string mediaSource: "Bluetooth"
    //property var recentSources: ["iPod", "FM Radio", "USB", "Bluetooth"]
    property string title: "Greatest Love"
    property string artist: "Arcade Fire"
    property string album: "Funeral"
    property string artUrl: "/arcade_fire-funeral.jpg"
    property string path: "../dummydata/media/media.mp3"
    property int index: 0
    property string searchString: ""
    property string listTitle: "Playlist"

    property bool shuffle: false

    function splitString(logo){
        return "/arcade_fire-funeral.jpg"
    }

    function changeSource(name) {
        recentSources.insert(0, {"name": mediaSource } )
        mediaSource = name
    }

    property ListModel recentSources: ListModel {
        id: recentSources
        ListElement {
            name: "iPod"
        }
        ListElement {
            name: "FM Radio"
        }
    }

    onIndexChanged: {
        if (index < 0) {
            index = 0
            return
        }
        else if (index >= playlist.count) {
            index = playlist.count-1
            return
        }
        if (path == "../dummydata/media/media.mp3")
            path = "../dummydata/media/media2.mp3"
        else if (path == "../dummydata/media/media2.mp3")
            path = "../dummydata/media/media3.mp3"
        else path = "../dummydata/media/media.mp3"

        var track = playlist.get(index);
        title = track.title;
        artist = track.artist;
        album = track.album;
        artUrl = track.artUrl;
    }

    property ListModel tracksSearch: ListModel {
        property string query: searchString
        onQueryChanged: {
            console.log("searching....?")
            clear();
            var i;
            for (i = 0; i < allTracks.count; i++) {
                var item = allTracks.get(i);
                if (item.title && (item.title.length > 0) && (item.title.toLocaleLowerCase().indexOf(query.toLocaleLowerCase()) !== -1)) {
                    if (item.artUrl) item.arUrl = Qt.resolvedUrl(item.artUrl);
                    append( item );
                }
            }
        }
    }
    property ListModel artistsSearch: ListModel {
        property string query: searchString
        onQueryChanged: {
            console.log("searching....?")
            clear();
            var i;
            for (i = 0; i < allArtists.count; i++) {
                var item = allArtists.get(i);
                if (item.artist && (item.artist.length > 0) && (item.artist.toLocaleLowerCase().indexOf(query.toLocaleLowerCase()) !== -1)) {
                    if (item.artUrl) item.arUrl = Qt.resolvedUrl(item.artUrl);
                    append( item );
                }
            }
        }
    }
    property ListModel albumsSearch: ListModel {
        property string query: searchString
        onQueryChanged: {
            console.log("searching....?")
            clear();
            var i;
            for (i = 0; i < allTracks.count; i++) {
                var item = allTracks.get(i);
                if (item.album && (item.album.length > 0) && (item.album.toLocaleLowerCase().indexOf(query.toLocaleLowerCase()) !== -1)) {
                    if (item.artUrl) item.arUrl = Qt.resolvedUrl(item.artUrl);
                    append( item );
                }
            }
        }
    }

    property ListModel playlist: allTracks
    property ListModel allAlbums: allAlbum //TODO
    property ListModel allArtists: allArtist

    property ListModel allArtist: ListModel {
        ListElement {
            artist: "Arcade Fire"
            albums:  [
                ListElement {
                    artist: "Arcade Fire"
                    album: "Funeral"
                    artUrl: "/arcade_fire-funeral.jpg"
                    tracks: [
                        ListElement {
                            title: "Wake Up"
                            artist: "Arcade Fire"
                            album: "Funeral"
                            length: 20000
                            artUrl: "/arcade_fire-funeral.jpg"
                            path: "../dummydata/media/media2.mp3"
                        }
                    ]
                }
            ]
        }
        ListElement {
            artist: "Circa Survive"
            albums:  [
                ListElement {
                    artist: "Circa Survive"
                    album: "On letting go"
                    artUrl: "/circa_survive-on_letting_go.jpg"
                    tracks: [
                        ListElement {
                            title: "The Greatest Lie"
                            artist: "Circa Survive"
                            album: "On letting go"
                            length: 60000
                            artUrl: "/circa_survive-on_letting_go.jpg"
                            path: "../dummydata/media/media3.mp3"
                        }
                    ]
                }
            ]
        }
        ListElement {
            artist: "Danger Doom"
            albums:  [
                ListElement {
                    artist: "Danger Doom"
                    album: "On letting go"
                    artUrl: ""
                    tracks: [
                        ListElement {
                            title: "The Greatest Lie"
                            artist: "Circa Survive"
                            album: "On letting go"
                            length: 60000
                            artUrl: ""
                            path: "../dummydata/media/media3.mp3"
                        }
                    ]
                }
            ]
        }
        ListElement {
            artist: "Devils Gun"
            albums:  [
                ListElement {
                    artist: "Devils Gun"
                    album: "Raising the beast"
                    artUrl: "/devils_gun-raising_the_beast.jpg"
                    tracks: [
                        ListElement {
                            title: "Raising The Beast"
                            artist: "Devils Gun"
                            album: "Raising the beast"
                            length: 30000
                            artUrl: "/devils_gun-raising_the_beast.jpg"
                            path: "../dummydata/media/media3.mp3"
                        }
                    ]
                }
            ]
        }
        ListElement {
            artist: "Editors"
            albums:  [
                ListElement {
                    artist: "Editors"
                    album: "The end has a star"
                    artUrl: "/editors-the_end_has_a_start.jpg"
                    tracks: [
                        ListElement {
                            title: "Escape the Nest"
                            artist: "Editors"
                            album: "The end has a star"
                            length: 30000
                            artUrl: "/devils_gun-raising_the_beast.jpg"
                            path: "../dummydata/media/media.mp3"
                        }
                    ]
                }
            ]
        }
        ListElement {
            artist: "Electric Light Orchestra"
            albums:  [
                ListElement {
                    artist: "Electric Light Orchestra"
                    album: "Out of the blue"
                    artUrl: ""
                    tracks: [
                        ListElement {
                            title: "It's Over"
                            artist: "Electric Light Orchestra"
                            album: "Out of the blue"
                            length: 30000
                            artUrl: ""
                            path: "../dummydata/media/media.mp3"
                        }
                    ]
                }
            ]
        }
        ListElement {
            title: "In the Belly of a Shark"
            artist: "Gallows"
            album: "In the belly of a shark"
            length: 90000
            artUrl: "/gallows-in_the_belly_of_a_shark.jpg"
            path: "/media2.mp3"
        }
        ListElement {
            title: "Fistful Of Dallas"
            artist: "Ground Components"
            album: "An eye for a brow a tooth for a pick"
            length: 90000
            artUrl: "/ground_components-an_eye_for_a_brow_a_tooth_for_a_pick.jpg"
            path: "/media.mp3"
        }
        ListElement {
            title: "Is It Any Wonder?"
            artist: "Keane"
            album: "Under the iron sea"
            length: 90000
            artUrl: "/keane-under_the_iron_sea.jpg"
            path: "/media2.mp3"
        }
        ListElement {
            title: "Hearing Voices Tonight"
            artist: "Kisschasy"
            album: "United paper people"
            length: 90000
            artUrl: "/kisschasy-united_paper_people.jpg"
            path: "/media.mp3"
        }
        ListElement {
            title: "America"
            artist: "Kool and the gang"
            album: "Still Kool"
            length: 90000
            artUrl: "/kool_and_the_gang-still_cool.jpg"
            path: "/media2.mp3"
        }
        ListElement {
            title: "Can't Let Her Get Away"
            artist: "Michael Jackson"
            album: "Dangerous"
            length: 90000
            artUrl: "/michael_jackson-dangerous.jpg"
            path: "/media.mp3"
        }
        ListElement {
            title: "Micro Cuts"
            artist: "Muse"
            album: "Origin of Symmetry"
            length: 90000
            artUrl: "/muse-origin_of_symmetry.jpg"
            path: "/media2.mp3"
        }
        ListElement {
            title: "Speak to Me"
            artist: "Pink Floyd"
            album: "Dark side of the moon"
            artUrl: "/pink_floyd-dark_side_of_the_moon.jpg"
        }
        ListElement {
            title: "Marooned"
            artist: "Pink Floyd"
            album: "The Division Bell"
            length: 90000
            artUrl: "/pink_floyd-the_division_bell.jpg"
            path: "/media2.mp3"
        }
        ListElement {
            title: "For God and Country"
            artist: "Smashing Pumpkins"
            album: "Zeitgeist"
            length: 90000
            artUrl: "/smashing_pumpkins-zeitgeist.jpg"
            path: "/media.mp3"
        }
        ListElement {
            title: "Back Where We Belong"
            artist: "The Last Goodnight"
            album: "Poison Kiss"
            length: 90000
            artUrl: "/the_last_goodnight-poison_kiss.jpg"
            path: "/media2.mp3"
        }
    }
    property ListModel allAlbum: ListModel {
        ListElement {
            artist: "Arcade Fire"
            album: "Funeral"
            artUrl: "/arcade_fire-funeral.jpg"
            tracks: [
                ListElement {
                    title: "Wake Up"
                    artist: "Arcade Fire"
                    album: "Funeral"
                    length: 20000
                    artUrl: "/arcade_fire-funeral.jpg"
                    path: "../dummydata/media/media2.mp3"
                }
            ]
        }
        ListElement {
            artist: "Circa Survive"
            album: "On letting go"
            artUrl: "/circa_survive-on_letting_go.jpg"
            tracks: [
                ListElement {
                    title: "The Greatest Lie"
                    artist: "Circa Survive"
                    album: "On letting go"
                    length: 60000
                    artUrl: "/circa_survive-on_letting_go.jpg"
                    path: "../dummydata/media/media3.mp3"
                }
            ]
        }
        ListElement {
            artist: "Danger Doom"
            album: "The mouse and the mask"
            artUrl: ""
            tracks: [
                ListElement {
                    title: "No Names"
                    artist: "Danger Doom"
                    album: "The mouse and the mask"
                    length: 90000
                    artUrl: ""
                    path: "../dummydata/media/media2.mp3"
                }
            ]
        }
        ListElement {
            title: "Raising The Beast"
            artist: "Devils Gun"
            album: "Raising the beast"
            length: 70000
            artUrl: "/devils_gun-raising_the_beast.jpg"
            path: "../dummydata/media/media3.mp3"
        }
        ListElement {
            title: "Escape the Nest"
            artist: "Editors"
            album: "The end has a star"
            length: 120000
            artUrl: "/editors-the_end_has_a_start.jpg"
            path: "../dummydata/media/media.mp3"
        }
        ListElement {
            title: "It's Over"
            artist: "Electric Light Orchestra"
            album: "Out of the blue"
            length: 90000
            artUrl: ""
            path: "/media.mp3"
        }
        ListElement {
            title: "In the Belly of a Shark"
            artist: "Gallows"
            album: "In the belly of a shark"
            length: 90000
            artUrl: "/gallows-in_the_belly_of_a_shark.jpg"
            path: "/media2.mp3"
        }
        ListElement {
            title: "Fistful Of Dallas"
            artist: "Ground Components"
            album: "An eye for a brow a tooth for a pick"
            length: 90000
            artUrl: "/ground_components-an_eye_for_a_brow_a_tooth_for_a_pick.jpg"
            path: "/media.mp3"
        }
        ListElement {
            title: "Is It Any Wonder?"
            artist: "Keane"
            album: "Under the iron sea"
            length: 90000
            artUrl: "/keane-under_the_iron_sea.jpg"
            path: "/media2.mp3"
        }
        ListElement {
            title: "Hearing Voices Tonight"
            artist: "Kisschasy"
            album: "United paper people"
            length: 90000
            artUrl: "/kisschasy-united_paper_people.jpg"
            path: "/media.mp3"
        }
        ListElement {
            title: "America"
            artist: "Kool and the gang"
            album: "Still Kool"
            length: 90000
            artUrl: "/kool_and_the_gang-still_cool.jpg"
            path: "/media2.mp3"
        }
        ListElement {
            title: "Can't Let Her Get Away"
            artist: "Michael Jackson"
            album: "Dangerous"
            length: 90000
            artUrl: "/michael_jackson-dangerous.jpg"
            path: "/media.mp3"
        }
        ListElement {
            title: "Micro Cuts"
            artist: "Muse"
            album: "Origin of Symmetry"
            length: 90000
            artUrl: "/muse-origin_of_symmetry.jpg"
            path: "/media2.mp3"
        }
        ListElement {
            title: "Speak to Me"
            artist: "Pink Floyd"
            album: "Dark side of the moon"
            artUrl: "/pink_floyd-dark_side_of_the_moon.jpg"
        }
        ListElement {
            title: "Marooned"
            artist: "Pink Floyd"
            album: "The Division Bell"
            length: 90000
            artUrl: "/pink_floyd-the_division_bell.jpg"
            path: "/media2.mp3"
        }
        ListElement {
            title: "For God and Country"
            artist: "Smashing Pumpkins"
            album: "Zeitgeist"
            length: 90000
            artUrl: "/smashing_pumpkins-zeitgeist.jpg"
            path: "/media.mp3"
        }
        ListElement {
            title: "Back Where We Belong"
            artist: "The Last Goodnight"
            album: "Poison Kiss"
            length: 90000
            artUrl: "/the_last_goodnight-poison_kiss.jpg"
            path: "/media2.mp3"
        }
    }
    property ListModel allTracks: ListModel {
        ListElement {
            title: "Wake Up"
            artist: "Arcade Fire"
            album: "Funeral"
            length: 20000
            artUrl: "/arcade_fire-funeral.jpg"
            path: "../dummydata/media/media2.mp3"
        }
        ListElement {
            title: "The Greatest Lie"
            artist: "Circa Survive"
            album: "On letting go"
            length: 60000
            artUrl: "/circa_survive-on_letting_go.jpg"
            path: "../dummydata/media/media3.mp3"
        }
        ListElement {
            title: "No Names"
            artist: "Danger Doom"
            album: "The mouse and the mask"
            length: 90000
            artUrl: ""
            path: "../dummydata/media/media2.mp3"
        }
        ListElement {
            title: "Raising The Beast"
            artist: "Devils Gun"
            album: "Raising the beast"
            length: 70000
            artUrl: "/devils_gun-raising_the_beast.jpg"
            path: "../dummydata/media/media3.mp3"
        }
        ListElement {
            title: "Escape the Nest"
            artist: "Editors"
            album: "The end has a star"
            length: 120000
            artUrl: "/editors-the_end_has_a_start.jpg"
            path: "../dummydata/media/media.mp3"
        }
        ListElement {
            title: "It's Over"
            artist: "Electric Light Orchestra"
            album: "Out of the blue"
            length: 90000
            artUrl: ""
            path: "/media.mp3"
        }
        ListElement {
            title: "In the Belly of a Shark"
            artist: "Gallows"
            album: "In the belly of a shark"
            length: 90000
            artUrl: "/gallows-in_the_belly_of_a_shark.jpg"
            path: "/media2.mp3"
        }
        ListElement {
            title: "Fistful Of Dallas"
            artist: "Ground Components"
            album: "An eye for a brow a tooth for a pick"
            length: 90000
            artUrl: "/ground_components-an_eye_for_a_brow_a_tooth_for_a_pick.jpg"
            path: "/media.mp3"
        }
        ListElement {
            title: "Is It Any Wonder?"
            artist: "Keane"
            album: "Under the iron sea"
            length: 90000
            artUrl: "/keane-under_the_iron_sea.jpg"
            path: "/media2.mp3"
        }
        ListElement {
            title: "Hearing Voices Tonight"
            artist: "Kisschasy"
            album: "United paper people"
            length: 90000
            artUrl: "/kisschasy-united_paper_people.jpg"
            path: "/media.mp3"
        }
        ListElement {
            title: "America"
            artist: "Kool and the gang"
            album: "Still Kool"
            length: 90000
            artUrl: "/kool_and_the_gang-still_cool.jpg"
            path: "/media2.mp3"
        }
        ListElement {
            title: "Can't Let Her Get Away"
            artist: "Michael Jackson"
            album: "Dangerous"
            length: 90000
            artUrl: "/michael_jackson-dangerous.jpg"
            path: "/media.mp3"
        }
        ListElement {
            title: "Micro Cuts"
            artist: "Muse"
            album: "Origin of Symmetry"
            length: 90000
            artUrl: "/muse-origin_of_symmetry.jpg"
            path: "/media2.mp3"
        }
        ListElement {
            title: "Speak to Me"
            artist: "Pink Floyd"
            album: "Dark side of the moon"
            length: 90000
            artUrl: "/pink_floyd-dark_side_of_the_moon.jpg"
            path: "/media.mp3"
        }
        ListElement {
            title: "Marooned"
            artist: "Pink Floyd"
            album: "The Division Bell"
            length: 90000
            artUrl: "/pink_floyd-the_division_bell.jpg"
            path: "/media2.mp3"
        }
        ListElement {
            title: "For God and Country"
            artist: "Smashing Pumpkins"
            album: "Zeitgeist"
            length: 90000
            artUrl: "/smashing_pumpkins-zeitgeist.jpg"
            path: "/media.mp3"
        }
        ListElement {
            title: "Back Where We Belong"
            artist: "The Last Goodnight"
            album: "Poison Kiss"
            length: 90000
            artUrl: "/the_last_goodnight-poison_kiss.jpg"
            path: "/media2.mp3"
        }
    }
}
