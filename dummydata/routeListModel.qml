import QtQuick 2.0

QtObject {
    id: routeListModel

    property ListModel favRoutes: ListModel {
        ListElement {
            route: "Home"
            direction: "127"
            routeTitle: "Volvo Jakobs Väg"
            routeSubTitle: "Torslanda"
            destination: "Volvo PVE"
            thumbnail: "img/martina.png"
            waypoints: [
            ]
        }
            ListElement {
                route: "Martina's favorite route"
                direction: "127"
                routeTitle: "American Museum of Natural History"
                routeSubTitle: "Landmark"
                destination: "Martina's favorite place"
                thumbnail: "img/martina.png"
                information: [
                    ListElement {
                        item1: "Phone"
                        item2: "555-12679"
                    },
                    ListElement {
                        item1: "Url"
                        item2: "americanmuseumofnaturalhistory.com"
                    }
                ]
                waypoints: [
                    ListElement {
                        wpPosition: "Central Park West and 79th Street"
                        wpInfo: "American Museum of Natural History"
                    },
                    ListElement {
                        wpPosition: "Central Park West and 79th Street"
                        wpInfo: "Staten Island Ferry Battery Park"
                    },
                    ListElement {
                        wpPosition: "48th and 49th Street"
                        wpInfo: "Atrium Water Tunnel"
                    }
                ]
            }
            // Add More Routes Here
        }
        property ListModel destinations: ListModel {
            ListElement {
                header: "Info card"
                subTitle: "Empire State Building"
                destination: "350 5th Ave"
                thumbnail: "img/martina.png"
                information: [
                    ListElement {
                        item1: "Phone"
                        item2: "555-113564"
                    },
                    ListElement {
                        item1: "Url"
                        item2: "empirestatebuilding.com"
                    }
                ]
            }
            ListElement {
                header: "Info card"
                subTitle: "American Museum of Natural History"
                destination: "Central Park West and 79th Street"
                thumbnail: "img/martina.png"
                information: [
                    ListElement {
                        item1: "Phone"
                        item2: "555-464651"
                    },
                    ListElement {
                        item1: "Url"
                        item2: "americanmuseumofnaturalhistory.com"
                    }
                ]
            }
            ListElement {
                header: "Info card"
                subTitle: "Residential Address"
                destination: "299 N Prospect Park West"
            }
            // Add More Routes Here
        }
        property ListModel recentRoutes: ListModel {
            ListElement {
                destination: "Empire State Building"
                info: "Landmark POI"
                routeTitle: "Empire State Building"
                routeSubTitle: "Landmark POI"
            }
            ListElement {
                destination: "Central Park West and 79th Street"
                info: "American Museum of Natural History"
                routeTitle: "Central Park West and 79th Street"
                routeSubTitle: "American Museum of Natural History"
            }
            ListElement {
                destination: "299 N Prospect Park West, Brooklyn"
                info: "Residential Address"
                routeTitle: "299 N Prospect Park West, Brooklyn"
                routeSubTitle: "Residential Address"
            }
            // Add More recents Here
        }

    property ListModel library: ListModel {
        ListElement {
            destination: "saved library item 1"
        }
        ListElement {
            destination: "saved library item 2"
        }
        ListElement {
            destination: "saved library item 3"
        }
        ListElement {
            destination: "saved library item 4"
        }
        // Add More recents Here
    }

    property ListModel pois: ListModel {
        ListElement {
            destination: "Accommodation"
            poiIcon: "/Navigation/PoiIcons/img_nav_poi_hotel_category_list.png"
        }
        ListElement {
            destination: "Business Facility"
            poiIcon: "/Navigation/PoiIcons/img_nav_poi_business_facility_list.png"
        }
        ListElement {
            destination: "Gas Stations"
            poiIcon: "/Navigation/PoiIcons/img_nav_poi_filling_station_list.png"
        }
        ListElement {
            destination: "Hospitals"
            poiIcon: "/Navigation/PoiIcons/img_nav_poi_hospital_list.png"
        }
        ListElement {
            destination: "Landmarks"
            poiIcon: "/Navigation/PoiIcons/img_nav_poi_landmark_pois_list.png"
        }
        ListElement {
            destination: "Tourist Information"
            poiIcon: "/Navigation/PoiIcons/img_nav_poi_tourist_info_list.png"
        }
        ListElement {
            destination: "Shopping"
            poiIcon: "/Navigation/PoiIcons/img_nav_poi_shopping_list.png"
        }
        ListElement {
            destination: "Restaurants"
            poiIcon: "/Navigation/PoiIcons/img_nav_poi_restaurant_list.png"
        }
        // Add More recents Here
    }
}
