import QtQuick 2.0
import "../Common"

Image{
    id:root
    signal leftBtnClicked
    signal centerBtnClicked
    signal rightBtnClicked

    property string leftBtnText
    property string centerBtnText
    property string rightBtnText

    property  alias centerBtn:centerBtn;


    Row{
        anchors.horizontalCenter: root.horizontalCenter
        y:908
        spacing: 3

        Spa9FlatButton{
            width:(contentW - centerBtn.width)/2
            height:70
            text:leftBtnText
            invisible: false

            textSize:24

            onClicked: {
                leftBtnClicked()
            }
        }

        Spa9FlatButton{
            id: centerBtn
            width:contentW/3
            height:70
            text:centerBtnText
            invisible: false
            textSize:24
            onClicked: {
                centerBtnClicked()
            }

        }
        Spa9FlatButton{
            id:rightBtn
            width:(contentW - centerBtn.width)/2
            height:70
            text:rightBtnText
            invisible: false
            textSize:24
            buttonText.anchors.fill: rightBtn
            onClicked: {
                rightBtnClicked()
            }
        }

    }


}


