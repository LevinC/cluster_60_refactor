import QtQuick 2.0

import "../Common/"

MouseArea{
    id:root
    width: globalStyle.screenWidth
    height: globalStyle.screenHeight

    property real currV

    property variant contentSources: ["8.Quicktip1.png","Keyless.png","8.Quicktip1.png","Keyless.png","8.Quicktip1.png","Keyless.png","8.Quicktip1.png","Keyless.png"]

    Rectangle{
        color:"black"
        opacity: .9
        anchors.fill: parent
    }

    ListView{
        id:list
        anchors.fill: parent
        orientation: ListView.Horizontal
        snapMode: ListView.SnapOneItem
        model: contentSources
        maximumFlickVelocity:3000

        property real centeredItem

        delegate: Item{
            width:globalStyle.screenWidth
            height:globalStyle.screenHeight

            Image{
                source: "ims/" +contentSources[index]
            }

            Spa9FlatButton{
                height:70
                width:280
                invisible: false
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom:parent.bottom
                anchors.bottomMargin: 80
                text:"Close"
                onClicked: {
                    root.visible=false
                }
            }
        }

        onContentXChanged: {
            root.currV= (contentX+width/2)/width
        }
    }

    PageInd{
        id:pageInd
        width:140
        height:30
        amount:contentSources.length
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 55
        curr:currV
    }
}
