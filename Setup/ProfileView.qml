import QtQuick 2.0

import "../Common/"
import "../"

SetupView{
    id:root
    source: "ims/5.Profile.png"

    leftBtnText: "Back"
    centerBtnText: "Finish later"
    rightBtnText: "Skip"

    Keyboard{
        id:keyBoard
        inputfieldbg.parent.visible:false
        state:"hidden"
    }

    MouseArea{
        id:textArea
        width:500
        height:72
        anchors.centerIn: parent
        anchors.verticalCenterOffset: -100

        Item {
            id:textInputField
            anchors.fill: parent
            Image {
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                source: globalStyle.imageUrl + "/Icons/img_hmc_mic" + globalStyle.skinUrl + ".png"
            }
            BorderImage {
                id: inputfieldbg
                width: parent.width
                height: parent.height
                border { left: 10; right: 10; top: 10; bottom: 10 }
                horizontalTileMode: BorderImage.Stretch
                source: globalStyle.imageUrl + "/HMI_Core/img_hmc_inputfield_OFF.png"
                opacity:1
                Behavior on opacity {CubicAnimation{}}
            }

            Rectangle{
                id:bgRect
                anchors.fill: parent

                radius: 9
                color:"transparent"
                border.color: "#dbdbdb"
                opacity:0
                Behavior on opacity {CubicAnimation{}}
            }

            states: [
                State {
                    name: "editing"
                    PropertyChanges {
                        target: inputfieldbg
                        opacity:0
                    }
                    PropertyChanges {
                        target: bgRect
                        opacity:.3
                    }
                }
            ]
        }

        onClicked: {
            keyBoard.show(true,textInput)

            if (textInputField.state==""){
                textInputField.state = "editing"
            }else{
                textInputField.state=""
                keyBoard.state = "hidden"
            }
        }

        SpaText{
            id: textInput
            anchors.leftMargin: 80
            anchors.rightMargin: 10
            verticalAlignment: Text.AlignVCenter
            property string keyboardText
            anchors.fill: parent
            text:keyboardText

            onTextChanged: {
                if (text.length==6){
                    loadNameTimer.start()
                }else{
                    loadNameTimer.stop()
                }
            }

            Timer{
                id:loadNameTimer
                interval: 2000
                onTriggered: {
                    root.state = "content"
                    keyBoard.state="hidden"
                }

                onRunningChanged: {
                    if (running){
                        console.log("running")
                        activityIndicator.loadingAnimation.start()
                    }else{
                        activityIndicator.loadingAnimation.stop()
                    }
                }
            }
        }
    }

    ActivityIndicator{
        id:activityIndicator
        anchors.centerIn: root
        loadingAnimation.duration: 2000
        loadingAnimation.to:350
        width:80
        height:80
        opacity:textInput.text.length==6 ? 1:0
    }

    Spa9FlatButton{
        id:editBtn
        text:"Edit"
        invisible:false
        width:500
        height:76
        anchors.horizontalCenter: root.horizontalCenter
        anchors.bottom: root.bottom
        anchors.bottomMargin: 233
        visible:false
        buttonType: "center"
    }

    states: [
        State {
            name: "content"
            PropertyChanges {
                target: root
                source: "ims/6.Profileresult.png"
                rightBtnText:"Next"
            }
            PropertyChanges {
                target: textArea
                visible:false
            }
            PropertyChanges {
                target: activityIndicator
                visible:false
            }
            PropertyChanges {
                target: editBtn
                visible:true
            }
        }
    ]


}
