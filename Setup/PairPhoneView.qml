import QtQuick 2.0

import "../Common"

SetupView{
    id:root
    source: "ims/3.Pairphone.png"

    leftBtnText: "Back"
    centerBtnText: "Finish later"
    rightBtnText: "Skip"

    Spa9FlatButton{
        anchors.centerIn: root
        buttonType: "standalone"
        width:510
        height:72
        invisible:false
        anchors.verticalCenterOffset: -50
        text:"Add a device"
    }
}
