import QtQuick 2.0

import "../Common"

SetupView{
    id:root
    source: "ims/7.Adjustments.png"

    leftBtnText: "Back"
    centerBtnText: "Finish later"
    rightBtnText: "Next"

    Row{
        anchors.horizontalCenter: root.horizontalCenter
        anchors.top: root.top
        anchors.topMargin: 214
        spacing:2
        Spa9FlatButton{
            text:"Seats"
            invisible:false
            width:165
            height:62
            buttonType: "left"
        }
        Spa9FlatButton{
            text:"Steering"
            invisible:false
            width:165
            height:62
        }
        Spa9FlatButton{
            text:"Mirror"
            invisible:false
            width:165
            height:62
        }
        Spa9FlatButton{
            text:"HUD"
            invisible:false
            width:165
            height:62
            buttonType: "right"

        }
    }

    Spa9FlatButton{
        invisible: false
        anchors.horizontalCenter: root.horizontalCenter
        anchors.bottom:root.bottom
        anchors.bottomMargin: 130
        width:80
        height:80

        Image {
            source: "ims/infobutton.png"
            anchors.fill: parent;
        }


        onClicked: {
            properSeating.visible=true;
        }

    }


    MouseArea{
        id:properSeating
        visible:false

        anchors.fill: parent

        Rectangle{
            anchors.fill: parent
            color:"black"
            opacity:.9
        }

        Image{
            source:"ims/propersitting.png"
        }

        Spa9FlatButton{
            text:"Close"
            invisible:false
            width:638
            height:72
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 200

            onClicked: {
                properSeating.visible=false;
            }
        }



    }
}
