import QtQuick 2.0

import "../Common/"

SetupView{
    source: "ims/4.Internetconnection.png"

    leftBtnText: "Back"
    centerBtnText: "Finish later"
    rightBtnText: "Skip"

    Column{
        spacing:1
        anchors.centerIn: parent
        anchors.verticalCenterOffset: 8
        Spa9FlatButton{
            id:btBtn
            height:76
            width:316
            text:"BLUETOOTH"
            buttonText.anchors.fill: btBtn
            buttonText.anchors.leftMargin: 76
            buttonText.horizontalAlignment: Text.AlignLeft
            invisible: false
        }
        Spa9FlatButton{
            id:usbBtn
            height:76
            width:316
            text:"USB"
            buttonText.anchors.fill: usbBtn
            buttonText.anchors.leftMargin: 76
            buttonText.horizontalAlignment: Text.AlignLeft
            invisible: false
        }
        Spa9FlatButton{
            id:simBtn
            height:76
            width:316
            text:"P-SIM"
            buttonText.anchors.fill: simBtn
            buttonText.anchors.leftMargin: 76
            buttonText.horizontalAlignment: Text.AlignLeft
            invisible: false
        }
        Spa9FlatButton{
            id:wifiBtn
            height:76
            width:316
            text:"WiFi"
            buttonText.anchors.fill: wifiBtn
            buttonText.anchors.leftMargin: 76
            buttonText.horizontalAlignment: Text.AlignLeft
            invisible: false
        }
    }
}
