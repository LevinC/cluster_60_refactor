import QtQuick 2.0

SetupView{
    source: "ims/9.Congrats.png"
    centerBtn.width: 0
    centerBtn.visible: false

    leftBtnText: "Finish"
    rightBtnText: "View Quick tips"
}
