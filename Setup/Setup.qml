import QtQuick 2.3

import "../Common"

MouseArea{
    id:root
    width: globalStyle.screenWidth
    height: globalStyle.screenHeight
    property int contentW: 700

    property variant contentSource: [welcomeScreen,termsView,pairPhoneView,internetConnView,profileView,adjustmentsView,congratsView];
    property int currInd: 0;

    Rectangle{
        anchors.fill: parent
        color:"black"
        opacity:.9
    }

    Image {
        source: "ims/setup_bg.png"
    }

    Loader{
        anchors.fill: root
        sourceComponent: contentSource[currInd]
    }

    Component{
        id:welcomeScreen
        WelcomeScreen{
         onLeftBtnClicked: root.close()
         onRightBtnClicked: root.moveToNext()
        }
    }

    Component{
        id:termsView
        TermsView{
         onLeftBtnClicked: root.moveToPrev()
         onCenterBtnClicked: root.close()
         onRightBtnClicked: root.moveToNext()
        }
    }

    Component{
        id:pairPhoneView
        PairPhoneView{
         onLeftBtnClicked: root.moveToPrev()
         onCenterBtnClicked: root.close()
         onRightBtnClicked: root.moveToNext()
        }
    }

    Component{
        id:internetConnView
        InternetConnView{
         onLeftBtnClicked: root.moveToPrev()
         onCenterBtnClicked: root.close()
         onRightBtnClicked: root.moveToNext()
        }
    }

    Component{
        id:profileView
        ProfileView{
         onLeftBtnClicked: root.moveToPrev()
         onCenterBtnClicked: root.close()
         onRightBtnClicked: root.moveToNext()
        }
    }

    Component{
        id:adjustmentsView
        AdjustmentsView{
         onLeftBtnClicked: root.moveToPrev()
         onCenterBtnClicked: root.close()
         onRightBtnClicked: root.moveToNext()
        }
    }

    Component{
        id:congratsView
        CongratsView{
         onLeftBtnClicked: root.visible=false
         onRightBtnClicked: root.moveToQuickTips()
        }
    }

    function show(){
        currInd=0
        root.visible=true
    }

    function moveToPrev(){
        if (currInd>0)currInd--
    }
    function moveToNext(){
        if (currInd<contentSource.length-1)currInd++
    }
    function close(){
        finishLaterView.visible=true
    }

    function moveToQuickTips(){
        root.visible=false
        coreScreen.showQuickTips()
    }

    FinishLaterView{
        id:finishLaterView
        visible:false
    }

}
