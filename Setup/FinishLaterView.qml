import QtQuick 2.0

import "../Common"

MouseArea{
    id:root
    width:globalStyle.screenWidth
    height:globalStyle.screenHeight



    Rectangle{
        anchors.fill:parent
        color:"black"
        opacity:.9

    }

    Image {
        id: contentIm
        source: "ims/finishlater.png"
    }

    MouseArea{
        width:520
        height:446
        anchors.centerIn: root

        Spa9FlatButton{
            width:parent.width-4
            height:70
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 4
            anchors.horizontalCenter: parent.horizontalCenter
            invisible: false
            buttonType: "center"
            text:"Cancel"
            onClicked: {
                root.visible=false
            }
        }

        Column{
            spacing:12
            anchors.centerIn: parent
            anchors.verticalCenterOffset: 10

            Spa9FlatButton{
                id:nextDriveCycleBtn
                width:456
                height:70
                invisible: false
                buttonType: "center"
                text:"Next Driving Cycle"
                buttonText.anchors.fill:nextDriveCycleBtn
                onClicked: {
                    root.visible=false
                    root.parent.visible=false
                }
            }
            Spa9FlatButton{
                width:456
                height:70
                invisible: false
                text:"In a Week"
                onClicked: {
                    root.visible=false
                    root.parent.visible=false
                }
            }
            Spa9FlatButton{
                width:456
                height:72
                invisible: false
                buttonType: "center"
                text:"Never"
                onClicked: {
                    root.visible=false
                    root.parent.visible=false
                }
            }
        }

    }
}

