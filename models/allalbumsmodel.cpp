#include "allalbumsmodel.h"
#include "mediafiltermodel.h"

AllAlbumsModel::AllAlbumsModel(const MediaIndexer *indexer, AllTracksModel *allTracksModel)
    : m_allTracksModel(allTracksModel)
{
    connect(indexer, &MediaIndexer::trackAdded, this, &AllAlbumsModel::newTrack);

    m_roleNames.insert(AllAlbumsModel::ArtistRole, "artist");
    m_roleNames.insert(AllAlbumsModel::AlbumRole, "album");
    m_roleNames.insert(AllAlbumsModel::ArtUrlRole, "artUrl");
    m_roleNames.insert(AllAlbumsModel::TracksModelRole, "tracks");
}

void AllAlbumsModel::newTrack(const QVariantMap &track)
{
    const QString artist = track.value(QStringLiteral("artist")).toString();
    const QString album = track.value(QStringLiteral("album")).toString();
    const QString path = track.value(QStringLiteral("path")).toString();

    // the key of our data structure
    const QString artistalbum = artist + album;

    const int i = getIndex(artistalbum);

    if (i == -1) {
        beginInsertAt(artistalbum);
        m_tracksModels.insert(artistalbum, std::make_shared<MediaFilterModel>(this, m_allTracksModel, QStringList(path)));
        m_data.insert(artistalbum, (QStringList() << artist << album));
        endInsert();
        emit albumAdded(artist, album);
    }
    else {
        m_tracksModels.value(artistalbum)->addItem(path);
        QModelIndex index = createIndex(i, 0);
        emit dataChanged(index, index);
    }
}

QVariant AllAlbumsModel::data(const QModelIndex &index, int role) const
{
    const QString key = getKey(index.row());

    switch (role) {
    case Qt::DisplayRole:
    case AllAlbumsModel::AlbumRole:
        return m_data.value(key).value(1);
    case AllAlbumsModel::ArtistRole:
        return m_data.value(key).value(0);
    case AllAlbumsModel::ArtUrlRole:
        {
            MediaFilterModel *model = m_tracksModels.value(key).get();
            if (model->rowCount(QModelIndex())) {
                QModelIndex index = model->index(0, 0, QModelIndex());
                return model->data(index, AllTracksModel::ArtUrlRole);
            }
            else {
                return QVariant();
            }
        }
    case AllAlbumsModel::TracksModelRole:
        return QVariant::fromValue(m_tracksModels.value(key).get());
    default:
        return QVariant();
    }
}
