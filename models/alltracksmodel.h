#ifndef ALLTRACKSMODEL_H_
#define ALLTRACKSMODEL_H_

#include "mediamodelbase.h"
#include "mediaindexer.h"

class AllTracksModel : public MediaModelBase
{
    Q_OBJECT

public:
    enum roles {
        TitleRole = Qt::UserRole,
        PathRole,
        ArtistRole,
        AlbumRole,
        ArtUrlRole
    };

    AllTracksModel(const MediaIndexer *);

    QHash<int, QByteArray> roleNames() const;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

private:
    void newTrack(const QVariantMap &track);

    QHash<int, QByteArray> m_roleNames;
    QHash<QString, QStringList> m_data;
};

#endif // ALLTRACKSMODEL_H_
