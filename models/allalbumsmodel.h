#ifndef ALLALBUMSMODEL_H_
#define ALLALBUMSMODEL_H_

#include "mediamodelbase.h"
#include "alltracksmodel.h"
#include "mediaindexer.h"
#include <memory>

class AllAlbumsModel : public MediaModelBase
{
    Q_OBJECT

public:
    enum roles {
        ArtistRole = Qt::UserRole,
        AlbumRole,
        ArtUrlRole,
        TracksModelRole
    };

    AllAlbumsModel(const MediaIndexer *indexer, AllTracksModel *tracksModel);

    QHash<int, QByteArray> roleNames() const {
        return m_roleNames;
    }

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

signals:
    void albumAdded(const QString&, const QString &);

private:
    void newTrack(const QVariantMap &track);

    AllTracksModel *m_allTracksModel;
    QHash<QString, std::shared_ptr<MediaFilterModel>> m_tracksModels;
    QHash<QString, QStringList> m_data;
    QHash<int, QByteArray> m_roleNames;
};

#endif // ALLALBUMSMODEL_H_
