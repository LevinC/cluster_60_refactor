#include "contacts.h"

Contacts::Contacts(QObject *parent)
  : QAbstractListModel(parent)
{
    m_roleNames.insert(Contacts::NameRole, "name");
    m_roleNames.insert(Contacts::MobileRole, "mobile");
    m_roleNames.insert(Contacts::HomeRole, "home");
    m_roleNames.insert(Contacts::WorkRole, "work");
    m_roleNames.insert(Contacts::HomeAddressRole, "homeadress");
    m_roleNames.insert(Contacts::WorkAddressRole, "workadress");
    m_roleNames.insert(Contacts::UrlRole, "url");
    m_roleNames.insert(Contacts::PhotoRole, "photo");
    m_roleNames.insert(Contacts::FavoriteRole, "favorite");
    m_roleNames.insert(Contacts::CallTypeRole, "type");
    m_roleNames.insert(Contacts::DateRole, "date");
}

Contacts::~Contacts()
{

}

void Contacts::setJsonData(const QVariantList &data)
{
    beginResetModel();
    m_data = data;
    for (auto i = m_data.begin(); i < m_data.end(); ++i) {
        QVariantMap map = i->toMap();
        map.insert(QStringLiteral("photo"),
                QString(QStringLiteral("image://contactimg/%1/%2"))
                    .arg(map.value(QStringLiteral("name")).toString())
                    .arg(map.value(QStringLiteral("sex")).toString()));
        i->setValue(map);
    }
    endResetModel();
}

QHash<int, QByteArray> Contacts::roleNames() const
{
    return m_roleNames;
}

QVariant Contacts::data(const QModelIndex &index, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return m_data.at(index.row()).toMap().value(QStringLiteral("name"));
    default:
        return m_data.at(index.row()).toMap().value(m_roleNames.value(role));
    }
}

void Contacts::setFavorite(int index, bool favorite)
{
    m_data.at(index).toMap().insert(QLatin1String("favorite"), favorite);
    QModelIndex index_ = this->index(index, 0, QModelIndex());
    dataChanged(index_, index_, QVector<int>(Contacts::FavoriteRole));
}
