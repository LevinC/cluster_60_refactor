#include "tagimgprovider.h"

TagImageProvider::TagImageProvider(const MediaIndexer *indexer)
    : QQuickImageProvider(QQuickImageProvider::Image),
    m_mediaIndexer(indexer)
{
}

TagImageProvider::~TagImageProvider()
{
}

QImage TagImageProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize)
{
    Q_UNUSED(requestedSize);
    QImage cover;

    if (m_refs.contains(id)) {
        cover = m_refs.value(id);
    }
    else {
        QVariantMap tags = m_mediaIndexer->readTags(id, true);
        cover = tags.value("cover").value<QImage>();
        m_refs[id] = cover;
    }

    size->setWidth(cover.width());
    size->setHeight(cover.height());

    return cover;
}

