#ifndef MEDIAINDEXER_H_
#define MEDIAINDEXER_H_

#include <QtCore/QThread>
#include <QtCore/QDir>
#include <QtCore/QVariantMap>

class MediaIndexer : public QThread
{
    Q_OBJECT

public:
    MediaIndexer(const QString &base_dir);
    virtual void run();

    virtual QVariantMap readTags(const QString &path, bool readCover=false) const;

signals:
    void trackAdded(const QVariantMap &song);
    void indexingDone();

private:
    QDir m_base;
    void recurseDir(const QDir &dir, bool topLevel = false);
    QString checkOtherExtension(QString file, QString suffix);
};


// simpler version that reads dummy data from a json file in assets
class MediaIndexerJson : public MediaIndexer
{
    Q_OBJECT

public:
    MediaIndexerJson() : MediaIndexer("") {}
    void run();
    QVariantMap readTags(const QString &path, bool readCover=false) const;

private:
    QMap<QString, QVariantMap> m_data;
};

#endif // MEDIAINDEXER_H_
