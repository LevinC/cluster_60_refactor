#include "alltracksmodel.h"

AllTracksModel::AllTracksModel(const MediaIndexer *indexer)
{
    connect(indexer, &MediaIndexer::trackAdded, this, &AllTracksModel::newTrack);

    m_roleNames.insert(AllTracksModel::TitleRole, "title");
    m_roleNames.insert(AllTracksModel::PathRole, "path");
    m_roleNames.insert(AllTracksModel::ArtistRole, "artist");
    m_roleNames.insert(AllTracksModel::AlbumRole, "album");
    m_roleNames.insert(AllTracksModel::ArtUrlRole, "artUrl");
}

QHash<int, QByteArray> AllTracksModel::roleNames() const
{
    return m_roleNames;
}

void AllTracksModel::newTrack(const QVariantMap &track)
{
    const QString artist = track.value(QStringLiteral("artist")).toString();
    const QString album = track.value(QStringLiteral("album")).toString();
    const QString title = track.value(QStringLiteral("title")).toString();
    const QString path = track.value(QStringLiteral("path")).toString();
    //const QString music = track.value(QStringLiteral("musicFile")).toString();

    beginInsertAt(path);
    m_data.insert(path, (QStringList() << title << artist << album << (QStringLiteral("image://tagimg/") + path)) << path);
    //m_data.insert(path, (QStringList() << title << artist << album << path) << music);
    endInsert();
}

QVariant AllTracksModel::data(const QModelIndex &index, int role) const
{
    QStringList song = m_data.value(getKey(index.row()));

    switch (role) {
    case Qt::DisplayRole:
    case AllTracksModel::TitleRole:
        return song.value(0);
    case AllTracksModel::ArtistRole:
        return song.value(1);
    case AllTracksModel::AlbumRole:
        return song.value(2);
    case AllTracksModel::ArtUrlRole:
        return song.value(3);
    case AllTracksModel::PathRole:
        return song.value(4);
    default:
        return QVariant();
    }
}
