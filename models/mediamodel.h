#ifndef __MEDIAMODEL_H__
#define __MEDIAMODEL_H__

#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QSortFilterProxyModel>
#include <QtCore/QIdentityProxyModel>

#include "mediaindexer.h"
#include "alltracksmodel.h"
#include "allalbumsmodel.h"
#include "allartistsmodel.h"

class MediaModel : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString mediaSource READ getMediaSource WRITE setMediaSource NOTIFY mediaSourceChanged)
    Q_PROPERTY(QStringList recentSources READ getRecentSources NOTIFY mediaSourceChanged)

    Q_PROPERTY(QObject *allTracks READ getAllTracks CONSTANT)
    Q_PROPERTY(QObject *allAlbums READ getAllAlbums CONSTANT)
    Q_PROPERTY(QObject *allArtists READ getAllArtists CONSTANT)

    Q_PROPERTY(QObject *playlist READ getPlaylist WRITE setPlayList NOTIFY playlistChanged)
    Q_PROPERTY(QString listTitle READ getListTitle WRITE setListTitle NOTIFY listTitleChanged)

    Q_PROPERTY(int index READ getIndex WRITE setIndex NOTIFY dataChanged)

    Q_PROPERTY(bool shuffle READ getShuffle WRITE setShuffle NOTIFY shuffleChanged)

    Q_PROPERTY(QString path READ getPath NOTIFY dataChanged)
    Q_PROPERTY(QString artist READ getArtist NOTIFY dataChanged)
    Q_PROPERTY(QString title READ getTitle NOTIFY dataChanged)
    Q_PROPERTY(QString album READ getAlbum NOTIFY dataChanged)
    Q_PROPERTY(QString artUrl READ getArtUrl NOTIFY dataChanged)

    Q_PROPERTY(QString searchString READ getSearchString WRITE setSearchString NOTIFY searchStringChanged)
    Q_PROPERTY(QObject *tracksSearch READ getTracksSearch NOTIFY searchStringChanged)
    Q_PROPERTY(QObject *albumsSearch READ getAlbumsSearch CONSTANT)
    Q_PROPERTY(QObject *artistsSearch READ getArtistsSearch CONSTANT)

public:
    MediaModel(const MediaIndexer &indexer);

    QString getMediaSource() const {
        return m_mediaSource;
    }

    QStringList getRecentSources() const {
        return m_recentSources;
    }

    QObject *getAllAlbums() {
        return &m_allAlbumsSorted;
    }

    QObject *getAllTracks() {
        return &m_allTracksSorted;
    }

    QObject *getAllArtists() {
        return &m_allArtistsSorted;
    }

    QObject *getPlaylist() {
        return m_playlist;
    }

    int getIndex() const {
        return m_index;
    }

    QString getListTitle() {
        return m_listTitle;
    }

    bool getShuffle() const {
        return m_shuffle;
    }

    QString getPath() const {
        return getData(AllTracksModel::PathRole).toString();
    }

    QString getArtist() const {
        return getData(AllTracksModel::ArtistRole).toString();
    }

    QString getTitle() const {
        return getData(AllTracksModel::TitleRole).toString();
    }

    QString getAlbum() const {
        return getData(AllTracksModel::AlbumRole).toString();
    }

    QString getArtUrl() const {
        return getData(AllTracksModel::ArtUrlRole).toString();
    }

    QString getSearchString() const {
        return m_searchString;
    }

    QObject *getTracksSearch() {
        return m_tracksSearch;
    }

    QObject *getAlbumsSearch() {
        return &m_albumsSearch;
    }

    QObject *getArtistsSearch() {
        return &m_artistsSearch;
    }

    void setMediaSource(const QString &source);

    void setIndex(int index);

    void setListTitle(const QString &title) {
        m_listTitle = title;
        emit listTitleChanged();
    }

    void setPlayList(QObject *playlist);

    void setShuffle(bool shuffle);

    void setSearchString(const QString &string);

public slots:
    void next();
    void prev();

    // These are used by letter picker, they don't really
    // belong here as they are more generic.
    int getIndexByFirstChar(QObject *model, const QString &chr);
    QStringList getSegments(QObject *model);

signals:
    void dataChanged();
    void playlistChanged();
    void listTitleChanged();
    void searchStringChanged();
    void mediaSourceChanged();
    void shuffleChanged();

private:
    void rowsInserted(const QModelIndex &parent, int start, int end);
    void rowsRemoved(const QModelIndex &parent, int start, int end);
    QVariant getData(int role) const;
    void indexingDone();

    AllTracksModel m_allTracks;
    AllAlbumsModel m_allAlbums;
    AllArtistsModel m_allArtists;
    QSortFilterProxyModel *m_tracksSearch;
    QSortFilterProxyModel m_albumsSearch;
    QSortFilterProxyModel m_artistsSearch;
    QSortFilterProxyModel m_allTracksSorted;
    QSortFilterProxyModel m_allAlbumsSorted;
    QSortFilterProxyModel m_allArtistsSorted;
    QIdentityProxyModel *m_playlist;
    QRegExp m_matchNothingRegexp;
    int m_index;
    bool m_shuffle;
    QString m_mediaSource;
    QStringList m_recentSources;
    QString m_listTitle;
    QString m_searchString;
    QList<int> m_shuffleList;
    QList<int> m_shufflePlayed;
};

#endif // __MEDIAMODEL_H__
