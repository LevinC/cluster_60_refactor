#ifndef __MEDIAMODELBASE_H__
#define __MEDIAMODELBASE_H__

#include <QStringList>
#include <QtCore/QAbstractListModel>
#include <algorithm>

class MediaFilterModel;

class MediaModelBase : public QAbstractListModel
{
    Q_OBJECT

friend class MediaFilterModel;

public:
    int rowCount(const QModelIndex &parent = QModelIndex()) const {
        return parent.isValid() ? 0 : m_index.length();
    }

protected:
// functions to help associate string keys with list indexes
    void beginInsertAt(const QString &key) {
        auto bound = std::lower_bound(m_index.begin(), m_index.end(), key);
        const int i = bound == m_index.end() ? m_index.size() : m_index.indexOf(*bound);
        beginInsertRows(QModelIndex(), i, i);
        m_index.insert(i, key);
    }

    void endInsert() {
        endInsertRows();
    }

    QString getKey(int index) const {
        return m_index.value(index);
    }

    int getIndex(const QString &key) const {
        return m_index.indexOf(key);
    }

private:
    QStringList m_index;
};

#endif // __MEDIAMODELBASE_H__
