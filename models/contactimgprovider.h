#ifndef __CONTACTSIMGPROVIDER_H__
#define __CONTACTSIMGPROVIDER_H__

#include <QtQuick/QQuickImageProvider>

class ContactImageProvider : public QQuickImageProvider
{
public:
    ContactImageProvider();
    ~ContactImageProvider();

    QImage requestImage(const QString &id, QSize *size, const QSize &requestedSize);

private:
    QImage m_noimg;
    QMap<QString, QImage> m_refs;
    QList<QImage> m_females;
    QList<QImage> m_males;
};

#endif // __CONTACTSIMGPROVIDER_H__
