#include "allartistsmodel.h"
#include "allalbumsmodel.h"
#include "mediafiltermodel.h"

AllArtistsModel::AllArtistsModel(AllAlbumsModel *albumsModel)
    : m_allAlbumsModel(albumsModel)
{
    connect(m_allAlbumsModel, &AllAlbumsModel::albumAdded, this, &AllArtistsModel::newAlbum);

    m_roleNames.insert(AllArtistsModel::ArtistRole, "artist");
    m_roleNames.insert(AllArtistsModel::AlbumsModelRole, "albums");
}


void AllArtistsModel::newAlbum(const QString &artist, const QString &album)
{
    const int i = getIndex(artist);
    if (i == -1) {
        beginInsertAt(artist);
        m_albumModels.insert(artist, std::make_shared<MediaFilterModel>(this, m_allAlbumsModel, QStringList(artist + album)));
        m_data.insert(artist, QStringList(artist));
        endInsert();
    }
    else {
        m_albumModels.value(artist)->addItem(artist + album);
        QModelIndex index = createIndex(i, 0);
        emit dataChanged(index, index);
    }
}


QVariant AllArtistsModel::data(const QModelIndex &index, int role) const
{
    switch(role) {
    case Qt::DisplayRole:
    case AllArtistsModel::ArtistRole:
        return m_data.value(getKey(index.row())).value(0);
    case AllArtistsModel::AlbumsModelRole:
        return QVariant::fromValue(m_albumModels.value(getKey(index.row())).get());
    default:
        return QVariant();
    }
}
