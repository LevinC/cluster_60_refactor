#ifndef __MEDIAFILERMODEL_H__
#define __MEDIAFILERMODEL_H__

#include <QtCore/QAbstractProxyModel>
#include <QtCore/QVariantList>
#include <algorithm>
#include "mediamodelbase.h"


// The purpose of MediaFilterModels is to share data between all tracks
// and album tracks, and all albums and artist's albums

class MediaFilterModel : public QAbstractProxyModel
{
    Q_OBJECT

public:
    MediaFilterModel(QObject *parent, MediaModelBase *source, QStringList items)
        : QAbstractProxyModel(parent) {
        setSourceModel(source);
        m_items = items;
    }

    void setItems(QStringList items) {
        beginResetModel();
        m_items = items;
        endResetModel();
    }

    void addItem(QString item) {
        auto bound = std::lower_bound(m_items.begin(), m_items.end(), item);
        int i = bound == m_items.end() ? m_items.size() : m_items.indexOf(*bound);
        beginInsertRows(QModelIndex(), i, i);
        m_items.insert(bound, item);
        endInsertRows();
    }

    QModelIndex index(int row, int column, const QModelIndex &parent) const {
        if (!parent.isValid())
            return createIndex(row, column);
        else
            return QModelIndex();
    }

    QModelIndex mapToSource(const QModelIndex &index) const {
        if (!index.isValid())
            return QModelIndex();
        else {
            const int i = qobject_cast<MediaModelBase*>(sourceModel())->m_index.indexOf( m_items.value( index.row() ) );
            return (i == -1) ? QModelIndex() : sourceModel()->index(i, 0, QModelIndex());
        }
    }

    QModelIndex mapFromSource(const QModelIndex &index) const {
        if (!index.isValid())
            return QModelIndex();
        else {
            const int i = m_items.indexOf( qobject_cast<MediaModelBase*>(sourceModel())->m_index.value( index.row() ) );
            return (i == -1) ? QModelIndex() : this->index(i, 0, QModelIndex());
        }
    }

    int rowCount(const QModelIndex &parent) const {
        return parent.isValid() ? 0 : m_items.length();
    }

    int columnCount(const QModelIndex &parent) const {
        return parent.isValid() ? 0 : 1;
    }

    QModelIndex parent(const QModelIndex &index) const {
        Q_UNUSED(index);
        return QModelIndex();
    }


private:
    QStringList m_items;
};

#endif // __MEDIAFILERMODEL_H__
