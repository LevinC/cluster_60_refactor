#ifndef __CONTACSMODEL_H__
#define __CONTACSMODEL_H__

#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QSortFilterProxyModel>

#include "contacts.h"

class QRegExp;

class ContactsModel : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QObject *contactList READ getContacts CONSTANT)
    Q_PROPERTY(QObject *callList READ getHistory CONSTANT)
    Q_PROPERTY(QObject *favList READ getFavourites CONSTANT)
    Q_PROPERTY(QObject *searchList READ getContactsSearch CONSTANT)
    Q_PROPERTY(QObject *keypadSearchList READ getContactsKeypadSearch CONSTANT)
    Q_PROPERTY(QString searchString READ getSearchString WRITE setSearchString NOTIFY searchStringChanged)
    Q_PROPERTY(QString searchKeys READ getSearchKeys WRITE setSearchKeys NOTIFY searchKeysChanged)

public:
    ContactsModel(QObject *parent = 0);
    ~ContactsModel();

    QObject *getContacts() {
        return &m_contactsSorted;
    }

    QObject *getHistory() {
        return &m_callList;
    }

    QObject *getFavourites() {
        return &m_favList;
    }

    QObject *getContactsSearch() {
        return &m_contactsSearch;
    }

    QObject *getContactsKeypadSearch() {
        return &m_contactsKeypadSearch;
    }

    QString getSearchString() const {
        return m_searchString;
    }

    QString getSearchKeys() const {
        return m_searchKeys;
    }

    void setSearchString(const QString &string);

    void setSearchKeys(const QString &keys);

    Q_INVOKABLE QVariantMap getContactFromNumber(QString number) const;

    Q_INVOKABLE void setFavorite(int index, bool favorite) {
        m_contacts.setFavorite(index, favorite);
    }

signals:
    void searchStringChanged();
    void searchKeysChanged();

private:
    QVariantList m_data;
    Contacts m_contacts;
    Contacts m_callList;
    QSortFilterProxyModel m_contactsSorted;
    QSortFilterProxyModel m_favList;
    QSortFilterProxyModel m_contactsSearch;
    QSortFilterProxyModel m_contactsKeypadSearch;
    QString m_searchString;
    QString m_searchKeys;
    QRegExp m_matchNothingRegexp;
};

#endif // __CONTACSMODEL_H__
