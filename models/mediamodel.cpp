#include "mediamodel.h"

MediaModel::MediaModel(const MediaIndexer &indexer)
    : m_allTracks(&indexer),
      m_allAlbums(&indexer, &m_allTracks),
      m_allArtists(&m_allAlbums),
      m_tracksSearch(new QSortFilterProxyModel(this)),
      m_playlist(nullptr),
      m_matchNothingRegexp(QStringLiteral("$^")),
      m_index(-1),
      m_shuffle(false),
      m_mediaSource(QStringLiteral("iPod")),
      m_recentSources(QStringList() << QStringLiteral("USB") << QStringLiteral("CD") << QStringLiteral("DLNA")),
      m_listTitle(QStringLiteral("All Tracks"))
{
    connect(&indexer, &MediaIndexer::indexingDone, this, &MediaModel::indexingDone);

    m_tracksSearch->setFilterRole(AllTracksModel::TitleRole);
    m_tracksSearch->setFilterRegExp(m_matchNothingRegexp);
    m_tracksSearch->setSourceModel(&m_allTracks);

    m_albumsSearch.setFilterRole(AllAlbumsModel::AlbumRole);
    m_albumsSearch.setFilterRegExp(m_matchNothingRegexp);
    m_albumsSearch.setSourceModel(&m_allAlbums);

    m_artistsSearch.setFilterRole(AllArtistsModel::ArtistRole);
    m_artistsSearch.setFilterRegExp(m_matchNothingRegexp);
    m_artistsSearch.setSourceModel(&m_allArtists);

    m_allTracksSorted.setSourceModel(&m_allTracks);
    m_allTracksSorted.setSortRole(AllTracksModel::TitleRole);
    m_allTracksSorted.setDynamicSortFilter(true);
    m_allTracksSorted.sort(0);

    m_allAlbumsSorted.setSourceModel(&m_allAlbums);
    m_allAlbumsSorted.setSortRole(AllAlbumsModel::AlbumRole);
    m_allAlbumsSorted.setDynamicSortFilter(true);
    m_allAlbumsSorted.sort(0);

    m_allArtistsSorted.setSourceModel(&m_allArtists);
    m_allArtistsSorted.setSortRole(AllArtistsModel::ArtistRole);
    m_allArtistsSorted.setDynamicSortFilter(true);
    m_allArtistsSorted.sort(0);

    setPlayList(&m_allTracksSorted);
}

void MediaModel::setMediaSource(const QString &source)
{
    int i = m_recentSources.indexOf(source);
    if (i != -1) {
        m_recentSources.removeAt(i);
    }
    m_recentSources.prepend(m_mediaSource);
    m_mediaSource = source;
    if (m_allAlbums.rowCount()) {
        // set a random playlist to simulate media source change
        setPlayList(m_allAlbums.data(
                    m_allAlbums.index(rand() % m_allAlbums.rowCount()),
                    AllAlbumsModel::TracksModelRole).value<QObject*>());
    }
    emit mediaSourceChanged();
}

void MediaModel::setIndex(int index)
{
    if (index != m_index) {
        int rowCount = m_playlist->rowCount();
        if (index >=0 && index < rowCount) {
            m_index = index;
        }
        else if (index == rowCount && rowCount != 0) {
            // wrap around
            m_index = 0;
        }
        else if (index == -1 && rowCount) {
            // wrap around
            m_index = rowCount -1;
        } else if (index == -1) {
            m_index = -1;
        }
    }
    emit dataChanged();
}

void MediaModel::setPlayList(QObject *playlist)
{
    auto model = qobject_cast<QAbstractItemModel*>(playlist);
    if (model) {
        if (m_playlist)
            m_playlist->deleteLater();
        m_playlist = new QIdentityProxyModel(this);
        m_playlist->setSourceModel(model);

        emit playlistChanged();

        if (m_playlist->rowCount() > 0) {
            if (m_shuffle) {
                setIndex(rand() % (m_playlist->rowCount()));
            } else {
                setIndex(0);
            }
        } else {
            setIndex(-1);
        }

        if (m_shuffle) {
            m_shuffleList = QList<int>();
            for (int i = 0; i < m_playlist->rowCount(); ++i) {
                if (i != m_index) {
                    m_shuffleList << i;
                }
            }
            std::random_shuffle(m_shuffleList.begin(), m_shuffleList.end());
            m_shufflePlayed = QList<int>();
        }
        connect(m_playlist, &QAbstractItemModel::rowsRemoved, this, &MediaModel::rowsRemoved);
        connect(m_playlist, &QAbstractItemModel::rowsInserted, this, &MediaModel::rowsInserted);
    }
}

void MediaModel::setShuffle(bool shuffle)
{
    if (shuffle != m_shuffle) {
        if (shuffle) {
            m_shuffleList = QList<int>();
            for (int i = 0; i < m_playlist->rowCount(); ++i) {
                if (i != m_index) {
                    m_shuffleList << i;
                }
            }
            std::random_shuffle(m_shuffleList.begin(), m_shuffleList.end());
            m_shufflePlayed = QList<int>();
        }
        else {
            m_shuffleList = QList<int>();
            m_shufflePlayed = QList<int>();
        }
        m_shuffle = shuffle;
        emit shuffleChanged();
    }
}

void MediaModel::setSearchString(const QString &string)
{
    if (m_searchString != string) {
        m_searchString = string;
        if (m_playlist && m_playlist->sourceModel() == m_tracksSearch) {
            m_tracksSearch->setParent(m_playlist); // current search will be deleted with the current playlist
            m_tracksSearch = new QSortFilterProxyModel(this);
            m_tracksSearch->setFilterRole(AllTracksModel::TitleRole);
            m_tracksSearch->setSourceModel(&m_allTracks);
        }
        if (!string.isEmpty()) {
            QRegExp regexp(QStringLiteral("^") + string, Qt::CaseInsensitive);
            m_tracksSearch->setFilterRegExp(regexp);
            m_albumsSearch.setFilterRegExp(regexp);
            m_artistsSearch.setFilterRegExp(regexp);
        }
        else {
            m_tracksSearch->setFilterRegExp(m_matchNothingRegexp);
            m_albumsSearch.setFilterRegExp(m_matchNothingRegexp);
            m_artistsSearch.setFilterRegExp(m_matchNothingRegexp);
        }
        emit searchStringChanged();
    }
}

void MediaModel::next()
{
    if (m_shuffle) {
        if (m_shuffleList.size()) {
            m_shufflePlayed.append(m_index);
            setIndex(m_shuffleList.takeFirst());
        }
    }
    else {
        setIndex(m_index+1);
    }
}

void MediaModel::prev()
{
    if (m_shuffle) {
        if (m_shufflePlayed.size()) {
            m_shuffleList.append(m_index);
            setIndex(m_shufflePlayed.takeFirst());
        }
    }
    else {
        setIndex(m_index-1);
    }
}

int MediaModel::getIndexByFirstChar(QObject *model, const QString &chr)
{
    auto model_ = qobject_cast<QAbstractItemModel*>(model);
    if (model_) {
        QSortFilterProxyModel proxy;
        proxy.setSourceModel(model_);
        proxy.setFilterRegExp(QRegExp(QStringLiteral("^") + chr, Qt::CaseInsensitive));
        if (proxy.rowCount()) {
            return proxy.mapToSource(proxy.index(0, 0, QModelIndex())).row();
        }
    }
    return -1;
}

QStringList MediaModel::getSegments(QObject *model)
{
    QStringList rval;
    QChar current;
    auto model_ = qobject_cast<QAbstractItemModel*>(model);
    if (model_) {
        QModelIndex index = model_->index(0, 0, QModelIndex());
        while (index.isValid()) {
            QString segment = model_->data(index).toString();
            if (segment.size()) {
                QChar chr = segment.at(0);
                if (current != chr) {
                    rval << chr;
                    current = chr;
                }
            }
            index = index.sibling(index.row()+1, 0);
        }
    }
    return rval;
}

void MediaModel::rowsInserted(const QModelIndex &parent, int start, int end)
{
    Q_ASSERT(!parent.isValid());

    if (m_index == -1) {
        setIndex(0);
    }
    else if (m_index < start) {
        // pass
    }
    else if (m_index >= start) {
        m_index += 1 + (end - start);
        emit dataChanged();
    }
}

void MediaModel::rowsRemoved(const QModelIndex &parent, int start, int end)
{
    if (m_index < start) {
        // pass
    } else if (m_index >= start && m_index <= end) {
        setIndex(-1);
    } else {
        m_index -= 1 + (start - end);
        emit dataChanged();
    }
}

QVariant MediaModel::getData(int role) const
{
    if (m_index == -1)
        return QVariant();
    else
        return m_playlist->data(m_playlist->index(m_index, 0, QModelIndex()), role);
}

void MediaModel::indexingDone()
{
    setIndex(0);
}
