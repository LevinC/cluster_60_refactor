#ifndef ALLARTISTSMSMODEL_H_
#define ALLARTISTSMSMODEL_H_

#include "mediamodelbase.h"
#include <memory>

class AllAlbumsModel;
class MediaFilterModel;

class AllArtistsModel : public MediaModelBase
{
    Q_OBJECT

public:
    AllArtistsModel(AllAlbumsModel *albumsModel);

    enum roles {
        ArtistRole = Qt::UserRole,
        AlbumsModelRole
    };

    QHash<int, QByteArray> roleNames() const {
        return m_roleNames;
    }

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

private:
    void newAlbum(const QString &artist,
            const QString &album);

    QHash<int, QByteArray> m_roleNames;
    AllAlbumsModel *m_allAlbumsModel;
    QHash<QString, QStringList> m_data;
    QHash<QString, std::shared_ptr<MediaFilterModel> > m_albumModels;
};

#endif // ALLARTISTSMSMODEL_H_
