#include <QtCore/QJsonDocument>
#include <QtCore/QJsonArray>
#include <QtCore/QDateTime>
#include <QtCore/QVariant>
#include <QtCore/QFile>

#include <algorithm>
#include <cstdlib>

#include "contactsmodel.h"

ContactsModel::ContactsModel(QObject *parent)
    : QObject(parent)
{
    QFile jsonContacts(":/modeldata/contactsdb.json");

    bool ok = jsonContacts.open(QFile::ReadOnly);
    Q_ASSERT( ok );
    m_data = QJsonDocument::fromJson(jsonContacts.readAll()).array().toVariantList();
    jsonContacts.close();

    QVariantList history = m_data.mid(0, 20);

    std::random_shuffle(history.begin(), history.end());
    QDateTime date = QDateTime::currentDateTime();

    for (auto i = history.begin(); i < history.end(); ++i) {
        QVariantMap m = i->toMap();
        int choice = rand() % 3;
        m.insert(QStringLiteral("type"),
                choice == 0 ? QStringLiteral("in") :
                choice == 1 ? QStringLiteral("out") :
                              QStringLiteral("missed"));
        m.insert(QStringLiteral("date"), date);
        date = date.addMSecs(-(rand() % (60*60*30*24*4) + 30));
        i->setValue(m);
    }

    m_callList.setJsonData(history);

    for (auto i = m_data.begin(); i < m_data.end(); ++i) {
        QVariantMap m = i->toMap();
        m.insert(QStringLiteral("favorite"),
                (rand()%10 == 1));
        i->setValue(m);
    }

//    std::sort(m_data.begin(), m_data.end(),
//            [](const QVariant &a, const QVariant &b) {
//                return 0 >= a.toMap().value("name").toString().localeAwareCompare(b.toMap().value("name").toString());
//            });

    m_contacts.setJsonData(m_data);

    m_contactsSorted.setSourceModel(&m_contacts);
    m_contactsSorted.setFilterRole(Contacts::NameRole);

    m_favList.setSourceModel(&m_contacts);
    m_favList.setFilterRole(Contacts::FavoriteRole);
    m_favList.setFilterFixedString("true");

    m_matchNothingRegexp.setPattern("$^");

    m_contactsSearch.setSourceModel(&m_contacts);
    m_contactsSearch.setFilterRole(Contacts::NameRole);
    m_contactsSearch.setFilterRegExp(m_matchNothingRegexp);

    m_contactsKeypadSearch.setSourceModel(&m_contacts);
    m_contactsKeypadSearch.setFilterRole(Contacts::NameRole);
    m_contactsKeypadSearch.setFilterRegExp(m_matchNothingRegexp);
}

ContactsModel::~ContactsModel() {

}

void ContactsModel::setSearchString(const QString &string)
{
    if (string != m_searchString) {
        m_searchString = string;
        if (!string.isEmpty()) {
            m_contactsSearch.setFilterRegExp(QRegExp('^' + string, Qt::CaseInsensitive));
        }
        else {
            m_contactsSearch.setFilterRegExp(m_matchNothingRegexp);
        }
        emit searchStringChanged();
    }
}

static QString keysTranslate(const QString &keys)
{
    QString rval;
    foreach(QChar k, keys) {
        if (k == '1') {
            rval.append(QStringLiteral(""));
        }
        else if (k == '2') {
            rval.append(QStringLiteral("[ABC]"));
        }
        else if (k == '3') {
            rval.append(QStringLiteral("[DEF]"));
        }
        else if (k == '4') {
            rval.append(QStringLiteral("[GHI]"));
        }
        else if (k == '5') {
            rval.append(QStringLiteral("[JKL]"));
        }
        else if (k == '6') {
            rval.append(QStringLiteral("[MNO]"));
        }
        else if (k == '7') {
            rval.append(QStringLiteral("[PQRS]"));
        }
        else if (k == '8') {
            rval.append(QStringLiteral("[TUV]"));
        }
        else if (k == '9') {
            rval.append(QStringLiteral("[WXYZ]"));
        }
        else if (k == '0') {
            rval.append(QStringLiteral(""));
        }
        else {
            rval.append(QStringLiteral("\x00"));
        }
    }
    return rval;
}

void ContactsModel::setSearchKeys(const QString &keys)
{
    if (keys != m_searchKeys) {
        m_searchKeys = keys;
        if (!keys.isEmpty()) {
            m_contactsKeypadSearch.setFilterRegExp(QRegExp("^" + keysTranslate(keys), Qt::CaseInsensitive));
        }
        else {
            m_contactsKeypadSearch.setFilterRegExp(m_matchNothingRegexp);
        }
        emit searchKeysChanged();
    }
}

QVariantMap ContactsModel::getContactFromNumber(QString number) const
{
    for (auto i = m_data.begin(); i < m_data.end(); ++i) {
        QVariantMap m = i->toMap();
        if (m.value(QStringLiteral("mobile")).toString() == number
                || m.value(QStringLiteral("work")).toString() == number
                || m.value(QStringLiteral("home")).toString() == number) {
            return m;
        }
    }
    return QVariantMap();
}

