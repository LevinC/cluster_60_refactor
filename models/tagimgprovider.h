#ifndef CONTACTSIMGPROVIDER_H_
#define CONTACTSIMGPROVIDER_H_

#include <QtQuick/QQuickImageProvider>
#include "mediaindexer.h"

class TagImageProvider : public QQuickImageProvider
{
public:
    TagImageProvider(const MediaIndexer *indexer);
    ~TagImageProvider();

    QImage requestImage(const QString &id, QSize *size, const QSize &requestedSize);

private:
    const MediaIndexer *m_mediaIndexer;
    QMap<QString, QImage> m_refs;
};

#endif // CONTACTSIMGPROVIDER_H_
