#include "mediaindexer.h"

#include <functional>

#include <QtCore/QDebug>
#include <QtCore/QFile>
#include <QtGui/QImage>

#include <QtCore/QJsonDocument>
#include <QtCore/QJsonArray>

namespace {
    // helper function to get correct function to decode string
    std::function<QString(const QByteArray&)> getStringReader(int encoding) {
        switch (encoding) {
        case 0:
            // Latin-1
            return [](const QByteArray &str) {
                int len = str.indexOf(QByteArray("\x00", 1)) == -1 ? str.size() : -1;
                return QString::fromLatin1(str.constData(), len);
            };
        case 1:
        case 2:
            // UTF-16 with or without BOM
            return [](const QByteArray &str) {
                int len = str.indexOf(QByteArray("\x00\x00", 2)) == -1 ? str.size()/2 : -1;
                return QString::fromUtf16(reinterpret_cast<const unsigned short*>(str.constData()), len); };
        case 3:
            // UTF-8
            return [](const QByteArray &str) {
                int len = str.indexOf(QByteArray("\x00", 1)) == -1 ? str.size() : -1;
                return QString::fromUtf8(str, len);
            };
        default:
            qWarning() << "unimplemented encoding used" << encoding;
            return [](const QByteArray &str) { return QString(str); };
        }
    }

    // helper class for tag parsing
    struct TagReader : public QFile
    {
        TagReader(const QString &fileName) : QFile(fileName) {}

        unsigned char readByte() {
            unsigned char rval;
            read(reinterpret_cast<char*>(&rval), 1);
            return rval;
        }

        quint64 readInt(const int size, const int mult=256) {
            quint64 n = readByte();
            for (int i = 1; i < size; ++i) {
                n = n * mult + readByte();
            }
            return n;
        }

        QString readTextFrame(const int frameSize) {
            int encoding = readByte();
            QByteArray rval = read(frameSize - 1);
            return getStringReader(encoding)(rval);
        }

        QString readNullTerminatedString(int encoding) {
            QByteArray array;
            if (encoding == 1 || encoding == 2) {
                // wide encoding
                char str[2];
                while (true) {
                    read(str, 2);
                    if (str[0] == '\x00'
                            && str[1] == '\x00')
                        break;
                    array.append(str[0]);
                    array.append(str[1]);
                }
            }
            else {
                char c;
                while ('\x00' != (c = readByte())) {
                    array.append(c);
                }
            }
            return getStringReader(encoding)(array);
        }
    };
}


QVariantMap MediaIndexer::readTags(const QString &path, bool readCover) const
{
    QVariantMap rval;

    rval["path"] = path;

    TagReader r(path);

    if (!r.open(QIODevice::ReadOnly)) {
        qWarning() << r.errorString() << path;
        return QVariantMap();
    }

    QByteArray header = r.read(3);
    if (header == "ID3") {
        int version = r.readInt(2);
        char flags = r.readByte();
        int size = r.readInt(4, 128);

        if (version < 0x300) {
            qWarning() << path << "Can't parse obsolete ID3v2 version";
            return QVariantMap();
        }

        if (flags & 0x80) {
            const int mult = version >= 0x400 ? 128 : 256;
            const int extendedSize = r.readInt(4, mult);
            const int extendedFlags = r.readInt(2);
            const int padding = r.readInt(4);
            r.seek(r.pos() + (extendedSize - 10));
        }

        while (r.pos() < size + 3) {
            const QByteArray frameId = r.read(4);
            const int frameSize = r.readInt(4);
            const int frameFlags = r.readInt(2);

            if ("TALB" == frameId) {
                rval["album"] = r.readTextFrame(frameSize);
            }
            else if ("TIT2" == frameId) {
                rval["title"] = r.readTextFrame(frameSize);
            }
            else if ("TPE1" == frameId) {
                rval["artist"] = r.readTextFrame(frameSize);
            }
            else if ("TRCK" == frameId) {
                rval["track"] = r.readTextFrame(frameSize);
            }
            else if ("TYER" == frameId) {
                rval["year"] = r.readTextFrame(frameSize);
            }
            else if ("TLEN" == frameId) {
                rval["length"] = r.readTextFrame(frameSize);
            }
            else if ("APIC" == frameId && readCover) {
                const int encoding = r.readByte();
                const QString mimeType = r.readNullTerminatedString(0);
                const int pictureType = r.readByte();
                const QString description = r.readNullTerminatedString(encoding);

                QImage img;
                if (img.load(&r, mimeType.toLatin1())) {
                    rval["cover"] = img;
                }
            }
            else {
                r.seek(r.pos() + frameSize);
            }
        }
    }
    else {
        return QVariantMap();
    }

    return rval;
}


MediaIndexer::MediaIndexer(const QString &base_dir)
    : m_base(base_dir)
{
}

QString MediaIndexer::checkOtherExtension(QString file, QString suffix)
{
    QFileInfo fileInfo(file);
    QString newFile = fileInfo.absolutePath();
    newFile += "/" + fileInfo.completeBaseName();
    newFile += "." + suffix;

    if (QFileInfo(newFile).isFile())
        return newFile;

    return file;
}

void MediaIndexer::recurseDir(const QDir &dir, bool topLevel) {
    foreach (const QString &fileOrDir, dir.entryList(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot)) {

        QString absFilePath = dir.absoluteFilePath(fileOrDir);

        if (QFileInfo(absFilePath).isDir()) {
            recurseDir( QDir( absFilePath ) );
        } else {
            QVariantMap track = readTags( absFilePath );

            if (!track.isEmpty()) {
                absFilePath = checkOtherExtension(absFilePath, "m4a");
                absFilePath = checkOtherExtension(absFilePath, "wav");
                track["musicFile"] = absFilePath;
                emit trackAdded( track );
            }
        }
    }
    if (topLevel)
        emit indexingDone();
}

void MediaIndexer::run() {
    recurseDir(m_base, true);
}


void MediaIndexerJson::run() {
    //QFile jsonMusic("modeldata/musicdb.json");
    QFile jsonMusic(":/modeldata/musicdb.json");
    jsonMusic.open(QFile::ReadOnly);
    QVariantList data = QJsonDocument::fromJson(jsonMusic.readAll()).array().toVariantList();
    jsonMusic.close();
    for (auto i = data.begin(); i < data.end(); ++i) {
        QVariantMap m = i->toMap();
        m_data.insert(m.value(QStringLiteral("path")).toString(), m);
        emit trackAdded(m);
    }
}

QVariantMap MediaIndexerJson::readTags(const QString &path, bool readCover) const
{
    QVariantMap rval = m_data.value(path);
    if (readCover) {
        const QString path = QStringLiteral(":/modeldata/albumcovers/");
        QImage img;
        if (img.load(path + rval.value("album").toString() + QStringLiteral(".png"))) {
            rval["cover"] = img;
        }
    }
    return rval;
}
