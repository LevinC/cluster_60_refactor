#include "contactimgprovider.h"
#include <QDir>
#include <cstdlib>

ContactImageProvider::ContactImageProvider()
: QQuickImageProvider(QQuickImageProvider::Image),
m_noimg(QImage(250, 250, QImage::Format_ARGB4444_Premultiplied))
{
    QDir photos(":/qml/ihu/resources/images/contact_photos/");

    const QString female_filter = QStringLiteral("f*.png");
    const QString male_filter = QStringLiteral("m*.png");

    foreach(const QString &file, photos.entryList(QStringList(female_filter))) {
        m_females.append(QImage(photos.filePath(file)));
    }

    foreach(const QString &file, photos.entryList(QStringList(male_filter))) {
        m_males.append(QImage(photos.filePath(file)));
    }

    m_noimg.fill(Qt::transparent);
}

ContactImageProvider::~ContactImageProvider()
{
}

QImage ContactImageProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize)
{
    Q_UNUSED(requestedSize);

    QImage img;

    if (m_refs.contains(id)) {
        img = m_refs.value(id);
    }
    else {
        QString gender = id.split('/')[1];

        if (QStringLiteral("female") == gender && m_females.length()) {
            int choice = rand() % m_females.length();
            img = m_females[choice];
        }
        else if (QStringLiteral("male") == gender && m_males.length()) {
            int choice = rand() % m_males.length();
            img = m_males[choice];
        }
        else {
            img = m_noimg;
        }

        m_refs.insert(id, img);

        size->setHeight(img.height());
        size->setWidth(img.width());
    }

    return img;
}
