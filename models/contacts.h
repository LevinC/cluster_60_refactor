#ifndef CONTACTS_H__
#define CONTACTS_H__

#include <QtCore/QObject>
#include <QtCore/QAbstractListModel>
#include <QtCore/QHash>
#include <QtCore/QModelIndex>

class ContactsModel;

class Contacts : public QAbstractListModel
{
    Q_OBJECT

    friend class ContactsModel;

public:
    enum roles {
        NameRole = Qt::UserRole,
        MobileRole,
        HomeRole,
        WorkRole,
        HomeAddressRole,
        WorkAddressRole,
        UrlRole,
        PhotoRole,
        FavoriteRole,
        CallTypeRole,
        DateRole
    };

    Contacts(QObject *parent = 0);
    ~Contacts();

    int rowCount(const QModelIndex &parent) const {
        return !parent.isValid() ? m_data.size() : 0;
    }

    QHash<int, QByteArray> roleNames() const;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

private:
    void setFavorite(int index, bool favorite);

    void setJsonData(const QVariantList &data);

    QVariantList m_data;
    QHash<int, QByteArray> m_roleNames;
};

#endif // CONTACTS_H__
