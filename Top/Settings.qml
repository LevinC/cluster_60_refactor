import QtQuick 2.0

Rectangle {
    id:root
    width: globalStyle.screenWidth
    height: globalStyle.screenHeight
    color: "black"

    MouseArea{
        anchors.fill: parent
    }

    Rectangle{
        width:100
        height:100
        anchors.bottom:root.bottom
        anchors.right:root.right

        MouseArea{
            anchors.fill: parent
            onClicked: {
                root.visible = false
            }
        }
    }
}

