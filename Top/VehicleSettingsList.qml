import QtQuick 2.0

import "../"
import "../Common/"
import "../VehicleSettings/"

Item {

    id:root
    signal changeVehicleObj
    property int selectedInd
    property SpaStyle globalStyle: SpaStyle{}
    width: globalStyle.screenWidth
    height: globalStyle.screenHeight

    Rectangle{
        anchors.fill:parent
        opacity: .95
        color:"black"
    }

    MouseArea{
        anchors.fill: parent
    }

    Column{
        width:500
        height:700
        anchors.centerIn: parent
        Rectangle{
            height:100
            width:parent.width
            color:"black"
            SpaText{
                horizontalAlignment: Text.AlignHCenter
                anchors.fill: parent
                text: "Select function"
                textStyle:homeScreenHeader
            }
        }
        ListView{
            width:parent.width
            height:500
            anchors.centerIn: parent
            id:listView
            model:VehicleSettingsModel{}
            delegate: dele
        }
    }

    Component{
        id:dele
        Spa9FlatButton{
            height:80
            width:listView.width
            onClicked: {
                selectedInd = index
                changeVehicleObj()
                coreScreen.onTopOfAll.sourceComponent = undefined
            }
            Row{
                anchors.fill: parent
                spacing: 10
                Image {
                    id:im
                    width:80
                    height:80
                    fillMode: Image.PreserveAspectFit
                    source: globalStyle.imageUrl + image
                }
                SpaText{
                    anchors.left:im.right
                    verticalAlignment: Text.AlignVCenter
                    height:80
                    width:80-height
                    text: title
                    textStyle:listHeader1
                }
            }
        }
    }

    Spa9FlatButton{
        width:200
        height:80
        text:"Cancel"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 80
        onClicked: {
            coreScreen.onTopOfAll.sourceComponent = undefined
        }
    }
}
