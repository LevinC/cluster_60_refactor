import QtQuick 2.0

import "../"
import "../VehicleSettings/"
import "../Common/"

Item {
    id:root
    property bool open
    property real desiredOpacity: 0.8
    property int animationDuration
    property int selectedListObj


    function dragStopped(speed) {
        if (speed > 0){
            openMenu()
        }else{
            closeMenu()
        }
    }

    function openClose(){
        if (open){
            closeMenu()
        }else{
            openMenu()
        }
    }

    function closeMenu(){
        animationDuration = (100 + Math.abs(dragger.drag.target.y - dragger.drag.minimumY)*.5)
        dragger.drag.target.y = dragger.drag.minimumY;
        open=false
    }

    function openMenu(){
        animationDuration = (100 + Math.abs(dragger.drag.target.y - dragger.drag.maximumY)*.5)
        dragger.drag.target.y = dragger.drag.maximumY;
        open = true;
    }

    Rectangle {
        id: dimmedbg
        anchors.fill: root
        color: "black"
        opacity: (dragger.drag.target.y - dragger.drag.minimumY) /(dragger.drag.maximumY-dragger.drag.minimumY) * desiredOpacity
    }


    MouseArea {
        anchors.fill:root
        enabled:open

        onClicked: {
            closeMenu()
        }
    }

    Item{
        id:draggableItem
        y:-contentArea.height
        width:root.width
        height:globalStyle.topPaneListHeight + topButtons.height + handle.height // + vehicleSettingsFav.height

        Behavior on y {  CubicAnimation {duration:animationDuration}}

        Image {
            anchors.bottom: handle.bottom
            source: globalStyle.imageUrl + "/SettingsPane/img_hmc_stngs_shadow" + globalStyle.componentSkinUrl + ".png"
            opacity: dimmedbg.opacity
        }

        Rectangle{
            id:contentArea
            width:root.width
            height:draggableItem.height - handle.height
            color:"black"

            Column{
                TopPaneButtons{
                    id:topButtons
                }

                ListView{
                    width: contentArea.width
                    clip:true
                    height: globalStyle.topPaneListHeight
                    model:TopPaneMenuContent{}
                }

                Row{
                    height:vehicleSettingsFav.height
                    visible:false

                    Item{
                        width:vehicleSettingsFav.width * 5/6
                        height:parent.height
                        Row{
                            id:vehicleSettingsFav

                            property bool editMode: false
                            height:80
                            width:contentArea.width
                            Repeater{
                                model: listModel
                                delegate:dele
                            }
                        }

                        Component{
                            id:dele
                            Spa9FlatButton{
                                iconSource: globalStyle.imageUrl + image
                                height:vehicleSettingsFav.height
                                width:contentArea.width/6
                                bgColor: vehicleSettingsFav.editMode ? "gray" : "transparent"
                                onClicked: {
                                    if (vehicleSettingsFav.editMode){
                                        selectedListObj = index
                                        console.log(selectedListObj)
                                        coreScreen.onTopOfAll.sourceComponent = vehicleSettingsList

                                    }
                                }
                            }
                        }

                        ListModel{
                            id: listModel
                            Component.onCompleted: {
                                listModel.append(vehicleModel.get(0))
                                listModel.append(vehicleModel.get(1))
                                listModel.append(vehicleModel.get(2))

                                listModel.append(vehicleModel.get(3))
                                listModel.append(vehicleModel.get(4))

                            }

                        }


                        VehicleSettingsModel{
                            id: vehicleModel
                        }

                    }
                    Spa9FlatButton{
                        height:vehicleSettingsFav.height
                        width:contentArea.width/6
                        text: vehicleSettingsFav.editMode? "done":"edit"
                        onClicked: {
                            vehicleSettingsFav.editMode = !vehicleSettingsFav.editMode
                        }
                    }

                }

            }

            Rectangle {
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0//vehicleSettingsFav.height
                height: 80
                width: globalStyle.screenWidth
                gradient: Gradient {
                    GradientStop { position: 0.0; color: "transparent" }
                    GradientStop { position: 1.0; color: globalStyle.skinUrl === "_S4" ? "#e3e3e3" : "#232323" }
                }
            }

        }

        Item{
            id:handle
            anchors.bottom: draggableItem.bottom
            width:300
            height:40
            anchors.horizontalCenter: draggableItem.horizontalCenter


            Image {
                anchors.horizontalCenter: handle.horizontalCenter
                source: globalStyle.imageUrl + "/SettingsPane/img_hmc_stngs_handle" + globalStyle.componentSkinUrl + ".png"
            }

            MouseArea{
                id:dragger
                anchors.fill: handle
                drag.axis: Drag.YAxis
                drag.target: draggableItem
                drag.minimumY: -contentArea.height
                drag.maximumY: 0

                onPressedChanged: {
                    if (pressed){
                        timer.start()
                    }else{
                        timer.stop()
                    }
                }

                Timer{
                    id:timer
                    interval: 5
                    repeat: true
                    property int speed:0
                    property int prevY:draggableItem.y

                    onTriggered: {
                        if (draggableItem.y - prevY!= speed && Math.abs(draggableItem.y - prevY) >= 1){
                            speed = draggableItem.y - prevY
                        }
                        prevY = draggableItem.y
                    }
                }
                onReleased: dragStopped(timer.speed)
                onClicked: openClose()
                onCanceled: dragStopped(timer.speed)
            }
        }
    }

    Component{
        id:vehicleSettingsList
        VehicleSettingsList{

            onChangeVehicleObj: {
                listModel.set(selectedListObj,vehicleModel.get(selectedInd))
            }

        }
    }
}

