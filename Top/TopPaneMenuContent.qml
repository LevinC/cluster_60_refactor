import QtQuick 2.0

import "../Common"

VisualItemModel{
    id:model

    TopPaneMenuItem{
        headerText: "Activate cruise control"
        subHeaderText: "(melcomap) Deactivate Adaptive Cruise Control"
        imSource: "/Icons/img_vfp_shift_cruise"
    }

    TopPaneMenuItem{
        headerText: "DAB interruption"
        subHeaderText:"Alarm (night)"
        imSource:"/Icons/img_hmc_dab"
    }

    TopPaneMenuItem{
        headerText: "Tire Pressure Monitor"
        subHeaderText:"Calibration completed (BRIGHT)"
        imSource:"/Icons/img_vef_tpms"
    }

    TopPaneMenuItem{
        headerText:"System malfunction"
        subHeaderText:"Service required (No phones paired)"
        imSource:"/Icons/img_hmc_carinfo"
    }
}
