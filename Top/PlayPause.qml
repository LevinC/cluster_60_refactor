import QtQuick 2.0
import QtMultimedia 5.0


Rectangle {
    Image {

        visible:  musicPlayer.volume === 0.0 && phoneModel.count === 0
        source: globalStyle.imageUrl + "/Icons/img_hmc_volume_0" + globalStyle.skinUrl + ".png"
    }
    Image {
        visible: musicPlayer.volume !== 0.0 && phoneModel.count === 0
        source: musicPlayer.playbackState !== Audio.PlayingState ? globalStyle.imageUrl + "/Icons/img_hmc_media_pause" + globalStyle.skinUrl + ".png" : globalStyle.imageUrl + "/Icons/img_hmc_media_play" + globalStyle.skinUrl + ".png"
    }
    Image {
        visible: phoneModel.count !== 0
        source: globalStyle.imageUrl + "/Icons/img_hmc_media_phone" + globalStyle.skinUrl + ".png"
    }
}

