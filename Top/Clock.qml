/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import "../Common/"

// Clock, A digital clock.
Item {
    id: root

    width: childrenRect.width
    height: childrenRect.height

    property variant hours: "00"
    property variant minutes: "00"
    property variant seconds: "00"
    property string _ampm
    property bool _hour12: false

    function timeChanged() {
        var date = new Date(Date.now());
        if (_hour12) {
            var h = Qt.formatTime(date, 'hap').slice(0,-2);
            var m = Qt.formatTime(date, 'mm');
            var ap = Qt.formatTime(date, 'ap');
            if (_ampm !== ap)
                _ampm = ap;
        }
        else {
            var h = Qt.formatTime(date, 'hh');
            var m = Qt.formatTime(date, 'mm');
        }

        if (hours !== h)
            hours = h;

        if (minutes !== m)
            minutes = m;
    }

    Timer {
        interval: 1000; running: true; repeat: true;
        onTriggered: root.timeChanged()
    }

    Component.onCompleted: root.timeChanged()

    Row {
        id: layout
        spacing: 2
        Row {
            id: clockLayout
            SpaText {
                id: hoursText
                text: root.hours
                textStyle: listItem1
                font.pixelSize: 42
                Behavior on text {
                    SequentialAnimation {
                        NumberAnimation { target: hoursText; property: "opacity"; to: 0.1; duration: 200 }
                        PropertyAction {  }
                        NumberAnimation { target: hoursText; property: "opacity"; to: 1; duration: 200 }
                    }
                }
            }

            SpaText {
                text: ":"
                textStyle: listItem1
                font.pixelSize: 42
            }

            SpaText {
                id : minutesText
                text: root.minutes
                textStyle: listItem1
                font.pixelSize: 42
                Behavior on text {
                    SequentialAnimation {
                        NumberAnimation { target: minutesText; property: "opacity"; to: 0.1; duration: 200 }
                        PropertyAction {  }
                        NumberAnimation { target: minutesText; property: "opacity"; to: 1; duration: 200 }
                    }
                }
            }
        }
        Column {
            spacing: -6
            anchors.verticalCenter: clockLayout.verticalCenter

            SpaText {
                visible: _hour12
                textStyle: listItem1
                font.pixelSize: 18
                color: globalStyle.greyLvl3
                opacity: _ampm === 'am' ? 1.0 : 0.2
                Behavior on opacity {
                    NumberAnimation { duration: 200 }
                }
                text: "AM"
            }

            SpaText {
                id: pm
                visible: _hour12
                textStyle: listItem1
                font.pixelSize: 18
                color: globalStyle.greyLvl3
                opacity: _ampm === 'pm' ? 1.0 : 0.2
                Behavior on opacity {
                    NumberAnimation { duration: 200 }
                }
                text: "PM"
            }
        }
    }
}
