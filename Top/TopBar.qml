import QtQuick 2.0

Item {
    id:root
    width: globalStyle.screenWidth
    height: globalStyle.screenHeight

    property alias topPane:topPane

    TopLeftBar{
        width: root.width/3
        height: 82
    }

    PlayPause{
        y:12
        x: clock._hour12 ? 602 : 612
    }

    Clock{
        id: clock
        y: 4
        anchors.right: root.right
        anchors.rightMargin: 8
    }

    TopPane{
        id: topPane
        anchors.fill: root
    }
}

