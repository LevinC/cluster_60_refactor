import QtQuick 2.0

import "../Common/"


Image{
    id: stngsBtn
    source: coreScreen.expandedMedia || coreScreen.expandedPhone ? globalStyle.imageUrl + "/SettingsPane/img_hmc_stngs_btn_2" + globalStyle.skinUrl + ".png" : globalStyle.imageUrl + "/SettingsPane/img_hmc_stngs_btn_0" + globalStyle.skinUrl + ".png"
    width: parent.width

    height:127

    Row{
        spacing: 2
        Spa9FlatButton {
            id: bsettings
            width: 330
            height: stngsBtn.height
            invisible: false
            buttonType: "center"
            //onClicked: globalStyle.headerOn = !globalStyle.headerOn
            Image {
                id: iconSettings
                anchors.verticalCenter: parent.verticalCenter
                x: 12
                anchors.verticalCenterOffset: coreScreen.expandedMedia || coreScreen.expandedPhone ? -20 : 0
                source: globalStyle.imageUrl + "/Icons/img_hmc_settings" + globalStyle.skinUrl + ".png"
            }
            SpaText {
                anchors.verticalCenter: parent.verticalCenter
                anchors.verticalCenterOffset: coreScreen.expandedMedia || coreScreen.expandedPhone ? 20 : 0
                anchors.left: coreScreen.expandedMedia || coreScreen.expandedPhone ? parent.left: iconSettings.right
                anchors.leftMargin: coreScreen.expandedMedia || coreScreen.expandedPhone ? 16 : 36
                text: "Settings"
            }

            onClicked: {
                coreScreen.settingsView.visible = true
            }
        }

        Spa9FlatButton {
            id: bmanual
            width: 326
            height: stngsBtn.height
            invisible: false
            buttonType: "center"
            Image {
                id: iconManu
                anchors.verticalCenter: parent.verticalCenter
                x: 12
                source: globalStyle.imageUrl + "/Icons/img_hmc_drvr_manual" + globalStyle.skinUrl + ".png"
            }
            SpaText {
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: iconManu.right
                anchors.leftMargin: 36
                text: "Manual"
            }
        }
        Spa9FlatButton {
            id: bprofile
            width: 106
            height: stngsBtn.height
            invisible: false
            buttonType: "center"
            onClicked: globalStyle.textInsteadofIcon = !globalStyle.textInsteadofIcon
            Image {
                anchors.centerIn: parent
                source: globalStyle.imageUrl + "/Icons/img_vef_driverprofile_txt" + globalStyle.skinUrl + ".png"
            }
            SpaText {
                text: "Profile"
                anchors.centerIn: parent
                anchors.verticalCenterOffset: 20
            }
        }
    }
}

