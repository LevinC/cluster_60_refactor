import QtQuick 2.0

import "../Common"

SpaListItem{
    width: globalStyle.screenWidth
    height:100

    property string headerText
    property string subHeaderText
    property string imSource


    Column {
        spacing: -2
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: icon.right
        anchors.leftMargin: 12
        SpaText {
            textStyle: listItem1
            text: headerText
        }

        SpaText {
            textStyle: listItem2
            text: subHeaderText
        }
    }
    Item {
        id: icon
        height: parent.height
        width: 84
        Image {
            anchors.centerIn: parent
            source: globalStyle.imageUrl + imSource + globalStyle.skinUrl + ".png"
        }
    }
    Rectangle {
        anchors.right: parent.right
        width: 25
        height: width
        color: "#50ff0000"
    }
}

