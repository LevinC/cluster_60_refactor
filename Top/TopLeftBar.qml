import QtQuick 2.0

Row {
    id: statusbarLeft
    spacing: 10
    y: 12
    x: 10


    Image {
        id:networkLevel
        source: globalStyle.imageUrl + "/TopStatusBar/img_hmc_topbar_phone_3" + globalStyle.skinUrl + ".png"
    }

    Image {
        id:bluetooth
        source: globalStyle.imageUrl + "/Icons/img_hmc_bt_top_ON" + globalStyle.skinUrl + ".png"
    }
    Image {
        id:wifiIndicator
        source: globalStyle.imageUrl + "/Icons/img_hmc_wifi_3" + globalStyle.skinUrl + ".png"
    }
    Image {
        id:usb
        source: globalStyle.imageUrl + "/Icons/img_hmc_usb_topbar" + globalStyle.skinUrl + ".png"
    }
}
