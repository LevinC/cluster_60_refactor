/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0

// Home, Implements a home screen with multiple sub screens that can be flicked.
Flickable {
    id: root

    property int screen: 1

    property real _velocityThreshold: 1500
    property bool _flicked: false
    property real _screenReal: contentX/width
    property int _screen: 1


    boundsBehavior: Flickable.StopAtBounds
    flickableDirection: Flickable.HorizontalFlick
    contentHeight: height
    contentX: width
    Behavior on contentX { NumberAnimation { easing.type: Easing.OutQuad; duration: 300 } }

    onScreenChanged: {
        if (_screen !== screen) {
            _screen = screen
            fixPosition()
        }
    }

    // Go to main home screen
    function goHome() {
        if (screen === 1) {
            if (!coreScreen.appActive) {
                coreScreen.normalizeWidgets();
            }
        } else {
            screen = 1;
        }
    }

    function updatedScreenForPosition() {
    }

    function fixPosition() {
        contentX = width * _screen
        screen = _screen
    }

    onMovementEnded: {
        if (!_flicked) {
            if (_screenReal - 0.2 > _screen) {
                _screen++;
            }
            else if (_screenReal + 0.2 < _screen) {
                _screen--;
            }
        }
        _flicked = false;
        fixPosition();
    }

    onFlickStarted: {
        if (horizontalVelocity > _velocityThreshold && _screen < 2) {
            _screen++;
            _flicked = true;
            cancelFlick()
            fixPosition()
        }
        else if (horizontalVelocity < -_velocityThreshold && _screen > 0) {
            _screen--;
            _flicked = true;
            cancelFlick()
            fixPosition()
        }
    }
}
