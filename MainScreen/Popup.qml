import QtQuick 2.0
import "../Common"

Item {
    id: root

    property int size: 0
    property string header: ""
    property string subheader: ""

    property Component mycomponent

    function show(hdr, sbhdr, sz, cntnt) {
        header = hdr
        subheader = sbhdr
        size = sz
        mycomponent = cntnt
        state = "visible";
    }

    function close() { state = "" }

    width:  globalStyle.screenWidth
    height: 1024

    opacity: 0
    visible: false

    Rectangle {
        id: dim
        anchors.fill: parent
        color: "#CC000000"
        MouseArea {
            anchors.fill: parent
            onClicked: close()
        }
    }
    BorderImage {
        id: bg
        border { top: 2; bottom: 2; left: 2; right: 2 }
        anchors.centerIn: parent
        width: 522
        height: 276 + (42*size)
        source: globalStyle.imageUrl + "/Popup/img_hmc_popup"+ globalStyle.skinUrl + ".png"
        MouseArea {
            anchors.fill: parent
            preventStealing: true
        }

        Item {
            id: headerItem
            height: 95
            width: 500
            anchors.horizontalCenter: parent.horizontalCenter
            Column {
                anchors.centerIn: parent
                SpaText {
                    width: 660
                    text: header
                    textStyle: listItem1
                    horizontalAlignment: Text.AlignHCenter
                }
                SpaText {
                    width: 660
                    text: subheader
                    textStyle: listItem2
                    horizontalAlignment: Text.AlignHCenter
                    visible: subheader != ""
                }
            }
            SpaDivider {
                anchors.bottom: parent.bottom
            }
        }
        Loader {
            id: myLoader
            anchors.top: headerItem.bottom
            anchors.bottom: bg.bottom
            anchors.left: bg.left
            anchors.right: bg.right
            sourceComponent: mycomponent
        }
        Connections {
            target: myLoader.item
            onClose: close()
        }
    }


    //    Timer {
    //        id: closeTimer
    //        interval: 5000
    //        repeat: false
    //        onTriggered: close()
    //    }

    states: [
        State {
            name: "visible"
            PropertyChanges { target: root; visible: true; opacity: 1; }
        }
    ]

    transitions: [
        Transition {
            to: ""
            SequentialAnimation {
                // ScriptAction { script: { closeTimer.stop() } }
                ParallelAnimation {
                    NumberAnimation { property: "opacity"; duration: 200; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]} }
                    NumberAnimation { property: "scale"; duration: 200; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]} }
                }
                PropertyAction { target: root; property: "visible"; value: false }
            }
        },
        Transition {
            to: "visible"
            SequentialAnimation {
                ParallelAnimation {
                    NumberAnimation { property: "opacity"; duration: 200; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]} }
                    NumberAnimation { property: "scale"; duration: 200; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]} }
                }
                NumberAnimation { property: "opacity"; duration: 200; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]} }
                // ScriptAction { script: closeTimer.restart() }
            }
        }
    ]
}
