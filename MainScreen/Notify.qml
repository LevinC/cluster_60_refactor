/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import "../Common/"

// Notify, Pop-up notification
Item {
    id: root
    z: 200
    height:100
    width:globalStyle.screenWidth
    //Notifications - This does not work according to spec because at this moment we have very limited functionality. Correct functionality to be added when it is time .

    property string text: ""
    property string notifyicon: ""
    visible: true

    // Show pop-up notification with specified text & icon.
    function show(txt, pic) {
        text = txt;
        notifyicon = pic;
        state = "visible";
        timer.restart();
    }

    // Close pop-up notification
    function close() {
        timer.stop();
        state = "";
    }

    Image {
        id: shadow
        source: globalStyle.imageUrl + "/SettingsPane/img_hmc_stngs_shadow_S0.png"
        anchors.top: notifybg.bottom
        opacity: 0
    }


    Image {
        id: notifybg
        anchors.top: parent.top
        anchors.topMargin: -85
        width:  globalStyle.screenWidth
        height: 84
        source: globalStyle.imageUrl + "/SettingsPane/img_hmc_settings_pane_bg.png"
        Rectangle {
            height: 1
            width: parent.width
            opacity: 0.1
            anchors.bottom: parent.bottom
        }


        Item {
            width: 668
            anchors.horizontalCenter: parent.horizontalCenter
            Image {
                id: icon
                anchors.left: parent.left
                source: root.notifyicon
            }
            SpaText {
                width: 456
                anchors.baseline: parent.top
                anchors.baselineOffset: 53
                anchors.left: icon.right
                anchors.leftMargin: 22
                text: root.text
            }
        }
        Image {
            id: light
            anchors.top: parent.top
            source: globalStyle.imageUrl + "/HIGH_HMI_CORE_APP_SPEC_SETTINGS_PANE_light.png"
            opacity: 0
        }
    }

    Timer {
        id: timer
        interval: 3000
        onTriggered: root.close();
    }

    states: State {
        name: "visible"
        PropertyChanges {
            target: notifybg
            anchors.topMargin: 0
        }
        PropertyChanges {
            target: notifybg
            opacity: 1
        }
        PropertyChanges {
            target: shadow
            opacity: 1
        }
    }

    transitions: [
        Transition {
            to: ""
            PropertyAnimation {target: notifybg; property: "anchors.topMargin"; duration: 200; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]}}
            PropertyAnimation {target: shadow; property: "opacity"; duration: 200; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]}}
        },
        Transition {
            to: "visible"
            PropertyAnimation {target: notifybg; property: "anchors.topMargin"; to: 0; duration: 200; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]}}
            PropertyAnimation {target: shadow; property: "opacity"; to: 1; duration: 200; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]}}
            SequentialAnimation {
                PropertyAnimation {target: light; property: "opacity"; from: 0; to: 1; duration: 400; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.0, 0.0), (0.58, 1.0), (1.0, 1.0)]}}
                PropertyAnimation {target: light; property: "opacity"; from: 1; to: 0; duration: 400; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]}}
            }
        }
    ]
}
