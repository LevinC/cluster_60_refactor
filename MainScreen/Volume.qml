/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import "../Common"

Item {
    id: volRoot
    z: 300
    visible: volBg.opacity != 0

    property int percent: musicPlayer.volume*100//volumeModel.percentage

    function show() {
        state = "visible";
        timer.restart();
    }

    function close() {
        timer.stop();
        state = "closed";
    }


    Image {
        id: volBg
        opacity: 0
        source: globalStyle.imageUrl + "/TopStatusBar/img_hmc_topbar_volume_bg.png"
        Image {
            source: volRoot.percent ===  0 ? globalStyle.imageUrl+ "/Icons/img_hmc_volume_mute_ON.png" : globalStyle.imageUrl+ "/Icons/img_hmc_volume_mute_OFF.png"
        }
        Rectangle {
            x: 81
            y: 42
            height: 5
            width: volRoot.percent * 555 / 100
            color: globalStyle.themeColorGraphics
        }
        SpaText {
            id: volumePercentage
            width: 100
            anchors.right: parent.right
            anchors.rightMargin: 55
            anchors.verticalCenter: parent.verticalCenter
            anchors.verticalCenterOffset: -2
            text: percent + "%"
            textStyle: infoText
            font.pixelSize: 42
            horizontalAlignment: Text.AlignRight
            visible: false
        }
    }
    Timer {
        id: timer
        interval: 1000
        onTriggered: volRoot.close();
    }

    state: "closed"
    states: [
        State {
            name: "closed"
            PropertyChanges {
                target: volBg
                opacity: 0
            }
        },
        State {
            name: "visible"
            PropertyChanges {
                target: volBg
                opacity: 1
            }
            PropertyChanges {
                target: settingsPane
                opacity: 0
            }
        }
    ]

    transitions: [
        Transition {
            to: "closed"
            SequentialAnimation {
                NumberAnimation { target: volBg; property: "opacity"; duration: 130; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]} }
                ParallelAnimation {

                    NumberAnimation { target: settingsPane; property: "opacity"; duration: 70; }
                    ScriptAction { script: pageIndi.show() }
                }
            }
        },
        Transition {
            to: "visible"
            SequentialAnimation {
                ParallelAnimation {
                    NumberAnimation { target: settingsPane; property: "opacity"; duration: 130; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]} }
                    ScriptAction { script: pageIndi.close() }
                }
                NumberAnimation { target: volBg; property: "opacity"; duration: 70; }
            }
        }
    ]
}
