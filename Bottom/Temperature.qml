/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0

Item {
    id: root

    width: display.width
    implicitHeight: slider.height

    property bool left_: true

    TempSlider {
        id: slider

        anchors {
            left:  left_? parent.left : undefined
            right: left_? undefined   : parent.right
        }

        side: left_ ? "LH" : "RH"

        onClosed: root.state = ""
    }

    SmallTempSetter {
        id: display

        anchors.bottom: parent.bottom

        temp: {
            var t = left_ ? climateModel.leftTemp : climateModel.rightTemp;
            return locale.temperatureScale === 'fahrenheit' ? climateModel.fahrenheitValues[t] : climateModel.celsiusValues[t]
        }

        MouseArea {
            id: displayMouseArea
            anchors.fill: parent
            onPressed: {display.opacity = 0.7; display.scale = 0.9}
            onReleased: {display.opacity = 1; display.scale = 1}
            onCanceled: {display.opacity = 1; display.scale = 1}
            onClicked: climateModel.fanOff ? !enabled : root.state = "slideropen"
        }
    }

    states: [
        State {
            name: "slideropen"
            AnchorChanges {
                target: display
                anchors {
                    verticalCenter: slider.verticalCenter
                    horizontalCenter: slider.horizontalCenter
                    bottom: undefined
                }
            }
            PropertyChanges {
                target: display
                state: "big"
                anchors {
                    horizontalCenterOffset: left_ ? 70 : -65
                }
            }
            PropertyChanges {
                target: slider
                state: "visible"
            }
            PropertyChanges {
                target: displayMouseArea
                enabled: false
            }
        }
    ]

    transitions: Transition {
        AnchorAnimation {
            duration: 200
            easing {
                type: Easing.OutCubic
                bezierCurve: [0, 0.42, 1, 1]
            }
        }
    }
}

///* Copyright (c) 2014 Volvo Car Corporation
// * All rights reserved.
// */

//import QtQuick 2.0

//Item {
//    id: root

//    implicitWidth: display.width
//    implicitHeight: slider.height

//    property bool left_: true

//    TempSlider {
//        id: slider

//        anchors {
//            left:  left_? parent.left : undefined
//            right: left_? undefined   : parent.right
//        }

//        leftSide: left_

//        onClosed: root.state = ""
//    }

//    SmallTempSetter {
//        id: display

//        anchors.bottom: parent.bottom

//        temp: {
//            var t = left_ ? climateModel.leftTemp : climateModel.rightTemp;
//            return locale.temperatureScale === 'fahrenheit' ? climateModel.fahrenheitValues[t] : climateModel.celsiusValues[t]
//        }

//        MouseArea {
//            id: displayMouseArea
//            anchors.fill: parent
//            onPressed: {display.opacity = 0.7; display.scale = 0.9}
//            onReleased: {display.opacity = 1; display.scale = 1}
//            onCanceled: {display.opacity = 1; display.scale = 1}
//            onClicked: climateModel.fanOff ? !enabled : root.state = "slideropen"
//        }
//    }

//    states: [
//        State {
//            name: "slideropen"
//            AnchorChanges {
//                target: display
//                anchors {
//                    verticalCenter: slider.verticalCenter
//                    horizontalCenter: slider.horizontalCenter
//                    bottom: undefined
//                }
//            }
//            PropertyChanges {
//                target: display
//                state: "big"
//                anchors {
//                    horizontalCenterOffset: left_ ? 70 : -65
//                }
//            }
//            PropertyChanges {
//                target: slider
//                state: "visible"
//            }
//            PropertyChanges {
//                target: displayMouseArea
//                enabled: false
//            }
//        }
//    ]

//    transitions: Transition {
//        AnchorAnimation {
//            duration: 200
//            easing {
//                type: Easing.OutCubic
//                bezierCurve: [0, 0.42, 1, 1]
//            }
//        }
//    }
//}
