/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import "../Common/"

Rectangle {
    id: root
    color:"black"
    anchors.fill: parent
    anchors.topMargin: 6
    visible: false
    opacity: 0

    function show() { state = "visible" }

    function close() { state = "" }

    function toggle() {
        if (state === "visible")
            state = "";
        else
            state = "visible";
    }

    SpaTabView {
        id: titleText
        header: "Climate"
        anchors.fill: parent
        highlightColor: "#597999"
        visibleSearchIcon: false
        tabsInteractive: false
        currentTab: 1
        tabs: VisualItemModel {

            SpaPage {
                subheader: "Parking Climate"
                enabled: false
            }

            ClimateMainPage {
                subheader: "Main"
            }

            SpaPage {
                subheader: "Rear Climate"
                enabled: false
            }
        }
    }

    states: [
        State {
            name: "visible"
            PropertyChanges {
                target: root
                visible: true
                opacity: 1.0
            }
            PropertyChanges {
                target: climateModel
                climatePageShown: true
            }
        },
        // [iOSInCar]
        State {
            name: ""
            when: !climateModel.climatePageShown
            PropertyChanges {
                target: root
                visible: false
                opacity: 0.0
            }
            PropertyChanges {
                target: climateModel
                climatePageShown: false
            }
        }
        // [iOSInCar]
    ]

    transitions: [
        Transition {
            to: ""
            SequentialAnimation {
                NumberAnimation { property: "opacity"; duration: 150; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]} }
                PropertyAction { property: "visible"; value: false }
            }
        },
        Transition {
            to: "visible"
            SequentialAnimation {
                PauseAnimation { duration: 300 }
                PropertyAction { property: "visible"; value: true }
                NumberAnimation { property: "opacity"; duration: 200; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]} }
            }
        }
    ]
}
