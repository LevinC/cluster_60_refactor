/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0

Item {
    id: largeFanManRoot

    property bool fanAuto: true
    property bool fanTop: false
    property bool fanMiddle: false
    property bool fanBottom: false
    property bool fanOff: false

    signal topClicked()
    signal middleClicked()
    signal bottomClicked()

    height: 281
    width: 768
    //opacity: fanOff ? 0 : 1
//    Behavior on opacity {
//        NumberAnimation { duration: 250 }
//    }

    Image {
        source: !fanAuto && fanTop || !fanAuto && fanMiddle || !fanAuto && fanBottom  ? globalStyle.imageUrl + "/VehicleFunction/Climate/Air_distrubution/img_clm_airdist_bg_manual" + globalStyle.skinUrl + ".png" : fanOff ?globalStyle.imageUrl + "/VehicleFunction/Climate/Air_distrubution/img_clm_airdist_bg_off" + globalStyle.skinUrl + ".png" :  globalStyle.imageUrl + "/VehicleFunction/Climate/Air_distrubution/img_clm_airdist_bg_auto" + globalStyle.skinUrl + ".png"
        Behavior on opacity {
            NumberAnimation { duration: 200 }
        }
        opacity: 1
        visible: opacity != 0
        anchors.horizontalCenter: parent.horizontalCenter
    }

//    Image {
//        source:  globalStyle.imageUrl + "/SPA_UI_COMPONENT_CLIMATE_AIR_DISTRIBUTION_passive_fields" + globalStyle.skinUrl + ".png"
//        Behavior on opacity {
//            NumberAnimation { duration: 200 }
//        }
//        opacity: !fanAuto && fanTop || !fanAuto && fanMiddle || !fanAuto && fanBottom ? 1 : 0;
//        visible: opacity != 0
//        anchors.horizontalCenter: parent.horizontalCenter
//    }

    Image {
        id: activeTop
        source: globalStyle.imageUrl + "/VehicleFunction/Climate/Air_distrubution/img_clm_airdist_fields_top" + globalStyle.skinUrl + ".png"
        Behavior on opacity {
            NumberAnimation { duration: 200 }
        }
        opacity: !fanAuto && fanTop ? 1 : 0;
        visible: opacity != 0
        anchors.horizontalCenter: parent.horizontalCenter
    }
    MouseArea {
        id: topMaskedArea
        anchors.fill: activeTop
        anchors.bottomMargin: 200
        // On Clicked
        onClicked: {
            // Check If Contains Mouse
            if (topMaskedArea.containsMouse) {
                //console.log("topMaskedArea.onClicked");
                // Emit Top Clicked Signal
                largeFanManRoot.topClicked();
            }
        }
    }
    // Mouse Area For Top
//    MaskedMouseArea {
//        id: topMaskedArea
//        anchors.fill: activeTop
//        maskSource: activeTop.source
//        // On Clicked
//        onClicked: {
//            // Check If Contains Mouse
//            if (topMaskedArea.containsMouse) {
//                //console.log("topMaskedArea.onClicked");
//                // Emit Top Clicked Signal
//                largeFanManRoot.topClicked();
//            }
//        }
//    }

    Image {
        id: activeMiddle
        source: globalStyle.imageUrl + "/VehicleFunction/Climate/Air_distrubution/img_clm_airdist_fields_middle" + globalStyle.skinUrl + ".png"
        Behavior on opacity {
            NumberAnimation { duration: 200 }
        }
        opacity: !fanAuto && fanMiddle ? 1 : 0;
        visible: opacity != 0
        anchors.horizontalCenter: parent.horizontalCenter
    }

    MouseArea {
        id: middleMaskedArea
        anchors.fill: activeMiddle
        anchors.bottomMargin: 100
        anchors.topMargin: 100
        // On Clicked
        onClicked: {
            // Check if Contains Mouse
            if (middleMaskedArea.containsMouse) {
                //console.log("middleMaskedArea.onClicked");
                // Emit Middle Clicked Signal
                largeFanManRoot.middleClicked();
            }
        }
    }
    // Mouse Area For Middle
//    MaskedMouseArea {
//        id: middleMaskedArea
//        anchors.fill: activeMiddle
//        maskSource: activeMiddle.source
//        // On Clicked
//        onClicked: {
//            // Check if Contains Mouse
//            if (middleMaskedArea.containsMouse) {
//                //console.log("middleMaskedArea.onClicked");
//                // Emit Middle Clicked Signal
//                largeFanManRoot.middleClicked();
//            }
//        }
//    }

    Image {
        id: activeBottom
        source: globalStyle.imageUrl + "/VehicleFunction/Climate/Air_distrubution/img_clm_airdist_fields_bottom" + globalStyle.skinUrl + ".png"
        Behavior on opacity {
            NumberAnimation { duration: 200 }
        }
        opacity: !fanAuto && fanBottom ? 1 : 0;
        visible: opacity != 0
        anchors.horizontalCenter: parent.horizontalCenter
    }

    MouseArea {
        id: bottomMaskedArea
        anchors.fill: activeBottom
        anchors.topMargin: 200
        // On Clicked
        onClicked: {
            // Check If Contains Mouse Area
            if (bottomMaskedArea.containsMouse) {
                //console.log("bottomMaskedArea.onClicked");
                // Emit Bottom Clicked Signal
                largeFanManRoot.bottomClicked();
            }
        }
    }
    // Mouse Area For Bottom
//    MaskedMouseArea {
//        id: bottomMaskedArea
//        anchors.fill: activeBottom
//        maskSource: activeBottom.source
//        // On Clicked
//        onClicked: {
//            // Check If Contains Mouse Area
//            if (bottomMaskedArea.containsMouse) {
//                //console.log("bottomMaskedArea.onClicked");
//                // Emit Bottom Clicked Signal
//                largeFanManRoot.bottomClicked();
//            }
//        }
//    }
}
