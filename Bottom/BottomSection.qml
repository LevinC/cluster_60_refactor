import QtQuick 2.3
import QtQuick.Controls 1.2

Item{
    width: globalStyle.screenWidth
    height: globalStyle.screenHeight

    property alias climatepage : climatepage

    ClimatePage {
        id: climatepage
        anchors.fill: parent
    }

    ClimateBar {
        id: climatebar
        anchors.fill: parent
        fanmanState: climatepage.state === "visible" ? "large" : ""
    }

}
