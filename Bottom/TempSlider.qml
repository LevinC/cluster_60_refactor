/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.0
import "../Common/"

Item {
    id: root

    property int desiredTemp: slider.value
    property string side: "LH"
    property string unit: locale.temperatureScale === 'fahrenheit' ? "_F" : "_C"

    signal closed

    width: 341
    height: 1024

    opacity: 0
    visible: false

    //scale: 1
    //transformOrigin: leftSide ? Item.Right : Item.Left

    BorderImage {
        id: background
        anchors.fill: parent
        anchors.margins: -15
        border { left: 50; right: 50 }
        source: globalStyle.imageUrl + "/VehicleFunction/Climate/img_clm_tempadjust_" + side + "_bg" + globalStyle.skinUrl + ".png"
        MouseArea {
            anchors.fill: parent
            preventStealing: true
        }
    }

    Image {
        id: scale
        anchors {
            top: parent.top
            topMargin: 128
            left: side === "LH" ? root.left : undefined
            right: side === "LH" ? undefined : root.right
        }
        source: globalStyle.imageUrl + "/VehicleFunction/Climate/img_clm_tempadjust_" + side + unit + globalStyle.skinUrl + ".png"
//        Column {
//            anchors.horizontalCenter: parent.horizontalCenter
//            Repeater {
//                model: locale.temperatureScale === 'fahrenheit' ? climateModel.fahrenheitValues.slice(0).reverse() : climateModel.celsiusValues.slice(0).reverse()
//                Text {
//                    id: scaletext
//                    height: 62
//                    text: modelData
//                    visible: false//index%2===0
//                    color: "white"
//                    font.family: _instrument102.name
//                    style: Text.Raised
//                    styleColor: "black"
//                    font.pixelSize: 32
//                    font.letterSpacing: text == "HI" ? 7 : 0
//                }
//            }
//        }
    }

    Slider {
        id: slider
        anchors {
            fill: scale
            topMargin: -1
            bottomMargin: -1
        }
        orientation: Qt.Vertical
        minimumValue: 0
        maximumValue: locale.temperatureScale === 'fahrenheit' ? climateModel.fahrenheitValues.length-1 : climateModel.celsiusValues.length-1
        stepSize: 1
        style: SliderStyle {
            groove: Item {}
            handle: Item {
                // Hack around some bug in QtQuickControls related to
                // vertically oriented sliders.
                width: 31; height: 31
                Image {
                    id: handleImage
                    anchors {
                        centerIn: parent
                        horizontalCenterOffset: -1
                    }
                    rotation: 90
                    mirror: side === "LH"  ? false : true
                    source: globalStyle.imageUrl + "/VehicleFunction/Climate/img_clm_tempadjust_handle.png" //+ (leftSide ? "_cw" + globalStyle.skinUrl + ".png" : "_ccw" + globalStyle.skinUrl + ".png")
                }
            }
        }
        value: side === "LH" ? climateModel.leftTemp : climateModel.rightTemp
        onValueChanged: {
            closeTimer.restart()
            if (side === "LH" ) {
                climateModel.setLeftTemp(value)
            } else {
                climateModel.setRightTemp(value)
            }
        }
    }

    Spa9PatchButton {
        width: 310; height: 84
        anchors {
            bottom: root.bottom
            bottomMargin: 10
            horizontalCenter: background.horizontalCenter
            horizontalCenterOffset: side === "LH" ? -10 : 10
        }
        iconSource: globalStyle.imageUrl + "/Icons/img_hmc_close_txt" + globalStyle.skinUrl + ".png"
        text: "Close"
        onClicked: closed()
    }

    Image {
        id: plusminuspanel
        anchors {
            verticalCenter: root.verticalCenter
            verticalCenterOffset: -5
            horizontalCenter: root.horizontalCenter
            horizontalCenterOffset: side === "LH"  ? 68 : -68
        }

        source: globalStyle.imageUrl + "/VehicleFunction/Climate/img_clm_temp_switch_bg" + globalStyle.skinUrl + ".png"

        Image {
            id: pressIndication
            anchors.fill: parent
            source: globalStyle.imageUrl + "/VehicleFunction/Climate/img_clm_temp_switch_plus_ON" + globalStyle.skinUrl + ".png"
            visible: false
        }

        Image {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 20
            source: globalStyle.imageUrl + "/Icons/img_clm_tempadjust_plus" + globalStyle.skinUrl + ".png"
        }
        Image {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 20
            source: globalStyle.imageUrl + "/Icons/img_clm_tempadjust_minus" + globalStyle.skinUrl + ".png"
        }

        MouseArea {
            anchors {
                fill: pressIndication
                bottomMargin: parent.height*2/3
            }
            onClicked: slider.value++
            onPressed: {
                pressIndication.source = globalStyle.imageUrl + "/VehicleFunction/Climate/img_clm_temp_switch_plus_ON" + globalStyle.skinUrl + ".png"
                pressIndication.visible = true
            }
            onReleased: pressIndication.visible = false
        }

        MouseArea {
            anchors {
                fill: pressIndication
                topMargin: parent.height*2/3
            }
            onClicked: slider.value--
            onPressed: {
                pressIndication.source = globalStyle.imageUrl + "/VehicleFunction/Climate/img_clm_temp_switch_minus_ON" + globalStyle.skinUrl + ".png"
                pressIndication.visible = true
            }
            onReleased: pressIndication.visible = false
        }
    }

    Timer {
        id: closeTimer
        interval: 50000
        repeat: false
        onTriggered: closed()
    }

    states: [
        State {
            name: "visible"
            PropertyChanges { target: root; visible: true; opacity: 1; }
        }
    ]

    transitions: [
        Transition {
            to: ""
            SequentialAnimation {
                ScriptAction { script: { closeTimer.stop() } }
                ParallelAnimation {
                    NumberAnimation { property: "opacity"; duration: 200; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]} }
                }
                PropertyAction { target: root; property: "visible"; value: false }
            }
        },
        Transition {
            to: "visible"
            SequentialAnimation {
                ParallelAnimation {
                    NumberAnimation { property: "opacity"; duration: 200; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]} }
                }
                NumberAnimation { property: "opacity"; duration: 200; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]} }
                ScriptAction { script: closeTimer.restart() }
            }
        }
    ]
}
