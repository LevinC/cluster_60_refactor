
/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import "../Common/"

Item {
    id: root

    width: 140
    height: 112

    property var temp
       property var _tempwhole: typeof temp === 'number' ? Math.floor(temp) : temp
       property var _temppart: typeof temp === 'number' ? (temp-Math.floor(temp)).toString().slice(1) : ''

    SpaText {
        id: tempLarge
        width: text == "OFF" ? 100 : 90
        anchors {
            right: parent.right
            rightMargin:  text == "LO" || text == "OFF" ? 27 : text == "HI" ? 17 : 37
            verticalCenter: parent.verticalCenter
        }
        text: climateModel.fanOff ? "OFF" : _tempwhole
        textStyle: text == "HI" || text == "LO" || text == "OFF" ? smallTempNumbers : largeTempNumbers //correct thinner font coming
        font.pixelSize: text == "HI" || text == "LO" ? 75 : text == "OFF" ? 60 : 75
        font.letterSpacing : text == "HI" ? 0 :  text =="OFF" ? -3 : -6
        smooth: true
    }
    SpaText {
        id: decimal
        anchors {
            right: parent.right
            rightMargin: 22
            verticalCenter: parent.verticalCenter
            verticalCenterOffset: 16
        }
        text: _temppart
        textStyle: smallTempNumbers
    }
    Image {
        source: globalStyle.imageUrl + "/VehicleFunction/Climate/img_clm_degree_c" + globalStyle.skinUrl + ".png"
        anchors.right: parent.right
        anchors.rightMargin: 18
        anchors.top: parent.top
        anchors.topMargin: 22
        visible: _tempwhole !== "HI" && _tempwhole !== "LO" && tempLarge.text !== "OFF"
    }

    states: [
        State {
            name: "big"
            PropertyChanges {
                target: root
                scale: 1.15
            }
        }
    ]

    transitions: Transition {
        NumberAnimation {
            property: "scale"
            duration: 200
            easing {
                type: Easing.OutCubic
                bezierCurve: [0, 0, 1, 1]
            }
        }
    }
}
