/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import "../Common/"

Item {
    id: root

    property bool fanAuto: true
    property bool defrost: false

    width: 428
    height: 84

    Image {
        id: autobg
        source: fanAuto ? globalStyle.imageUrl + "/VehicleFunction/Climate/img_clm_fanlevels_bg_overlay_auto.png": globalStyle.imageUrl + "/VehicleFunction/Climate/img_clm_fanlevels_bg_overlay_manual.png"
        visible: !climateModel.fanOff
    }

    Row {
        spacing: 2
        Repeater {
            id: fanspeedModel
            model: 5
            delegate:
                MouseArea {
                id: mouseArea
                onClicked: {climateModel.fanSpeed = index+1; climateModel.fanMax = false; climateModel.fanOff = false; fanMarking.opacity = 1; orangeMarker.opacity = 1; }
                width: 84
                height: 84
                SpaText {
                    id: fanMarking
                    anchors.right: speedimage.right
                    anchors.baseline: speedimage.top
                    anchors.baselineOffset: -6
                    text: (defrost ? ("Defrost Fan %1").arg(index+1) : ("Auto Fan %1").arg(index +1))
                    font.pixelSize: 20
                    //removes label immediately if quickly changing fan speed. TODO. according to spec it should not just immediately disappear but fade out 200ms.
                    visible: climateModel.fanSpeed !== index+1 || !climateModel.fanAuto ? false : true
                    opacity: 0
                    Behavior on opacity { SequentialAnimation {
                            PropertyAnimation {
                                target: fanMarking
                                property: "opacity"
                                from: 0
                                to: 1
                                duration: 10 }
                            PauseAnimation { duration: 1500 }
                            PropertyAnimation {
                                target: fanMarking
                                property: "opacity"
                                from: 1
                                to: 0
                                duration: 200
                                easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]}
                            }
                        }
                    }
                }
                Image {
                    id: speedimage
                    source: globalStyle.imageUrl + "/HIGH_APP_CLIMATE_LAYOUT_fanctrl_" + (index + 1) + ".png"
                    opacity: (climateModel.fanSpeed >= (index+1)) ? 1 : 0;
                    visible: opacity != 0
                }
                Rectangle {
                    id: orangeMarker
                    width: 84
                    height: 2
                    color: globalStyle.themeColorGraphics
                    anchors.right: speedimage.right
                    anchors.top: speedimage.bottom
                    anchors.topMargin: 10
                    //removes label immediately if quickly changing fan speed
                    visible: climateModel.fanSpeed !== index+1 || !climateModel.fanAuto ? false : true
                    opacity: 0
                    Behavior on opacity { SequentialAnimation {
                            PropertyAnimation {
                                target: orangeMarker
                                property: "opacity"
                                from: 0
                                to: 1
                                duration: 10 }
                            PauseAnimation { duration: 1500 }
                            PropertyAnimation {
                                target: orangeMarker
                                property: "opacity"
                                from: 1
                                to: 0
                                duration: 200
                                easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]}
                            }
                        }
                    }
                }
                Rectangle {
                    id: pressIndication
                    anchors.fill: parent
                    color: parent.pressed ?globalStyle.themeColorGraphicsPressed : "#0Fffffff"
                }
            }
        }
    }
}
