/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import "../Common/"

SpaPage {
    id: root
    width: 768

    interactive: false

    Row {
        id: topclimatebuttons
        anchors.top: parent.top
        anchors.topMargin: 42
        anchors.horizontalCenter: parent.horizontalCenter
        width: 600
        spacing: 2
        Spa9FlatButton {
            width: 118
            height: 84
            invisible: false
            buttonType: "left"
            iconSource: globalStyle.imageUrl + "/Icons/img_clm_maxDefrost" + globalStyle.skinUrl + ".png"
            selectedIconSource: globalStyle.imageUrl + "/Icons/img_clm_maxDefrost" + globalStyle.skinUrl + ".png"
            centeredIcon: true
            selected: climateModel.defrost
            onClicked: coreScreen.notAvailableNotify()/*climateModel.clickDefrost();*/
        }
        Spa9FlatButton {
            width: 118
            height: 84
            invisible: false
            buttonType: "center"
            iconSource: globalStyle.imageUrl + "/Icons/img_clm_frontDefrost" + globalStyle.skinUrl + ".png"
            selectedIconSource: globalStyle.imageUrl + "/Icons/img_clm_frontDefrost" + globalStyle.skinUrl + ".png"
            centeredIcon: true
            selected: climateModel.front
            onClicked: coreScreen.notAvailableNotify()/*climateModel.clickFront();*/
        }
        Spa9FlatButton {
            width: 118
            height: 84
            invisible: false
            buttonType: "center"
            iconSource: globalStyle.imageUrl + "/Icons/img_clm_rearDefrost" + globalStyle.skinUrl + ".png"
            selectedIconSource: globalStyle.imageUrl + "/Icons/img_clm_rearDefrost" + globalStyle.skinUrl + ".png"
            centeredIcon: true
            selected: climateModel.rear
            onClicked: coreScreen.notAvailableNotify()/*climateModel.clickRear();*/
        }
        Spa9FlatButton {
            width: 118
            height: 84
            invisible: false
            buttonType: "center"
            iconSource: globalStyle.imageUrl + "/img_clm_AC" + globalStyle.skinUrl + ".png"
            selectedIconSource: globalStyle.imageUrl + "/img_clm_AC" + globalStyle.skinUrl + ".png"
            centeredIcon: true
            enabled: climateModel.acenabled
            selected: climateModel.ac
            onClicked: coreScreen.notAvailableNotify()/*climateModel.clickAC()*/
        }
        Spa9FlatButton {
            width: 118
            height: 84
            invisible: false
            buttonType: "right"
            iconSource: globalStyle.imageUrl + "/Icons/img_clm_recirc" + globalStyle.skinUrl + ".png"
            selectedIconSource: globalStyle.imageUrl + "/Icons/img_clm_recirc" + globalStyle.skinUrl + ".png"
            centeredIcon: true
            enabled: climateModel.recircenabled
            selected: climateModel.recirc
            onClicked: coreScreen.notAvailableNotify()/*climateModel.clickRecirc();*/
        }
    }


    LargeFanMan {
        id: fanMan
        width: 768
        anchors.top: topclimatebuttons.bottom
        anchors.topMargin: 46
        fanAuto: climateModel.fanAuto
        fanTop: climateModel.fanTop
        fanMiddle: climateModel.fanMiddle
        fanBottom: climateModel.fanBottom
        fanOff: climateModel.fanOff
        onTopClicked: climateModel.clickFanTop();
        onMiddleClicked: climateModel.clickFanMiddle();
        onBottomClicked: climateModel.clickFanBottom();
    }
    Row {
        id: fanCtrl
        anchors.top: fanMan.bottom
        anchors.topMargin: 52
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: 6
        Spa9FlatButton {
            id: fanOffButton
            onClicked:climateModel.clickFanOff();
            width: 78
            height: 84
            invisible: false
            buttonType: "left"
            text: "Off"
            iconSource: globalStyle.imageUrl + "/Icons/img_clm_fanOff" + globalStyle.skinUrl + ".png"
            selectedIconSource: globalStyle.imageUrl + "/Icons/img_clm_fanOff" + globalStyle.skinUrl + ".png"
            selected: climateModel.fanOff
            centeredIcon: true
        }
        LargeFanSpeed {
            fanAuto: climateModel.fanAuto
            defrost: climateModel.defrost
        }
        Spa9FlatButton {
            onClicked: climateModel.clickFanMax();
            width: 77
            height: 84
            invisible: false
            buttonType: "right"
            text: "Max"
            iconSource: globalStyle.imageUrl + "/Icons/img_clm_fanMax" + globalStyle.skinUrl + ".png"
            selectedIconSource: globalStyle.imageUrl + "/Icons/img_clm_fanMax" + globalStyle.skinUrl + ".png"
            centeredIcon: true
            selected: climateModel.fanMax
        }
    }
    Spa9FlatButton {
        id: autoclimate
        width: 440
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: fanCtrl.bottom
        anchors.topMargin: 44
        buttonType: "standalone"
        invisible: false
        //text: "AUTO Climate"
        centeredIcon: true
        selected: climateModel.fanAuto
        onClicked: selected ? null : climateModel.clickFanAuto();
        Column {
            spacing: -12
            anchors.centerIn: parent
            SpaText {
                text: "AUTO"
                font.pixelSize: 40
            }
            SpaText {
                text: "Climate"
                font.pixelSize: 33
            }
        }
    }
}
