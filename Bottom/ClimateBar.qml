/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved. */

import QtQuick 2.0
import "../Common/"

Item {
    id: root

    Rectangle{
        width: globalStyle.screenWidth
        height:112
        anchors.bottom:parent.bottom
        color: "black"
    }

    property alias fanmanState: fanman.state

    Item {
        width: 210
        height: 198
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
    FanMan {
        id: fanman
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 46+8
    }

    StaticFanMan {
        id: staticFanman
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: -8
    }
    }

    Seat {
        id: leftSeatIndicator
        width: 150
        height: 97
        anchors {
            bottom: parent.bottom
            bottomMargin: 8
            left: leftTemp.right
        }
        side: "left"
        level: climateModel.leftSeatHeat
        onClicked: climateModel.stepLeftSeatHeat()
    }

    Spa9PatchButton {
        id: closeClimatePageButton

        width: 150; height: 84

        anchors {
            horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom
            bottomMargin: 10
        }

        opacity: 0
        visible: opacity != 0

        iconSource: globalStyle.imageUrl + "/Icons/img_hmc_close_txt" + globalStyle.skinUrl + ".png"
        text: "Close"

        onClicked: climatepage.close();
    }

    Seat {
        id: rightSeatIndicator
        width: 150
        height: 97
        anchors {
            bottom: parent.bottom
            bottomMargin: 8
            right: rightTemp.left
        }
        level: climateModel.rightSeatHeat
        onClicked: climateModel.stepRightSeatHeat()
    }

    // Mouse Area To Block Mouse Events While Left Or Right Temperature is Shown
    MouseArea {
        anchors.fill: parent
        preventStealing: true
        visible: climateModel.leftTempShown || climateModel.rightTempShown
        onPressed: {
            climateModel.leftTempShown = false;
            climateModel.rightTempShown = false;
        }
    }

    Temperature {
        id: leftTemp
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        left_: true
        // [iOSInCar]
        // On State Changed
        onStateChanged: {
            // Set Climate Model Left Temp Shown
            climateModel.leftTempShown = (leftTemp.state === "slideropen");
        }
        // Connections to Climate Model
        Connections {
            target: climateModel
            onLeftTempShownChanged: {
                //console.log("rightTemp.climateModel.onLeftTempShownChanged - leftTempShown:" + climateModel.leftTempShown);
                // Check If Shown
                if (!climateModel.leftTempShown) {
                    // Reset State
                    leftTemp.state = "";
                }
            }
        }
        // [iOSInCar]
    }

    Temperature {
        id: rightTemp
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        left_: false
        // [iOSInCar]
        // On State Changed
        onStateChanged: {
            // Set Climate Model Right Temp Shown
            climateModel.rightTempShown = (rightTemp.state === "slideropen");
        }
        // Connections to Climate Model
        Connections {
            target: climateModel
            onRightTempShownChanged: {
                //console.log("rightTemp.climateModel.onRightTempShownChanged - rightTempShown:" + climateModel.rightTempShown);
                // Check If Shown
                if (!climateModel.rightTempShown) {
                    // Reset State
                    rightTemp.state = "";
                }
            }
        }
        // [iOSInCar]
    }
    states: State {
        name: "open"
        when: fanmanState === "large"
        PropertyChanges {
            target: closeClimatePageButton
            opacity: 1
        }
    }
    transitions: [
        Transition {
            from: ""
            to: "open"
            SequentialAnimation {
            PauseAnimation { duration: 300 }
            PropertyAnimation { target: closeClimatePageButton; property: "opacity";  duration: 200; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (0.58, 1.0), (1.0, 1.0)]} }
            //unsure about what the spec says but I felt 300 ms was too long so I changed it to 200
            }
        },
        Transition {
            from: "open"
            to: ""
            PropertyAnimation { target: closeClimatePageButton; property: "opacity";  duration: 150; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.15, 0.0), (0.15, 1.0), (1.0, 1.0)]} }
        }
    ]
}
