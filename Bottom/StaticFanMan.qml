import QtQuick 2.0

import "../dummydata/"

Item {
    width: 210
    height: 100

    Image {
        id: fanSpeedAuto
        source: globalStyle.imageUrl + "/VehicleFunction/Climate/img_clm_auto_levels_" + climateModel.fanSpeed + globalStyle.skinUrl + ".png"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        opacity: 1
    }
    Image {
        id: climatetext
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        source: climateModel.fanMax ? globalStyle.imageUrl + "/VehicleFunction/Climate/img_clm_txt_max" + globalStyle.skinUrl + ".png" : climateModel.fanOff ? globalStyle.imageUrl + "/VehicleFunction/Climate/img_clm_txt_off" + globalStyle.skinUrl + ".png" : climateModel.fanAuto ? globalStyle.imageUrl + "/VehicleFunction/Climate/img_clm_txt_auto" + globalStyle.skinUrl + ".png" : ""
        visible: opacity != 0
        scale: 0.9
    }

    MouseArea {
        id: mousearea
        anchors.fill: parent
        onClicked: climatepage.show();
    }

    states: [
        State {
            name: "large"
            when: fanman.state === "large"
            PropertyChanges {
                target:climatetext
                opacity: 0
            }
            PropertyChanges {
                target: fanSpeedAuto
                opacity: 0
            }
        }
    ]
    transitions: [
        Transition {
            from: ""
            to: "large"
            NumberAnimation { target: climatetext; property: "opacity"; duration: 150; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.15, 0.0), (0.15, 1.0), (1.0, 1.0)]} }
            NumberAnimation { target: fanSpeedAuto; property: "opacity"; duration: 150; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.15, 0.0), (0.15, 1.0), (1.0, 1.0)]} }
        },
        Transition {
            from: "large"
            to: ""
            NumberAnimation { target: climatetext; property: "opacity"; duration: 150; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.15, 0.0), (0.15, 1.0), (1.0, 1.0)]} }
            NumberAnimation { target: fanSpeedAuto; property: "opacity"; duration: 150; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.15, 0.0), (0.15, 1.0), (1.0, 1.0)]} }
        }
    ]
}
