/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import "../Common/"

Item {
    id: root

    width: 210
    height: 100

    property bool fanSpeedVisible: climateModel.fanSpeed !== 3 //&& climateModel.fanSpeed > 0

    // The rather weird definition of fan man scaling.
    // See "HIGH_CLIMATE_BAR.pdf"
    property real _x: home.contentX // screen swipe position
    property int  _W: home.width    // screen width
    property real _y: widgetPane.height
    property int  _H: 820/*838*/            // max widgets height
    property int  _h: /*838*/820-752        // max - min widgets height

    transform: Scale { id: scaling; origin.x: 105; origin.y: 120; xScale: 0.6; yScale: 0.6/*xScale: 0.65+0.35*(1-(Math.abs((_x-_W)/_W)))*(1-(1+(_y-_H)/_h)); yScale: 0.65+0.35*(1-(Math.abs((_x-_W)/_W)))*(1-(1+(_y-_H)/_h))*/}

    SpaText {
        id: cleanZone
        text: "CLEANZONE"
        font.family: smallTempNumbers.family
        color: "#597999"
        width: parent.width
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 28
        y: -30
    }

    Image {
        id: fan
        source: climateModel.fanOff ? globalStyle.imageUrl + "/VehicleFunction/Climate/img_clm_fan_large_OFF" + globalStyle.skinUrl + ".png" : globalStyle.imageUrl + "/VehicleFunction/Climate/img_clm_fan_large_ON" + globalStyle.skinUrl + ".png"
        scale: 0.5
        opacity: 1
        Behavior on opacity {
            NumberAnimation { duration: 250 }
        }
        x: (climateModel.fanTop || climateModel.fanMiddle || climateModel.fanBottom) && !climateModel.fanOff ? 15 : 47
        y: fanSpeedVisible ? 2 : (climateModel.fanTop || climateModel.fanMiddle || climateModel.fanBottom) && !climateModel.fanOff ? 22 : 2
    }

    Image {
        id: man
        source: globalStyle.imageUrl + "/VehicleFunction/Climate/img_clm_manikin_large_ON.png"
        scale: 0.45
        opacity: 0.5
        Behavior on opacity {
            NumberAnimation { duration: 250 }
        }
        x: (climateModel.fanTop || climateModel.fanMiddle || climateModel.fanBottom) && !climateModel.fanOff ? 35 : 10
        y: fanSpeedVisible ? -33 : -32
    }

    Image {
        id: airFields
        anchors.horizontalCenter: parent.horizontalCenter
        source: globalStyle.imageUrl + "/VehicleFunction/Climate/img_clm_manual_fan_distribution_bg" + globalStyle.skinUrl + ".png"
        visible: (climateModel.fanTop || climateModel.fanMiddle || climateModel.fanBottom)
        Image {
            anchors.fill: parent
            source: globalStyle.imageUrl + "/VehicleFunction/Climate/img_clm_manual_fan_distribution_top" + globalStyle.skinUrl + ".png"
            visible: airFields.visible && climateModel.fanTop
        }
        Image {
            anchors.fill: parent
            source: globalStyle.imageUrl + "/VehicleFunction/Climate/img_clm_manual_fan_distribution_middle" + globalStyle.skinUrl + ".png"
            visible: airFields.visible && climateModel.fanMiddle
        }
        Image {
            anchors.fill: parent
            source: globalStyle.imageUrl + "/VehicleFunction/Climate/img_clm_manual_fan_distribution_bottom" + globalStyle.skinUrl + ".png"
            visible: airFields.visible && climateModel.fanBottom
        }
    }

Image {
    id: staticFanManImages
    anchors.horizontalCenter: parent.horizontalCenter
    visible: if (climateModel.fanTop || climateModel.fanMiddle || climateModel.fanBottom) { false } else { true }
    source: climateModel.fanOff ? globalStyle.imageUrl + "/VehicleFunction/Climate/img_clm_fan_off" + globalStyle.skinUrl + ".png" : globalStyle.imageUrl + "/VehicleFunction/Climate/img_clm_fanman" + globalStyle.skinUrl + ".png"
}

    states: [
        State {
            name: "large"
            PropertyChanges {
                target: scaling
                xScale: 1
                yScale: 1
            }
            PropertyChanges {
                target: fan
                scale: 1.0
                opacity: 0//climateModel.fanOff ? 0.05 : 1
                x: -88//-132
                y: -410//-448
                //y: 348
            }
            PropertyChanges {
                target: man
                scale: 1.0
                opacity: 0//climateModel.fanOff ? 0.05 : 1
                x: 92//52
                y: -456//-492
                //y: 292
            }
            PropertyChanges {
                target: staticFanManImages
                opacity: 0
            }
            PropertyChanges {
                target: airFields
                opacity: 0
            }
            PropertyChanges {
                target: cleanZone
                opacity: 0
            }
        }
    ]
    transitions: [
        Transition {
            from: ""
            to: "large"
            ParallelAnimation {
                NumberAnimation { target: staticFanManImages; property: "opacity"; duration: 0; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.15, 0.0), (0.15, 1.0), (1.0, 1.0)]} }
                NumberAnimation { target: scaling; property: "xScale"; duration: 300; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]} }
                NumberAnimation { target: scaling; property: "yScale"; duration: 300; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]} }
               NumberAnimation { property: "scale"; duration: 300; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]} }
                NumberAnimation { property: "x"; duration: 300; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]} }
                NumberAnimation { property: "y"; duration: 300; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]} }
                SequentialAnimation {
                    PauseAnimation { duration: 150 }
                    ParallelAnimation {
            NumberAnimation { target: airFields; property: "opacity"; duration: 150; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.15, 0.0), (0.15, 1.0), (1.0, 1.0)]} }
                     }
                    NumberAnimation { target: man; property: "opacity"; duration: 0; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.15, 0.0), (0.15, 1.0), (1.0, 1.0)]} }
                    NumberAnimation { target: fan; property: "opacity"; duration: 0; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.15, 0.0), (0.15, 1.0), (1.0, 1.0)]} }
                }
            }
        },
        Transition {
            from: "large"
            to: ""
                    SequentialAnimation {
                        ParallelAnimation {
                            NumberAnimation { target: scaling; property: "xScale"; duration: 300; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]} }
                            NumberAnimation { target: scaling; property: "yScale"; duration: 300; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]} }
                            NumberAnimation { property: "scale"; duration: 300; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]} }
                        NumberAnimation { property: "x"; duration: 300; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]} }
                        NumberAnimation { property: "y"; duration: 300; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]} }
                        }
                        ParallelAnimation {
                            NumberAnimation { target: airFields; property: "opacity"; duration: 150; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.15, 0.0), (0.15, 1.0), (1.0, 1.0)]} }
                            NumberAnimation { target: staticFanManImages; property: "opacity"; duration: 150; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.15, 0.0), (0.15, 1.0), (1.0, 1.0)]} }
               }
            }
        }
    ]
}
