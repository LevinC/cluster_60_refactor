/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import "../Common"

Item {
    id: root
    property string side: "right"
    property string sideAbb: side === "left" ? "L" : "R"
    property int level: 0

    signal clicked

    width: childrenRect.width
    height: childrenRect.height

    property bool _active: false

    opacity: _active ? 0.7 : 1;
    scale: _active? 0.9 : 1

    Behavior on opacity {
        NumberAnimation { duration: 100 }
    }

    Image {
        id: icon
        source: level === 0 ? globalStyle.imageUrl + "/VehicleFunction/Climate/img_clm_seat_off_"+ sideAbb + globalStyle.skinUrl + ".png" : globalStyle.imageUrl + "/VehicleFunction/Climate/img_clm_seat_heat_"+ sideAbb + globalStyle.skinUrl + ".png"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        //transform: Scale { origin.x: icon.width / 2; xScale: leftSeat ? 1 : -1 }
    }
    Image {
        id: levelIndication
        source: level > 0 ? globalStyle.imageUrl + "/VehicleFunction/Climate/img_clm_seat_indication_heat_"+  level + "_" + side + ".png" : ""
        anchors {
            left: side === "left" ? parent.left : undefined
            right: side === "left" ? undefined : parent.right
            leftMargin: 100
            rightMargin: 100
            top: parent.top
            topMargin: 56
        }
    }

    MouseArea {
        anchors.fill: parent
        onPressed: parent._active = true;
        onReleased: parent._active = false;
        onCanceled: parent._active = false;
        onClicked: {
            parent._active = false;
            parent.clicked()
        }
    }
}

///* Copyright (c) 2014 Volvo Car Corporation
// * All rights reserved.
// */

//import QtQuick 2.0
//import "../../../common/"

//Item {
//    id: root
//    property bool leftSeat: false
//    property int level: 0

//    signal clicked

//    width: childrenRect.width
//    height: childrenRect.height

//    property bool _active: false

//    Image {
//        id: icon
//        source: globalStyle.imageUrl + "/climate_seat_level_" + level + ".png"
//        anchors.horizontalCenter: parent.horizontalCenter
//        anchors.bottom: parent.bottom
//        transform: Scale { origin.x: icon.width / 2; xScale: leftSeat ? 1 : -1 }
//        opacity: _active ? 0.7 : 1;
//        scale: _active? 0.9 : 1
//        Behavior on opacity {
//            NumberAnimation { duration: 100 }
//        }

//    }

//    MouseArea {
//        anchors.fill: parent
//        onPressed: parent._active = true;
//        onReleased: parent._active = false;
//        onCanceled: parent._active = false;
//        onClicked: {
//            parent._active = false;
//            parent.clicked()
//        }
//    }
//}
