/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import QtGraphicalEffects 1.0
import "."

MouseArea {
    id: root
    width: globalStyle.drawerwidth
    height: 100

    property bool selectable: false
    property bool selected: false
    property bool centeredIcon: iconSource === "" ? false : true

    property bool invisible: true

    property alias textSize: buttonText.font.pixelSize
    property alias buttonText:buttonText
    property alias bgRect:bgRect

    property string buttonType: "center"
    property string buttonColor: "flatbtn"

    property string selectedButtonColor: ""
    property string bgColor:"transparent"

    property string text: ""
    property string iconSource: source.length!==0 ? globalStyle.imageUrl + source + globalStyle.skinUrl + ".png" : ""
    property string source: ""
    property string selectedIconSource: ""

    onClicked: if (selectable) selected = !selected

    // Don't let underlaying items get the mouse events when the button is disabled
    MouseArea {
        anchors.fill: root
        onPressed: {
            mouse.accepted = !root.enabled
        }
    }


    Rectangle{
        id:bgRect
        anchors.fill: root
        color: bgColor
    }

    Rectangle {
        id: colorRect
        anchors.fill: root
        color: selectedButtonColor
        opacity: 0
    }
    BorderImage {
        id: borderimage1
        anchors.fill: root
        border { left: 10; right: 10; top: 9; bottom: 12;}
        anchors.rightMargin: -5
        anchors.leftMargin: -5
        anchors.topMargin: -4
        anchors.bottomMargin: -7
        horizontalTileMode: BorderImage.Stretch
        source: globalStyle.imageUrl + "/Button/Flat/img_com_" + buttonColor + globalStyle.componentSkinUrl + "_" + buttonType + "_OFF.png"

        opacity: invisible ? 0 : 1
    }

    Image {
        id: fgImage
        width: 84
        height: 84
        anchors.horizontalCenter: root.horizontalCenter
        anchors.verticalCenter: root.verticalCenter
        source: selected && enabled ? root.selectedIconSource: root.iconSource
        visible: selected && enabled && selectable ? false : true
    }
    ColorOverlay {
        id: colorOverlayIcon
        anchors.fill: fgImage
        source: fgImage
        color: root.selectedButtonColor
        visible: selected && enabled && selectable ? true : false
    }

    SpaText {
        id: buttonText
        anchors.centerIn: root
        anchors.verticalCenterOffset: iconSource !== "" && text !== "" ? 0.3*root.height : 0
        width: 145
        text: root.text
        textStyle: buttonLabelTextDefault
        font.pixelSize: iconSource === "" ? 24 : 16
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        wrapMode: Text.WordWrap
    }

    states: [
        State {
            name: "pressed"
            when: pressed && enabled

            PropertyChanges {
                target: borderimage1
                source: globalStyle.imageUrl + "/Button/Flat/img_com_" + buttonColor + globalStyle.componentSkinUrl + "_" + buttonType + "_PRS.png"
                opacity: selectedButtonColor === ""  ? 1 : 0
            }
            PropertyChanges {
                target: colorRect
                opacity:  selectedButtonColor !== "" || buttonColor === "flatbtn" ? 1 : 0
            }
        },
        State {
            name: "active"
            when: !pressed && selected && enabled && !selectable

            PropertyChanges {
                target: borderimage1
                source: globalStyle.imageUrl + "/Button/Flat/img_com_" + buttonColor + globalStyle.componentSkinUrl + "_" + buttonType + "_ON.png"
                opacity: selectedButtonColor === ""  ? 1 : 0
            }
            PropertyChanges {
                target: colorRect
                opacity:  selectedButtonColor !== "" || buttonColor === "flatbtn" ? 0.5 : 0
            }
        },
        State {
            name: "unavailable"
            when: !enabled

            PropertyChanges {
                target: buttonText
                opacity: 0.2
            }
            PropertyChanges {
                target: fgImage
                opacity: 0.2
            }
            PropertyChanges {
                target: borderimage1
                source: globalStyle.imageUrl + "/Button/Flat/img_com_" + buttonColor + globalStyle.componentSkinUrl + "_" + buttonType + "_OFF.png"
                opacity: invisible ? 0 : 1
            }
        }
    ]

    transitions: [
        Transition {
            from: ""
            to: "unavailable"
            reversible: true
            animations: [
                NumberAnimation { property: "opacity"; duration: 250 }
            ]
        }
    ]
}
