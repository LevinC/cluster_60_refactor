
import QtQuick 2.0
import "."

Item {
    id: root

    property alias header: header1.text

    property VisualItemModel tabs

    property alias currentTab: tabViewContainer.currentIndex
    property alias currentTabItem: tabViewContainer.currentItem
    property alias tabsInteractive: tabViewContainer.interactive

    property alias tabWidth: tabViewContainer.width
    property alias tabHeight: tabViewContainer.height

    property bool visibleSearchIcon: true
    property bool visiblearrow: true

    property string highlightColor: "white"

    function _updateKeyboardState() {
        if (keyboard !== null && tabViewContainer.currentItem)
            keyboard.show(visible && tabViewContainer.currentItem.hasKeyBoard, tabViewContainer.currentItem)
    }

    onVisibleChanged: _updateKeyboardState()

    SpaText {
        id: header1
        textStyle: homeScreenHeader
        x: 50
        y: 62
        color: highlightColor
    }
    Item {
        id: tabBar
        width: parent.width - 20
        height: 81
        anchors.top: parent.top
        anchors.topMargin: 64
        anchors.left: parent.left

        ListView {
            id: tabtitles
            width: parent.width
            height: 39
            anchors.bottom: tabBar.bottom
            anchors.left: parent.left
            anchors.leftMargin: 50
            model: tabs ? tabs.count : 0
            delegate: tabBarItem
            orientation: ListView.Horizontal
            highlight: Item {
                Rectangle {
                    width: parent.width-32
                    height: 2
                    color:  highlightColor
                    anchors.top: parent.top
                    anchors.topMargin: 37
                }
            }
            highlightFollowsCurrentItem: true
            highlightRangeMode: ListView.ApplyRange
            preferredHighlightBegin: 200
            preferredHighlightEnd: 600
            currentIndex: tabViewContainer.currentIndex
            interactive: false
        }
    }

    SpaDivider {
        id: tabBarDivider
        headerDivider: true
        anchors.top: tabBar.bottom
    }

    Image {
        id:leftArrow
        anchors {
            bottom:tabBar.bottom
            bottomMargin: -3
            left: parent.left
            leftMargin: 0//10
        }
        source: globalStyle.imageUrl + "/PagegroupeView/img_hmc_pgv_arrow_left" + globalStyle.skinUrl + ".png"
        visible: visiblearrow == false || tabViewContainer.currentIndex == 0 ? false : true
    }
    Image {
        id:rightArrow
        anchors {
            bottom:tabBar.bottom
            bottomMargin: -3
            right: parent.right
            rightMargin: 0//10
        }
        source: globalStyle.imageUrl + "/PagegroupeView/img_hmc_pgv_arrow_right" + globalStyle.skinUrl + ".png"
        visible: visiblearrow == false || tabtitles.currentIndex == tabs.count-1 ? false : true
    }

    ListView {
        id: tabViewContainer
        width: parent.width
        height: parent.height
        anchors.top:tabBar.bottom
        anchors.topMargin: 3
        clip: true
        model: tabs
        orientation: ListView.Horizontal
        snapMode: ListView.SnapOneItem
        currentIndex: tabtitles.currentIndex
        highlightRangeMode: ListView.StrictlyEnforceRange
        boundsBehavior: Flickable.StopAtBounds
        interactive: true
        highlightMoveDuration: 100

        onCurrentItemChanged: {
            _updateKeyboardState()
        }

        Connections {
            target: tabViewContainer.currentItem
            onMovementStarted: _updateKeyboardState()
        }
    }

    SpaScrollBar {
        id: scrollBar

        width: 8
        height: 680

        anchors.top: tabBarDivider.bottom
        anchors.right: parent.right
        anchors.rightMargin: 31

        flickable: tabViewContainer.currentItem
        opacity: !tabViewContainer.moving
        Behavior on opacity {NumberAnimation {duration: 100}}
    }

    SpaLetterPicker {

        property bool hasLetterPicker: tabViewContainer.currentItem && tabViewContainer.currentItem.hasLetterPicker

        anchors {
            top: tabBarDivider.bottom
            bottom: parent.bottom
            bottomMargin: 90
        }
        pressedColor: highlightColor
        listPage: hasLetterPicker ? tabViewContainer.currentItem : null
        opacity: !tabViewContainer.moving
        Behavior on opacity {NumberAnimation {duration: 100}}

        visible: hasLetterPicker
    }

    Component {
        id: tabBarItem
        Item {
            height: 39
            width: index > 0 || visibleSearchIcon===false ? tabText.implicitWidth + 32 :  25+32
            Image {
                id: searchicon
                source: tabViewContainer.currentIndex == 0 ? globalStyle.imageUrl + "/Icons/img_hmc_search_pgv_ON" + globalStyle.skinUrl + ".png" : globalStyle.imageUrl + "/Icons/img_hmc_search_pgv_OFF" + globalStyle.skinUrl + ".png"
                width: 64
                height: 64
                anchors.right:  parent.right
                anchors.rightMargin: 12
                anchors.verticalCenter: parent.verticalCenter
                anchors.verticalCenterOffset: -10
                visible: index > 0 || visibleSearchIcon===false ? false: true
            }
            SpaText {
                id: tabText
                text: tabs.children[index].subheader
                textStyle: index === tabViewContainer.currentIndex ? pageGroupViewActive : pageGroupViewDefault
                anchors.left: parent.left

                // This renders wrong font when running from cpp_runtime
                //  Behavior on font.pixelSize {
                //      NumberAnimation { duration: 300; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]} }
                //  }

                Behavior on color {
                    ColorAnimation  { duration: 300; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]} }
                }

                Behavior on text {
                    SequentialAnimation {
                        NumberAnimation { target: root; properties: "opacity"; to: 0; duration: 130; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]} }
                        PropertyAnimation {target: root; properties: "text"; duration: 0 }
                        NumberAnimation { target: root; properties: "opacity"; to: 1.0; duration: 70;  }
                    }
                }
            }
            MouseArea {
                anchors.fill: parent
                onClicked: tabViewContainer.currentIndex = index;
                enabled: tabs.children[index].enabled
            }
        }
    }
}
