/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import QtGraphicalEffects 1.0
import "."

// TODO rebase all spa buttons on SpaButton to get rid of copy/paste syndrom
SpaButton {
    id: root
    width: 232
    height: 110

    property bool selectable: false
    property bool selected: false
    property bool hideDiod: false

    property string text: ""
    property alias textwidth: buttonText.width

    property string iconSource: ""

    onClicked: if (selectable) selected = !selected

    Image {
        id: fgImage
        width: 84
        height: 84
        opacity: 1
        anchors.left: parent.left
        anchors.leftMargin: 17
        anchors.verticalCenter: parent.verticalCenter
        source: parent.iconSource
    }
    Image {
        id: diode
        anchors.left: parent.left
        anchors.leftMargin: 11
        anchors.verticalCenter: parent.verticalCenter
        source: globalStyle.imageUrl + "/HomeScreen/VehicleFunctionPane/img_hmc_diod_indication_OFF.png"
        visible: !hideDiod
    }
    SpaText {
        id: buttonText
        textStyle: helpText
        text: parent.text
        color:"white"
        smooth: true
        opacity: 1
        visible: false
        width: root.width+textShadow.radius*2
        height: root.height + textShadow.radius*2
        anchors.left: fgImage.right
        anchors.leftMargin: -6
        anchors.verticalCenter: parent.verticalCenter
        verticalAlignment: Text.AlignVCenter
        wrapMode: Text.Wrap
    }

    DropShadow {
        id: textShadow
        anchors.fill: source
        source: buttonText

        fast: true
        cached: true

        horizontalOffset: 0
        verticalOffset: 2
        radius: 2
        samples: 4
    }

    states: [
        State {
            name: "Pressed"
            when: pressed && enabled
            PropertyChanges {target: bg; source: globalStyle.imageUrl + "/button_9patches/9PATCH_BUTTON_Standalone_2.png" }
        },
        State {
            name: "Active"
            when: selected && enabled
            PropertyChanges {target: diode; source: globalStyle.imageUrl + "/HomeScreen/VehicleFunctionPane/img_hmc_diod_indication_ON.png" }
        },

        State {
            name: "Unavailable"
            when: !enabled
            PropertyChanges { target: buttonText; opacity: 0.2 }
            PropertyChanges { target: textShadow; opacity: 0.2 }
            PropertyChanges { target: fgImage; opacity: 0.2 }
        }

    ]
}
