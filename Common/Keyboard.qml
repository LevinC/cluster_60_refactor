/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import "../Common/"
import "../"

Item {
    id: root
    height: globalStyle.screenHeight
    width: globalStyle.screenWidth

    property alias inputfieldbg:inputfieldbg

    property SpaStyle globalStyle: SpaStyle{}

    property bool shift: false
    property bool caps: false
    property Item page: SpaPage { id: defaultPage }

    property var topRow_en: ListModel {
        ListElement { large: 'Q'; small: 'q' }
        ListElement { large: 'W'; small: 'w' }
        ListElement { large: 'E'; small: 'e' }
        ListElement { large: 'R'; small: 'r' }
        ListElement { large: 'T'; small: 't' }
        ListElement { large: 'Y'; small: 'y' }
        ListElement { large: 'U'; small: 'u' }
        ListElement { large: 'I'; small: 'i' }
        ListElement { large: 'O'; small: 'o' }
        ListElement { large: 'P'; small: 'p' }
    }
    property var middleRow_en: ListModel {
        ListElement { large: 'A'; small: 'a' }
        ListElement { large: 'S'; small: 's' }
        ListElement { large: 'D'; small: 'd' }
        ListElement { large: 'F'; small: 'f' }
        ListElement { large: 'G'; small: 'g' }
        ListElement { large: 'H'; small: 'h' }
        ListElement { large: 'J'; small: 'j' }
        ListElement { large: 'K'; small: 'k' }
        ListElement { large: 'L'; small: 'l' }
    }
    property var bottomRow_en: ListModel {
        ListElement { large: 'Z'; small: 'z' }
        ListElement { large: 'X'; small: 'x' }
        ListElement { large: 'C'; small: 'c' }
        ListElement { large: 'V'; small: 'v' }
        ListElement { large: 'B'; small: 'b' }
        ListElement { large: 'N'; small: 'n' }
        ListElement { large: 'M'; small: 'm' }
        ListElement { large: ','; small: ',' }
        ListElement { large: '.'; small: '.' }
    }

    signal key(string key)
    signal backspace()
    signal erase()

    // Show keyboard.
    function show(show, page) {
        if (show) {
            if (page !== root.page) {
                if (!page) page = root.defaultPage
                // rebind keyboard to new page
                root.page.keyboardText = root.page.keyboardText // removes binding
                textinput.text = page.keyboardText
                page.keyboardText = Qt.binding(function(){return textinput.text})
                root.page = page
            }
            if (textinput.text) {
                state = "hidden"
            }
            else {
                state = ""
            }
        }
        else {
            state = "hidden"
        }
    }

    onKey: {
        textinput.text += key
        shift = caps
    }

    onErase: textinput.text = ""
    onBackspace: textinput.text = textinput.text.slice(0,textinput.text.length-1)


    Item {
        id: keyboard
        width:globalStyle.screenWidth
        height: globalStyle.screenHeight
        anchors.bottom: parent.bottom
        opacity: 1
        visible: true

        Image {
            width: globalStyle.screenWidth
            anchors.bottom: keyboard.bottom
            anchors.bottomMargin: -209
            source: globalStyle.imageUrl + "/OnScreenKeyboard/img_hmc_keyboard_bg" + globalStyle.skinUrl + ".png"
        }

        MouseArea {
            id: mouseEater
            anchors {
                fill: parent
                topMargin: 590
            }
        }
        BorderImage {
            //Needs to be below the keyboardbuttons so that the press_event comes over the toolbar.
            id: toolbar
            width: globalStyle.screenWidth-10
            height: 84
            y: 588
            z: 0
            anchors.horizontalCenter: parent.horizontalCenter
            border { left: 10; right: 10; top: 42; bottom: 43;}
            horizontalTileMode: BorderImage.Stretch
            source: globalStyle.imageUrl + "/Button/3D/img_com_3Dbtn_S0_standalone_OFF.png"

            BorderImage {
                id: inputfieldbg
                width: 458
                height: 60
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                anchors.verticalCenterOffset: -2
                border { left: 5; right: 5; top: 5; bottom: 5;}
                horizontalTileMode: BorderImage.Stretch
                source: globalStyle.imageUrl + "/HMI_Core/img_hmc_inputfield_ON.png"
                opacity: 1
                TextInput {
                    id: textinput
                    anchors {
                        verticalCenter: parent.verticalCenter
                        left: parent.left
                        leftMargin: 25
                    }
                    text: ""
                    color: globalStyle.greyLvl1
                    width: 380
                    font.pixelSize: 24
                    selectByMouse: false //changed to false because it selects too easily
                    selectedTextColor: globalStyle.themeColorFont
                    selectionColor: "transparent"
                    cursorVisible: true
                    clip: true
                    activeFocusOnPress: false
                }

                MouseArea {
                    id: clickinputfield
                    anchors.fill: inputfieldbg
                    enabled: false
                    onClicked: root.state=""
                }
                Image {
                    source: globalStyle.imageUrl + "/Icons/img_hmc_delete_input.png"
                    anchors {
                        right: parent.right
                        rightMargin: -10
                        verticalCenter: parent.verticalCenter
                    }
                    MouseArea {
                        id: erasebutton
                        width: 74
                        height: 84
                        anchors.centerIn: parent
                        enabled: true
                        onClicked: root.erase()
                    }
                }
            }
        }

        Column {
            id: allrows
            spacing: -4
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10
            anchors.left: parent.left
            anchors.leftMargin: 10
            width: 758
            Column {
                id: firstthreerows
                spacing: 0
                Row {
                    id: row1
                    spacing: 0
                    Repeater {
                        model: topRow_en
                        Spa9patchKeyboardButton {
                            width: 75
                            height: 84
                            text: caps || shift ? large : small
                            popupText: caps || shift ? large : small
                            onClicked: root.key(text)
                        }
                    }
                }
                Row {
                    id: row2
                    spacing: 0
                    Item {
                        id: keyboardfills
                        width: 37
                        height: 84

                        BorderImage {
                            anchors {
                                fill: parent
                                rightMargin:  -4
                                leftMargin:   -4
                                bottomMargin: -4
                                topMargin:    -4
                            }
                            border { left: 5; right: 5; top: 38; bottom: 37;}
                            horizontalTileMode: BorderImage.Stretch
                            source: globalStyle.imageUrl + "/Button/3D/img_com_3Dbtn_S0_standalone_OFF.png"
                        }
                    }
                    Repeater {
                        model: middleRow_en
                        Spa9patchKeyboardButton {
                            width: 75
                            height: 84
                            text: caps || shift ? large : small
                            popupText: caps || shift ? large : small
                            onClicked: root.key(text)
                        }
                    }
                    Item {
                        id: keyboardfills2
                        width: 37
                        height: 84

                        BorderImage {
                            anchors {
                                fill: parent
                                rightMargin:  -4
                                leftMargin:   -4
                                bottomMargin: -4
                                topMargin:    -4
                            }
                            border { left: 5; right: 5; top: 38; bottom: 37;}
                            horizontalTileMode: BorderImage.Stretch
                            source: globalStyle.imageUrl + "/Button/3D/img_com_3Dbtn_S0_standalone_OFF.png"
                        }
                    }
                }
                Row {
                    id: row3
                    spacing: 0
                    Item {
                        id: keyboardfills3
                        width: 37
                        height: 84
                        BorderImage {
                            anchors {
                                fill: parent
                                rightMargin:  -4
                                leftMargin:   -4
                                bottomMargin: -4
                                topMargin:    -4
                            }
                            border { left: 5; right: 5; top: 38; bottom: 37;}
                            horizontalTileMode: BorderImage.Stretch
                            source: globalStyle.imageUrl + "/Button/3D/img_com_3Dbtn_S0_standalone_OFF.png"
                        }
                    }

                    Repeater {
                        model:bottomRow_en
                        Spa9patchKeyboardButton {
                            width: 75
                            height: 84
                            text: caps || shift ? large : small
                            popupText: caps || shift ? large : small
                            onClicked: root.key(text)
                        }
                    }

                    Item {
                        id: keyboardfills4
                        width: 37
                        height: 84
                        BorderImage {
                            anchors {
                                fill: parent
                                rightMargin:  -4
                                leftMargin:   -4
                                bottomMargin: -4
                                topMargin:    -4
                            }
                            border { left: 5; right: 5; top: 38; bottom: 37;}
                            horizontalTileMode: BorderImage.Stretch
                            source: globalStyle.imageUrl + "/Button/3D/img_com_3Dbtn_S0_standalone_OFF.png"
                        }
                    }
                }
            }
            Row {
                id: row4
                Spa9PatchButton {
                    width: 74
                    height: 84
                    iconSource: globalStyle.imageUrl + "/Icons/img_hmc_hidekeyboard.png"
                    buttonType: "center"
                    onClicked: root.state= "hidden"
                }
                Spa9PatchButton {
                    width: 74
                    height: 84
                    iconSource: globalStyle.imageUrl + "/Icons/img_hmc_shift.png"
                    selectedIconSource: globalStyle.imageUrl + "/Icons/img_hmc_shift.png"
                    buttonType: "center"
                    selectable: true
                    onClicked: {
                        shift=!shift
                        if (caps == true)
                            caps=false
                        selected = false }
                    onPressAndHold: {
                        caps=true
                        selected = true
                    }
                }
                Spa9PatchButton {
                    width: 74
                    height: 84
                    iconSource: globalStyle.imageUrl + "/ICON_123.png"
                    buttonType: "center"
                    onClicked: coreScreen.notAvailableNotify()
                }
                Spa9PatchButton {
                    width: 263+74+2
                    height: 84
                    text: "SPACE"
                    textSize: 31
                    buttonType: "center"
                    onClicked: {
                        root.key(" ");
                    }
                }
                Spa9PatchButton {
                    width: 114
                    height: 84
                    iconSource: globalStyle.imageUrl + "/Icons/img_hmc_erase.png"
                    buttonType: "center"
                    onClicked: {
                        root.backspace();
                    }
                }

                Spa9PatchButton {
                    width: 74
                    height: 84
                    iconSource: globalStyle.imageUrl + "/Icons/img_hmc_writing_scribble.png"
                    buttonType: "center"
                    onClicked: coreScreen.notAvailableNotify()
                }
            }
        }
    }

    states: [
        State {
            name: "hidden"
            PropertyChanges {
                target: root
                visible: false
            }
            PropertyChanges {
                target: keyboard
                anchors.bottomMargin: -435
                opacity: 0
            }
            PropertyChanges {
                target: toolbar
                y: globalStyle.screenHeight
                opacity: 0
            }
            PropertyChanges {
                target: inputfieldbg
                source: globalStyle.imageUrl + "/HMI_Core/img_hmc_inputfield_OFF.png"
                opacity: 0
            }
            PropertyChanges {
                target: textinput
                cursorVisible: false
                selectByMouse: false
            }
        }
    ]

    transitions: [
        Transition {
            from: ""; to: "hidden"
            SequentialAnimation {
                ParallelAnimation {
                    PropertyAnimation {target: keyboard; property: "anchors.bottomMargin"; duration: 200;}
                    PropertyAnimation {target: toolbar; property: "y"; duration: 200;}
                    PropertyAnimation {target: toolbar; property: "opacity"; duration: 200}
                    SequentialAnimation {
                        PropertyAnimation {target: inputfieldbg; property: "opacity"; from: 1; to: 0; duration: 130; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]}}
                        PropertyAction {target: inputfieldbg; property: "source"}
                        PropertyAnimation {target: inputfieldbg; property: "opacity"; from: 0; to: 1; duration: 70}
                    }
                }
                PropertyAnimation {target: keyboard; property: "opacity"; duration: 200}
                PropertyAction {target: root; property: "visible"}
            }
        },
        Transition {
            from: "hidden"; to: ""
            ParallelAnimation {
                PropertyAnimation {target: keyboard; property: "anchors.bottomMargin"; duration: 200; }
                PropertyAnimation {target: toolbar; property: "y"; duration: 200; }
                SequentialAnimation {
                    PropertyAnimation {target: toolbar; property: "opacity"; duration: 130; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]}}
                    PropertyAction {target: inputfieldbg; property: "source"}
                    PropertyAnimation {target: inputfieldbg; property: "opacity"; from: 0; to: 1; duration: 70}
                    PropertyAction {target: textinput; property: "cursorVisible";}
                }
            }
        }
    ]
    Component.onCompleted: state = "hidden"
}
