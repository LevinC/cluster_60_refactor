import QtQuick 2.0

import "../"

ListView {
    id:sideMenu
    width: globalStyle.drawerwidth
    opacity:0
    interactive: false

    states: [
        State {
            name: "expanded"
            PropertyChanges {target:sideMenu; opacity:1}
        },
        State {
            name: "fullScreen"
            PropertyChanges {target:sideMenu; opacity:1}
        }
    ]

    transitions: [
        Transition {
            CubicAnimation{target:sideMenu; property:'opacity'}
        }
    ]

}

