/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import QtGraphicalEffects 1.0
import "."

MouseArea {
    id: root

    width: 75
    height: 84

    property alias text: buttonText.text
    property alias popupText: popupText.text

    BorderImage {
        id: bg
        height: parent.height
        width: parent.width
        anchors {
            bottom: parent.bottom
            right: parent.right
            left: parent.left
            top: parent.top
            rightMargin:  -4
            leftMargin:   -4
            bottomMargin: -4
            topMargin:    -4
        }
        border {
            left: 10
            right: 10
            top: 42
            bottom: 43
        }
        opacity: 1
        horizontalTileMode: BorderImage.Stretch

        source: globalStyle.imageUrl + "/Button/3D/img_com_3Dbtn_S0_center_OFF.png"
    }
    Image {
        id: popover
        anchors {
            bottom: parent.bottom
            bottomMargin: 3
            right: parent.right
            rightMargin: 1
            left: parent.left
            leftMargin: 1
        }
        height: 152
        width: parent.width
        source: globalStyle.imageUrl + "/OnScreenKeyboard/img_hmc_keyboard_popup_bg.png"

        opacity: 0
        visible: opacity != 0
    }
    SpaText {
        id: popupText
        width: parent-10
        font.family: infoText.family
        font.pixelSize: 32
        color: globalStyle.esStyle ? globalStyle.themeColorFont : globalStyle.white
        horizontalAlignment: Text.AlignHCenter
        anchors.horizontalCenter: parent.horizontalCenter
        y: -60
        visible: false
        opacity: 0
        wrapMode: Text.Wrap
    }

    DropShadow {
        id: popupTextShadow
        anchors.fill: source
        horizontalOffset: 0
        verticalOffset: 2
        radius: 2
        samples: 4
        source: popupText
        opacity: 0
    }

    Item {
        id: content
        anchors.centerIn: parent

        SpaText {
            id: buttonText
            width: parent-10
            font.family: infoText.family
            font.pixelSize: 32
            color: globalStyle.white
            horizontalAlignment: Text.AlignHCenter
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            //This is to achieve the correct placement when you have both text and an icon. This is for buttons that have icon above the text.
            anchors.verticalCenterOffset: 0
            //
            visible: false
            opacity: 1
            wrapMode: Text.Wrap
        }
        DropShadow {
            id: textShadow
            anchors.fill: source
            horizontalOffset: 0
            verticalOffset: 2
            radius: 2
            samples: 4
            source: buttonText
            opacity: 1
        }
    }

    states: [
        State {
            name: "Pressed"
            when: pressed && enabled
            PropertyChanges { target: popover; opacity: 1 }
            PropertyChanges { target: buttonText; opacity: 0.2 }
            PropertyChanges {target: textShadow; opacity: 0.2 }
            PropertyChanges {target: popupText; opacity: 1 }
            PropertyChanges {target: popupTextShadow; opacity: 1 }
        },
        State {
            name: "Unavailable"
            when: !enabled
            PropertyChanges { target: buttonText; opacity: 0.2 }
            PropertyChanges { target: textShadow; opacity: 0.2 }
        }
    ]
}
