/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import "../Common/"

Item {
    id: statusPageIndicator
    width: topPane.width
    height: 26

    //spa graphic spec has page indication arrows as one component. And no mousarea.
    property int currentindex: home.currentIndex

    function show() {
        state = "visible";
    }

    function close() {
        state = "closed";
    }

    Image {
        id:pageIndicator
        width: 36
        height: 9
        source: {
            switch (currentindex) {
            case 0:
                return globalStyle.imageUrl + "/HomeScreen/img_hmc_paneindicator" + globalStyle.componentSkinUrl + "_left.png"
            case 1:
                return globalStyle.imageUrl + "/HomeScreen/img_hmc_paneindicator" + globalStyle.componentSkinUrl + "_center.png"
            case 2:
                return globalStyle.imageUrl + "/HomeScreen/img_hmc_paneindicator" + globalStyle.componentSkinUrl + "_right.png"
            }
        }
        anchors.horizontalCenter: statusPageIndicator.horizontalCenter
        anchors.bottom: statusPageIndicator.bottom
        anchors.bottomMargin: 6
    }

    states: [
        State {
            name: "closed"
            PropertyChanges {
                target: statusPageIndicator
                opacity: 0
            }
        },
        State {
            name: "visible"
            PropertyChanges {
                target: statusPageIndicator
                opacity: 1
            }
        }
    ]

    transitions: [
        Transition {
            to: "closed"
                NumberAnimation { target: statusPageIndicator; property: "opacity"; duration: 130; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]} }
        },
        Transition {
            to: "visible"
                NumberAnimation { target: statusPageIndicator; property: "opacity"; duration: 70; }
        }
    ]

}
