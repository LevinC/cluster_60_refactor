/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */
import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.0

import "."
import "../dummydata/"

Item {
    id: root

    property SpaListPage listPage: parent

    property var _segments

    function _updateSegments() { if (listPage && listPage.model) _segments = mediaModel.getSegments(listPage.model); else _segments = null }

    property string pressedColor: "#18ffffff"/*switch (widgetId) {
                                    case 0: globalStyle.firstColor
                                        break
                                    case 1: globalStyle.secondColor
                                        break
                                    case 2: globalStyle.thirdColor
                                        break
                                    case 3: globalStyle.fourthColor
                                    }*/

    onListPageChanged: _updateSegments()

    Connections {
        target: listPage
        onCountChanged: _updateSegments()
    }

    Slider {
        id: slider

        anchors {
            top: parent.top
            bottom: parent.bottom
        }
        width: 50

        activeFocusOnPress: true
        orientation: Qt.Vertical

        value: 1
        minimumValue: 1
        maximumValue: _segments ? _segments.length : 1
        onMaximumValueChanged: minimumValue = 1 // weird bug workaround
        stepSize: 1

        updateValueWhileDragging: true

        onValueChanged: if (root._segments) listPage.positionViewAtIndex(mediaModel.getIndexByFirstChar(listPage.model, _segments[_segments.length-value]), ListView.Beginning)

        style: SliderStyle {
            handle: Item{}
            groove: Item {
                implicitWidth: 100
                implicitHeight: slider.width
                Rectangle {
                    id: groove
                    anchors {
                        left: parent.left
                        right: parent.right
                        bottom: parent.bottom
                    }
                    height: 28
                    color: slider.pressed ? pressedColor : "#959595"
                    opacity: slider.pressed ? globalStyle.coloredBgOpacityOne : 0.1
                }
                Row {
                    anchors {
                        right: parent.right
                        bottom: parent.bottom
                    }
                    layoutDirection: Qt.RightToLeft
                    Repeater {
                        model: _segments ? _segments : 0
                        delegate: Item {
                            width: slider.height / root._segments.length
                            height: groove.height
                            SpaText {
                                id: modeldataText
                                anchors.centerIn: parent
                                text: modelData
                                font.pixelSize: 18
                                rotation: 90
                            }
                        }
                    }
                }
            }
        }
    }

    BorderImage {
        x: 255
        y: 200
        width: 222
        height: width
        border { top: 2; bottom: 2; right: 2; left: 2 }
        source: globalStyle.imageUrl + "/Popup/img_hmc_popup" + globalStyle.skinUrl + ".png"
        opacity: slider.pressed ? 1 : 0
        Behavior on opacity { NumberAnimation { duration: 200 } }
        SpaText {
            anchors.centerIn: parent
            text: root._segments && root._segments.length > 0 ? _segments[_segments.length-slider.value] : ''
            font.pixelSize: 120
        }
    }

}
