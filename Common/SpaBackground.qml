import QtQuick 2.0
//Item {
//    Image {
//        id: img1
//        source: globalStyle.imageUrl + "/HomeScreen/Background/img_hmc_bg_S3.png"
//    }
//    Image {
//        anchors.left: img1.right
//        mirror: true
//        source: globalStyle.imageUrl + "/HomeScreen/Background/img_hmc_bg_S3.png"
//    }
//    Image {
//        id: img2
//        anchors.top: img1.bottom
//        source: globalStyle.imageUrl + "/HomeScreen/Background/img_hmc_bg_S3.png"
//    }
//    Image {
//        anchors.top: img2.top
//        anchors.left: img2.right
//        mirror: true
//        source: globalStyle.imageUrl + "/HomeScreen/Background/img_hmc_bg_S3.png"
//    }
//}

Item {
    Image {
        id: img1
        source: globalStyle.imageUrl + "/HomeScreen/Background/img_hmc_bg" + globalStyle.componentSkinUrl + ".png"
    }
    Image {
        anchors.left: img1.right
        mirror: true
        source: globalStyle.imageUrl + "/HomeScreen/Background/img_hmc_bg" + globalStyle.componentSkinUrl + ".png"
    }
    Image {
        id: img2
        rotation: 180
        anchors.top: img1.bottom
        anchors.topMargin: -1
        source: globalStyle.imageUrl + "/HomeScreen/Background/img_hmc_bg" + globalStyle.componentSkinUrl + ".png"
    }
    Image {
        rotation: 180
        anchors.top: img2.top
        anchors.topMargin: -1
        anchors.left: img2.right
        mirror: true
        source: globalStyle.imageUrl + "/HomeScreen/Background/img_hmc_bg" + globalStyle.componentSkinUrl + ".png"
    }
}
