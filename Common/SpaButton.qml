///* Copyright (c) 2014 Volvo Car Corporation
// * All rights reserved.
// */

//import QtQuick 2.0
/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import QtGraphicalEffects 1.0
import "."

MouseArea {
    id: root
    width: globalStyle.normalHeight-globalStyle.tilebuttondiff
    height: 100//globalStyle.recomButtonSize

    property bool selectable: false
    property bool selected: false
    property bool centeredIcon: iconSource === "" ? false : true
    property bool activeCallButton: false
    property bool invisible: true

    //property alias textSize: buttonText.font.pixelSize

    property string buttonType: "center"
    property string buttonColor: "flatbtn"
    property string selectedButtonColor: globalStyle.colors[0]

    property string text: ""
    property string iconSource: ""
    property string selectedIconSource: ""

        property bool carFunctionButton: false

    onClicked: if (selectable) selected = !selected

    // Don't let underlaying items get the mouse events when the button is disabled
    MouseArea {
        anchors.fill: parent
        onPressed: {
            mouse.accepted = !root.enabled
        }
    }
Rectangle {
    id: colorRect
    anchors.fill: parent
    color: selectedButtonColor
    opacity: 0
}

    BorderImage {
        id: borderimage1
        anchors.fill: parent
        source: globalStyle.imageUrl + "/Button/Flat/img_com_" + buttonColor + globalStyle.componentSkinUrl + "_" + buttonType + "_OFF.png"
        border { left: 10; right: 10; top: 9; bottom: 12;}
        anchors.rightMargin: -5
        anchors.leftMargin: -5
        anchors.topMargin: -4
        anchors.bottomMargin: -7
        horizontalTileMode: BorderImage.Stretch

        opacity: invisible ? 0 : 1
    }

    states: [
        State {
            name: "pressed"
            when: pressed && enabled

            PropertyChanges {
                target: borderimage1
                source: globalStyle.imageUrl + "/Button/Flat/img_com_" + buttonColor + globalStyle.componentSkinUrl + "_" + buttonType + "_PRS.png"
                opacity: buttonColor !== "flatbtn" ? 1 : 0
                //
                //visible: false
            }
            PropertyChanges {
                target: colorRect//colorOverlay
                /*visible:*/opacity:  selectedButtonColor !== "" || buttonColor === "flatbtn" ? 1 : 0
            }
        },
        State {
            name: "active"
            when: !pressed && selected && enabled && !selectable

            PropertyChanges {
                target: borderimage1
                source: globalStyle.imageUrl + "/Button/Flat/img_com_" + buttonColor + globalStyle.componentSkinUrl + "_" + buttonType + "_ON.png"
                //visible: true
                opacity: 0
            }

            PropertyChanges {
//                target: colorOverlay
//                visible: selectedButtonColor !== "" || buttonColor === ""
                target: colorRect//colorOverlay
                /*visible:*/opacity:  selectedButtonColor !== "" && !carFunctionButton || buttonColor === "flatbtn" && !carFunctionButton ? 1 : 0
            }
        },
        State {
            name: "unavailable"
            when: !enabled
            PropertyChanges {
                target: borderimage1
                source: globalStyle.imageUrl + "/Button/Flat/img_com_" + buttonColor + globalStyle.componentSkinUrl + "_" + buttonType + "_OFF.png"
                opacity: 1
            }
        }
    ]

    transitions: [
        Transition {
            from: ""
            to: "unavailable"
            reversible: true
            animations: [
                NumberAnimation { property: "opacity"; duration: 250 }
            ]
        }
    ]
}
//import QtGraphicalEffects 1.0
//import "."

//MouseArea {
//    id: root

//    width: 150
//    height: 84

//    property bool selectable: false
//    property bool selected: false
//    property string buttonColor: "3Dbtn"
//    property string buttonType: "standalone"

//    onClicked: if (selectable) selected = !selected

//    BorderImage {
//        id: bg

//        anchors {
//            fill: parent
//            rightMargin:  -5
//            leftMargin:   -5
//            bottomMargin: -7
//            topMargin:    -4
//        }

//        border {
//            left: 10
//            right: 10
//            top: 42
//            bottom: 43
//        }
//        opacity: 1
//        horizontalTileMode: BorderImage.Stretch

//        source: globalStyle.imageUrl + "/Button/3D/img_com_" + buttonColor + globalStyle.componentSkinUrl +"_" + buttonType + "_OFF.png"
//    }

//    states: [
//        State {
//            name: "pressed"
//            when: pressed && !(selected && selectable) && enabled
//            PropertyChanges { target: bg; source: globalStyle.imageUrl + "/Button/3D/img_com_" + buttonColor + "_S0_" + buttonType + "_PRS.png" }
//        },
//        State {
//            name: "selected"
//            when: !pressed && selected && selectable && enabled
//            PropertyChanges { target: bg; source: globalStyle.imageUrl + "/Button/3D/img_com_" + buttonColor + "_S0_" + buttonType + "_ON.png" }
//        },
//        State {
//            name: "Unavailable"
//            when: !enabled
//            PropertyChanges { target: bg; source: globalStyle.imageUrl + "/Button/3D/img_com_" + buttonColor + "_S0_" + buttonType + "_OFF.png" }
//        }
//    ]
//}
