import QtQuick 2.0

import "../"

Item{
    id: activityIndicator

    property alias loadingAnimation:loadingAnim
    opacity: 0
    visible: true
    width:150
    height:width

    Image {
        id: image
        anchors.fill: parent
        source: globalStyle.imageUrl + "/ActivityIndicator/Large/img_hmc_act_large_infinite_0.png"
        rotation: 0
    }

    RotationAnimation {
        id:loadingAnim
        target: image
        property: "rotation"
        from: 0; to: 180
        duration: 900
        direction: RotationAnimation.Clockwise
        onStopped: activityIndicator.state=""
        onStarted: activityIndicator.state="shown"
    }

    states: [
        State {
            name: "shown"
            PropertyChanges {
                target:activityIndicator
                opacity:1
            }
        }
    ]

    transitions: [Transition {CubicAnimation{target:activityIndicator; property:"opacity"; duration:100}}]
}
