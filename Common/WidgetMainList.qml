import QtQuick 2.0
import QtGraphicalEffects 1.0


import "../"

Item {
    id: root
    property var objectModel
    property var listDelegate
    property int currInd
    property string listHeader
    opacity: 0

    SpaText{
        id:listHeaderView
        width: root.width
        x:18
        height: 36
        text:listHeader
        textStyle: infoText
        color:globalStyle.colors[widgetID]
    }

    ListView{
        id:listView
        clip:true
        y: listHeaderView.height
        currentIndex: currInd
        height: root.height - listHeaderView.height
        width:parent.width-x*2
        delegate: listDelegate
        model:objectModel
        opacity:0
    }

    LinearGradient {
        id: mask
        anchors.fill: listView
        gradient: Gradient {
            GradientStop { position: 0.0; color: "white" }
            GradientStop { position: 0.9; color: "white" }
            GradientStop { position: 1.0; color: "transparent" }
        }
        visible: false
    }

    OpacityMask {
        anchors.fill: listView
        source: listView
        maskSource: mask
    }

    SpaScrollBar {
        id: scrollBar
        width: 4
        height: listView.height-4
        anchors.right: parent.right
        flickable: listView
    }

    states: [
        State {
            name: "expanded"
            PropertyChanges {
                target: root
                opacity:1
            }
        }
    ]

    transitions: [
        Transition {
            CubicAnimation{target:root; property:'opacity'}
        }
    ]
}
