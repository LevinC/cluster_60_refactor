import QtQuick 2.0
import "../"

Item {
    id:root
    property int amount
    property real curr
    property string color:""

    Row{
        spacing:4
        Repeater{
            model: amount
            delegate: pageInd
        }
    }

    Component{
        id:pageInd
        Item{
            width:12
            height:12

            Rectangle{
                anchors.fill: parent
                radius: width/2
                color: "black"
            }

            Rectangle{
                anchors.fill: parent
                radius: width/2
                opacity:{
                    var diff = Math.abs((index +0.5) - curr)*.8
                    if (diff >.8)diff=.8
                    return 1 - diff
                }
            }



        }
    }
}
