import QtQuick 2.0

import "../"

Item {
    id:header
    width:parent.width - globalStyle.drawerwidth -24
    x: globalStyle.drawerwidth + 24
    height:globalStyle.normalHeight

    property alias header : h1
    property alias subHeader : h2
    property alias batteryImage:batteryImage

    Column {
        id: headerItem
        height: h1.height + h2.height
        anchors.verticalCenter: parent.verticalCenter
        anchors.verticalCenterOffset: -8

        Row{
            width:parent.width
            anchors.top: parent.top
            SpaText {
                id:h1
                verticalAlignment: Text.AlignVCenter
                textStyle: homeScreenHeader
                font.pixelSize: 44
                Behavior on font.pixelSize { CubicAnimation {} }
            }

            Item  {
                height: 20
                width: 80
                anchors.verticalCenter: parent.verticalCenter
                Image {
                    id:batteryImage
                    visible:false
                    anchors.right: parent.right
                    height: 68
                    width: height
                    anchors.verticalCenter: parent.verticalCenter
                    source: globalStyle.imageUrl + "/Icons/img_tel_btry_lvl3_small" + globalStyle.skinUrl + ".png"
                }
            }
        }
        SpaText {
            id:h2
            width:parent.width
            height:font.pixelSize+2
            anchors.bottom: parent.bottom
            font.pixelSize: 32
            verticalAlignment: Text.AlignVCenter
            textStyle: homeScreenSubHeader
            Behavior on font.pixelSize { CubicAnimation {} }
        }
    }
    states: [
        State {
            name: "expanded"
            PropertyChanges {target: header; height: globalStyle.topExpandedHeight}
            PropertyChanges {target: h1; font.pixelSize:32}
            PropertyChanges {target: h2; font.pixelSize:24}
            PropertyChanges {target: headerItem; anchors.verticalCenterOffset:-28}
        },
        State {
            name: "minimized"
            PropertyChanges {target: header; height: globalStyle.minimizedHeight-globalStyle.bottomMargin}
            PropertyChanges {target: h2; opacity: 0}
            PropertyChanges {target: headerItem; anchors.verticalCenterOffset:16}
            PropertyChanges {target: h1; font.pixelSize:32}
            PropertyChanges {target: h2; font.pixelSize:24}
        },
        State {
            name: "fullScreen"
            PropertyChanges {
                target: header
                height: globalStyle.topExpandedHeight
            }
        }
    ]

    transitions: [
        Transition {
            CubicAnimation{target:header; property:'height'}
            CubicAnimation{target:headerItem; property:'anchors.verticalCenterOffset'}
            CubicAnimation{target:h2; property:'opacity'}
        }
    ]

    Behavior on opacity{ CubicAnimation{}}
}

