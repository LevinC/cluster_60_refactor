/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import "."

// SpaListPage is used instead of SpaPage when showing lists
// It is important that they implement the same interface.
// (QML does not support interfaces, multiple inheritance
// nor mixins AFAIK).

ListView {
    property string subheader
    property bool hasToolBarButton: false
    property bool hasKeyBoard: false
    property bool hasLetterPicker: false
    property string keyboardText

    property string listItemColor

    width: globalStyle.screenWidth; height: width

    snapMode: ListView.SnapToItem

    // allow last item to be scrolled into view without being
    // partially blocked by hierarchy navigation buttons
    footer: Item { height: 84+12 }

    highlightFollowsCurrentItem: true
}
