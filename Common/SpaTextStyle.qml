pragma Singleton
import QtQuick 2.0

// for now TODO fix
import "../"

Item {
    id: root

    FontLoader {
        id: _VolvoSansProMedium
        source: "../ihu/resources/fonts/VolvoSansGlobalMedium.ttf"
        name: "Arial"
    }
    FontLoader {
        id: _VolvoSansProLight
        source: "../ihu/resources/fonts/VolvoSansGlobalLight.ttf"
        name: "Arial"
    }
    FontLoader {
        id: _instrument102
        source: "../ihu/resources/fonts/instrument102.ttf"
    }
    FontLoader {
        id: _instrument103_thinner
        source: "../ihu/resources/fonts/Instrument103_Thinner_20140326.otf"
        name: "Arial"
    }

    // for now TODO fix
    SpaStyle {
        id: globalStyle
    }

    property var homeScreenSubHeader: { 'pixelSize': 28, 'family': _VolvoSansProLight.name, 'color': globalStyle.greyLvl1 }
    property var homeScreenHeader: { 'pixelSize': 38 /*36*/, 'family': _VolvoSansProLight.name, 'color': globalStyle.white }
    property var pageGroupViewDefault: { 'pixelSize': 24, 'family': _VolvoSansProLight.name, 'color': globalStyle.greyLvl1 }
    property var pageGroupViewActive: { 'pixelSize': 24, 'family': _VolvoSansProMedium.name, 'color': globalStyle.greyLvl1 }
    property var listHeader1: { 'pixelSize': 24, 'family': _VolvoSansProLight.name, 'color': globalStyle.greyLvl3 }
    property var listHeader2: { 'pixelSize': 20, 'family': _VolvoSansProLight.name, 'color': globalStyle.greyLvl3 }
    property var listSorter: { 'pixelSize': 24, 'family': _VolvoSansProLight.name, 'color': globalStyle.greyLvl3 }
    property var listItem1: { 'pixelSize': 32, 'family': _VolvoSansProLight.name, 'color': globalStyle.white }
    property var listItem2: { 'pixelSize': 24, 'family': _VolvoSansProLight.name, 'color': "#a8a8a8"}
    property var buttonLabelTextDefault: { 'pixelSize': 24, 'family': _VolvoSansProLight.name, 'color': globalStyle.greyLvl1 }
    property var infoText: { 'pixelSize': 24, 'family': _VolvoSansProLight.name, 'color': globalStyle.greyLvl1 }
    property var helpText: { 'pixelSize': 22, 'family': _VolvoSansProLight.name, 'color': globalStyle.greyLvl3 }
    property var largeTempNumbers: { 'pixelSize': 75, 'family': _instrument103_thinner.name, 'color': globalStyle.greyLvl1 }
    property var smallTempNumbers: { 'pixelSize': 26, 'family': _instrument102.name, 'color': globalStyle.greyLvl1 }
    property var appPaneButtons: { 'pixelSize': 16, 'family': _VolvoSansProLight.name, 'color': globalStyle.white }
}
