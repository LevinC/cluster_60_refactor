import QtQuick 2.0

import "../"
import "../Common"
import "../dummydata"



SpaListItem{
    id: wrapper
    property string iconSource:iconSource
    property string header
    property string subHeader
    property real nowPlayingOpacity
    property bool selected

    onSelectedChanged: {
        if (selected){
            wrapper.state = "selected"
        }else{
            wrapper.state = ""
        }
    }


    height:84
    width: parent.width

    widgetItem: true
    pressedColor: globalStyle.colors[1]

    dividerOpacity: !ListView.view.moving

    Row{
        anchors.fill: parent
        Column{
            height: parent.height
            width: parent.width - coverImage.width
            SpaText {
                id: textLarge
                width: parent.width-x
                x: 18
                height:40
                textStyle: listItem1
                text: header
                elide: Text.ElideRight
            }

            SpaText {
                id: textSmall
                width: parent.width-x
                x: 18
                height:40
                textStyle: listItem2
                text: subHeader
                elide: Text.ElideRight
            }
        }
    }

    Image {
        id: coverImage
        height: parent.height-16
        width: height
        anchors.right: parent.right
        anchors.rightMargin: 8
        anchors.verticalCenter: parent.verticalCenter
        source: iconSource
    }


    Image {
        id: nowPlaying
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        opacity: 0
        source: globalStyle.imageUrl + "/Icons/img_med_now_playing" + globalStyle.skinUrl + ".png"
        Behavior on opacity {
            PropertyAnimation { target: nowPlaying; property: "opacity"; duration: 100; }
        }
    }

    states: [
        State {
            name: "selected"
            PropertyChanges {target: coverImage;opacity: 0}
            PropertyChanges {target: nowPlaying;opacity: 1}
        }
    ]

    transitions: [
        Transition {
            CubicAnimation{target:coverImage; property:'opacity'}
            CubicAnimation{target:nowPlaying; property:'opacity'}
        }
    ]
}
