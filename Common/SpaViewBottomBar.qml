/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import QtGraphicalEffects 1.0
import "."

Item {
    width: globalStyle.screenWidth
    height: 1024

    property bool hasToolBarButton: false
    property bool hasKeyBoard: false
    property string keyboardText

    SpaBackground {
        id: background
        width: globalStyle.screenWidth
        height: 1024
        visible: false
    }

    Image {
        id: mask
        source: globalStyle.imageUrl + "/HIGH_APP_FRAMEWORK_LIST_LAYOUT_opacity_mask.png"

        visible: false
    }

    OpacityMask {
        anchors.fill: background
        source: background
        maskSource: mask
    }

    BorderImage {
        id: keyboardToolbar

        //width: 488
        //width: 668
        width: 600
        height: 84

        anchors {
            right: parent.right
            rightMargin: 25
            //horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom
            bottomMargin: 116
        }

        border { left: 10; right: 10; top: 42; bottom: 42 }

        horizontalTileMode: BorderImage.Stretch

        visible: opacity != 0
        opacity: hasKeyBoard ? 1 : 0
        Behavior on opacity {
            PropertyAnimation {target: keyboardToolbar; property: "opacity"; duration: 200; easing.type: "InSine"}
        }

            source: globalStyle.imageUrl + "/Button/3D/img_com_3Dbtn_S0_standalone_OFF.png"

        BorderImage {
            id: inputfieldbg
            width: 458
            height: 60

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            anchors.verticalCenterOffset: -2

            border { left: 10; right: 10; top: 10; bottom: 10 }

            horizontalTileMode: BorderImage.Stretch

            source: globalStyle.imageUrl + "/HMI_Core/img_hmc_inputfield_OFF.png"

            opacity: 1

            SpaText {
                id: textinput
                anchors {
                    left: parent.left
                    leftMargin: 25
                    verticalCenter: parent.verticalCenter
                }
                text: keyboardText
                textStyle: helpText
                elide: Text.ElideRight
                width: 410
            }

            MouseArea {
                id: clickinputfield
                anchors.fill: inputfieldbg
                onClicked: keyboard.state = ""
            }
        }
    }
}
