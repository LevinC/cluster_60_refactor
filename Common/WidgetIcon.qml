import QtQuick 2.0

import "../"


Item {
    id:icon

    property int minimizedIconSize: globalStyle.widgetIconSmallSize
    property int normalIconSize: globalStyle.widgetIconSize
    property int expandedIconSize: globalStyle.widgetIconExpandedSize

    width: globalStyle.drawerwidth
    height: globalStyle.normalHeight
    x:globalStyle.widgetWidthInset
    y:globalStyle.widgetHeightInset
    z:10
    clip:true

    property alias iconImage : iconImage

    Image {
        id: iconImage
        anchors.horizontalCenter: icon.horizontalCenter
        anchors.verticalCenter: icon.verticalCenter
        width: height
        height: normalIconSize
        fillMode: Image.PreserveAspectFit
    }

    states: [
        State {
            name: "expanded"
            PropertyChanges {target: icon; height: 160}
            PropertyChanges {target: iconImage; height: expandedIconSize}
        },
        State {
            name: "minimized"
            PropertyChanges {target: icon; height: globalStyle.minimizedHeight}
            PropertyChanges {target: iconImage; height: globalStyle.minimizedHeight}
        },
        State {
            name: "fullScreen"
            PropertyChanges {target: icon; height: 160}
            PropertyChanges {target: iconImage; height: expandedIconSize}
        }
    ]

    transitions: [
        Transition {
            CubicAnimation{target:icon; property:'height'}
            CubicAnimation{target:iconImage; property:'height'}
            CubicAnimation{target:iconImage; property:'width'}
            CubicAnimation{target:iconImage; property:"opacity"}
            CubicAnimation{target:iconImage; property:"anchors.verticalCenterOffset"}
        }
    ]
}
