import QtQuick 2.0
import QtGraphicalEffects 1.0


Item {
    id:root
    property string startColor
    property string endColor
    property int breakPos: 100 * draggableItem.x /root.width
    property color centerC

    Component.onCompleted: {
        centerC = centerColor()
    }


    function centerColor(){
        var output = "#"
        for (var i=1;i<6;i+=2){
            var startDec = parseInt(startColor.substring(i, i+2),16);
            var endDec = parseInt(endColor.substring(i, i+2),16);
            var meanColor = Math.round((startDec + endDec)/2);

            var meanString = meanColor.toString(16)
            if (meanString.length<2){
                meanString = "0" + meanString
            }

            output = output + meanString

        }

        return output
    }

    LinearGradient{
        height:parent.height
        width: parent.width *breakPos / 100
        start:Qt.point(0,height/2)
        end:Qt.point(width,height/2)

        gradient: Gradient {
            GradientStop { position: 0.0; color: startColor }
            GradientStop { position: 1.0; color: centerC }
        }

    }
    LinearGradient{
        x:parent.width *breakPos / 100
        height:parent.height
        width: parent.width *(100-breakPos) / 100
        start:Qt.point(0,height/2)
        end:Qt.point(width,height/2)

        gradient: Gradient {
            GradientStop { position: 0.0; color: centerC }
            GradientStop { position: 1.0; color: endColor }
        }
    }

    MouseArea{
        anchors.fill: parent
        drag.axis: Drag.XAxis
        drag.target: draggableItem
        drag.minimumY: 0
        drag.maximumY: parent.width

        Rectangle{
            id: draggableItem
            height:parent.height
            x:parent.width/2-.5
            width:1
            opacity:0
        }

    }
}

