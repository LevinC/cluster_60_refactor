/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import "."

// SpaView is like SpaTabView but for a single SpaPage
Item {
    id: spaViewRoot

    width: globalStyle.screenWidth
    height: globalStyle.screenWidth

    property alias header: header.text
    property alias subheader: tabText.text

    property string highlightColor: globalStyle.colors[widgetID]

    property bool staticPage: false

    default property alias _containerChilds: container.children

    SpaText {
        id: header
        y: globalStyle.backButtonTop ? 62 : 58//58+50
        textStyle: homeScreenHeader
        width: parent.width
        horizontalAlignment: Text.AlignLeft

        x: 50

        color: highlightColor
    }

    Item {
        id: tabBar
        width: parent.width //- 20
        height: 81
        anchors.top: parent.top
        anchors.topMargin: 58//58+50
        anchors.left: parent.left
        SpaText {
            id: tabText
            height: 39
            width: parent.width
            anchors {
                left: parent.left
                leftMargin: 50
                bottom: tabBar.bottom
            }
            textStyle: pageGroupViewActive
            horizontalAlignment: globalStyle.backButtonTop ? Text.AlignHCenter : Text.AlignLeft
        }
    }

    SpaDivider {
        id: divider
        anchors.top: tabBar.bottom
        headerDivider: true
    }

    Item {
        id: container

        anchors {
            top:tabBar.bottom
            topMargin: 1
            bottom: parent.bottom
            left: parent.left
            //leftMargin: 50+14 // NEW letterpicker width
            right: parent.right
        }

        clip: true
    }

    SpaScrollBar {

        id: scrollBar

        width: 6
        height: 680

        anchors {
            top: divider.bottom
            right: parent.right
            rightMargin: 31
        }

        flickable: staticPage ? null : container.children[0]
    }

}
