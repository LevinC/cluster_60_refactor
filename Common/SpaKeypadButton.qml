/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import QtGraphicalEffects 1.0
import "."

Spa9PatchButton {
    id: root
    buttonType: "center"
    bottomMarginButton: -10

    property alias key:     key.text
    property alias starKey: star.visible
    property alias abcHint: abcHint.text

    width: 166
    height: 79

    SpaText {
        id: key
        anchors {
            horizontalCenter: parent.horizontalCenter
            baseline:         parent.top
            baselineOffset:   40
        }
        font.family: infoText.family
        color: globalStyle.white
        font.pixelSize: 36
    }

    Image {
        id: star
        anchors {
            top: parent.top
            topMargin: -15
            horizontalCenter: parent.horizontalCenter
        }
        source: globalStyle.imageUrl + "/Icons/img_tel_star" + globalStyle.skinUrl + ".png"
    }

    SpaText {
        id: abcHint
        anchors {
            horizontalCenter: parent.horizontalCenter
            baseline:         parent.bottom
            baselineOffset:   -18
        }
        textStyle: helpText
        font.pixelSize: 18
    }
}
