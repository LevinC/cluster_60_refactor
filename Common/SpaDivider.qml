/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0

Image {
    property bool headerDivider: false

    anchors.left: parent.left
    anchors.right: parent.right
    source:/* headerDivider ? globalStyle.imageUrl + "/ListItem/img_hmc_pagegroupview_divider_9patch" + globalStyle.skinUrl + ".png" : */globalStyle.imageUrl + "/ListItem/img_hmc_divider" + globalStyle.skinUrl + ".png"
}
