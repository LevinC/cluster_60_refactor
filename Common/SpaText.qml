/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import "."

////////////////////////////////////////////////////////////////////////////////
// Class: SpaText
//
// SpaText is the standard text item.
//
// Use the textStyle property to specify one of the standar styles that includes
// font, size and color of the text.
//
// Usage example:
//
// (start code)
//
//   SpaText {
//       text: “Example Text”
//       textStyle: SpaTextStyle.infoText
//   }
//
// (end)
//

Text {
    id: root

    property var textStyle: listItem1

    color: textStyle && globalStyle.skinUrl !== "_S4" ? textStyle.color : ''
    font.pixelSize: textStyle ? textStyle.pixelSize : 22
    font.family: textStyle ? textStyle.family : ''

    FontLoader {
        id: _VolvoSansProMedium
        source: "../resources/fonts/VolvoSansGlobalMedium.ttf"
        name: "Arial"
    }
    FontLoader {
        id: _VolvoSansProLight
        source: "../resources/fonts/VolvoSansGlobalLight.ttf"
        name: "Arial"
    }
    FontLoader {
        id: _instrument102
        source: "../resources/fonts/instrument102.ttf"
    }
    FontLoader {
        id: _instrument103_thinner
        source: "../resources/fonts/Instrument103_Thinner_20140326.otf"
        name: "Arial"
    }

    property var homeScreenSubHeader: { 'pixelSize': 32, 'family': _VolvoSansProLight.name, 'color': globalStyle.greyLvl1 }
    property var homeScreenHeader: { 'pixelSize': 44, 'family': _VolvoSansProLight.name, 'color': globalStyle.white }
    property var pageGroupViewDefault: { 'pixelSize': 24, 'family': _VolvoSansProLight.name, 'color': globalStyle.greyLvl1 }
    property var pageGroupViewActive: { 'pixelSize': 24, 'family': _VolvoSansProMedium.name, 'color': globalStyle.greyLvl1 }
    property var listHeader1: { 'pixelSize': 24, 'family': _VolvoSansProLight.name, 'color': globalStyle.greyLvl3 }
    property var listHeader2: { 'pixelSize': 20, 'family': _VolvoSansProLight.name, 'color': globalStyle.greyLvl3 }
    property var listSorter: { 'pixelSize': 24, 'family': _VolvoSansProLight.name, 'color': globalStyle.greyLvl3 }
    property var listItem1: { 'pixelSize': 28, 'family': _VolvoSansProLight.name, 'color': globalStyle.white }
    property var listItem2: { 'pixelSize': 24, 'family': _VolvoSansProLight.name, 'color': "#a8a8a8"}
    property var buttonLabelTextDefault: { 'pixelSize': 16, 'family': _VolvoSansProLight.name, 'color': globalStyle.greyLvl1 }
    property var infoText: { 'pixelSize': 24, 'family': _VolvoSansProLight.name, 'color': globalStyle.greyLvl1 }
    property var helpText: { 'pixelSize': 22, 'family': _VolvoSansProLight.name, 'color': globalStyle.greyLvl3 }
    property var largeTempNumbers: { 'pixelSize': 75, 'family': _instrument103_thinner.name, 'color': globalStyle.greyLvl1 }
    property var smallTempNumbers: { 'pixelSize': 26, 'family': _instrument102.name, 'color': globalStyle.greyLvl1 }
    property var appPaneButtons: { 'pixelSize': 16, 'family': _VolvoSansProLight.name, 'color': globalStyle.white }
}
