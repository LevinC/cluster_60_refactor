/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import QtGraphicalEffects 1.0
import "."

MouseArea {
    id: root

    width: 150
    height: 84

    property bool selectable: false
    property bool selected: false
    property string buttonColor: "3Dbtn" //3Dgrnbtn = green
    property string buttonType: "standalone"

    property int bottomMarginButton: 0

    property alias text: buttonText.text
    property alias textSize: buttonText.font.pixelSize
    property alias bg:bg


    property string iconSource: ""
    property string selectedIconSource: ""

    onClicked: if (selectable) selected = !selected

    // Don't let underlaying items get the mouse events when the button is disabled
    MouseArea {
        anchors.fill: parent
        onPressed: mouse.accepted = !root.enabled
    }

    BorderImage {
        id: bg

        anchors {
            fill: parent
            rightMargin: buttonType === "center" || buttonType === "left" ? -4 : 0
            leftMargin: buttonType === "center" || buttonType === "right" ? -4 : 0
            bottomMargin: bottomMarginButton
            topMargin: 0
        }

        border {
            left: 10
            right: 10
            top: 42
            bottom: 43
        }
        opacity: 0.7
        horizontalTileMode: BorderImage.Stretch

        source: globalStyle.imageUrl + "/Button/3D/img_com_" + buttonColor + globalStyle.componentSkinUrl + "_" + buttonType + "_OFF.png"
    }
    Item {
        id: content
        anchors.centerIn: parent


        Image {
            id: fgImage
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            source: selected ? root.selectedIconSource : root.iconSource
        }
        SpaText {
            id: buttonText
            width: root.width-10
            textStyle: buttonLabelTextDefault
            horizontalAlignment: Text.AlignHCenter
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            //This is to achieve the correct placement when you have both text and an icon. This is for buttons that have icon above the text.
            anchors.verticalCenterOffset: iconSource !== "" ? 17 : 0
            //
            opacity: 1
            wrapMode: Text.Wrap
        }
    }

    states: [
        State {
            name: "pressed"
            when: pressed && !(selected && selectable) && enabled
            PropertyChanges { target: bg; source: globalStyle.imageUrl + "/Button/3D/img_com_" + buttonColor + globalStyle.componentSkinUrl + "_" + buttonType + "_PRS.png" }
        },
        State {
            name: "active"
            when: !pressed && selected && selectable && enabled
            PropertyChanges { target: bg; source: globalStyle.imageUrl + "/Button/3D/img_com_" + buttonColor + "_S0_" + buttonType + "_ON.png" }
        },
        State {
            name: "Pressed_Active"
            when: pressed && selected && enabled
            PropertyChanges {target: bg; source: globalStyle.imageUrl +"/Button/3D/img_com_" + buttonColor + "_S0_" + buttonType + "_PRS.png" }
            PropertyChanges {target: buttonText; textStyle: SpaTextStyle.iconButtonLabelTextActive}
        },
        State {
            name: "Unavailable"
            when: !enabled
            PropertyChanges {
                target: buttonText
                opacity: 0.2
            }
            PropertyChanges {
                target: fgImage
                opacity: 0.2
            }
            PropertyChanges { target: bg; source: globalStyle.imageUrl + "/Button/3D/img_com_" + buttonColor + "_S0_" + buttonType + "_OFF.png" }
        }
    ]
}
