import QtQuick 2.0
import QtGraphicalEffects 1.0

import "../"

Item {
    id: widget
    property alias widgetObjects: widgetObjects
    property alias widgetTopSection: widgetTopSection
    property alias widgetSideMenu:widgetSideMenu
    property int topHeight:widgetTopSection.height
    property string headerTextString
    property string subHeaderTextString
    property string minimizedTextString : headerTextString
    property string leftMenuBGColor: "transparent"
    property alias sideMenuHolder:sideMenuHolder
    property alias batteryImage: widgetTopSection.batteryImage
    property alias content:content
    property int expHeight : globalStyle.expandedHeight
    property alias gradient:gradient

    Behavior on opacity {CubicAnimation{}}

    clip:true
    x:0
    width: globalStyle.screenWidth
    height:content.height + globalStyle.widgetHeightInset*2
    property int widgetID

    //This function resets the widget to its original state.
    function resetWidget(){
        widgetObjects.state = ""
        sideMenuHolder.state = ""
    }

    MouseArea{
        anchors.fill: parent
        onClicked: {
            if (widget.state === "expanded"){
                widgetPane.normalizeWidgets()
            }else{
                widgetPane.expandWidgetAtInd(widgetID)
            }
        }
    }

    Item{
        id:content
        z:1
        anchors.centerIn: widget
        width: widget.width -globalStyle.widgetWidthInset*2
        height: globalStyle.normalHeight

        Item{
            id:widgetObjects
            height:content.height
            width:content.width
            clip:true

            Item{
                id:sideMenuHolder
                width:globalStyle.drawerwidth
                height:globalStyle.fullHeight
                clip:false

                Rectangle{
                    id:leftMenuBG
                    width: globalStyle.drawerwidth
                    height: parent.height
                    color : leftMenuBGColor
                    opacity:1
                }

                Image{
                    id:gradient
                    width:globalStyle.drawerwidth
                    height:content.height
                    property int wID : widgetID +1
                    source: globalStyle.imageUrl + "/assets/color_tile_" + wID + ".png"
                    fillMode: Image.Stretch
                }

                WidgetSideMenu{
                    id:widgetSideMenu
                    state: widget.state
                    y:globalStyle.widgetIconExpandedSize
                    height:globalStyle.expandedHeight -y
                }

                Spa9FlatButton{
                    id:collapseMenuBtn

                    width:globalStyle.drawerwidth
                    height:84
                    anchors.bottom: sideMenuHolder.bottom
                    source:"/Icons/img_hmc_arrow_up"
                    bgColor: globalStyle.navigationColor
                    selectedButtonColor: globalStyle.colors[widgetID]
                    onClicked: {
                        if (sideMenuHolder.state =="collapsed"){
                            sideMenuHolder.state = ""
                        }else{
                            sideMenuHolder.state = "collapsed"
                        }
                    }
                }
                states: [
                    State {
                        name: "collapsed"
                        PropertyChanges {
                            target: sideMenuHolder
                            height:globalStyle.widgetIconExpandedSize +collapseMenuBtn.height
                        }
                        PropertyChanges {
                            target: collapseMenuBtn
                            source:"/Icons/img_hmc_arrow_down"
                        }
                        PropertyChanges {
                            target: widgetSideMenu
                            visible:false
                        }
                        PropertyChanges {
                            target: widgetSideMenu
                            opacity:0
                        }
                    }
                ]
                transitions: [
                    Transition {
                        CubicAnimation{target:sideMenuHolder; property:'height'; duration:200}
                        CubicAnimation{target:widgetSideMenu; property:'opacity'; duration:300}

                    }

                ]
            }

            WidgetHeader{
                id:widgetTopSection
                state: widget.state
                subHeader.text: subHeaderTextString
                header.text: headerTextString
            }

            states: [
                State {
                    name: "slidedLeft"
                    PropertyChanges {target: sideMenuHolder; x: -globalStyle.drawerwidth}
                }
            ]
            transitions: [
                Transition {
                    CubicAnimation{target:sideMenuHolder; property:'x'}
                }
            ]
        }

        BorderImage {
            id: widgetborder
            border {left: 16; right: 16; top: 16; bottom: 16;}
            anchors.fill: content
            anchors.leftMargin: -10
            anchors.rightMargin: -10
            anchors.topMargin: -10
            anchors.bottomMargin: -10
            source: globalStyle.imageUrl + "/HomeScreen/img_hmc_hmtile" + globalStyle.componentSkinUrl + ".png"
        }

        Image {
            id: widgethighlight
            source: globalStyle.imageUrl + "/HomeScreen/img_hmc_top_highlight" + globalStyle.componentSkinUrl + ".png"
            width: content.width
            height: Math.min(141,content.height)
        }
    }

    states: [
        State {
            name: "expanded"
            PropertyChanges {target: content; height: expHeight}
            PropertyChanges {target: leftMenuBG; opacity:1}
        },
        State {
            name: "minimized"
            PropertyChanges {target: content; height: globalStyle.minimizedHeight}
            PropertyChanges {target: widgetTopSection.header; text: minimizedTextString}
        },
        State {
            name: "fullScreen"
            PropertyChanges {target: content; height: globalStyle.fullHeight}
            PropertyChanges {target: leftMenuBG; opacity:1}
        }
    ]

    transitions: [
        Transition {
            CubicAnimation{target:widget; property:'height'}
            CubicAnimation{target:content; property:'height'}

            CubicAnimation{target:leftMenuBG; property:'opacity'}
        }
    ]
}
