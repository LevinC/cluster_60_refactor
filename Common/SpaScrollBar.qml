/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import "."

Item {
    id: scrollBar

    property Flickable flickable

    property real _position: flickable ? flickable.visibleArea.heightRatio < 1 ? flickable.visibleArea.yPosition : 0 : 0
    property real _pageSize: flickable ? flickable.visibleArea.heightRatio : 0

    visible: _pageSize < 1 && _pageSize > 0

    // A light, semi-transparent background
    Rectangle {
        id: background
        anchors.fill: parent
        color: globalStyle.greyLvl4
        opacity: 0.1
    }

    // Size the bar to the required size, depending upon the orientation.
    Rectangle {
        anchors {
            left: parent.left
            right: parent.right
        }

        y: _position * parent.height

        height: _pageSize * parent.height

        color: globalStyle.greyLvl4
    }
}

