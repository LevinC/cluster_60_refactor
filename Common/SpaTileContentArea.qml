import QtQuick 2.0
import QtGraphicalEffects 1.0
import "."

Item {
    width: 744-globalStyle.drawerwidth
    height: globalStyle.headerOn ? parent.height-120 : parent.height
    anchors.right: parent.right
    anchors.top: parent.top
    anchors.topMargin: globalStyle.headerOn ? 120/*114*/ : 0

    property alias content: contentArea.children
    property alias listTitleText: listTitleText.text

    property string titleColor: switch (widgetId) {
                                            case 0: globalStyle.firstColor
                                                break
                                            case 1: globalStyle.secondColor
                                                break
                                            case 2: globalStyle.thirdColor
                                                break
                                            case 3: globalStyle.fourthColor
                                            }
    Item {
        id: listTitle
        width: parent.width
        height: 36
        Behavior on opacity { NumberAnimation { duration: 200 } }

        Rectangle {
            width: parent.width
            height: 2
            opacity: 0.4
            anchors.bottom: listTitle.bottom
            anchors.bottomMargin: -1
            color: globalStyle.skinUrl === "_S4" ? "black" : "#000000"
        }
        Rectangle {
            width: parent.width
            height: 2
            opacity: 0.4
            anchors.top: listTitle.top
            color: globalStyle.skinUrl === "_S4" ? "black" : "#18ffffff"
        }

        SpaText {
            id: listTitleText
            textStyle: SpaTextStyle.infoText
            width: 427
            elide: Text.ElideRight
            anchors.left: listTitle.left
            anchors.leftMargin: 20
            anchors.baseline: parent.bottom
            anchors.baselineOffset: -9
            color: titleColor
        }
    }
    Item {
        id: contentArea
        anchors.fill: parent
        anchors.topMargin: listTitle.height
    }
}
