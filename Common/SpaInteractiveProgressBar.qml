/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.0
import "."

MouseArea {
    id: root
    height: 68

    clip: false

    preventStealing: true

    property alias maxValue: slider.maximumValue
    property alias currentValue: slider.value

    function milliSecondsToString(milliseconds) {
        milliseconds = milliseconds > 0 ? milliseconds : 0;
        var timeInSeconds = Math.floor(milliseconds / 1000);
        var minutes = Math.floor(timeInSeconds / 60);
        var minutesString = minutes < 10 ? "0" + minutes : minutes;
        var seconds = Math.floor(timeInSeconds % 60);
        var secondsString = seconds < 10 ? "0" + seconds : seconds;
        return minutesString + ":" + secondsString;
    }

    ProgressBar {
        id: progressbar
        y: 38
        value: (slider.value-slider.minimumValue)/(slider.maximumValue-slider.minimumValue)
        style: ProgressBarStyle {
            background: Rectangle {
                implicitHeight: 3
                implicitWidth: root.width
                color: "#959595"
                opacity: 0.2
            }
            progress: Rectangle {
                color: globalStyle.colors[1]///globalStyle.themeColorGraphics
            }

        }
    }
    Slider {
        opacity:0
        id: slider
        width: root.width
        height: root.height
        y: 12
        orientation: Qt.Horizontal

        minimumValue: 0
        onValueChanged: musicPlayer.seek(value);

        style: SliderStyle {
            groove: Item {
                height: 2
            }
            handle: Item {
                width: 1
                height: 1
                Rectangle {
                    width: 15
                    height: 56
                    anchors.centerIn: parent
                    Rectangle {
                        color: "black"
                        width: 5
                        height: 38
                        anchors.centerIn: parent
                    }
                }
            }
        }
    }

    SpaText {
        id: startTime
        anchors.left: parent.left
        anchors.bottom: root.bottom
        text: milliSecondsToString(slider.value)
        textStyle: listHeader2
        font.pixelSize: 16
        color: globalStyle.greyLvl2
    }

    SpaText {
        id: endTime
        anchors.right: parent.right
        anchors.bottom: root.bottom
        text: milliSecondsToString(slider.maximumValue)
        textStyle: listHeader2
        font.pixelSize: 16
        color: globalStyle.greyLvl2
    }
}
