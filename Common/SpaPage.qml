/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import "."

Flickable {
    id: root

    // Tab page title
    property string subheader
    property bool hasToolBarButton: false
    property bool hasKeyBoard: false
    property bool hasLetterPicker: false
    property string keyboardText

    width: globalStyle.screenWidth
    height: globalStyle.screenWidth

    contentWidth: width
    contentHeight: childrenRect.height
}
