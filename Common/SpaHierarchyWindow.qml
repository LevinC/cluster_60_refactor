/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import QtQuick.Controls 1.0
import QtGraphicalEffects 1.0
import "."

// SpaHierarchyWindow is the top level of a list hierarchy.
Item {
    id: root

    width: globalStyle.screenWidth
    height: globalStyle.screenHeight

    property alias stack: stackView

    property alias hasToolBarButton: bottomBar.hasToolBarButton
    property alias hasKeyBoard: bottomBar.hasKeyBoard
    property alias keyboardText: bottomBar.keyboardText

    StackView {
        id: stackView
        anchors.fill: parent

        function push_(component, properties, splitBelowItem) {
            push(component, properties)
        }

        delegate: StackViewDelegate {
            function transitionFinished(properties)
            {
                properties.exitItem.opacity = 1
            }

            pushTransition: StackViewTransition {
                SequentialAnimation {
                    PropertyAction {
                        target: enterItem
                        property: "opacity"
                        value: 0
                    }
                    PropertyAnimation {
                        target: exitItem
                        property: "opacity"
                        from: 1
                        to: 0
                        duration: 130//300
                        easing.type: Easing.InQuad
                    }
                    PropertyAnimation {
                        target: enterItem
                        property: "opacity"
                        from: 0
                        to: 1
                        duration: 70//300
                    }
                }
            }
        }
    }

        Spa9PatchButton {
            id: toolbarButton
            width: 480
            height: 84

            anchors {
                horizontalCenter: parent.horizontalCenter
                bottom: root.bottom
                bottomMargin: 6
            }

            buttonType: "center"

            text: "Edit"

            visible: opacity != 0
            opacity: bottomBar.hasToolBarButton ? 1 : 0
            Behavior on opacity {
                PropertyAnimation {target: toolbarButton; property: "opacity"; duration: 200; easing.type: "InSine"}
            }

            onClicked: coreScreen.notAvailableNotify()
        }

        SpaViewBottomBar {
            id: bottomBar
            y: 0
            hasToolBarButton: false
            hasKeyBoard: false
            Behavior on y {
                PropertyAnimation { property: "y"; duration: 200; easing.type: "InSine"}
            }
        }

        Spa9PatchButton {
            anchors.left: root.left
            anchors.leftMargin: 18
            anchors.bottom: root.bottom
            anchors.bottomMargin: 6
            width: 130
            height : 84
            iconSource: globalStyle.imageUrl + "/Icons/img_hmc_back" + globalStyle.skinUrl + ".png"
            text: "Back"
            enabled: root.stack.depth > 1
            onClicked: root.stack.depth > 1 ? root.stack.pop() : coreScreen.closeApp();
        }

        Spa9PatchButton {
            width: 130
            height : 84
            anchors.right: root.right
            anchors.rightMargin: 18
            anchors.bottom: root.bottom
            anchors.bottomMargin: 6
            iconSource: globalStyle.imageUrl + "/Icons/img_hmc_close_txt" + globalStyle.skinUrl + ".png"
            text: "Close"
            onClicked: coreScreen.closeApp();
        }
}
