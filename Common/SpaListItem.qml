/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import "."
import "../"

Item {
    property alias pressed: mouseArea.pressed
    property alias dividerOpacity: divider.opacity
    property bool widgetItem: false
    property string pressedColor: "#597999"
    signal clicked

    x: widgetItem ? 0 : 50  //old v526

    height: globalStyle.listItemSize
    width: 665  // v526

    Rectangle {
        anchors.fill: parent
        color: pressed ? pressedColor : "transparent"
        opacity: pressed ? 0.6 : 1
    }

    SpaDivider {
        id: divider
        anchors.top: parent.top
        anchors.topMargin: -2

        Behavior on opacity {CubicAnimation{}}
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked: parent.clicked()
    }
}
