import QtQuick 2.0

import "../../Common"
import "../../"

Widget {
    id:widget
    widgetSideMenu.model: weatherSideMenu
    headerTextString: "15°C Mostly Sunny"
    subHeaderTextString: "New York"
    property string iconS: "/weather_app_icon"
    property string bgSource:"/weather_app_mockup_v426.png"
    property string currSource: "weather"
    expHeight: 574


    function changeFromAppPane(s,isM){
        coreScreen.mainListView.contentX = globalStyle.screenWidth
        coreScreen.widgetPane.expandWidgetAtInd(3)
        currSource=s
    }

    onCurrSourceChanged: {
        if (currSource =="weather"){
            headerTextString = "15°C Mostly Sunny"
            subHeaderTextString = "New York"
            iconS =  "/weather_app_icon"
            widgetSideMenu.model = weatherSideMenu;
            mainAreaLoader.sourceComponent =weatherMainAreaHolder
        }else if (currSource =="performance"){
            headerTextString = "6.0 ℓ/100km"
            subHeaderTextString = "Average Fuel Consumption"
            iconS = "/img_hmc_default_drvr_perf"
            widgetSideMenu.model = 0;
            mainAreaLoader.sourceComponent = performanceHolder
        }else if (currSource =="travellink"){
            headerTextString = "12.0 / 24.0 C"
            subHeaderTextString = "Weather / New York"
            iconS = ""
            widgetSideMenu.model = 0;
            mainAreaLoader.sourceComponent = travelLinkMainAreaHolder
        }
    }


    Loader{
        id:mainAreaLoader
        sourceComponent: weatherMainAreaHolder
    }

    Component{id:performanceHolder; Item{}}

    Component{
        id:weatherMainAreaHolder
        Item{
            id:mainArea
            x:globalStyle.drawerwidth
            width:widget.width - globalStyle.drawerwidth
            y: widgetTopSection.height
            height: widget.expHeight - widgetTopSection.height - globalStyle.bottomMargin
            clip:true
            opacity:0
            state:widget.state

            Image{
                anchors.top: parent.top
                anchors.left: parent.left
                source :globalStyle.imageUrl + "/weather_app_mockup_v426.png"
            }

            states: [
                State {
                    name: "expanded"
                    PropertyChanges {
                        target: mainArea; opacity:1
                    }
                }
            ]

            transitions: [Transition {CubicAnimation{target:mainArea; property:"opacity"}}]
        }
    }
    Component{
        id:travelLinkMainAreaHolder

        Item{
            id:mainArea
            height: widget.expHeight
            width:globalStyle.screenWidth-globalStyle.drawerwidth
            anchors.topMargin: widgetTopSection.height
            x: globalStyle.drawerwidth*1.3
            anchors.bottomMargin: globalStyle.bottomMargin
            state:widget.state


            clip:true
            opacity:0

            Item{
                id:holder
                height:300
                width:400
                anchors.centerIn: mainArea

                Column{
                    spacing: 5
                    Row{
                        spacing: 5

                        Spa9FlatButton{
                            height:holder.width/3 - spacing
                            width:holder.height/2 - spacing/2
                            text:"Alerts"
                        }

                        Spa9FlatButton{
                            height:holder.width/3 - spacing
                            width:holder.height/2 - spacing/2
                            text:"Fuel"


                        }
                    }
                    Row{
                        spacing: 5
                        Spa9FlatButton{
                            height:holder.width/3 - spacing
                            width:holder.height/2 - spacing/2

                            text:"Sports"
                        }

                        Spa9FlatButton{
                            height:holder.width/3 - spacing
                            width:holder.height/2 - spacing/2

                            text:"Weather"
                        }
                    }
                    Row{
                        spacing: 5
                        Spa9FlatButton{
                            height:holder.width/3 - spacing
                            width:holder.height/2 - spacing/2

                            text:"Favorites"
                        }

                    }
                }
            }
            states: [
                State {
                    name: "expanded"
                    PropertyChanges {
                        target: mainArea; opacity:1
                    }
                }
            ]

            transitions: [Transition {CubicAnimation{target:mainArea; property:"opacity"}}]
        }
    }


    WidgetIcon{
        id:icon
        z:10
        state: widget.state
        iconImage.source: globalStyle.imageUrl + iconS + globalStyle.skinUrl + ".png"
    }

    ExtraSideMenu{
        id:weatherSideMenu
    }
}
