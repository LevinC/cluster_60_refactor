import QtQuick 2.0

import "../../Common/"

VisualItemModel{
    Column {
        x: 5
        SpaText {
            width: 212
            text: "New York"
            textStyle: helpText
            elide: Text.ElideRight
            font.pixelSize: 20
        }
        SpaText {
            width: 212
            text: "Now"
            textStyle: helpText
            elide: Text.ElideRight
            font.pixelSize: 20
        }
        SpaText {
            width: 212
            text: "15°C"
            textStyle: infoText
            elide: Text.ElideRight
            font.pixelSize: 38
            color: "white"
        }
        SpaText {
            width: 212
            text: "Mostly Sunny"
            textStyle: infoText
            elide: Text.ElideRight
            font.pixelSize: 28
            color: "white"
        }
        Item {
            width: 10
            height: 10
        }
        SpaText {
            width: 212
            text: "Today"
            textStyle: helpText
            elide: Text.ElideRight
            font.pixelSize: 24
        }
        SpaText {
            width: 212
            text: "15°C"
            textStyle: infoText
            elide: Text.ElideRight
            font.pixelSize: 24
        }
        SpaText {
            width: 212
            text: "Partly Cloudy"
            textStyle: infoText
            elide: Text.ElideRight
            font.pixelSize: 24
        }
        Item {
            width: 10
            height: 5
        }
        SpaText {
            width: 212
            text: "Tonight"
            textStyle: helpText
            elide: Text.ElideRight
            font.pixelSize: 24
        }
        SpaText {
            width: 212
            text: "5°C"
            textStyle: infoText
            elide: Text.ElideRight
            font.pixelSize: 24
        }
        SpaText {
            width: 212
            text: "Clear"
            textStyle: infoText
            elide: Text.ElideRight
            font.pixelSize: 24
        }
        Item {
            width: 10
            height: 5
        }
        SpaText {
            width: 212
            text: "Chance of Precip:"
            textStyle: helpText
            elide: Text.ElideRight
            font.pixelSize: 20
        }
        SpaText {
            width: 212
            text: "10 %"
            textStyle: infoText
            elide: Text.ElideRight
            font.pixelSize: 24
        }
    }
}
