import QtQuick 2.0
import QtMultimedia 5.0


import "../../Common"
import "../../dummydata/"
import "../Media/Radio/"
import "../../"

Widget {
    id:widget
    minimizedTextString: headerTextString + " - " + subHeaderTextString
    property int leftSideModelCount:5


    property string currSource:"bt"
    property bool isMedia : true
    property string prevSource: "ipod"
    property bool prevIsMedia: true
    property string prevPrevSource: "fm"
    property bool prevPrevIsMedia: false

    function prevBtnPressed(){
        var tempSource = currSource
        var tempIsMedia = isMedia

        switchSource(prevSource,prevIsMedia)
        prevSource = tempSource
        prevIsMedia = tempIsMedia
    }

    function prevPrevBtnPressed(){
        var tempSource = currSource
        var tempIsMedia = isMedia

        switchSource(prevPrevSource,prevPrevIsMedia)

        prevPrevSource = prevSource
        prevPrevIsMedia = prevIsMedia
        prevSource = tempSource
        prevIsMedia = tempIsMedia
    }

    function changeFromAppPane(s,isM){
        coreScreen.mainListView.contentX = globalStyle.screenWidth
        coreScreen.widgetPane.expandWidgetAtInd(1)

        if (s === currSource)return;

        prevPrevSource = prevSource
        prevPrevIsMedia = prevIsMedia
        prevSource = currSource
        prevIsMedia = isMedia
        switchSource(s,isM)
    }

    function switchSource(s,media){
        isMedia = media
        currSource=s

        musicPlayer.pause()
        hideAll()
        delay.start()
    }


    property string iconS:{
        if (isMedia &&mediaModel.artUrl){
            return globalStyle.dummyDataMediaURL + mediaModel.artUrl;
        }else if (!isMedia &&radioModel.logoNow){
            return globalStyle.imageUrl + radioModel.logoNow
        }else{
            return smallIconSource
        }
    }

    property string smallIconSource : globalStyle.imageUrl + "/assets/ic_app_" + currSource + "_def" + globalStyle.skinUrl + ".png"

    headerTextString: isMedia ? mediaModel.title : radioModel.stationNow + "/" + radioModel.progNow
    subHeaderTextString: isMedia  ? mediaModel.artist : radioModel.freqNow

    widgetSideMenu.model:mediaSideMenu;


    onCurrSourceChanged: {
        leftSideModelCount = 4;
        if (isMedia){
            leftSideModelCount = 5;

            if(currSource == "usb"){
                nPlaying.similarBtn.enabled =false
                nPlaying.similarBtn.opacity=0
            }else{
                nPlaying.similarBtn.enabled =false
                nPlaying.similarBtn.opacity=1
            }

            widgetSideMenu.model = mediaSideMenu
        }else{
            if(currSource == "fm"){
                widgetSideMenu.model = fmSideMenu
            }else if(currSource == "am"){
                leftSideModelCount = 5;
                widgetSideMenu.model = amSideMenu
            }else if(currSource == "dab"){
                widgetSideMenu.model = dabSideMenu
            }else if(currSource == "sirius"){
                leftSideModelCount = 5;
                widgetSideMenu.model = siriusSideMenu
            }else if(currSource == "fav"){
                widgetSideMenu.model = radioFavSideMenu
            }
        }
    }

    function hideAll(){
        widgetSideMenu.visible = 0;
        widgetTopSection.visible = 0;
        pauseOverlay.visible = 0;
        mainArea.visible = 0;
        icon.visible = 0;
        sideMenuLine.visible = 0;
        smallIcon.visible = 0;
        nowPlayingViewHolder.opacity = 0;
        widgetSideMenu.model.btn2.state = ""
        nowPlaying.state=""
        activityIndicator.loadingAnimation.start()
    }

    function showAll(){
        widgetSideMenu.visible = 1;
        widgetTopSection.visible = 1;
        pauseOverlay.visible = 1;
        mainArea.visible = 1;
        icon.visible = 1;
        sideMenuLine.visible = 1;
        nowPlayingViewHolder.opacity = 1;
        smallIcon.visible = 1;
    }

    Timer{
        id:delay
        interval:1000
        onTriggered: {
            showAll()
        }
    }

    property var mediaApp: {
        var component = Qt.createComponent("Media.qml")
        if (component.errorString()) {
            console.log(component.errorString())
            return null
        }
        else {
            return component.createObject()
        }
    }

    property var radioApp: {
        var component = Qt.createComponent("../Media/Radio/radio.qml")
        if (component.errorString()) {
            console.log(component.errorString())
            return null
        }
        else {
            return component.createObject()
        }
    }

    Item{
        id:mainArea
        x:globalStyle.drawerwidth
        width:widget.width - globalStyle.drawerwidth
        y: widgetTopSection.height
        height: widget.expHeight - widgetTopSection.height
        clip:true

        Loader{
            id:mainAreaLoader
            sourceComponent: isMedia ? mediaMainArea :radioMainArea
        }
    }

    Item {
        id: nowPlayingViewHolder
        z:1
        anchors.fill: content
        anchors.leftMargin: globalStyle.drawerwidth
        state:widget.state
        opacity:0
        enabled: false
        visible: isMedia || currSource === "dab" || currSource === "sirius" ? true:false
        clip:true

        Item{
            id:nowPlaying
            width:parent.width
            height:parent.height
            x:-width

            SIRIUSNowPlayingView{
                anchors.fill: parent
                visible: currSource ==="sirius"
            }

            DABNowPlayingView{
                anchors.fill: parent
                visible: currSource ==="dab" || currSource==="fav"
            }

            NowPlayingView{
                id:nPlaying
                anchors.fill: parent
                subHeaderString: subHeaderTextString
                headerString: headerTextString
                onGoback: {
                    mediaSideMenu.btn2.state=""
                }
                visible:isMedia
            }

            states: [
                State {
                    name: "shown"
                    PropertyChanges {
                        target: nowPlaying
                        x:0
                    }
                }
            ]

            transitions: [
                Transition {
                    CubicAnimation{target:nowPlaying; property:'x'}
                }
            ]

        }

        states: [State {name: "expanded";
                PropertyChanges {target: nowPlayingViewHolder; opacity:1}
                PropertyChanges {target: nowPlayingViewHolder; enabled:true}
            }
        ]
        transitions: [Transition {CubicAnimation{target:nowPlayingViewHolder; property:"opacity"}}]
    }

    Rectangle{
        id:sideMenuLine
        height:1
        width:globalStyle.drawerwidth
        y:(leftSideModelCount-2)/leftSideModelCount*widgetSideMenu.height + globalStyle.widgetIconExpandedSize + globalStyle.widgetHeightInset
        x:globalStyle.widgetWidthInset
        z:2
        opacity:.1
        state:parent.state
    }

    Component{
        id:mediaMainArea
        WidgetMainList{
            listHeader: "Bluetooth - Playlist"
            x:20
            width:widget.content.width-globalStyle.drawerwidth-x
            height:widget.content.height - widget.topHeight
            currInd: mediaModel.index
            objectModel:mediaModel.playlist
            state: widget.state
            listDelegate: Component{MusicListItem{}}
        }

    }

    Component{
        id:radioMainArea
        WidgetMainList{
            x:20
            width:widget.content.width-globalStyle.drawerwidth-x
            height:widget.content.height - widget.topHeight
            objectModel:radioModel.stations
            state: widget.state
            listDelegate: Component{RadioListItem{}}
        }
    }

    MediaPlayerSideMenu{id: mediaSideMenu}
    FMSideMenu{id: fmSideMenu}
    AMSideMenu{id: amSideMenu}
    DABSideMenu{id: dabSideMenu}
    SIRIUSSideMenu{id: siriusSideMenu}
    RadioFavSideMenu{id:radioFavSideMenu}

    Image{
        z:10
        id:smallIcon
        anchors.centerIn: icon
        width: icon.iconImage.width
        height: icon.iconImage.height

        source: smallIconSource
        fillMode: Image.PreserveAspectFit
        opacity: widget.state === "minimized" || nowPlaying.state === "shown" ? 1:0
        Behavior on opacity {CubicAnimation{}}
    }

    WidgetIcon{
        id:icon
        state: widget.state
        iconImage.source: iconS
        opacity:1

        Image {
            z:10
            id: pauseOverlay
            state: widget.state
            anchors.fill: icon.iconImage
            fillMode: Image.PreserveAspectFit
            opacity: musicPlayer.playbackState === Audio.PlayingState && state !== "minimized" ? 0:1
            source: globalStyle.imageUrl + "/Icons/img_rad_pause_cg" + globalStyle.skinUrl + ".png"
            MouseArea{
                anchors.fill: pauseOverlay
                onClicked: {
                    if (pauseOverlay.source !== ""){
                        musicPlayer.playbackState === Audio.PlayingState ? musicPlayer.pause() : musicPlayer.play();
                    }
                }
                enabled: phoneModel.count === 0
            }

            states: [
                State {
                    when: widget.state === "minimized" || (nowPlaying.state === "shown" && widget.state === "expanded")
                    PropertyChanges {
                        target:pauseOverlay
                        visible: false
                    }
                    PropertyChanges {
                        target:icon.iconImage
                        opacity:0
                    }
                }
            ]
        }

    }

    ActivityIndicator{
        id:activityIndicator
        anchors.centerIn: widget
    }
}


