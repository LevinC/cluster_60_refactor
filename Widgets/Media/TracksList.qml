import QtQuick 2.0
import "../../Common"

SearchListList {
    model: mediaModel.tracksSearch
    delegate: albumsListComp

    Component {
        id: albumsListComp
        SpaListItem {
            id: albumDelegate
            
            dividerOpacity: !ListView.view.moving

            onClicked: mediaAppRoot.setTrack("Search / '"+mediaModel.searchString+"'", mediaModel.tracksSearch, index)

            Column {
                spacing: -2
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: 23
                SpaText {
                    textStyle: listItem1
                    text: title
                }
                SpaText {
                    textStyle: listItem2
                    text: artist
                }
            }
            Image {
                id: rightArrow3
                height: parent.height
                width: height
                fillMode: Image.PreserveAspectCrop
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.topMargin: 0
                source: globalStyle.imageUrl + "/Icons/img_hmc_arrow_right" + globalStyle.skinUrl + ".png"
                asynchronous: true
            }
        }
    }
}
