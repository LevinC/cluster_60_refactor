import QtQuick 2.0
import QtMultimedia 5.0


import "../../Common"
import "../../"
import "../../dummydata/"

Rectangle{
    id:nowPlaying
    color:"black"

    property string headerString
    property string subHeaderString
    property alias similarBtn:similarBtn

    signal goback

    MouseArea{
        anchors.topMargin: 80
        anchors.fill: parent
    }

    Column {
        height:parent.height
        width:parent.width
        anchors.bottom:parent.bottom
        id: rectContainer

        Item {
            height: 80
            width: height
        }

        Row{
            id:topRow
            anchors.horizontalCenter: parent.horizontalCenter
            height:238
            width:height+200+spacing*2
            spacing: 6

            Spa9FlatButton {
                id:similarBtn
                width: 100
                height:100
                y:topRow.height-height
                selectedButtonColor: globalStyle.colors[1]
                text:"Similar"
                invisible: false
            }
            Image {
                id: coverartImage
                width: 238
                height: width
                source: mediaModel.artUrl ? globalStyle.dummyDataMediaURL + mediaModel.artUrl : globalStyle.imageUrl + "/Icons/img_med_default_covertart_large" + globalStyle.skinUrl + ".png"
                asynchronous: true
                Behavior on source {
                    SequentialAnimation {
                        NumberAnimation { target: coverartImage; properties: "opacity"; to: 0.0; duration: 130; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]} }
                        PropertyAction { target: coverartImage; property: "source" }
                        NumberAnimation { target: coverartImage; properties: "opacity"; to: 1.0; duration: 70;}
                    }
                }
                Image {
                    id: pauseoverlayDrawer
                    anchors.fill: coverartImage
                    opacity: musicPlayer.playbackState !== Audio.PlayingState
                    source: globalStyle.imageUrl + "/Icons/img_rad_pause_cg" + globalStyle.skinUrl + ".png"
                    Behavior on opacity { NumberAnimation { duration: 150; easing {type: Easing.Bezier; bezierCurve: [(0.0, 0.0), (0.42, 0.0), (1.0, 1.0), (1.0, 1.0)]} } }
                }
                MouseArea {
                    anchors.fill: coverartImage
                    onClicked: {
                        musicPlayer.playbackState === Audio.PlayingState ? musicPlayer.pause() : musicPlayer.play();
                    }
                }
            }
            Spa9FlatButton {
                width: 100
                height:100
                y:topRow.height-height
                selectedButtonColor: globalStyle.colors[1]
                selectable: true
                text:"Shuffle"
                invisible: false
            }
        }

        Item {
            height: 26
            width: height
        }

        SpaText {
            id:header
            anchors.horizontalCenter: parent.horizontalCenter
            verticalAlignment: Text.AlignVCenter
            textStyle: homeScreenHeader
            font.pixelSize: 32
            text:headerString
        }
        SpaText {
            id:subHeader
            anchors.horizontalCenter: parent.horizontalCenter
            verticalAlignment: Text.AlignVCenter
            textStyle: homeScreenSubHeader
            font.pixelSize: 22
            text:subHeaderString
        }

        SpaInteractiveProgressBar {
            id: progbarDrawer
            width:parent.width - 132
            currentValue: musicPlayer.position
            maxValue: musicPlayer.duration
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Item {
            height: 40
            width: height
        }
        Row {
            spacing: 2
            anchors.horizontalCenter: parent.horizontalCenter
            Spa9FlatButton {
                width: (rectContainer.width)/4
                selectedButtonColor: globalStyle.colors[1]
                iconSource: globalStyle.imageUrl + "/prev.png"
                invisible: false
                onClicked: musicPlayer.prev();
            }
            Spa9FlatButton {
                width: (rectContainer.width)/4
                selectedButtonColor: globalStyle.colors[1]
                iconSource: globalStyle.imageUrl + "/Icons/img_med_morelikethis" + globalStyle.skinUrl + ".png"
                invisible: false
                onClicked: {
                    nowPlaying.state = ""
                    goback()
                }
            }
            Spa9FlatButton {
                width: (rectContainer.width)/4
                selectedButtonColor: globalStyle.colors[1]
                iconSource: globalStyle.imageUrl + "/next.png"
                invisible: false
                onClicked: musicPlayer.next();
            }
        }
    }
}
