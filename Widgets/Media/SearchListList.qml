import QtQuick 2.0
import "../../Common"

Item {
    id: root
    property string headerText
    property alias delegate: listView.delegate
    property alias model: listView.model
    property int _showmore: 0

    property int listitemsize: globalStyle.listItemSize
    property string keyboardText

    height: listView.height + showMoreResults.height
    width: 768

    ListView {
        id: listView
        height: count > 5 + _showmore ? listitemsize * (5 + _showmore) /*+ (40)*/ : listitemsize *count /*+ (count == 0 ? 0 : 40)*/
        width: parent.width
        clip: true
        interactive: false
    }

   SpaListItem {
        id: showMoreResults
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: listView.bottom
        visible: listView.height != listitemsize*listView.count //? 1 : 0/*+(listView.count == 0 ? 0 : 40)*/
        onVisibleChanged: _showmore = 0
        width: visible ? 655 : 0
        height: visible ? globalStyle.listItemSize : 0
        onClicked: _showmore += 10;
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            Image {
                source: globalStyle.imageUrl + "/Icons/img_hmc_arrow_down.png"
                anchors.verticalCenter: parent.verticalCenter
                //anchors.verticalCenterOffset: -4
            }
            SpaText {
                height: 110
                text: "Show more results"
                verticalAlignment: Text.AlignVCenter
                textStyle: listItem1
            }
        }
    }
}
