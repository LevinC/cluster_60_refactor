import QtQuick 2.3
import QtGraphicalEffects 1.0
import "../../Common/"

SpaListPage {
    hasKeyBoard: true
    height: 680

    onKeyboardTextChanged: {
        if (keyboardText !== "") {
        model = myModel
        mediaModel.searchString = keyboardText
        if (artistSearch.count === 0 && trackSearch.count != 0 && albumSearch.count != 0) {
                myModel.clear()
                myModel.append({"listComponent": "TracksList.qml", "sectionName":"Song"})
                myModel.append({"listComponent": "AlbumsList.qml", "sectionName":"Album"})
            }
            else if (trackSearch.count === 0 && artistSearch.count != 0 && albumSearch.count != 0) {
                myModel.clear()
                myModel.append({"listComponent": "ArtistList.qml", "sectionName":"Artist"})
                myModel.append({"listComponent": "AlbumsList.qml", "sectionName":"Album"})
            }
            else if(albumSearch.count === 0 && trackSearch.count != 0 && artistSearch.count != 0) {
                myModel.clear()
                myModel.append({"listComponent": "ArtistList.qml", "sectionName":"Artist"})
                myModel.append({"listComponent": "TracksList.qml", "sectionName":"Song"})
            }
            else if(artistSearch.count === 0 && trackSearch.count === 0 && albumSearch.count != 0) {
                myModel.clear()
                myModel.append({"listComponent": "AlbumsList.qml", "sectionName":"Album"})
            }
            else if(artistSearch.count === 0 && albumSearch.count === 0 && trackSearch.count != 0) {
                myModel.clear()
                myModel.append({"listComponent": "TracksList.qml", "sectionName":"Song"})
            }
            else if(trackSearch.count === 0 && albumSearch.count === 0 && artistSearch.count != 0) {
                myModel.clear()
                myModel.append({"listComponent": "ArtistList.qml", "sectionName":"Artist"})
            }
            else if(artistSearch.count === 0 && trackSearch.count === 0 && albumSearch.count === 0) {
                myModel.clear()
            }
            else {
                myModel.clear()
                myModel.append({"listComponent": "ArtistList.qml", "sectionName":"Artist"})
                myModel.append({"listComponent": "TracksList.qml", "sectionName":"Song"})
                myModel.append({"listComponent": "AlbumsList.qml", "sectionName":"Album"})
            }
        }
        else {
            myModel.clear()
        }
    }

   spacing: -1

    highlightFollowsCurrentItem: true

    section.property: "sectionName"
    section.criteria: ViewSection.FullString
    section.delegate: listsorter
    section.labelPositioning: 3
    //model: myModel

    ListView {
        id: artistSearch
         model: mediaModel.artistsSearch
         delegate: Text {
             text: artist
             visible: false
         }
     }
    ListView {
        id: trackSearch
         model: mediaModel.tracksSearch
         delegate: Text {
             text: title
             visible: false
         }
     }
    ListView {
        id: albumSearch
         model: mediaModel.albumsSearch
         delegate: Text {
             text: album
             visible: false
         }
     }

    ListModel {
        id: myModel
        ListElement { listComponent: "ArtistList.qml"; sectionName: "Artist"; }
        ListElement { listComponent: "TracksList.qml"; sectionName: "Song"; }
        ListElement { listComponent: "AlbumsList.qml"; sectionName: "Album"; }
    }

    SpaText {
        id: noResults
        text: "No matching results"
        textStyle: helpText
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 150
        visible: artistSearch.count === 0 && trackSearch.count === 0 && albumSearch.count === 0&& mediaModel.searchString != ""
        opacity: keyboardText === "" ? 0 : 1
    }

    delegate: Loader {
        id: listLoader
        source: listComponent
    }

    Component {
        id: listsorter
        Item {
            height: 40
            width: 668
            x: 50

            Image {
                source: globalStyle.imageUrl + "/ScrollableView/img_hmc_list_sorter" + globalStyle.skinUrl + ".png"
            }
            Rectangle {
                anchors.fill: parent
                gradient:  Gradient {
                    GradientStop { position: 0.0; color: globalStyle.colors[1] }
                    GradientStop { position: 0.3; color: globalStyle.colors[1]}
                    GradientStop { position: 1.0; color: "transparent" }
                }
                opacity: globalStyle.coloredBgOpacityOne
            }
            SpaText {
                text: section
                x: 13
                y: 5
            }
        }
    }
}
