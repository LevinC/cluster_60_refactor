/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.3
import QtMultimedia 5.0
import "../../Common/"

SpaView {
    id: root

    property alias model: details.model

    header: "Artist"

    SpaListPage {
        id: details

        delegate: Component {
            SpaListItem {
                id: a2albumsDelegate
                width: 665 

                dividerOpacity: !ListView.view.moving

                onClicked: stack.push_(detailsAlbum, { "model": tracks, "subheader": album, "header": artist }, a2albumsDelegate)

                SpaText {
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    anchors.left: art.right
                    anchors.leftMargin: 18
                    textStyle: listItem1
                    text: album
                }
                Image {
                    id: art
                    height: 84
                    width: height
                    fillMode: Image.PreserveAspectCrop
                    anchors.left: parent.left
                    anchors.verticalCenter: parent.verticalCenter
                    source: artUrl ? globalStyle.dummyDataMediaURL + artUrl : globalStyle.imageUrl + "/Icons/img_med_default_covertart" + globalStyle.skinUrl + ".png"
                    asynchronous: true
                    mipmap: true
                }
                Image{
                    height: parent.height
                    width: height
                    anchors.right: parent.right
                    anchors.top: parent.top
                    anchors.topMargin: 0
                    source: globalStyle.imageUrl + "/Icons/img_hmc_arrow_right" + globalStyle.skinUrl + ".png"
                }
            }
        }
    }
}
