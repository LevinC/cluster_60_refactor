/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.0
import "../../../Common"

MouseArea {
    id: root
    width: 585
    height: 84

    clip: false

    preventStealing: true

    property alias maxValue: slider.maximumValue
    //property alias currentValue: slider.value

    property real currentValue

    onCurrentValueChanged: {
        radioModel.freqNowreal += currentValue
    }

    Slider {
        id: slider
        width: parent.width
        height: 44
        //y: 40
        orientation: Qt.Horizontal
        stepSize: 0.1
        anchors.top: parent.top

        minimumValue: 88
        value: radioModel.freqNow
        onValueChanged: {
            currentValue = (value-radioModel.freqNow)
        }

        style: SliderStyle {
            groove: Rectangle {
                height: slider.height
                width: slider.width
                color: "#18959595"
                Rectangle {
                    width: 1
                    height:slider.height+20
                }
                Rectangle {
                    width: 1
                    height:slider.height+20
                    anchors.right: parent.right
                }
                Rectangle {
                    width: 2
                    height:slider.height
                    x: 95
                    color: globalStyle.colors[1]
                }
                Rectangle {
                    width: 2
                    height:slider.height
                    x: 110
                    color: globalStyle.colors[1]
                }
                Rectangle {
                    width: 2
                    height:slider.height
                    x: 130
                    color: globalStyle.colors[1]
                }
                Rectangle {
                    width: 2
                    height:slider.height
                    x: 240
                    color: globalStyle.colors[1]
                }
                Rectangle {
                    width: 2
                    height:slider.height
                    x: 403
                    color: globalStyle.colors[1]
                }
                Rectangle {
                    width: 2
                    height:slider.height
                    x: 493
                    color: globalStyle.colors[1]
                }
                Rectangle {
                    width: 2
                    height:slider.height
                    x: 513
                    color: globalStyle.colors[1]
                }
                Rectangle {
                    width: 2
                    height:slider.height
                    x: 560
                    color: globalStyle.colors[1]
                }
            }
            handle: Item {
                width: 1
                height: 1
                Image {
                    anchors.centerIn: parent
                    source: globalStyle.imageUrl + "/Slider/Horizontal/img_hmc_handle_horizontal_wide" + globalStyle.skinUrl + ".png"
                }
            }
        }
    }
    SpaText {
        id: curTime
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.baseline: root.bottom
        anchors.baselineOffset: 0

        text: slider.minimumValue
        textStyle: listHeader2
        color: globalStyle.greyLvl2
    }
    SpaText {
        anchors.left: parent.left
        anchors.leftMargin: 100
        anchors.baseline: root.bottom
        anchors.baselineOffset: -10

        text: "92"
        textStyle: listHeader2
        color: globalStyle.greyLvl3
    }
    SpaText {
        anchors.left: parent.left
        anchors.leftMargin: 230
        anchors.baseline: root.bottom
        anchors.baselineOffset: -10

        text: "96"
        textStyle: listHeader2
        color: globalStyle.greyLvl3
    }
    SpaText {
        anchors.left: parent.left
        anchors.leftMargin: 350
        anchors.baseline: root.bottom
        anchors.baselineOffset: -10

        text: "100"
        textStyle: listHeader2
        color: globalStyle.greyLvl3
    }
    SpaText {
        anchors.left: parent.left
        anchors.leftMargin: 460
        anchors.baseline: root.bottom
        anchors.baselineOffset: -10

        text: "104"
        textStyle: listHeader2
        color: globalStyle.greyLvl3
    }
    SpaText {
        id: endTime

        anchors.right: parent.right
        anchors.baseline: root.bottom
        anchors.baselineOffset: 0

        text: maxValue
        textStyle: listHeader2
        color: globalStyle.greyLvl2
    }
}
