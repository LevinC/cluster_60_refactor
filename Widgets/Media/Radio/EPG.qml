import QtQuick 2.0
import "../../../Common"
import "../../../"

SpaHierarchyWindow {

    property SpaStyle globalStyle : SpaStyle{}

    property int spacingHeight: 80
    property int yOffset: 0

    stack.initialItem: SpaView {
        header: "SIRIUS"
        subheader: "EPG"
        staticPage: true

        _containerChilds: [
            Image{
                anchors.fill:parent
                source: globalStyle.imageUrl + "/assets/epg.png"

            }
        ]
    }
}
