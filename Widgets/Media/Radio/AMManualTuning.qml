import QtQuick 2.0

import "../../../Common"

ManualTuning {

    yOffset: -100
    spacingHeight: 30
    headerText: "AM Radio"

    Spa9FlatButton{
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 140
        height:84
        width:84
        selectedButtonColor: globalStyle.colors[widgetID]
        text:"Traffic"
        invisible: false
        selectable: true
    }
}

