import QtQuick 2.0

import "../../../Common"

VisualItemModel{
    id:objectModel

    property int listItemH: (widgetSideMenu.height)/4
    property alias btn2 : btn2

    Loader{id:btn2; sourceComponent: libraryBtn}

    Component{
        id:libraryBtn
        Spa9FlatButton{
            height:listItemH
            source:"/Icons/img_hmc_library"
            selectedButtonColor: globalStyle.colors[widgetID]
            onClicked: coreScreen.showApp(radioApp)
        }
    }
}
