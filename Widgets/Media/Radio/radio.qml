/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.3
import "../../../Common/"

SpaHierarchyWindow {
    id: mediaAppRoot

    function setTrack(listTitle, playlist, trackIndex) {
        mediaModel.index = trackIndex
        mediaModel.listTitle = listTitle;
        mediaModel.playlist = playlist;
        musicPlayer.setTrack(trackIndex);
        coreScreen.closeApp(_resetState);
    }

    function shufflePlay(listTitle, playlist) {
        mediaModel.listTitle = listTitle;
        mediaModel.shuffle = true;
        mediaModel.playlist = playlist;
        musicPlayer.play();
        coreScreen.closeApp(_resetState);
    }

    function _resetState() {
        keyboard.erase();
        stack.pop({'item' : stack.initialItem, 'immediate' : true });
        stack.initialItem.currentTab = 1;
        // Reset List Views
        allsongsview.currentIndex = 0;
        allsongsview.positionViewAtBeginning();
        allartistsview.currentIndex = 0;
        allartistsview.positionViewAtBeginning();
        allalbumsview.currentIndex = 0;
        allalbumsview.positionViewAtBeginning();
    }

    stack.initialItem: SpaTabView {
        id: tabView

        header: "Radio"

        highlightColor: globalStyle.colors[1]

        currentTab: 0
        visibleSearchIcon: false

        tabs: VisualItemModel {

            SpaListPage {
                id: stations
                subheader: "Stations"
                delegate: stationDelegate
                model: radioModel.stations
            }

            SpaListPage {
                id: genre
                subheader: "Genre"
                delegate: genreDelegate
                model: radioModel.genres
            }
        }
    }

    Component {
        id:stationDelegate
        SpaListItem {
            pressedColor: globalStyle.colors[1]
            dividerOpacity: !ListView.view.moving

            //onClicked: stack.push_(detailsAlbum, { "model": tracks, "subheader": album, "header": qsTr("Album") + translationModel.autoTr })

            Column {
                spacing: -2
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: art.right
                anchors.leftMargin: 23
                SpaText {
                    textStyle: listItem1
                    text: station
                }
                Row {
                    spacing: 10
                    height: smallText.height
                    Image {
                        anchors.verticalCenter: parent.verticalCenter
                        source: switch (hd) {
                                default: return ""
                                case "1": return globalStyle.imageUrl + "/Icons/img_rad_hd_inlist_smalll_1" + globalStyle.skinUrl + ".png"
                                case "2": return globalStyle.imageUrl + "/Icons/img_rad_hd_inlist_smalll_2" + globalStyle.skinUrl + ".png"
                                case "3": return globalStyle.imageUrl + "/Icons/img_rad_hd_inlist_smalll_3" + globalStyle.skinUrl + ".png"
                                }

                    }
                    SpaText {
                        id: smallText
                        textStyle: listItem2
                        text: freq
                    }
                }
            }

            Image {
                id: art
                height: 84
                width: height
                fillMode: Image.PreserveAspectCrop
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                source: logo ? globalStyle.imageUrl + logo : ""
                asynchronous: true
                mipmap: true
            }
            Spa9FlatButton {
                height: parent.height
                width: height
                invisible: false
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                selectedButtonColor: globalStyle.colors[1]
                iconSource: globalStyle.imageUrl + "/Icons/img_hmc_fav_OFF" + globalStyle.skinUrl + ".png"

                onClicked: {

                    if (iconSource == globalStyle.imageUrl + "/Icons/img_hmc_fav_OFF" + globalStyle.skinUrl + ".png"){
                        iconSource = globalStyle.imageUrl + "/Icons/img_hmc_fav_ON" + globalStyle.skinUrl + ".png";
                        coreScreen.addToFavNotify();

                    }else{
                        iconSource = globalStyle.imageUrl + "/Icons/img_hmc_fav_OFF" + globalStyle.skinUrl + ".png"
                        coreScreen.removedFromFavnotify();

                    }
                }
            }
        }
    }
    Component {
        id: genreDelegate
        SpaListItem {
            pressedColor: globalStyle.colors[1]
            dividerOpacity: !ListView.view.moving

            //onClicked: stack.push_(detailsAlbum, { "model": tracks, "subheader": album, "header": qsTr("Album") + translationModel.autoTr })

            Column {
                spacing: -2
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: 23

                SpaText {
                    textStyle: listItem1
                    text: genre
                }
            }
        }
    }
}
