import QtQuick 2.0
import QtMultimedia 5.0


import "../../../Common"
import "../../"
import "../../../dummydata/"

Rectangle{
    id:nowPlaying
    color:"black"

    property string headerString
    property string subHeaderString

    signal goback

    MouseArea{
        anchors.topMargin: 80
        anchors.fill: parent
    }

    Column {
        height:parent.height
        width:parent.width
        anchors.bottom:parent.bottom
        id: rectContainer

        Item {
            height: 80
            width: height
        }

        Image {
            id: coverartImage
            width: 238
            height: width
            anchors.horizontalCenter: parent.horizontalCenter
            source: globalStyle.imageUrl + "/sirius_artwork.jpeg"
            asynchronous: true

        }

        Item {
            height: 26
            width: height
        }

        SpaText {
            id:header
            anchors.horizontalCenter: parent.horizontalCenter
            verticalAlignment: Text.AlignVCenter
            textStyle: homeScreenHeader
            font.pixelSize: 32
            text:"SIRIUS"
        }
        SpaText {
            id:subHeader
            anchors.horizontalCenter: parent.horizontalCenter
            verticalAlignment: Text.AlignVCenter
            textStyle: homeScreenSubHeader
            font.pixelSize: 22
            text:"Arcade Fire - Wake Up"
        }

        Item {
            height: 40
            width: height
        }
        Row {
            spacing: 2
            anchors.horizontalCenter: parent.horizontalCenter
            Spa9FlatButton {
                width: (rectContainer.width)/4
                selectedButtonColor: globalStyle.colors[1]
                text:"Jump"
                invisible: false
            }
            Spa9FlatButton {
                width: (rectContainer.width)/4
                selectedButtonColor: globalStyle.colors[1]
                text: "Alerts"
                invisible: false

            }
            Spa9FlatButton {
                width: (rectContainer.width)/4
                selectedButtonColor: globalStyle.colors[1]
                invisible: false
                text: "Itunes tag"
            }
        }
    }
}
