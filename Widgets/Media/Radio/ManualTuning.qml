import QtQuick 2.0
import "../../../Common"

SpaHierarchyWindow {

    property int spacingHeight: 80
    property int yOffset: 0
    property string headerText: "FM Radio"

    stack.initialItem: SpaView {
        header: headerText
        subheader: "Manual Tuning"
        staticPage: true

        _containerChilds: [
            Item {
                y:yOffset
                width: parent.width
                height: parent.height
                Row {
                    spacing: 15
                    x: 50
                    y: 15
                    Image {
                        anchors.verticalCenter: parent.verticalCenter
                        height: 84
                        width: height
                        source: radioModel.logoNow !== "" ? globalStyle.imageUrl + radioModel.logoNow : globalStyle.imageUrl + "/Icons/img_med_default_fm" + globalStyle.skinUrl + ".png"
                    }
                    Column {
                        id: infotext
                        anchors.verticalCenter: parent.verticalCenter
                        opacity: 1
                        spacing: -4
                        SpaText {
                            id: smallHeader
                            text: radioModel.stationNow + (radioModel.hdNow !== "" ? "HD" + radioModel.hdNow : "")
                            textStyle: listItem1
                        }
                        SpaText {
                            text: radioModel.progNow
                            textStyle:  listItem2
                        }
                    }
                }
                Column {
                    anchors.centerIn: parent
                    Row {
                        spacing: 2
                        anchors.horizontalCenter: parent.horizontalCenter
                        Spa9FlatButton {
                            height: itemMhz.height
                            width: 84
                            iconSource: globalStyle.imageUrl + "/Icons/img_hmc_arrow_left" + globalStyle.skinUrl + ".png"
                            onClicked: radioModel.freqNowreal -= 0.1
                        }
                        Column {
                            id: buttons
                            spacing: 2
                            height: childrenRect.height
                            Rectangle {
                                id: itemMhz
                                width: 230
                                height: 140
                                color: "#18959595"
                                Column {
                                    width: parent.width
                                    height: childrenRect.height
                                    anchors.centerIn: parent
                                    spacing: -12
                                    SpaText {
                                        font.pixelSize: 72
                                        text: radioModel.freqNow
                                        width: parent.width
                                        horizontalAlignment: Text.AlignHCenter
                                    }
                                    SpaText {
                                        font.pixelSize: 24
                                        text: "MHZ"
                                        width: parent.width
                                        horizontalAlignment: Text.AlignHCenter
                                    }
                                }
                            }
                            Spa9FlatButton {
                                width: itemMhz.width
                                height: 84
                                iconSource: globalStyle.imageUrl + "/Icons/img_hmc_fav_OFF" + globalStyle.skinUrl + ".png"
                                invisible: false
                            }
                        }

                        Spa9FlatButton {
                            height: itemMhz.height
                            width: 84
                            //selectedButtonColor: contentDrawer.buttonsPressedColor
                            iconSource: globalStyle.imageUrl + "/Icons/img_hmc_arrow_right" + globalStyle.skinUrl + ".png"
                            onClicked: radioModel.freqNowreal += 0.1
                        }
                    }
                    Item {
                        height: spacingHeight
                        width: height
                    }

                    ManualTuningSlider {
                        id: manTuneDrawer
                        maxValue: 108
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                }
            }
        ]
    }
}
