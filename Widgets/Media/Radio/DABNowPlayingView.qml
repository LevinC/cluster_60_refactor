import QtQuick 2.0

import "../../../Common"

Rectangle{
    id:nowPlaying
    color:"black"
    anchors.topMargin: globalStyle.topExpandedHeight

    MouseArea{
        anchors.topMargin: 80
        anchors.fill: parent
    }

    Column {
        height:parent.height
        width:parent.width
        anchors.bottom:parent.bottom
        id: rectContainer

        Item {
            height: 80
            width: height
        }

        Image {
            id: coverartImage
            width: 238
            height: width
            anchors.horizontalCenter: parent.horizontalCenter
            source: globalStyle.imageUrl + "/dab_artwork.jpg"
        }

        Item {
            height: 26
            width: height
        }
    }
}
