import QtQuick 2.0

import "../../../Common"
import "../"

VisualItemModel{
    id:objectModel

    property int listItemH: (widgetSideMenu.height)/4
    property alias btn2:btn2


    Loader{sourceComponent: libraryBtn}
    Loader{id:btn2; sourceComponent: tuningBtn}
    Loader{sourceComponent: prevSourceBtn}
    Loader{sourceComponent: prevPrevSourceBtn}

    Component{
        id:libraryBtn
        Spa9FlatButton{
            height:listItemH
            source:"/Icons/img_hmc_library"
            selectedButtonColor: globalStyle.colors[widgetID]
            onClicked: coreScreen.showApp(radioApp)

        }
    }

    Component{
        id:tuningBtn
        Spa9FlatButton{
            height:listItemH
            source:"/Icons/img_med_manual_tuning_fm"
            selectedButtonColor: globalStyle.colors[widgetID]
            onClicked: coreScreen.showApp(manualTuning)
        }
    }

    Component{
        id:prevSourceBtn
        PrevSourceBtn{

        }
    }
    Component{
        id:prevPrevSourceBtn
        PrevPrevSourceBtn{
        }
    }

    Component {
        id: manualTuning
        ManualTuning {

        }
    }
}
