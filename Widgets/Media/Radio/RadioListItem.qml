import QtQuick 2.0

import "../../../Common"
import "../../../dummydata"

MediaListItem{
    id:wrapper
    iconSource:logo ? globalStyle.imageUrl + logo :globalStyle.imageUrl + "/Icons/img_med_default_fm" + globalStyle.skinUrl + ".png"
    header: station
    subHeader: freq
    selected: wrapper.ListView.isCurrentItem ? 1 : 0

    onClicked: {
        radioModel.freqNow = freq
        ListView.view.currentIndex = index
    }
}
