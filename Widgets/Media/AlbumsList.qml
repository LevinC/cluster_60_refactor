import QtQuick 2.0
import "../../Common"

SearchListList {
    model: mediaModel.albumsSearch
    delegate: albumsListComp

    Component {
        id: albumsListComp
        SpaListItem {
            id: albumDelegate            
            dividerOpacity: !ListView.view.moving

            onClicked: stack.push_({item: detailsAlbum, properties: { "model": tracks, "subheader": album, "header": artist }})

            Column {
                spacing: -2
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: art.right
                anchors.leftMargin: 23
                SpaText {
                    textStyle: listItem1
                    text: album
                }
                SpaText {
                    textStyle: listItem2
                    text: artist
                }
            }
            Image {
                id: art
                height: parent.height
                width: height
                anchors.verticalCenter: parent.verticalCenter
                fillMode: Image.PreserveAspectCrop
                source:  artUrl ? "../../dummydata/media/" + artUrl : globalStyle.imageUrl + "/Icons/img_med_default_covertart" + globalStyle.skinUrl + ".png"
                asynchronous: true
            }
            Image {
                id: rightArrow3
                height: parent.height
                width: height
                fillMode: Image.PreserveAspectCrop
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.topMargin: 0
                source: globalStyle.imageUrl + "/Icons/img_hmc_arrow_right" + globalStyle.skinUrl + ".png"
                asynchronous: true
            }
        }
    }
}
