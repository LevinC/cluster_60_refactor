/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.3
import "../../Common/"

SpaHierarchyWindow {
    id: mediaAppRoot

    function setTrack(listTitle, playlist, trackIndex) {
        mediaModel.index = trackIndex
        mediaModel.listTitle = listTitle;
        mediaModel.playlist = playlist;
        musicPlayer.setTrack(trackIndex);
        coreScreen.closeApp(_resetState);
    }

    function shufflePlay(listTitle, playlist) {
        mediaModel.listTitle = listTitle;
        mediaModel.shuffle = true;
        mediaModel.playlist = playlist;
        musicPlayer.play();
        coreScreen.closeApp(_resetState);
    }

    function _resetState() {
        keyboard.erase();
        stack.pop({'item' : stack.initialItem, 'immediate' : true });
        stack.initialItem.currentTab = 1;
        // Reset List Views
        allsongsview.currentIndex = 0;
        allsongsview.positionViewAtBeginning();
        allartistsview.currentIndex = 0;
        allartistsview.positionViewAtBeginning();
        allalbumsview.currentIndex = 0;
        allalbumsview.positionViewAtBeginning();
    }

    hasKeyBoard: stack.depth > 1 ? false : stack.initialItem.currentTabItem.hasKeyBoard
    keyboardText: searchpage.keyboardText

    stack.initialItem: SpaTabView {
        id: tabView

        header: "Library"

        highlightColor: globalStyle.colors[1]

        currentTab: 1

        tabs: VisualItemModel {

            MediaSearchResultsPage {
                id: searchpage
                subheader: ""
                hasLetterPicker: false
            }

            SpaListPage {
                id: allartistsview
                subheader: "Artist"
                delegate: artistsDelegate
                model: mediaModel.allArtists
                section.property: "artist"
                section.criteria: ViewSection.FirstCharacter
                section.delegate: Item {}
                hasLetterPicker: false//true
            }

            SpaListPage {
                id: allsongsview
                subheader: "Song"
                delegate: tracksDelegatelvl1
                model: mediaModel.allTracks
                section.property: "title"
                section.criteria: ViewSection.FirstCharacter
                section.delegate: Item {}
                hasLetterPicker: true
            }

            SpaListPage {
                id: allalbumsview
                subheader: "Album"
                delegate: albumsDelegate
                model: mediaModel.allAlbums
                hasLetterPicker: false//true
            }
        }

        Component {
            id:albumsDelegate
            SpaListItem {
                id: aalbumsDelegate
                pressedColor: globalStyle.colors[1]
                //dividerOpacity: !ListView.view.moving

                onClicked: stack.push_(detailsAlbum, { "model": tracks, "subheader": album, "header": "Album" }, aalbumsDelegate)

                Column {
                    spacing: -2
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: art.right
                    anchors.leftMargin: 23
                    SpaText {
                        textStyle: listItem1
                        text: album
                    }

                    SpaText {
                        textStyle: listItem2
                        text: artist
                    }
                }

                Image {
                    id: art
                    height: 84
                    width: height
                    fillMode: Image.PreserveAspectCrop
                    anchors.left: parent.left
                    anchors.verticalCenter: parent.verticalCenter
                    source: artUrl ? globalStyle.dummyDataMediaURL + artUrl : globalStyle.imageUrl + "/Icons/img_med_default_covertart" + globalStyle.skinUrl + ".png"
                    asynchronous: true
                    mipmap: true
                }
                Image{
                    height: parent.height
                    width: height
                    anchors.right: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                    source: globalStyle.imageUrl + "/Icons/img_hmc_arrow_right" + globalStyle.skinUrl + ".png"
                }
            }
        }

        Component {
            id:artistsDelegate
            SpaListItem {
                id: aartistsDelegate
                pressedColor: globalStyle.colors[1]
                //dividerOpacity: !ListView.view.moving

                onClicked: stack.push_(detailsArtist, { "model": albums, "subheader": artist, "header": "Artist" }, aartistsDelegate)

                SpaText {
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    anchors.left: parent.left
                    anchors.leftMargin: 23
                    textStyle: listItem1
                    text: artist
                }
                Image{
                    height: parent.height
                    width: height
                    anchors.right: parent.right
                    anchors.top: parent.top
                    anchors.topMargin: 0
                    source: globalStyle.imageUrl + "/Icons/img_hmc_arrow_right" + globalStyle.skinUrl + ".png"
                }
            }
        }

        Component {
            id:tracksDelegatelvl1
            SpaListItem {
                pressedColor: globalStyle.colors[1]
                //dividerOpacity: !ListView.view.moving

                onClicked: mediaAppRoot.setTrack('Songs', mediaModel.allTracks, index)

                Column {
                    spacing: -2
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: 23
                    SpaText {
                        textStyle: listItem1
                        text: title
                    }

                    SpaText {
                        textStyle: listItem2
                        text: artist
                    }
                }
            }
        }

        Component {
            id: detailsAlbum
            DetailsAlbum {}
        }

        Component {
            id: detailsArtist
            DetailsArtist {}
        }
    }
}
