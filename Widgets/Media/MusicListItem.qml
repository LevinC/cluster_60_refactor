import QtQuick 2.0
import QtMultimedia 5.0


import "../../Common"
import "../../dummydata/"

MediaListItem{
    id:wrapper
    iconSource: artUrl ? globalStyle.dummyDataMediaURL + artUrl :globalStyle.imageUrl + "/Icons/img_med_default_covertart_large" + globalStyle.skinUrl + ".png"
    header: title
    subHeader: artist
    selected: wrapper.ListView.isCurrentItem && musicPlayer.playbackState === Audio.PlayingState ? 1 : 0

    onClicked: {
        musicPlayer.setTrack(index)
    }
}
