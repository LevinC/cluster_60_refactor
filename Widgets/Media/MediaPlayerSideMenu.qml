import QtQuick 2.0

import "../../Common"

VisualItemModel{
    id:objectModel

    property int listItemH: (widgetSideMenu.height)/5
    property alias btn2:btn2

    Loader{sourceComponent: libraryBtn}
    Loader{sourceComponent: switchBtn}
    Loader{id:btn2; sourceComponent: moreBtn; states: [State {name: "back"; PropertyChanges {target: btn2; sourceComponent:backBtn}}]}
    Loader{sourceComponent: prevSourceBtn}
    Loader{sourceComponent: prevPrevSourceBtn}

    Component{
        id:libraryBtn
        Spa9FlatButton{
            height:listItemH
            source:"/Icons/img_hmc_library"
            selectedButtonColor: globalStyle.colors[widgetID]
            onClicked: coreScreen.showApp(mediaApp)
            text: "Library"
        }
    }

    Component{
        id:switchBtn
        Spa9FlatButton{
            height:listItemH
            source:"/Icons/img_med_switch"
            selectedButtonColor: globalStyle.colors[widgetID]
        }
    }

    Component{
        id:moreBtn
        Spa9FlatButton{
            height:listItemH
            source:"/assets/now_playing"
            selectedButtonColor: globalStyle.colors[widgetID]
            onClicked: {
                btn2.state = "back"
                nowPlaying.state="shown"
            }
        }
    }

    Component{
        id:backBtn
        Spa9FlatButton{
            height:listItemH
            source:"/Icons/img_hmc_arrow_left"
            selectedButtonColor: globalStyle.colors[widgetID]
            onClicked: {
                btn2.state = ""
                nowPlaying.state=""
            }
        }
    }

    Component{
        id:prevSourceBtn
        PrevSourceBtn{
        }
    }
    Component{
        id:prevPrevSourceBtn
        PrevPrevSourceBtn{
        }
    }
}
