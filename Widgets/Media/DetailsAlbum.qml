/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import QtMultimedia 5.0
import "../../Common/"

SpaView {
    id: root

    property alias model: details.model

    SpaListPage {
        id: details

        delegate: Component {
            id: tracksDelegatelvl2

            SpaListItem {
                width: 665

                dividerOpacity: !ListView.view.moving

                onClicked: mediaAppRoot.setTrack('Album / ' + album, root.model, index)

                SpaText {
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    anchors.left: parent.left
                    anchors.leftMargin: 18
                    textStyle: listItem1
                    text: title
                }
            }
        }
    }
}
