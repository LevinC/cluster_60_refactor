import QtQuick 2.0
import QtMultimedia 5.0
import QtGraphicalEffects 1.0


import "../../Common"

Widget {
    id:widget
    property bool muted: false
    property bool inCall: phoneModel.count !== 0
    property bool multiCall: phoneModel.count > 1
    property alias selectedListHolder:selectedListHolder

    minimizedTextString: {
        if (phoneModel.count == 0){
            return headerTextString
        }else{
            return headerTextString + " " + subHeaderTextString
        }
    }

    batteryImage.visible: true


    subHeaderTextString: {
        if (phoneModel.count == 0){
            return "AT&T"
        }else{
            return _callText(phoneModel.activeCallState, phoneModel.activeDuration)
        }
    }

    headerTextString: {
        if (phoneModel.count == 0){
            return "Work Phone"
        }else{
            return contactsModel.getContactFromNumber(phoneModel.activeLineIdentification).name
        }
    }

    widgetSideMenu.model: PhoneSideMenu{}



    WidgetIcon{
        id:icon
        state: widget.state
        iconImage.source: {
            if (phoneModel.count === 0) {
                globalStyle.imageUrl + "/assets/ic_app_phone_def" + globalStyle.skinUrl + ".png"
            }
            else {
                !activeCallInline.noContactPic ? phoneModel.activeCallImageSource : globalStyle.imageUrl + "/Icons/img_med_default_contact" + globalStyle.skinUrl + ".png"
            }
        }
    }

    Component {
        id: selectPhonePop
        SelectPhonePopup {
        }
    }

    property var phoneApp: {
        var component = Qt.createComponent("phone.qml")
        if (component.errorString()) {
            console.log(component.errorString())
            return null
        }
        else {
            return component.createObject()
        }
    }

    property var keypadApp: {
        var component = Qt.createComponent("keypadapp.qml")
        if (component.errorString()) {
            console.log(component.errorString())
            return null
        }
        else {
            return component.createObject()
        }
    }

    onInCallChanged: {
        if (inCall) {
            fadeOut.start()
        }
        else {
            fadeIn.start()
        }
    }

    SequentialAnimation {
        id: fadeIn
        ScriptAction { script: ringingFile.stop() }
        PropertyAction { target: musicPlayer; property: "volume"; value: 0.0 }
        ScriptAction { script: if (musicPlayer.playingState === true ) { musicPlayer.play() } }
        NumberAnimation { target: musicPlayer; property: "volume"; from: 0.0; to: musicPlayer.mediaVolume; duration: 1500; }
    }

    SequentialAnimation {
        id: fadeOut
        ScriptAction { script: musicPlayer.saveState() }
        ScriptAction { script: musicPlayer.saveVol() }
        NumberAnimation { target: musicPlayer; property: "volume"; to: 0.0; duration: 1000; }
        ScriptAction { script: if (musicPlayer.playingState === true) { musicPlayer.pause() } }
        ScriptAction { script: ringingFile.play() }
    }

    Audio {
        id: ringingFile
        //source: "../../resources/phone-calling.wav"
        autoLoad: true
        loops: 6
        volume: 0.9
    }


    Item{
        id:mainArea
        x:globalStyle.drawerwidth
        width:widget.content.width - globalStyle.drawerwidth
        y: widgetTopSection.height
        height: widget.expHeight - widgetTopSection.height
        clip:true

        WidgetMainList{
            listHeader: "Favorites"
            x:20
            width:widget.content.width-globalStyle.drawerwidth-x
            height:widget.content.height - widget.topHeight
            objectModel:contactsModel.favList
            listDelegate: Component{PhoneListItem{}}
            visible: phoneModel.count === 0
            state: widget.state
        }
    }

    Item {
        x:globalStyle.drawerwidth
        width:widget.width - globalStyle.drawerwidth
        y: topHeight
        height: widget.expHeight - topHeight
        ActiveCall {
            id: activeCallInline
            visible: phoneModel.count !== 0
        }
    }

    Rectangle{
        id:selectedListHolder
        z:1
        anchors.fill: content
        color: "black"
        anchors.leftMargin: globalStyle.drawerwidth
        state:widget.state
        visible:false;
        clip:true

        SelectList{}

        onStateChanged: {
            if (state != "Expanded"){
                visible = false;
            }
        }

    }

    function _callText(state, duration) {
        switch (state) {
        case "incoming":
            return "Incoming"
        case "dialing":
            return "Connecting"
        case "alerting":
            return "Connecting"
        case "active":
            return Qt.formatDateTime( new Date(duration) , "mm:ss")
        case "held":
            return "Held call"
        case "waiting":
            return "Waiting call"
        case "disconnected":
            return Qt.formatDateTime( new Date(duration) , "mm:ss");
            //return "Busy" //for now in the concept es prototype
        }
        return "Unknown call state...";
    }
}
