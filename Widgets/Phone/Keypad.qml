/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */
import QtQuick 2.0
import "../../Common/"

Column {
    id: root

    property bool cmdKeys: true

    signal key(string key)
    signal erase()
    signal call()

    width: 500

    onKey: textinput.text += key
    onErase: textinput.text = textinput.text.slice(0,textinput.text.length-1)

    TextInput {
        id: textinput
        height: 73
        text: ""
        color: globalStyle.greyLvl1
        width: 500
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 42
        cursorVisible: false
    }
    ListView {
        id: resultsList
        model: contactsModel.keypadSearchList
        interactive: false
        delegate: keypadResults
        height: count === 0 ? 0 : 84
        width: root.width
        opacity: 1
        clip: true
        Behavior on height {
            ParallelAnimation {
                NumberAnimation {property: "opacity"; to: 0; duration: 200}
                NumberAnimation {property: "height";duration: 100}
                NumberAnimation {property: "opacity"; to: 1; duration: 200}
            }
        }
    }

    Column {
        height: childrenRect.height
        Grid {
            columns: 3
            rowSpacing: 5
            columnSpacing: 2
            Repeater {
                model: [['1', ''],     ['2', 'ABC'], ['3', 'DEF'],
                    ['4', 'GHI'],  ['5', 'JKL'], ['6', 'MNO'],
                    ['7', 'PQRS'], ['8', 'TUV'], ['9', 'WXYZ'],
                    ['*', 'p'],    ['0', '+'],   ['#', 'w']]
                SpaKeypadButton {
                    onClicked: root.key(key)
                    key: modelData[0] === '*' ? "" :modelData[0]
                    abcHint: modelData[1]
                    starKey: modelData[0] === '*' ? true : false
                }
            }
        }

        Row {
            Spa9PatchButton{
                width: 250
                height: 90
                buttonType: "center"
                buttonColor: "3Dgrnbtn"
                enabled: textinput.text!=""
                onClicked: {
                    phoneModel.dial(textinput.text)
                    coreScreen.closeApp()
                    textinput.text = ""
                }
                Row {
                    anchors.centerIn: parent
                    Image {
                        id: callImg
                        source: globalStyle.imageUrl + "/Icons/img_tel_outgoing" + globalStyle.skinUrl + ".png"
                        opacity: textinput.text!="" ? 1 : 0.2
                    }
                    SpaText {
                        width: 100
                        anchors.baseline: parent.bottom
                        anchors.baselineOffset: -30
                        text: "Call"
                        font.pixelSize: 36
                        font.letterSpacing: 4
                        opacity: textinput.text!="" ? 1 : 0.2
                    }
                }
            }
            Spa9PatchButton {
                width: 250
                height: 90
                buttonType: "center"
                onClicked: root.erase()
                enabled: textinput.text!=""
                Image {
                    anchors.centerIn: parent
                    source: globalStyle.imageUrl + "/Icons/img_tel_erase" + globalStyle.skinUrl + ".png"
                    opacity: textinput.text!="" ? 1 : 0.2
                }
            }
        }
    }
    Component {
        id: keypadResults
        SpaListItem {
            id: wrapper
            onClicked: {
                phoneModel.dial(mobile)
                phoneModel.activeCallImageSource = photo
                coreScreen.closeApp()
                textinput.text = ""
            }
            x: 0
            SpaText {
                id: txt
                anchors.baseline: parent.top
                anchors.baselineOffset: 38
                anchors.left: img.right
                anchors.leftMargin: 23

                text: name

                wrapMode: Text.Wrap
                textStyle: listItem1
            }
            SpaText {
                id: phoneNo
                anchors.left: img.right
                anchors.leftMargin: 23
                anchors.baseline: parent.top
                anchors.baselineOffset: 69

                text: mobile

                wrapMode: Text.Wrap
                textStyle: listItem2
            }
            Image {
                id: img
                height: parent.height
                width: height
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.topMargin: 1

                source: photo

                fillMode: Image.PreserveAspectCrop
                asynchronous: true
            }
            Spa9FlatButton {
                anchors {
                    left: parent.left
                    leftMargin: 416
                    verticalCenter: parent.verticalCenter
                }
                height: 84
                width: 84
                buttonType: "center"
                textSize: 42
                text: resultsList.count
                onClicked: coreScreen.notAvailableNotify()
            }
        }
    }
}
