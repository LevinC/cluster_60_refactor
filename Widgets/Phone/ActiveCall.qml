/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import "../../Common/"

Column {
    id: flow
    anchors.fill: parent
    anchors.rightMargin: 20
    anchors.leftMargin: 15
    spacing: 2
    y:topHeight + 40

    Column {
        id: buttons
        height: parent.height/*-infotextItem.height-30*/
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: 4

        Item{
            height:20
            width:height
        }

        Row {
            spacing: 4
            Spa9FlatButton {
                id: mute
                width: 233
                height: 161
                buttonType: "top_left"
                iconSource: globalStyle.imageUrl + "/Icons/img_hmc_mute" + globalStyle.skinUrl + ".png"
                selectedIconSource: globalStyle.imageUrl + "/Icons/img_hmc_mute" + globalStyle.skinUrl + ".png"
                enabled: phoneModel.activeCallState !== "disconnected"
                // TODO toggle mute
                invisible: false
                selected: muted
                selectedButtonColor: globalStyle.colors[2]
                onClicked: muted = !muted
                text: "Mute"

            }
            Spa9FlatButton {
                id: addcall
                width: 233
                height: 161
                buttonType: "bottom_right"
                iconSource: globalStyle.imageUrl + "/Icons/img_tel_addcall" + globalStyle.skinUrl + ".png"
                enabled: phoneModel.activeCallState !== "disconnected"
                onClicked: coreScreen.showApp(phoneApp)
                invisible: false
                selectedButtonColor: globalStyle.colors[2]
                text: "Add call"
            }
        }
        Row {
            spacing: 4
            Spa9FlatButton {
                id: keypad
                width: 233
                height: 161
                buttonType: "bottom_left"
                iconSource: globalStyle.imageUrl + "/Icons/img_tel_keypad" + globalStyle.skinUrl + ".png"
                enabled: phoneModel.activeCallState !== "disconnected"
                onClicked: coreScreen.showApp(keypadApp)
                invisible: false
                selectedButtonColor: globalStyle.colors[2]
                text: "Keypad"
            }

            Spa9FlatButton {
                id: privatecall
                width: 233
                height: 161
                buttonType: "top_right"
                iconSource: globalStyle.imageUrl + "/Icons/img_tel_private" + globalStyle.skinUrl + ".png"
                enabled: phoneModel.activeCallState !== "disconnected"
                invisible: false
                selectedButtonColor: globalStyle.colors[2]
                text: "Private"
            }
        }

        Item {
            height: 8
            width: 20
        }

        Spa9FlatButton {
            id: endcall
            anchors.horizontalCenter: parent.horizontalCenter
            width:  parent.width
            height: 82
            buttonColor: "flatredbtn"
            enabled: phoneModel.activeCallState !== "disconnected"
            invisible: false
            buttonType: "standalone"
            Row {
                anchors.centerIn: parent
                Image {
                    id: callImg
                    source: globalStyle.imageUrl + "/Icons/img_tel_decline" + globalStyle.skinUrl + ".png"
                    opacity: phoneModel.activeCallState === "disconnected" ? 0.2 : 1
                }
            }
            onClicked: {
                //TODO Update recent called model
                phoneModel.hangupAll()
                muted = false
            }
        }
    }
}
