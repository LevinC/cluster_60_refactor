import QtQuick 2.0

import "../../Common"

Item{
    id:root
    anchors.fill:parent


    MouseArea{
        anchors.fill:parent

        onClicked: {console.log("asdasd")}
    }

    SpaText{
        width:parent.width
        anchors.leftMargin: 50
        anchors.rightMargin: 50
        anchors.bottomMargin: 50
        anchors.bottom:parent.bottom
        height:100
        horizontalAlignment: Text.AlignHCenter
        text: "Can be changed in settings"
    }

    Row{
        spacing: 20
        anchors.centerIn: parent
        Spa9FlatButton{
            height:160
            width:200
            text: "Recent"
            bgColor: "#222222"
            selectedButtonColor: globalStyle.colors[2]
            onClicked: {
                root.parent.visible = false;
            }
        }
        Spa9FlatButton{
            height:160
            width:200
            bgColor: "#222222"
            text: "Favorites"
            selectedButtonColor: globalStyle.colors[2]
            onClicked: {
                root.parent.visible = false;
            }
        }
    }
}


