import QtQuick 2.0

import "../../"
import "../../Common"

SpaListItem{
    id: wrapper
    property string iconSource:iconSource
    property string header
    property string subHeader
    property real nowPlayingOpacity
    property bool selected

    onSelectedChanged: {
        if (selected){
            wrapper.state = "selected"
        }else{
            wrapper.state = ""
        }
    }

    height:84
    width: parent.width
    widgetItem: true
    pressedColor: globalStyle.colors[1]
    dividerOpacity: !ListView.view.moving

    SpaText {
        id: textLarge
        height:parent.height
        width: parent.width-x
        x:18
        verticalAlignment: Text.AlignVCenter
        textStyle: listItem1
        text: name
        elide: Text.ElideRight
    }

    onClicked: {
        phoneModel.dial(mobile);
        phoneModel.activeCallImageSource = photo
    }
}
