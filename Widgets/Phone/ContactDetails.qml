/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.3
import "../../Common/"

SpaListPage {
    id: root

    property var contact

    signal dial(int contactIndex, string category)
    signal navigate(int contactIndex, string category)
    signal sms(int contactIndex, string category)
    signal email(int contactIndex, string category)

    model: ListModel { }

    header: contactHeader
    delegate: infoRow
    section.delegate: categoryHeader
    section.property: "category"

    Component {
        id: categoryHeader
        Item  {
            x: 50
            height: 40
            width: root.width-100
            Image {
                source: globalStyle.imageUrl + "/ScrollableView/img_hmc_list_sorter.png"
                SpaText {
                    id: headerText
                    anchors.left: parent.left
                    anchors.leftMargin: 22
                    anchors.verticalCenter: parent.verticalCenter
                    textStyle: infoText
                    text: section
                }
            }
        }
    }

    Component {
        id: infoRow
        Rectangle {
            x: 50
            width: root.width-100
            height: nameValuePair.height < 84 ? 84: nameValuePair.height
            color: mouse.pressed ? globalStyle.colors[2] : "#18959595"

            Image {
                id: categoryIcon
                anchors.verticalCenter: parent.verticalCenter
                source: {
                    if (category == "Phone Numbers")
                        if (name == "Mobile") {
                            globalStyle.imageUrl + "/default_number_contactDetails.png"
                        }
                        else { globalStyle.imageUrl + "/Icons/img_tel_phone.png" }
                    else if (category == "Addresses")
                        globalStyle.imageUrl + "/Icons/img_hmc_navi_conn.png"
                    else if (category == "URL")
                        globalStyle.imageUrl + "/Icons/img_hmc_browser.png"
                }
            }

            Column {
                id: nameValuePair

                anchors {
                    left: parent.left; leftMargin: 190
                    //top: parent.top; topMargin: 5
                    verticalCenter: parent.verticalCenter
                }

                spacing: 3

                SpaText {
                    id: nameField
                    text: name
                    textStyle: listItem1
                    width: root.width-100
                }

                SpaText {
                    id: valueField
                    wrapMode: Text.WordWrap
                    text: value
                    textStyle: listItem2
                    color: globalStyle.themeColorFont
                    width: category == "Addresses" ? 200 : 480
                }
                Item {
                    width: height
                    height: category != "Phone Numbers" ? 5 :0
                }
            }

            MouseArea {
                id: mouse
                anchors.fill: nameValuePair

                onClicked: {
                    if (category == "Phone Numbers") {
                        phoneModel.dial(value);
                        phoneModel.activeCallImageSource = contact.photo;
                        coreScreen.closeApp()
                    }
                }
            }
            Spa9FlatButton {
                width: height
                height: parent.height
                anchors.right: parent.right
                iconSource: globalStyle.imageUrl + "/Icons/img_tel_msg_txt" + globalStyle.skinUrl + ".png"
                text: "Message"
                buttonType: "center"
                visible: category == "Phone Numbers"
                onClicked: coreScreen.notAvailableNotify()
            }
            SpaDivider {

            }
        }
    }

    Component {
        id: contactHeader
        Item {
            width: root.width
            height: 170

            Image {
                id: photo
                source: contact.photo
                height: 127
                width: height
                anchors.top: parent.top
                anchors.topMargin: 22
                anchors.left: parent.left
                anchors.leftMargin: 72
                fillMode: Image.PreserveAspectCrop
                asynchronous: true
                mipmap: true
            }

            SpaText {
                anchors.left: photo.right
                anchors.leftMargin: 20
                anchors.top: parent.top
                anchors.topMargin: 35
                text: contact.name
                font.pixelSize: 32
            }
        }
    }

    onContactChanged: {
        model.clear();
        if (contact.mobile && contact.mobile !== "")
            model.append({"category": QT_TR_NOOP("Phone Numbers"), "name": QT_TR_NOOP("Mobile"),"value": contact.mobile});
        if (contact.home && contact.home !== "")
            model.append({"category": QT_TR_NOOP("Phone Numbers"), "name": QT_TR_NOOP("Home"), "value": contact.home});
        if (contact.work && contact.work !== "")
            model.append({"category": QT_TR_NOOP("Phone Numbers"), "name": QT_TR_NOOP("Work"), "value": contact.work});
        if (contact.homeadress && contact.homeadress !== "")
            model.append({"category": QT_TR_NOOP("Addresses"), "name": QT_TR_NOOP("Home"), "value": contact.homeadress});
        if (contact.workadress && contact.workadress !== "")
            model.append({"category": QT_TR_NOOP("Addresses"), "name": QT_TR_NOOP("Work"), "value": contact.workadress});
        if (contact.homeemail && contact.homeemail !== "")
            model.append({"category": QT_TR_NOOP("E-Mail Addresses"), "name": QT_TR_NOOP("Home"), "value": contact.homeemail});
        if (contact.workemail && contact.workemail !== "")
            model.append({"category": QT_TR_NOOP("E-Mail Addresses"), "name": QT_TR_NOOP("Work"), "value": contact.workemail});
        if (contact.url && contact.url !== "")
            model.append({"category": QT_TR_NOOP("URL"), "name": QT_TR_NOOP("URL"), "value": contact.url});
    }
}
