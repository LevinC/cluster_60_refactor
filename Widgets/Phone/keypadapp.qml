/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import "../../Common/"

SpaHierarchyWindow {
    stack.initialItem: SpaView {
        header: "Phone"
        subheader: "Keypad"
        staticPage: true

        _containerChilds: [
            Keypad {
                id: keypad
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 145
                anchors.horizontalCenter: parent.horizontalCenter
            }
        ]
    }
}
