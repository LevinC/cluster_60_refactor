/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.3
import "../../Common/"

SpaHierarchyWindow {
    id: phoneAppRoot

    function placeCall(num) {
        phoneModel.dial(num);
        coreScreen.closeApp(_resetState)
    }

    function _resetState() {
        keyboard.erase()
        stack.initialItem.currentTab = 1
        stack.pop({'item' : stack.initialItem, 'immediate' : true})
        contacts.currentIndex = 0;
        contacts.positionViewAtBeginning();
        recents.currentIndex = 0;
        recents.positionViewAtBeginning();
        favorites.currentIndex = 0;
        favorites.positionViewAtBeginning();
    }

    hasToolBarButton: stack.depth > 1 ? false : stack.initialItem.currentTabItem && stack.initialItem.currentTabItem.hasToolBarButton
    hasKeyBoard: stack.depth > 1 ? false : stack.initialItem.currentTabItem && stack.initialItem.currentTabItem.hasKeyBoard
    keyboardText: search.keyboardText

    stack.initialItem: SpaTabView {
        id: tabView
        header: "Phone"

        highlightColor: globalStyle.colors[2]

        currentTab: 2

        tabs: VisualItemModel {
            id: pages
            SpaListPage {
                id: search
                subheader: ""
                delegate: searchDelegate
                model: contactsModel.searchList
                hasKeyBoard: true
                onKeyboardTextChanged: contactsModel.searchString = keyboardText
            }
            SpaListPage {
                id: contacts
                subheader: "Contacts"
                delegate: contactDelegate
                model: contactsModel.contactList
                section.property: "name"
                section.criteria: ViewSection.FirstCharacter
                section.delegate: listsorter
                section.labelPositioning: 3
                footer: Item { height: 84+12+6 }
                hasLetterPicker: true
            }
            SpaListPage {
                id: recents
                subheader: "Recent"
                delegate: recentDelegate
                model: contactsModel.callList
            }
            SpaListPage {
                id: favorites
                subheader: "Favorites"
                delegate: favDelegate
                model: contactsModel.favList
                hasToolBarButton: true
            }
        }
    }

    Component {
        id: listsorter
        Item {
            height: 40
            width: 668
            x: 50
            Image {
                source: globalStyle.imageUrl + "/ScrollableView/img_hmc_list_sorter" + globalStyle.skinUrl + ".png"
            }
//            Rectangle {
//                anchors.fill: parent
//                gradient:  Gradient {
//                    GradientStop { position: 0.0; color: globalStyle.colors[2] }
//                    GradientStop { position: 0.3; color: globalStyle.colors[2] }
//                    GradientStop { position: 1.0; color: "transparent" }
//                }
//                opacity: globalStyle.coloredBgOpacityOne
//            }
            SpaText {
                text: section
                x: 13
                y: 5
            }
        }
    }

    Component {
        id: detailsLevel
        DetailsPage {
            id: details
        }
    }

    Component {
        id:recentDelegate
        SpaListItem {
            id: aRecentDelegate
            pressedColor: globalStyle.colors[2]
            //dividerOpacity: !ListView.view.moving

            onClicked: {
                phoneAppRoot.placeCall(mobile)
                phoneModel.activeCallImageSource = photo
            }
            Column {
                anchors {
                    left: callIcon.right //parent.left
                    leftMargin: 23
                    verticalCenter: parent.verticalCenter
                }
                spacing: -2
                SpaText {
                    textStyle: listItem1
                    text: name
                }

                SpaText {
                    width: 290
                    textStyle: helpText
                    text: {
                        if (mobile) {
                        switch (type) {
                        case "in":
                            return ("Mobile: %1").arg(mobile)
                        case "out":
                            return ("Work: %1").arg(mobile)
                        case "missed":
                            return ("Home: %1").arg(mobile)
                        }
                        }
                        else {
                            switch (type) {
                            case "in":
                                return number
                            case "out":
                                return number
                            case "missed":
                                return number
                            }
                        }
                    }
                }
            }
                        Image {
                            id: callIcon
                            height: 84
                            width: height
                            anchors {
                                left: parent.left
                                top: parent.top
                                topMargin: -1
                            }
                            source: {
                                switch (type) {
                                case "in":
                                    return globalStyle.imageUrl + "/Icons/img_tel_incoming.png"
                                case "out":
                                    return globalStyle.imageUrl + "/Icons/img_tel_outgoing.png"
                                case "missed":
                                    return globalStyle.imageUrl + "/Icons/img_tel_missdcall.png"
                                }
                            }

                       }
            Column {
                anchors {
                    right: infoButton.left
                    rightMargin: 23
                    verticalCenter: parent.verticalCenter
                }
                spacing: -2
                SpaText {
                    textStyle: listItem1
                    text: time //date.toLocaleTimeString(Qt.locale("de-De"), Locale.NarrowFormat) : time
                }
                SpaText {
                    textStyle: listItem2
                    text: date //date.toLocaleDateString(Qt.locale("de-De"), Locale.ShortFormat) : date
                }
            }
            Rectangle {
                width: 2
                height: parent.height
                anchors.right: infoButton.left
                color: "black"
            }

            Spa9FlatButton {
                id: infoButton
                width: height
                height: parent.height
                anchors.right: parent.right
                selectedButtonColor: globalStyle.colors[2]
                buttonType: "center"
                invisible: false
                iconSource: globalStyle.imageUrl + "/Icons/img_tel_contact" + globalStyle.skinUrl + ".png"//globalStyle.imageUrl + "/contact_card" + globalStyle.skinUrl + ".png"
                onClicked: stack.push_(detailsLevel,
                                       { "contact" : { "name": name
                                               , "mobile": mobile
                                               , "home": home
                                               , "work": work
                                               , "homeadress": homeadress
                                               , "workadress": workadress
                                               , "url": url
                                               , "photo": photo } },
                                       aRecentDelegate)
            }
        }
    }
    Component {
        id:favDelegate
        SpaListItem {
            id: aFavDelegate
            pressedColor: globalStyle.colors[2]

            //dividerOpacity: !ListView.view.moving

            onClicked: stack.push_(detailsLevel,
                                   { "contact" : { "name": name
                                           , "mobile": mobile
                                           , "home": home
                                           , "work": work
                                           , "homeadress": homeadress
                                           , "workadress": workadress
                                           , "url": url
                                           , "photo": photo } },
                                   aFavDelegate)

            SpaText {
                anchors {
                    left: img.right
                    leftMargin: 23
                    verticalCenter: parent.verticalCenter
                }
                textStyle: listItem1
                text: name
                verticalAlignment: Text.AlignVCenter
            }
            Image {
                id: img
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                fillMode: Image.PreserveAspectCrop
                width: parent.height
                height: parent.height
                source: photo
                asynchronous: true
                mipmap: true
            }
            Rectangle {
                width: 2
                height: parent.height
                anchors.right: infoButton.left
                color: "black"
            }
            Spa9FlatButton {
                id: infoButton
                width: height
                height: parent.height
                anchors.right: parent.right
                buttonType: "center"
                selectedButtonColor: globalStyle.colors[2]
                iconSource: globalStyle.imageUrl + "/Icons/img_hmc_fav_OFF" + globalStyle.skinUrl + ".png"
                selectedIconSource: globalStyle.imageUrl + "/Icons/img_hmc_fav_ON" + globalStyle.skinUrl + ".png"
                selectable: true
            }
            Image{
                height: parent.height
                width: height
                anchors.right: infoButton.left
                anchors.top: parent.top
                anchors.topMargin: 0
                source: globalStyle.imageUrl + "/Icons/img_hmc_arrow_right" + globalStyle.skinUrl + ".png"
            }
        }
    }
    Component {
        id:contactDelegate
        SpaListItem {
            id: aContactDelegate
            pressedColor: globalStyle.colors[2]

            //dividerOpacity: !ListView.view.moving

            onClicked: stack.push_(detailsLevel,
                                   { "contact" : { "name": name
                                           , "mobile": mobile
                                           , "home": home
                                           , "work": work
                                           , "homeadress": homeadress
                                           , "workadress": workadress
                                           , "url": url
                                           , "photo": photo } },
                                   aContactDelegate)

            SpaText {
                anchors {
                    left: parent.left
                    leftMargin: 23
                    verticalCenter: parent.verticalCenter
                }
                verticalAlignment: Text.AlignVCenter
                textStyle: listItem1
                text: name
            }
            Rectangle {
                width: 2
                height: parent.height
                anchors.right: infoButton.left
                color: "black"
            }
            Spa9FlatButton {
                id: infoButton
                width: height
                height: parent.height
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                buttonType: "center"
                selectedButtonColor: globalStyle.colors[2]
                iconSource: globalStyle.imageUrl + "/Icons/img_hmc_fav_OFF" + globalStyle.skinUrl + ".png"
                selectedIconSource: globalStyle.imageUrl + "/Icons/img_hmc_fav_ON" + globalStyle.skinUrl + ".png"
                selectable: true
                //selected: favorite
                //onClicked: contactsModel.setFavorite(index, !favorite)
            }
            Image{
                height: parent.height
                width: height
                anchors.right: infoButton.left
                anchors.top: parent.top
                anchors.topMargin: 0
                source: globalStyle.imageUrl + "/Icons/img_hmc_arrow_right" + globalStyle.skinUrl + ".png"
            }
        }
    }
    Component {
        id:searchDelegate
        SpaListItem {
            id: aSearchDelegate
            pressedColor: globalStyle.colors[2]

            //dividerOpacity: !ListView.view.moving

            onClicked: stack.push_(detailsLevel,
                                   { "contact" : { "name": name
                                           , "mobile": mobile
                                           , "home": home
                                           , "work": work
                                           , "homeadress": homeadress
                                           , "workadress": workadress
                                           , "url": url
                                           , "photo": photo } },
                                   aSearchDelegate)

            SpaText {
                anchors {
                    left: parent.left
                    leftMargin: 23
                    verticalCenter: parent.verticalCenter
                }
                verticalAlignment: Text.AlignVCenter
                textStyle: listItem1
                text: name
            }
            Rectangle {
                width: 2
                height: parent.height
                anchors.right: infoButton.left
                color: "black"
            }
            Spa9FlatButton {
                id: infoButton
                width: height
                height: parent.height
                buttonType: "center"
                iconSource: globalStyle.imageUrl + "/Icons/img_hmc_fav_OFF" + globalStyle.skinUrl + ".png"
                selectedIconSource: globalStyle.imageUrl + "/Icons/img_hmc_fav_ON" + globalStyle.skinUrl + ".png"
                centeredIcon: true
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                selectable: true
                selected: favorite
                onClicked: contactsModel.setFavorite(index, !favorite)
            }
            Image{
                height: parent.height
                width: height
                anchors.right: infoButton.left
                anchors.top: parent.top
                anchors.topMargin: 0
                source: globalStyle.imageUrl + "/Icons/img_hmc_arrow_right" + globalStyle.skinUrl + ".png"
            }
        }
    }
}
