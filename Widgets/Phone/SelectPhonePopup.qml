import QtQuick 2.0
import "../../Common"

Item {
    anchors.fill: parent
    clip: true
    signal close()

    ListView {
        height: parent.height
        width: parent.width
        model: 3
        delegate: SpaListItem {
            width: parent.width
            x: 0
            Column {
                spacing: -2
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: 23
                SpaText {
                    textStyle: listItem1
                    text: switch(index) {
                          case 0: return "iPhone"
                          case 1: return "Samsung Galaxy"
                          case 2: return "HTC"
                          }
                }

                SpaText {
                    textStyle: listItem2
                    text: switch(index) {
                          case 0: return "Connected"
                          case 1: return "Disconnected"
                          case 2: return "Disconnected"
                          }
                }
            }
            Spa9FlatButton {
                id: button
                anchors.right: parent.right
                height: parent.height
                width: height+40
                text: "Disconnect"
                invisible: false
                visible: index === 0
            }
        }
    }
    Row {
        id: row
        anchors.bottom: parent.bottom
        width: parent.width-10
        anchors.horizontalCenter: parent.horizontalCenter
        Spa9PatchButton {
            width: parent.width/2
            text: "Add New"
            buttonType: "left"
            onClicked: coreScreen.notAvailableNotify()
        }
        Spa9PatchButton {
            width: parent.width/2
            text: "Cancel"
            buttonType: "right"
            onClicked: close()
        }
    }
}

