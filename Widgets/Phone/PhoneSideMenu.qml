import QtQuick 2.0

import "../../Common"

VisualItemModel{
    id:objectModel

    property int listItemH: (widgetSideMenu.height)/4

    Loader{sourceComponent: contactBtn}
    Loader{sourceComponent: keypadBtn}
    Loader{sourceComponent: switchBtn}
    Loader{sourceComponent: onCallBtn}

    Component{
        id:contactBtn
        Spa9FlatButton{
            height:listItemH
            text: "Contacts"
            iconSource: globalStyle.imageUrl + "/Icons/img_tel_contact" + globalStyle.skinUrl + ".png"
            selectedButtonColor: globalStyle.colors[widgetID]
            onClicked: coreScreen.showApp(phoneApp)
        }
    }
    Component{
        id:keypadBtn
        Spa9FlatButton{
            height:listItemH
            text: "Keypad"
            iconSource: globalStyle.imageUrl + "/Icons/img_tel_keypad" + globalStyle.skinUrl + ".png"
            selectedButtonColor: globalStyle.colors[widgetID]
            onClicked: coreScreen.showApp(keypadApp)

        }
    }
    Component{
        id:switchBtn
        Spa9FlatButton{
            height:listItemH
            text: "Switch"
            iconSource: globalStyle.imageUrl + "/Icons/img_tel_switch" + globalStyle.skinUrl + ".png"
            selectedButtonColor: globalStyle.colors[widgetID]
            onClicked: coreScreen.showPopup("Select Phone", "Connected Devices", 9, selectPhonePop)
        }
    }

    Component{
        id:onCallBtn
        Spa9FlatButton{
            height:listItemH
            text:"On Call"
            source: ""
            iconSource: ""
            selectedButtonColor: globalStyle.colors[widgetID]
            onClicked: {
                if(widget.selectedListHolder.visible){
                    widget.selectedListHolder.visible = false;
                }else
                    widget.selectedListHolder.visible = true;
            }
        }
    }

}
