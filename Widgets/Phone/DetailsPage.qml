/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import "../../Common/"

SpaView {
    id: root

    property alias contact: details.contact

    header: "Contacts"
    subheader: contact.name

    ContactDetails {
        id: details
    }

}
