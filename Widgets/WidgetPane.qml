import QtQuick 2.3

import "../Common"

import "../Widgets/Phone/"
import "../Widgets/Media/"
import "../Widgets/Media/Radio/"
import "../Widgets/Navi/"
import "../Widgets/Extra/"

Column {
    id: widgetPane
    width: globalStyle.screenWidth
    height: globalStyle.screenHeight
    property bool full: false


    property alias mediaWidget:mediaWidget
    property alias extraWidget:extraWidget

    Repeater {
        id: widgets
        model: model
    }

    VisualItemModel{
        id:model

        NaviWidget{
            widgetID:0
        }

        MediaWidget{
            id:mediaWidget
            widgetID:1
        }

        PhoneWidget{
            widgetID:2
        }
        ExtraWidget{
            id:extraWidget
            widgetID:3
        }
    }

    function expandWidgetAtInd(expWidget){
        var i
        for (i =0;i<widgets.count;i++){
            if(expWidget!=i){
                widgets.itemAt(i).state = "minimized"
                widgets.itemAt(i).resetWidget()
            }else{
                widgets.itemAt(i).state = "expanded"
            }
        }
    }

    function normalizeWidgets(){
        var i
        for (i =0;i<widgets.count;i++){
            widgets.itemAt(i).state = ""
            widgets.itemAt(i).resetWidget()
        }
    }
}
