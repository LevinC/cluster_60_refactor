import QtQuick 2.0
import "../../Common"
import "../../"


Rectangle {
    id:map
    clip:true
    enabled:false
    property alias flickableMap : flickableMap

    property int mapCenterX :flickableMap.contentWidth/2 - flickableMap.width/2- globalStyle.drawerwidth/2;
    property int mapCenterY :flickableMap.contentHeight/2 + 150;

    Flickable{
        id:flickableMap
        anchors.fill: parent
        contentX: mapCenterX - 183/2;
        contentY: mapCenterY - globalStyle.expandedHeight +348;
        contentWidth: mapIm.width; contentHeight: mapIm.height
        Image {
            id: mapIm
            source: globalStyle.imageUrl + "/karta.png"
        }
        MouseArea {
            anchors.fill: parent
            onPressAndHold: {

                var xPos = mouseX - mapCenterX;
                var yPos;
                if (map.state == "expanded"){
                    yPos = mouseY + 150 - globalStyle.expandedHeight;
                }else if (map.state == "fullScreen"){
                    yPos = mouseY + 150 - globalStyle.fullHeight;
                }



                flickableMap.contentX = xPos;
                flickableMap.contentY = yPos;

            }

            onClicked: {
                if (map.state != "fullScreen"){
                    widgetPane.normalizeWidgets();
                }
            }
        }
        Behavior on contentX {CubicAnimation{}}
        Behavior on contentY {CubicAnimation{}}
    }


    onStateChanged: {
        if (state == "expanded" || state == "" || state == "minimized"){
            flickableMap.contentX = mapCenterX-183/2;
            flickableMap.contentY = mapCenterY - globalStyle.expandedHeight +348;
        }else if (state == "fullScreen"){
            flickableMap.contentX = mapCenterX-183/2
            flickableMap.contentY = mapCenterY -globalStyle.fullHeight + 348;
        }
    }

    states: [
        State {
            name: "minimized"
            PropertyChanges {target: map;opacity:0}
            PropertyChanges {target: map;enabled:false}
        },
        State {
            name: "expanded"
            PropertyChanges {target: map;opacity:1}
            PropertyChanges {target: flickableMap;interactive:false}
            PropertyChanges {target: map;enabled:true}
        },
        State {
            name: "fullScreen"
            PropertyChanges {target: map;opacity:1}
            PropertyChanges {target: flickableMap;interactive:true}
            PropertyChanges {target: map;enabled:true}
        }
    ]

    transitions: [
        Transition {
            CubicAnimation{target:map; property:'opacity'}
        }
    ]

}

