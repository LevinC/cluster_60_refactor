import QtQuick 2.0
import "../../Common"

Item {
    anchors.fill: parent
    signal close()
    signal cancel()

    SpaText {
        anchors.top: parent.top
        anchors.bottom: row.top
        anchors.left: parent.left
        anchors.right: parent.right
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        wrapMode: Text.WordWrap
        text: "Do you want to cancel the current navigation?"
    }

    Row {
        id: row
        anchors.bottom: parent.bottom
        width: parent.width-10
        height: childrenRect.height
        anchors.horizontalCenter: parent.horizontalCenter
        Spa9PatchButton {
            width: parent.width/2
            text: "Confirm"
            buttonType: "left"
            onClicked: { onRoute=false; close();cancel()}
        }
        Spa9PatchButton {
            width: parent.width/2
            text: "Cancel"
            buttonType: "right"
            onClicked: close()
        }
    }

}

