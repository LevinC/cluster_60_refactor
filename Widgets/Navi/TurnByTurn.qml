import QtQuick 2.0

import "../../Common"
import "../../"

Rectangle {
    id:root
    width: globalStyle.drawerwidth
    height: content.height
    anchors.top:content.top
    anchors.left: content.left
    anchors.leftMargin:-globalStyle.drawerwidth-20
    clip:true
    color:globalStyle.navigationColor

    Behavior on anchors.leftMargin{CubicAnimation{}}

    Rectangle{
        id:header
        width:parent.width
        height:140
        color:globalStyle.navigationColor

        Column{
            anchors.fill: parent
            Image{
                height:80
                width:80
                anchors.horizontalCenter: parent.horizontalCenter
                source: globalStyle.imageUrl +"/icons/img_nav_tbt_rotary_straight" + globalStyle.skinUrl + ".png"
            }
            SpaText{
                height:20
                width:parent.width
                horizontalAlignment: Text.AlignHCenter
                text:"200 m"
            }
            SpaText{
                height:20
                width:parent.width
                horizontalAlignment: Text.AlignHCenter
                text:"Street name"
            }
        }
    }

    ListView{
        clip: true
        anchors.top:header.bottom
        anchors.bottom: backButton.top
        width:parent.width
        model:10
        delegate: Component{

            Spa9FlatButton{
                source: "/icons/img_nav_tbt_rotary_straight"
                width:globalStyle.drawerwidth
                height:120
                text:"200 m"
            }
        }
    }

    Rectangle{
        id:backButton
        height:80
        width:globalStyle.drawerwidth
        anchors.bottom: parent.bottom
        color:globalStyle.navigationColor

        Spa9FlatButton{
            anchors.fill: parent
            text:"Back"
            invisible: false
            onClicked: {
                widget.turnByTurn.state = ""
                widgetObjects.state=""
                lvl2Menu.state="shown"
            }
        }
    }

    states: [
        State {
            name: "shown"
            PropertyChanges {
                target: root
                anchors.leftMargin:0
            }
        }
    ]


}

