import QtQuick 2.0

import "../../Common/"

VisualItemModel{
    property alias btn4:btn4


    property int listItemH: (widgetSideMenu.height)/4


    Loader{
        sourceComponent: onRoute ? endNav : setDest
    }

    Loader{
        sourceComponent: onRoute ? setDest : poiBtn
    }

    Loader{
        sourceComponent: onRoute ? muteGuide : empty
    }

    Loader{
        id:btn4
        sourceComponent: onRoute ? moreBtn : empty
        states: [
            State {
                name: "back"
                PropertyChanges {
                    target: btn4
                    sourceComponent: backBtn
                }
            }
        ]
    }

    Component{
        id:poiBtn
        Spa9FlatButton{
            width:globalStyle.drawerwidth
            height:listItemH
            source:"/img_nav_on_route"
            selectedButtonColor: globalStyle.colors[widgetID]
            text: "POI/Traffic Ahead"
            onClicked: {
                widget.showPoi()
            }
        }
    }

    Component{
        id:endNav
        Spa9FlatButton{
            width:globalStyle.drawerwidth
            height:listItemH
            source:"/clear_route"
            selectedButtonColor: globalStyle.colors[widgetID]
            text: "End Navigation"
            onClicked: {
                coreScreen.showPopup("Cancel Navigation", "", 0, naviPop)
            }
        }
    }

    Component{
        id:setDest
        Spa9FlatButton{
            width:globalStyle.drawerwidth
            height:listItemH
            source:"/Icons/img_nav_pin_in_list_dest"
            selectedButtonColor: globalStyle.colors[widgetID]
            text: "Set Destination"
            onClicked: {
                naviApp.stack.initialItem.currentTab = 0
                coreScreen.showApp(naviApp)
            }
        }
    }

    Component{
        id:muteGuide
        Spa9FlatButton{
            width:globalStyle.drawerwidth
            height:listItemH
            source:"/img_nav_mute_OFF"
            selectedButtonColor: globalStyle.colors[widgetID]
            text: "Mute Guidance"
            onClicked: {

            }
        }
    }


    Component{
        id:moreBtn
        Spa9FlatButton{
            width:globalStyle.drawerwidth
            height:listItemH
            source:"/Icons/more_icon"
            selectedButtonColor: globalStyle.colors[widgetID]
            onClicked: {
                lvl2Menu.toggleVisible()
            }
        }
    }

    Component{
        id:backBtn
        Spa9FlatButton{
            width:globalStyle.drawerwidth
            height:listItemH
            source:"/Icons/img_hmc_arrow_left"
            selectedButtonColor: globalStyle.colors[widgetID]
            onClicked: {
                lvl2Menu.toggleVisible()
            }
        }
    }

    Component{
        id:empty
        Spa9FlatButton{
            width:globalStyle.drawerwidth
            height:listItemH
            source:""
            enabled:false
            opacity:0
        }
    }


}
