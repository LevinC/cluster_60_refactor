/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import QtPositioning 5.3
import "../../Common/"


SpaView {
    id: root

    header: "Info card"
    subheader: "destination"

    staticPage: true

    property int routeIndex: 0

    Item {
        width: 768
        height: 600
        Column {
            width: 480
            height: 178
            anchors.left: parent.left
            anchors.leftMargin: 50
            anchors.top: parent.top
            anchors.topMargin: 20
            SpaText {
                id: dest
                text: subheader
                width: 480
                textStyle: listHeader1
                wrapMode: Text.WordWrap
            }
            SpaText {
                text: "New York"
                width: 480
                textStyle: listHeader1
                wrapMode: Text.WordWrap
            }
            SpaText {
                text: "New York"
                width: 480
                textStyle: listHeader1
                wrapMode: Text.WordWrap
            }
            SpaText {
                text: "USA"
                width: 480
                textStyle: listHeader1
                wrapMode: Text.WordWrap
            }
            Row {
                SpaText {
                    text: "7.65875 N"
                    width: 240
                    textStyle: listHeader1
                    wrapMode: Text.WordWrap
                }
                SpaText {
                    text: "13.5563 E"
                    width: 240
                    textStyle: listHeader1
                    wrapMode: Text.WordWrap
                }
            }
            Row {
                SpaText {
                    text: "ETA: --:--"
                    width: 240
                    textStyle: listHeader1
                    wrapMode: Text.WordWrap
                }
                SpaText {
                    text: "Distance: xxxx km"
                    width: 240
                    textStyle: listHeader1
                    wrapMode: Text.WordWrap
                }
            }
        }
        Image {
            id: img
            width: 178
            height: width
            anchors.right: parent.right
            anchors.rightMargin: 50
            anchors.top: parent.top
            anchors.topMargin: 20
            source: routeListModel.favRoutes.get(routeIndex).thumbnail
        }
        SpaDivider {
            anchors.top: img.bottom
            anchors.topMargin: 24//54
            anchors.rightMargin: 50
            anchors.leftMargin: 50
        }

        Spa9FlatButton {
            id: button
            anchors.top: img.bottom
            anchors.topMargin: 34//64
            anchors.horizontalCenter: parent.horizontalCenter
            height: globalStyle.okButtonSize
            width: 624
            buttonColor: "flatgrnbtn"
            buttonType: "standalone"
            text: "Start route"
            invisible: false

            onClicked: {
                onRoute = true
                coreScreen.closeApp()
            }
        }
        Row {
            id: buttonS
            anchors.top: button.bottom
            anchors.topMargin: 10
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 2
            Spa9FlatButton {
                width: 624/3
                height: globalStyle.okButtonSize
                text: "Add as waypoint"
                visible: !mapElement.pauseRoute
                invisible: false
            }
            Spa9FlatButton {
                id: buttonSave
                width: mapElement.pauseRoute ? 624/2 : 624/3
                height: globalStyle.okButtonSize
                text: "Save"
                invisible: false
            }
            Spa9FlatButton {
                width: buttonSave.width
                height: globalStyle.okButtonSize
                text: "Show nearby"
                invisible: false
            }
        }

        SpaDivider {
            id: buttonDivider
            anchors.top: buttonS.bottom
            anchors.topMargin: 10
            anchors.rightMargin: 50
            anchors.leftMargin: 50
        }
        ListView {
            id: waypointsList
            width: 768
            height: 400
            anchors.top: buttonDivider.top
            clip: true
            snapMode: ListView.SnapToItem

            model: routeListModel.destinations.get(routeIndex).information

            footer: Item {
                height: 64
                width: 500
            }

            delegate: SpaListItem {
                x: 50

                dividerOpacity: !ListView.view.moving

                //onClicked:
                Column {
                    spacing: 0
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: icon.right
                    anchors.leftMargin: 23
                    SpaText {
                        textStyle: listItem1
                        text: item1
                        width: 668
                        wrapMode: Text.WordWrap
                    }
                    SpaText {
                        textStyle: listItem1
                        text: item2
                        width: 668
                        wrapMode: Text.WordWrap
                    }
                }
                Image{
                    id: icon
                    height: parent.height
                    width: height
                    anchors.left: parent.left
                    anchors.top: parent.top
                    anchors.topMargin: 0
                    source: {
                        switch (index) {
                        case 0:
                            return globalStyle.imageUrl + "/Icons/img_hmc_phone" + globalStyle.skinUrl + ".png"
                        case 1:
                            return globalStyle.imageUrl + "/Icons/img_hmc_browser" + globalStyle.skinUrl + ".png"
                        }
                    }
                }
            }
        }
        SpaScrollBar {
            id: scrollBar

            width: 6
            height: waypointsList.height

            anchors.top: buttonDivider.bottom
            anchors.right: parent.right
            anchors.rightMargin: 31

            flickable: waypointsList
        }
    }
}
