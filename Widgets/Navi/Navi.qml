/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import "../../Common/"
import "../../"

SpaHierarchyWindow {
    id: naviAppRoot

    SpaStyle{
        id:globalStyle
    }

    function setDest() {
        resetSearchPage()
        core.closeApp();
    }

    function resetSearchPage() {
        keyboard.erase()
        stack.pop({'item': stack.initialItem, 'immediate' : true })
    }

    function _keyboardBinding() {
        keyboard.show( naviAppRoot.state === "search", searchResultsPage )
    }

//    hasToolBarButton: stack.depth > 1 ? false : stack.initialItem.currentTabItem.hasToolBarButton
//    hasKeyBoard: stack.depth > 1 ? false : stack.initialItem.currentTabItem.hasKeyBoard
    hasToolBarButton: stack.depth > 1 ? false : stack.initialItem.currentTabItem && stack.initialItem.currentTabItem.hasToolBarButton
    hasKeyBoard: stack.depth > 1 ? false : stack.initialItem.currentTabItem && stack.initialItem.currentTabItem.hasKeyBoard
    keyboardText: searchResultsPage.keyboardText

    stack.initialItem: SpaTabView {
        id: tabView
        header: "Destinations"
        highlightColor: globalStyle.colors[0]

        tabs: VisualItemModel {

            SpaListPage {
                id: searchResultsPage
                hasKeyBoard: true
                keyboardText: ""
                onKeyboardTextChanged: core.notAvailableNotify()
            }
            SpaListPage {
                id: favorites
                subheader: "Favorites"
                delegate: favDelegate
                model: routeListModel.favRoutes
                hasToolBarButton: true
            }
            SpaListPage {
                id: recents
                subheader: "Recent"
                delegate: recentDelegate
                model: routeListModel.recentRoutes
            }

            SpaListPage {
                id: library
                subheader: "Library"
                header: Item {
                    height: globalStyle.listItemSize
                    width: 665
                    SpaText {
                        anchors.centerIn: parent
                        text: "Nothing has been saved to library"
                    }
                }
            }

            SpaListPage {
                id: poi
                subheader: "POI"
                delegate: poiDelegate
                model: routeListModel.pois
            }
            SpaListPage {
                id: divisions
                subheader: "Divisions"
                delegate: addressDelegate
                model: ListModel {
                    ListElement { category: "Prefecture:" }
                    ListElement { category: "City:" }
                    ListElement { category: "Area:" }
                    ListElement { category: "Number:" }
                }
            }

            SpaListPage {
                id: address
                subheader: "Address"
                delegate: addressDelegate
                model: ListModel {
                    ListElement { category: "Area:" }
                    ListElement { category: "City:" }
                    ListElement { category: "Street:" }
                    ListElement { category: "Number:" }
                    ListElement { category: "Junction:" }
                }
            }
        }

        Component {
            id:searchDelegate
            SpaListItem {
                id: aSearchDelegate
                pressedColor: globalStyle.firstColor

                //dividerOpacity: !ListView.view.moving

                onClicked: stack.push_(infoCardPosition,{"subheader": destination })

                Column {
                    spacing: -2
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: 23
                    SpaText {
                        textStyle: listItem1
                        text: destination
                    }
                }
            }
        }
        Component {
            id:recentDelegate
            SpaListItem {
                id: aRecDelegate
                pressedColor: globalStyle.colors[0]

                //dividerOpacity: !ListView.view.moving

                onClicked: stack.push_(infoCardPosition,{"subheader": routeTitle, "routeIndex": index })
                Column {
                    spacing: -2
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: pin.right
                    anchors.leftMargin: 23
                    SpaText {
                        width: 460
                        textStyle: listItem1
                        text: destination
                        elide: Text.ElideRight
                    }
                    SpaText {
                        width: 460
                        textStyle: listItem2
                        text: info
                        elide: Text.ElideRight
                    }
                }
                Image{
                    id: pin
                    height: parent.height
                    width: height
                    anchors.left: parent.left
                    anchors.top: parent.top
                    anchors.topMargin: 0
                    source: globalStyle.imageUrl + "/Icons/img_nav_pin_in_list_unspec" + globalStyle.skinUrl + ".png"
                }
                Image{
                    height: parent.height
                    width: height
                    anchors.right: parent.right
                    anchors.top: parent.top
                    anchors.topMargin: 0
                    source: globalStyle.imageUrl + "/Icons/img_hmc_arrow_right" + globalStyle.skinUrl + ".png"
                }
            }
        }

        Component {
            id:favDelegate
            SpaListItem {
                id: aFavDelegate
                pressedColor: globalStyle.colors[0]

                dividerOpacity: !ListView.view.moving

                //onClicked: stack.push_(infoCard,{"subheader": routeSubTitle, "header": routeTitle, "routeIndex": index}, aFavDelegate)

                SpaText {
                    anchors.baseline: parent.top
                    anchors.baselineOffset: 38
                    anchors.left: icon.right
                    anchors.leftMargin: 23
                    textStyle: listItem1
                    text: routeTitle
                }

                SpaText {
                    anchors.left: icon.right
                    anchors.leftMargin: 23
                    anchors.baseline: parent.top
                    anchors.baselineOffset: 69
                    textStyle: listItem2
                    text: routeSubTitle
                }
                Image{
                    id: icon
                    height: parent.height
                    width: height
                    anchors.left: parent.left
                    anchors.top: parent.top
                    anchors.topMargin: 0
                    source: index === 0 ? globalStyle.imageUrl + "/Icons/img_nav_pin_in_list_home" + globalStyle.skinUrl + ".png" : globalStyle.imageUrl + "/Icons/img_nav_pin_in_list_favourite" + globalStyle.skinUrl + ".png"
                }
            }
        }
        Component {
            id:addressDelegate
            SpaListItem {
                pressedColor: globalStyle.colors[0]

                //dividerOpacity: !ListView.view.moving

                //onClicked:
                Row {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: 23
                    SpaText {
                        textStyle: listItem1
                        text: category
                        height: 84
                        width: 140
                        verticalAlignment: Text.AlignVCenter
                    }
                    Item {
                        height: 84
                        width: 480
                        anchors.verticalCenter: parent.verticalCenter
                        Image {
                            source: globalStyle.imageUrl + "/Icons/img_hmc_mic" + globalStyle.skinUrl + ".png"
                        }
                        BorderImage {
                            id: inputfieldbg
                            width: parent.width
                            height: parent.height
                            border { left: 10; right: 10; top: 10; bottom: 10 }
                            horizontalTileMode: BorderImage.Stretch
                            source: globalStyle.imageUrl + "/HMI_Core/img_hmc_inputfield_OFF.png"
                        }
                    }
                }
            }
        }
        Component {
            id:poiDelegate
            SpaListItem {
                pressedColor: globalStyle.colors[0]

                //dividerOpacity: !ListView.view.moving

                //onClicked:
                SpaText {
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    anchors.left: icon.right
                    anchors.leftMargin: 23
                    textStyle: listItem1
                    text: destination
                }
                Image {
                    id: icon
                    height: parent.height
                    width: height
                    anchors.left: parent.left
                    anchors.top: parent.top
                    anchors.topMargin: 0
                    source: globalStyle.imageUrl + "/HMI_Core/img_hmc_listitem_bg" + globalStyle.skinUrl + ".png"
                    Image{
                        height: parent.height
                        width: height
                        source: globalStyle.imageUrl + poiIcon
                    }
                }
                Image{
                    height: parent.height
                    width: height
                    anchors.right: parent.right
                    anchors.top: parent.top
                    source: globalStyle.imageUrl + "/Icons/img_hmc_arrow_right" + globalStyle.skinUrl + ".png"
                }
            }
        }
        Component {
            id: infoCardPosition
            InfoCard {
            }
        }
    }
}
