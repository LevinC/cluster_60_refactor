import QtQuick 2.0

import "../../Common"
import "../../"

Widget {
    id:widget
    leftMenuBGColor: globalStyle.navigationColor
    widgetSideMenu.model:naviSideMenu

    property bool firstExpand:true
    property bool onRoute:false
    property alias turnByTurn:turnByTurn

    gradient.opacity: state === "expanded" ||state === "fullScreen" ? 0:1
    subHeaderTextString: onRoute ? "ETA: 23 min   Dist: 14 km" : "Current Position"
    headerTextString: onRoute ? "Next: Washington Avenue" : "Harlem Avenue"

    property var naviApp: {
        var component = Qt.createComponent("Navi.qml")
        if (component.errorString()) {
            console.log(component.errorString())
            return null
        }
        else {
            return component.createObject()
        }
    }

    NaviSideMenu{
        id:naviSideMenu
    }


    Component {
        id: naviPop
        EndNaviPopup {
            onCancel: {
                lvl2Menu.state =""
                lvl3Menu.state = ""
                widgetObjects.state=""
            }
        }
    }
    Map{
        id:mapElement
        state: parent.state
        anchors.fill:widget.content
        opacity: 0
        flickableMap.interactive : false

        MapHeader{
            state:widget.state
            width: content.width-globalStyle.drawerwidth
            x:globalStyle.drawerwidth
        }

    }

    Rectangle{
        z:2
        anchors.right:mapElement.right
        anchors.bottom:mapElement.bottom
        width:54
        height:54
        visible: widget.state == "fullScreen" || widget.state == "expanded" ? true:false
        color: "#50000000"

        Image {
            id:fullScreenIm
            anchors.fill: parent
            source:globalStyle.imageUrl + "/img_nav_gotofull" + globalStyle.skinUrl + ".png"

        }

        Spa9FlatButton {
            id:fullScreenBtn
            anchors.fill: parent
            visible:lvl2Menu.state !=="shown" && lvl3Menu.state !=="shown"
            onClicked: {
                if (widget.state == "fullScreen"){
                    widget.state = "expanded"
                    widget.sideMenuHolder.state = ""
                    fullScreenIm.source = globalStyle.imageUrl + "/img_nav_gotofull" + globalStyle.skinUrl + ".png"
                }else{
                    widget.state = "fullScreen"
                    widget.sideMenuHolder.state = "collapsed"
                    fullScreenIm.source = globalStyle.imageUrl + "/img_nav_extfullscrn" + globalStyle.skinUrl + ".png"
                }
            }
        }
    }

    Image {
        id:recentDest
        opacity:0
        source: globalStyle.imageUrl + "/assets/recent_destinations.png"
        anchors.left:mapElement.left
        anchors.leftMargin: 10 + globalStyle.drawerwidth
        anchors.bottom:mapElement.bottom
        anchors.bottomMargin: 10
    }

    SequentialAnimation{
        id:fadeOutRecent
        PauseAnimation { duration: 450 }
        NumberAnimation{
            target:recentDest
            property:"opacity"
            to:1
            duration: 500
        }
        PauseAnimation { duration: 3000 }
        NumberAnimation{
            target:recentDest
            property:"opacity"
            to:0
            duration: 500
        }
    }

    NaviMenuTwo{
        z:0
        id:lvl2Menu
        anchors.fill: content
        visible:false
        clip:true

        onResetSideMenues: {
            lvl2Menu.state = ""

            lvl3Menu.state = ""
            widgetObjects.state=""
            turnByTurn.state = ""
        }

        onStateChanged: {
            if (state ===""){
                naviSideMenu.btn4.state = ""
            }else{
                naviSideMenu.btn4.state = "back"
            }
        }

        onShowItin: {
            showMenu3()
            lvl3Menu.contentLoader.sourceComponent = lvl3Menu.itinerary
        }

        onShowPOI: {
            showMenu3()
            lvl3Menu.contentLoader.sourceComponent = lvl3Menu.poi
        }
    }

    function showPOI(){
        showMenu3()
        lvl3Menu.contentLoader.sourceComponent = lvl3Menu.poi
    }

    NaviMenu3{
        id:lvl3Menu
        z:2
        width:widget.content.width
        height:450
        y:content.y

        onBack: {
            widgetObjects.state=""
            lvl2Menu.state = "shown"
            lvl3Menu.state = ""
        }
    }

    function toggleSlideMenu(){
        if (widgetObjects.state==="slidedLeft"){
            widgetObjects.state=""
            lvl2Menu.state="shown"
            lvl3Menu.state = ""
        }else{
            widgetObjects.state="slidedLeft"
            lvl2Menu.state="hidden"
            lvl3Menu.state = "shown"
        }
    }

    function showMenu3(){
        widgetObjects.state="slidedLeft"
        lvl2Menu.state="hidden"
        lvl3Menu.state = "shown"
    }


    Rectangle{
        id:sideMenuLine
        height:1
        width:globalStyle.drawerwidth-40
        x:20 + globalStyle.widgetHeightInset + sideMenuHolder.x
        y:globalStyle.widgetIconSize + globalStyle.widgetHeightInset
        z:2
        opacity:0
        state:parent.state

        states: [
            State {
                name: "expanded"
                PropertyChanges {target: sideMenuLine; y: globalStyle.widgetIconExpandedSize+globalStyle.widgetHeightInset}
                PropertyChanges {target: sideMenuLine; opacity:.1}
                PropertyChanges {target: widgetTopSection; opacity:0}
            },
            State {
                name: "fullScreen"
                PropertyChanges {target: sideMenuLine; y: globalStyle.widgetIconExpandedSize+globalStyle.widgetHeightInset}
                PropertyChanges {target: sideMenuLine; opacity:.1}
                PropertyChanges {target: widgetTopSection; opacity:0}
            }
        ]

        transitions: [
            Transition {
                CubicAnimation{target:sideMenuLine; property:'y'}
            }
        ]
    }

    NavigationIcon{
        z:2
        state:widget.state
        iconImage.source: globalStyle.imageUrl + "/assets/ic_app_navconn_def" + globalStyle.skinUrl + ".png"
    }

    onStateChanged: {
        if (state == "minimized"){
            lvl2Menu.state = ""
            lvl3Menu.state = ""

        }
        if (state =="expanded"){
            if (firstExpand){
                firstExpand=false
                fadeOutRecent.start()
            }
        }else{
            fadeOutRecent.stop()
            recentDest.opacity = 0
        }
    }

    TurnByTurn{
     id:turnByTurn
    }

}
