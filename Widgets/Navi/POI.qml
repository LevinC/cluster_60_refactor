import QtQuick 2.0

import "../../Common/"

Item {
    id:root
    signal goBack

    Row{
        id:topRow
        height:80
        spacing: 2
        Spa9FlatButton {
            width: root.width/4
            height: topRow.height
            buttonType: "center"
            invisible: false
            SpaText {
                width:parent.width
                anchors.verticalCenter: parent.verticalCenter
                horizontalAlignment: Text.AlignHCenter
                text: "Back"
            }
            onClicked: goBack()
        }
        Spa9FlatButton {
            width: root.width * 3/8
            height: topRow.height
            buttonType: "center"
            invisible: false
            SpaText {
                width:parent.width
                anchors.verticalCenter: parent.verticalCenter
                horizontalAlignment: Text.AlignHCenter
                text: "POI:s"
            }
        }
        Spa9FlatButton {
            width: root.width * 3/8
            height: topRow.height
            buttonType: "center"
            invisible: false

            SpaText {
                width:parent.width
                anchors.verticalCenter: parent.verticalCenter
                horizontalAlignment: Text.AlignHCenter
                text: "Traffic"
            }
        }
    }

    ListView{
        y:topRow.height
        width:root.width
        height:root.height - topRow.height
        clip:true

        model:model

        VisualItemModel{
            id:model
            ItineraryListObject{}
            ItineraryListObject{}
            ItineraryListObject{}
            ItineraryListObject{}
            ItineraryListObject{}
        }

    }
}

