import QtQuick 2.0

import "../../"
import "../../Common/"

Item {
    id:sideMenu

    signal showItin
    signal showPOI
    property int buttonHeight:widget.expHeight/objectModel.count
    signal resetSideMenues

    function toggleVisible(){
        if (state==="shown"){
            state=""
        }else{
            state="shown"
        }
    }

    MouseArea{
        anchors.fill: parent
        onClicked: {
            resetSideMenues()
        }
    }

    Rectangle{
        id:list
        width: globalStyle.drawerwidth
        height:sideMenu.height
        color: globalStyle.navigationColor
        Column{
            anchors.fill: parent
            Repeater{
                id:listView
                model: objectModel
            }

        }
    }

    VisualItemModel{
        id:objectModel
        Loader{
            sourceComponent: itineraryBtn
        }
        Loader{
            sourceComponent: poiBtn
        }
        Loader{
            sourceComponent: tBtBtn
        }
        Loader{
            sourceComponent: avoidBtn
        }
        Loader{
            sourceComponent: guideBtn
        }
    }

    Component{
        id:itineraryBtn
        Spa9FlatButton{
            width:globalStyle.drawerwidth
            height:buttonHeight
            source:"/Icons/img_nav_route_item"
            selectedButtonColor: globalStyle.colors[widgetID]
            text: "Itinerary"
            onClicked: {
                showItin()
            }
        }
    }

    Component{
        id:poiBtn
        Spa9FlatButton{
            width:globalStyle.drawerwidth
            height:buttonHeight
            source:"/img_nav_on_route"
            selectedButtonColor: globalStyle.colors[widgetID]
            text: "POI/Traffic Ahead"
            onClicked: {
                showPOI()
            }
        }
    }

    Component{
        id:tBtBtn
        Spa9FlatButton{
            width:globalStyle.drawerwidth
            height:buttonHeight
            source:"/img_nav_tbt_list"
            selectedButtonColor: globalStyle.colors[widgetID]
            text: "Turn-by-Turn"
            onClicked: {
                widget.turnByTurn.state = "shown"
                widgetObjects.state="slidedLeft"
                lvl2Menu.state="hidden"
            }
        }
    }

    Component{
        id:avoidBtn
        Spa9FlatButton{
            width:globalStyle.drawerwidth
            height:buttonHeight
            source:"/img_nav_toolbar_avoid_ON"
            selectedButtonColor: globalStyle.colors[widgetID]
            text: "Avoid Ahead"
            onClicked: {
            }
        }
    }

    Component{
        id:guideBtn
        Spa9FlatButton{
            width:globalStyle.drawerwidth
            height:buttonHeight
            source:"/Icons/img_nav_pause_OFF"
            selectedButtonColor: globalStyle.colors[widgetID]
            text: "Resume Guidance"
            onClicked: {
            }
        }
    }

    states: [
        State {
            name: "shown"
            PropertyChanges {target: list; x:globalStyle.drawerwidth}
            PropertyChanges {target: sideMenu; visible:true}

        },
        State {
            name: "hidden"
            PropertyChanges {target: list; x:-list.width}
            PropertyChanges {target: sideMenu; visible:true}

        }
    ]

    transitions: [
        Transition {
            to:""
            CubicAnimation{target:list; property:'x'}
            SequentialAnimation {
                PauseAnimation { duration: 300 }
                CubicAnimation { target: sideMenu; property:'visible'; duration: 10}
            }
        },
        Transition {
            to:"shown";
            CubicAnimation{target:list; property:'x';duration:200}
            CubicAnimation { target: sideMenu; property:'opacity'}
        },
        Transition {
            to:"hidden";
            CubicAnimation{target:list; property:'x';duration:200}
            CubicAnimation { target: sideMenu; property:'opacity'}
        }
    ]


}
