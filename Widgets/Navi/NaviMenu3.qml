import QtQuick 2.0

import "../../"

Rectangle {
    id:topMenu

    property alias contentLoader:contentLoader
    property alias itinerary:itinerary
    property alias poi:poi

    signal back

    clip:true
    z:0
    x:globalStyle.drawerwidth + width
    color: globalStyle.navigationColor
    opacity:0

    Behavior on opacity  {CubicAnimation{}}

    MouseArea{
        anchors.fill: parent
        onClicked: {}
    }

    states: [
        State {
            name: "shown"
            PropertyChanges {target: topMenu; x:content.x}
            PropertyChanges {target: topMenu; opacity:1}
        }
    ]

    transitions: [
        Transition {
            CubicAnimation{target:topMenu; property:'x'}
            CubicAnimation{target:topMenu; property:'opacity'}
        }
    ]

    Loader{
        id:contentLoader
    }

    Component{
        id:itinerary
        Itinerary{
            width: topMenu.width
            height:topMenu.height
            onGoBack: back()
        }
    }

    Component{
        id:poi
        POI{
            width: topMenu.width
            height:topMenu.height
            onGoBack: back()
        }
    }


}

