import QtQuick 2.0

import "../../Common/"
import "../../"

Rectangle {
    id: mapHeader
    height: 42
    visible:false;
    color: "#70000000"
    Item{
        id:toggleItem
        anchors.fill:parent

        Row {
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.leftMargin: 4
            Image {
                id:flag
                height: 42
                width: 42
                y: 0
                source: globalStyle.imageUrl + "/Icons/img_nav_pin_in_list_dest" + globalStyle.skinUrl + ".png"
            }

            SpaText {
                id: leftTextField
                height:parent.height
                anchors.leftMargin: 12
                verticalAlignment:Text.AlignVCenter
                text:"Washington Avenue"
                font.pixelSize: 24
                width: 250
                elide: Text.ElideRight
            }
        }
        SpaText {
            id: rightTextField
            anchors.fill:parent
            verticalAlignment:Text.AlignVCenter
            anchors.rightMargin: 12
            text:"ETA: 23 min Dist: 14 km"
            font.pixelSize: 24
            horizontalAlignment: Text.AlignRight
        }


        states: [
            State {
                name: "toggle"
                PropertyChanges {target: rightTextField; text:"Position: Harbour Lane"}
                PropertyChanges {target: leftTextField; text:"Altitude: 500 m"}
                PropertyChanges {target: flag; width:0}
            }
        ]
    }

    MouseArea {
        anchors.fill: parent

        onClicked: {
            if (toggleItem.state==""){
                toggleItem.state = "toggle"
            }else{
                toggleItem.state = ""
            }
        }
    }


    states: [
        State {
            name: "expanded"
            PropertyChanges {target: mapHeader; visible:true}
        },
        State {
            name: "fullScreen"
            PropertyChanges {target: mapHeader; visible:true}
        }
    ]
}
