import QtQuick 2.0

import "../../Common/"
import "../../"


Item {
    id:icon
    property alias iconImage : iconImage
    width: globalStyle.drawerwidth
    x:widgetObjects.x + globalStyle.widgetWidthInset + sideMenuHolder.x
    y:globalStyle.widgetHeightInset
    height: globalStyle.normalHeight

    Image {
        id: iconImage
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        anchors.verticalCenterOffset: -20
        width: height
        height: globalStyle.widgetIconSize-30
    }

    SpaText {
        id: text
        width:parent.width
        height:40
        color:"white"
        y: globalStyle.normalHeight - height-20
        verticalAlignment:Text.AlignVCenter
        anchors.rightMargin: 12
        text:"200 m"
        font.pixelSize: 22
        horizontalAlignment: Text.AlignHCenter
        Behavior on font.pixelSize { CubicAnimation {} }
    }

    states: [
//        State {
//            name: "expanded"
//            PropertyChanges {target: icon; height: globalStyle.topExpandedHeight}
//            PropertyChanges {target: iconImage; height: globalStyle.widgetIconSize-50}
//            PropertyChanges {target: text; y:globalStyle.topExpandedHeight-text.height*1.4}
//        },
        State {
            name: "minimized"
            PropertyChanges {target: text; opacity:0}
            PropertyChanges {target: icon; height: globalStyle.minimizedHeight}
            PropertyChanges {target: iconImage; height: globalStyle.minimizedHeight}
            PropertyChanges {target: iconImage; anchors.verticalCenterOffset:0}
            PropertyChanges {target: text; y:globalStyle.minimizedHeight-text.height+6}
            PropertyChanges {target: text; font.pixelSize: 10}
        }
//        ,
//        State {
//            name: "fullScreen"
//            PropertyChanges {target: icon; height: globalStyle.topExpandedHeight}
//            PropertyChanges {target: iconImage; height: globalStyle.widgetIconSize-50}
//            PropertyChanges {target: text; y:globalStyle.topExpandedHeight-text.height*1.4}
//        }
    ]

    transitions: [
        Transition {
            CubicAnimation{target:icon; property:'height'}
            CubicAnimation{target:iconImage; property:'height'}
            CubicAnimation{target:iconImage; property:'width'}
            CubicAnimation{target:iconImage; property:'y'}
            CubicAnimation{target:text; property:'y'}
            CubicAnimation{target:text; property:'opacity'}
            CubicAnimation{target:iconImage; property:'anchors.verticalCenterOffset'}

        }
    ]
}

