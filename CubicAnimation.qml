import QtQuick 2.0

NumberAnimation {duration: 300
    easing {type: Easing.Bezier
        bezierCurve: [0.42,0.0,    1.0,1.0,    1.0,1.0]
    }
}
