import QtQuick 2.0
import "../Common/"

Item {
    id: vehicleSettingsRoot

    SpaText {
        id: vehicleSettingsTitle
        anchors.left: parent.left
        anchors.leftMargin: 35
        anchors.top: parent.top
        anchors.topMargin: 20
        height: 50
        color:"white"
        text: qsTr("Car Functions")
    }

    VehicleSettingsGridView {
        id: vehicleSettingsGrid
        anchors.top: vehicleSettingsTitle.bottom
        anchors.horizontalCenter: vehicleSettingsRoot.horizontalCenter
        width: 711
        cellWidth: 236
        cellHeight: 114
    }
}
