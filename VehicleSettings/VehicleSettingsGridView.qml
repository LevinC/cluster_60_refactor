import QtQuick 2.0

GridView {
    id: vehicleSettingsGridViewRoot
    interactive: false
    height: vehicleSettingsGridViewRoot.model.count * cellHeight //globalStyle.gridCellSize
    model: VehicleSettingsModel { }
    delegate: VehicleSettingsItemDelegate { }
}
