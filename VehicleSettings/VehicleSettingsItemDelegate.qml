/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import "../Common/"

Item {
    id: vehicleSettingsItemDelegate

    width: 242
    height: 121

    Rectangle{
        anchors.fill: parent
        anchors.rightMargin: 5
        anchors.leftMargin: 5
        anchors.topMargin: 5
        anchors.bottomMargin: 5

        color:"#151515"

        SpaCarFunctionButton {
            id: gridViewItemBackground
            text: qsTranslate("Vehicle functions", title)
            textwidth: 137
            selectable: true
            selected: model.selected === 1
            hideDiod: model.hideDiod
            iconSource: globalStyle.imageUrl + image
            invisible: true
        }

    }
}
