/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0

// Data model for the application pane
ListModel {
    ListElement { selected: 1; available: 1; title: QT_TRANSLATE_NOOP("Vehicle functions", "Distance Alert"); image: "/Icons/img_vfp_dis_alert.png"; dimage: "es_graphics/ICON_CAR_FUNCTIONS_DISTANCE_ALERT_no_text_1.png"; gridId: 0}
    ListElement { selected: 1; available: 1; title: QT_TRANSLATE_NOOP("Vehicle functions", "Traffic Sign Information"); image: "/Icons/img_vfp_trfc_sign.png"; dimage: "es_graphics/ICON_CAR_FUNCTIONS_TRAFFIC_SIGN_INFORMATION_no_text_1.png"; gridId: 1}
    ListElement { selected: 1; available: 1; title: QT_TRANSLATE_NOOP("Vehicle functions", "Eco Mode"); image: "/Icons/img_vfp_eco_drvmode.png"; dimage: "es_graphics/ICON_CAR_FUNCTIONS_DISTANCE_ALERT_no_text_1.png"; gridId: 2}

    ListElement { selected: 1; available: 1; title: QT_TRANSLATE_NOOP("Vehicle functions", "Active Bending Lights"); image: "/Icons/img_vfp_bending_light.png"; dimage: "es_graphics/ICON_CAR_FUNCTIONS_ACTIVE_BENDING_LIGHTS_no_text_1.png"; gridId: 3}
    ListElement { selected: 0; available: 1; title: QT_TRANSLATE_NOOP("Vehicle functions", "Blind Spot Warning System"); image: "/Icons/img_vfp_blis.png"; dimage: "es_graphics/ICON_CAR_FUNCTIONS_BLIS_no_text_1.png"; gridId: 4}
    ListElement { selected: 0; available: 1; title: QT_TRANSLATE_NOOP("Vehicle functions", "Cornering Lights"); image: "/Icons/img_vfp_conering_light.png"; dimage: "es_graphics/ICON_CAR_FUNCTIONS_BLIS_no_text_1.png"; gridId: 5}

    ListElement { selected: 1; available: 1; title: QT_TRANSLATE_NOOP("Vehicle functions", "Speed Limit Adaptation"); image: "/Icons/img_vfp_spd_limadapt.png"; dimage: "es_graphics/ICON_CAR_FUNCTIONS_LANE_KEEPING_AID_no_text_1.png"; gridId: 9}
    ListElement { selected: 0; available: 1; title: QT_TRANSLATE_NOOP("Vehicle functions", "Lane Keeping Aid"); image: "/Icons/img_vfp_lka.png"; dimage: "es_graphics/ICON_CAR_FUNCTIONS_LANE_KEEPING_AID_no_text_1.png"; gridId: 10}
    ListElement { selected: 1; available: 1; title: QT_TRANSLATE_NOOP("Vehicle functions", "Head Up Display"); image: "/Icons/img_vfp_hud.png"; dimage: "es_graphics/ICON_CAR_FUNCTIONS_LANE_KEEPING_AID_no_text_1.png"; gridId: 11}

    ListElement { selected: 0; available: 1; title: QT_TRANSLATE_NOOP("Vehicle functions", "Wiper Service Mode"); image: "/Icons/img_vfp_wpr_service.png"; dimage: "es_graphics/ICON_CAR_FUNCTIONS_LANE_KEEPING_AID_no_text_1.png"; gridId: 12}
    ListElement { selected: 1; available: 1; title: QT_TRANSLATE_NOOP("Vehicle functions", "Stop/Start Functionality"); image: "/Icons/img_vfp_start_stop.png"; dimage: "es_graphics/ICON_CAR_FUNCTIONS_LANE_KEEPING_AID_no_text_1.png"; gridId: 13}
    ListElement { selected: 0; available: 1; title: QT_TRANSLATE_NOOP("Vehicle functions", "Reduced Guard"); image: "/Icons/img_vfp_reduced_guard.png"; dimage: "es_graphics/ICON_CAR_FUNCTIONS_LANE_KEEPING_AID_no_text_1.png"; gridId: 14}

    ListElement { selected: 0; available: 1; hideDiod: true; title: QT_TRANSLATE_NOOP("Vehicle functions", "Fold Left 3rd Row Seat"); image: "/Icons/img_vfp_rear_raise.png"; dimage: "es_graphics/ICON_CAR_FUNCTIONS_BLIS_no_text_1.png"; gridId: 6}
    ListElement { selected: 0; available: 1; hideDiod: true; title: QT_TRANSLATE_NOOP("Vehicle functions", "Fold Right 3rd Row Seat"); image: "/Icons/img_vfp_rear_raise.png"; dimage: "es_graphics/ICON_CAR_FUNCTIONS_BLIS_no_text_1.png"; gridId: 7}
    ListElement { selected: 0; available: 1; hideDiod: true; title: QT_TRANSLATE_NOOP("Vehicle functions", "Cameras"); image: "/Icons/img_vfp_camera.png"; dimage: "es_graphics/ICON_CAR_FUNCTIONS_BLIS_no_text_1.png"; gridId: 8}
}
