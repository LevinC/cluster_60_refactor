import QtQuick 2.3
import QtQuick.Controls 1.2
import QtMultimedia 5.0


import "Widgets/"
import "Common/"
import "Widgets/Media/"
import "MainScreen/"
import "AppPane/"
import "Bottom/"
import "Top/"
import "VehicleSettings/"
import "Setup/"
import "dummydata/"

Rectangle {
    id:coreScreen
    anchors.fill:parent
    focus:true
    color:"black"

    property alias mainListView : home
    property alias widgetPane: widgetPane
    property alias settingsView:settingsView


    Keys.onPressed: {
        // Check If Not Auto Repeat
        if (!event.isAutoRepeat) {
            // Non Auto Repeat Keys
            switch (event.key) {
            case Qt.Key_Space:
                coreScreen.homePressed();
                event.accepted = true;
                break;
            case Qt.Key_I:
            case Qt.Key_F:
                musicPlayer.prev();
                event.accepted = true;
                break;
            case Qt.Key_O:
            case Qt.Key_H:
                musicPlayer.next();
                event.accepted = true;
                break;
            case Qt.Key_P:
            case Qt.Key_G:
                musicPlayer.playbackState === Audio.PlayingState ? musicPlayer.pause() : musicPlayer.play();
                event.accepted = true;
                break;
            case Qt.Key_T:
            case Qt.Key_Plus:
                coreScreen.showVol();
                volumeModel.increaseVol();
                event.accepted = true;
                break;
            case Qt.Key_B:
            case Qt.Key_Minus:
                coreScreen.showVol();
                volumeModel.decreaseVol();
                event.accepted = true;
                break;
            case Qt.Key_Escape:
                Qt.quit();
                event.accepted = true;
                break;
            case Qt.Key_S:
                if(!setup.visible){
                    setup.show()
                }


                break;
            }
        } else {
            // Auto Repeat Keys
            switch (event.key) {
            case Qt.Key_T:
            case Qt.Key_Plus:
                coreScreen.showVol();
                volumeModel.increaseVol();
                event.accepted = true;
                break;
            case Qt.Key_B:
            case Qt.Key_Minus:
                coreScreen.showVol();
                volumeModel.decreaseVol();
                event.accepted = true;
                break;
            }
        }
    }

    StackView{
        id:appStackView
        width: parent.width
        height: parent.height - 110
        delegate: StackViewDelegate {
            function transitionFinished(properties) {
                properties.exitItem.opacity = 1
            }
        }

        property var _oneShotPopCallback: null

        initialItem: Item {
            height: coreScreen.height
            width: coreScreen.width

            Pageindicator {
                id: pageIndi
                width: parent.width
                anchors.bottom: home.top
                opacity: 1
            }

            ListView{
                id:home
                anchors.fill: parent
                anchors.topMargin: 73
                //                anchors.bottomMargin: 84
                orientation: ListView.Horizontal
                snapMode: ListView.SnapOneItem
                highlightRangeMode: ListView.StrictlyEnforceRange
                boundsBehavior: Flickable.StopAtBounds
                highlightMoveDuration: 300
                highlightFollowsCurrentItem: true
                model : model
                currentIndex: 1


                VisualItemModel{
                    id :model
                    VehicleSettings{
                        width:globalStyle.screenWidth
                        height:800/1024 * globalStyle.screenWidth
                    }
                    WidgetPane{
                        id:widgetPane

                    }
                    AppPane{
                        width: home.width
                        height: home.width

                    }
                }
                Behavior on contentX {CubicAnimation{}}
            }
        }
    }

    BottomSection{
        id :bottomSection
        anchors.fill: parent
    }

    TopBar{
        id: settingsPane
    }

    Settings{
        id:settingsView
        visible:false
    }

    property alias onTopOfAll:onTopOfAll
    Item{
        width:globalStyle.screenWidth
        height:globalStyle.screenHeight
        Loader{
            id:onTopOfAll
            anchors.fill: parent
        }
    }


    function showApp(app) {
        appStackView.push(app);
    }

    function closeApp(oneShotCallback) {
        if (oneShotCallback) {
            appStackView._oneShotPopCallback = function() {
                oneShotCallback()
                appStackView._oneShotPopCallback = null
            }
        }
        appStackView.pop()
    }

    // Actions taken when home is pressed
    function homePressed() {
        if (settingsPane.topPane.open) {
            settingsPane.topPane.closeMenu()
        }
        else if (bottomSection.climatepage.state === "visible" || climateModel.leftTempShown === true || climateModel.rightTempShown === true) {
            bottomSection.climatepage.close();
            climateModel.leftTempShown = false;
            climateModel.rightTempShown = false;
        }
        else if (widgetPane.full === true) {
            coreScreen.expandWidget(0)
            //0 är alltså navi appen. Det är bara navi appen som kan gå till fullskärm I demobilarna.
        }
        else {
            goHome()
        }
    }

    // Go to main home screen
    function goHome() {
        if (home.currentIndex === 1) {
            if (!coreScreen.appActive) {
                widgetPane.normalizeWidgets()
            }
            else {
                coreScreen.closeApp();
            }
        } else {
            home.currentIndex = 1;
        }
    }

    Volume {
        id: vol
        anchors.left: parent.left
        anchors.top: parent.top
    }

    Notify {
        id: notify
        property bool clickable:false
        anchors.left: parent.left
        anchors.top: parent.top

        MouseArea {
            anchors.fill: notify;
            enabled:notify.clickable

            onClicked: {
                closeApp();
                coreScreen.widgetPane.mediaWidget.changeFromAppPane("fav",false)
                notify.close()
            }
        }
    }

    Popup {
        id: popup
    }

    Setup{
        id:setup
        visible: false
    }

    SwipeGuide{
        id:swipeGuide
        visible:false
    }

    function showQuickTips(){
        swipeGuide.visible=true;
    }

    Keyboard {
        id: keyboard
    }

    // Show pop-up notification
    function showNotify(txt, pic) {
        notify.show(txt, pic);
    }
    // Show pop-up notification
    function showPopup(hdr, sbhdr, sz, cntnt) {
        popup.show(hdr, sbhdr, sz, cntnt);
    }
    // Show not available pop-up notification
    function notAvailableNotify() {
        notify.clickable = false;
        notify.show("Not available in demo mode", globalStyle.imageUrl + "/Icons/img_hmc_blocked.png")
    }

    function addToFavNotify() {
        notify.clickable = true;
        notify.show("Added to favorites", globalStyle.imageUrl + "/Icons/img_hmc_mrf" +globalStyle.skinUrl +".png");
    }

    function removedFromFavnotify() {
        notify.clickable = false;
        notify.show("Removed from favorites", globalStyle.imageUrl + "/Icons/img_hmc_mrf"+globalStyle.skinUrl +".png");
    }

    // Show volume notification
    function showVol() {
        vol.show()
    }

    function goHomeToWidget(widget, mediatype) {
        climatepage.close();
        goHome();
        closeApp();
        widgets.expandWidgetName(widget, mediatype);
    }

}
