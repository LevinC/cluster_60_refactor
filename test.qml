import QtQuick 2.3

Rectangle {
    id:root
    width: globalStyle.screenWidth * scale
    height: globalStyle.screenHeight * scale
    scale:1

    property SpaStyle globalStyle: SpaStyle {
        id: globalStyle
    }

    Item{
        anchors.centerIn:parent
        width:parent.width/root.scale
        height:parent.height/root.scale
        CoreScreen{
        }
    }
}
