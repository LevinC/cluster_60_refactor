import QtQuick 2.0

QtObject {

    property real mainScale: 1.2
    property int screenWidth : 768
    property int screenHeight : 1024

    property int widgetWidthInset :16
    property int widgetHeightInset :9

    property int drawerwidth: 160
    property int topExpandedHeight : 140
    property int expandedHeight: 654
    property int normalHeight: 174
    property int minimizedHeight: 62
    property int fullHeight: 818
    property int listItemSize: 100

    property int leftMargin: 10
    property int bottomMargin: 7
    property int topMargin: 3
    property int rightMargin: 10

    property int widgetIconSize: 174
    property int widgetIconSmallSize:48
    property int widgetIconExpandedSize: 160

    property int topPaneListHeight: 286

    property real coloredBgOpacityOne: 1

    property bool textInsteadofIcon

    property variant colorStop1: ["#c76c7e8a","#393939","#93855c","#515354"]
    property variant colorStop2: ["#c7424c54","#131313","#584e37","#303030"]
    property variant colorStop3: ["#c716191b","#0f0f0f","#1c1910","#111111"]

    property variant _s4Colors :["#7593a7","#cec3b8","#ceb675","#b8bdce"]
    property variant not_s4Colors :["#739fbf","#cec3b8","#ceb675","#b8bdce"]
    property variant colors: componentSkinUrl === "_S4" ? _s4Colors : not_s4Colors

    property string componentSkinUrl: "_S0"
    property string skinUrl: componentSkinUrl === "_S4" ? "_S4" : ""

    property url imageUrl: Qt.resolvedUrl("resources/images/spa_graphics")
    property url dummyDataMediaURL: Qt.resolvedUrl("dummydata/media")

    //Font COLORS and theme color
    property color themeColorFont: "#ffffff"//"#739fbf"
    property color themeColorGraphics: "#ffffff"//"#8fb3cc"
    property color themeColorGraphicsActive: "#ffffff"//"#66597999"
    property color themeColorGraphicsPressed: "#ffffff"//"#E5597999"
    property string white: componentSkinUrl === "_S4" ? "black" : "#ffffff"
    property color greyLvl1: componentSkinUrl === "_S4" ? "#101010" : "#bfbfbf"
    property color greyLvl2: componentSkinUrl === "_S4" ? "black" : "#ffffff"
    property color greyLvl3: componentSkinUrl === "_S4" ? "#303030" : "#959595"
    property color greyLvl4: componentSkinUrl === "_S4" ? "#303030" : "#636363"
    property color navigationColor: "#1f2f3b"

    property variant appPaneColor0: ["#34495a","#0c1014"]
    property variant appPaneColor1: ["#211e1e","#070707"]
    property variant appPaneColor2: ["#695d3b","#16140c"]
    property variant appPaneColor3: ["#494c52","#111111"]
    property variant appPaneColor : [appPaneColor0,appPaneColor1,appPaneColor2,appPaneColor3]

    property int minButtonSize: 84//25 mm
    property int okButtonSize: 111//25 mm
    property int recomButtonSize: 135//140//25 mm
}

