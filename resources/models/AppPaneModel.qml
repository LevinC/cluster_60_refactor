/* Copyright (c) 2014 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0

QtObject {
    id: appPaneModelRoot

    property ListModel model: ListModel {
        ListElement { app: "navigation"; showMode: "expanded"; title: QT_TRANSLATE_NOOP("App pane", "Navi");       image: "/Icons/img_hmc_navi_conn";      gridId: 0 }
        //ListElement { app: "navigation"; showMode: "N/A"; title: QT_TRANSLATE_NOOP("App pane", "Navi");       image: "/Icons/img_hmc_inter_navi";     gridId: 1 }
        ListElement { app: "";      showMode: "N/A"; title: QT_TRANSLATE_NOOP("App pane", "");    image: "";            gridId: -1 }
        ListElement { app: "";      showMode: "N/A"; title: QT_TRANSLATE_NOOP("App pane", "");    image: "";            gridId: -1 }
        ListElement { app: "";      showMode: "N/A"; title: QT_TRANSLATE_NOOP("App pane", "");    image: "";            gridId: -1 }

        ListElement { app: "media" ;     showMode: "expanded" ; title: QT_TRANSLATE_NOOP("App pane", "Bluetooth"); image: "/Icons/img_hmc_bt";          gridId: 8 }
        ListElement { app: "media" ;     showMode: "expanded" ; title: QT_TRANSLATE_NOOP("App pane", "iPod")      ;  image: "/Icons/img_hmc_ipod";             gridId: 9 }
        ListElement { app: "media" ;     showMode: "expanded" ; title: QT_TRANSLATE_NOOP("App pane", "FM Radio");  image: "/Icons/img_hmc_fm";                 gridId: 10 ; mediatype: "fm"; active: "true" ; }
        ListElement { app: "media" ;     showMode: "N/A" ; title: QT_TRANSLATE_NOOP("App pane", "AM Radio");  image: "/Icons/img_hmc_am";                 gridId: 7 }
        //ListElement { app: "media" ;     showMode: "N/A" ; title: QT_TRANSLATE_NOOP("App pane", "DAB Radio"); image: "/Icons/img_hmc_dab";                gridId: 11 }
        ListElement { app: "media" ;     showMode: "N/A" ; title: QT_TRANSLATE_NOOP("App pane", "Sirius");    image: "/Icons/img_hmc_sirius";             gridId: 12 }
        ListElement { app: "media" ;     showMode: "expanded" ; title: QT_TRANSLATE_NOOP("App pane", "USB");       image: "/Icons/img_hmc_usb";                gridId: 13 }
        ListElement { app: "media" ;     showMode: "N/A" ; title: QT_TRANSLATE_NOOP("App pane", "AUX in");    image: "/Icons/img_hmc_aux";                gridId: 15 }
        ListElement { app: "media" ;     showMode: "N/A" ; title: QT_TRANSLATE_NOOP("App pane", "Radio Favorites");        image: "/Icons/img_hmc_mrf";                 gridId: 19 }

        ListElement { app: "extra";      showMode: "N/A"; title: QT_TRANSLATE_NOOP("App pane", "Sound Experince");    image: "/Icons/img_hmc_pre_sound";            gridId: 2 }
        ListElement { app: "extra";      showMode: "N/A"; title: QT_TRANSLATE_NOOP("App pane", "Messages");   image: "/Icons/img_hmc_msg";           gridId: 5 }
        ListElement { app: "extra";      showMode: "N/A"; title: QT_TRANSLATE_NOOP("App pane", "Services");   image: "/Icons/img_hmc_carinfo";           gridId: 6 }
        ListElement { app: "extra";      showMode: "expanded"; title: QT_TRANSLATE_NOOP("App pane", "Driver Performance");    image: "/Icons/img_hmc_dvr_perf"; gridId: 3 }
        ListElement { app: "extra" ;     showMode: "N/A" ; title: QT_TRANSLATE_NOOP("App pane", "Download Manager");        image: "/Icons/img_hmc_ota";                 gridId: 16 }
        ListElement { app: "extra" ;     showMode: "N/A" ; title: QT_TRANSLATE_NOOP("App pane", "Apple Carplay");        image: "/ICON_APP_SCREEN_IOS_IN_CAR_no_text_0";                 gridId: 17 }
        ListElement { app: "extra" ;     showMode: "N/A" ; title: QT_TRANSLATE_NOOP("App pane", "Android Auto");        image: "/android-auto-android";                 gridId: 18 }
        //ListElement { app: "extra";      showMode: "N/A"; title: QT_TRANSLATE_NOOP("App pane", "Browser");    image: "/Icons/img_hmc_browser";            gridId: 4 }
        ListElement { app: "extra" ;     showMode: "N/A" ; title: QT_TRANSLATE_NOOP("App pane", "Sirius Travellink with extra long text");        image: "/Icons/img_hmc_cd";                 gridId: 14 }

        ListElement { app: "extra" ;     showMode: "N/A" ; title: QT_TRANSLATE_NOOP("App pane", "Glympse");        image: "/Glympse_logo";                 gridId: 20 }
    }
}
